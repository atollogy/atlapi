#!/usr/bin/env python3

"""Atollogy API Server setup module."""

import os
from setuptools import setup, find_packages
from codecs import open
from os import path
from pathlib import Path
import subprocess as sp

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

with open(path.join(here, "requirements.txt"), encoding="utf-8") as f:
    raw_reqs = f.readlines()
    # dep_pkgs = [l.strip().split('==')[-1].split('_')[0] for l in raw_reqs if not l.startswith('#') and '://' in l]
    # example of git links in requirements.txt transformation:
    # dep_pkgs entry becomes: "atollogy.data-api"
    install_reqs = [l.strip() for l in raw_reqs if not l.startswith('#') and '://' not in l]
    dep_links = [l.strip() for l in raw_reqs if not l.startswith('#') and '://' in l]

setup(
    name="atlapi",
    version="1.50.01",
    description="Atollogy core data ingest API server",
    long_description=long_description + "\n\n" + open("CHANGELOG.rst").read(),
    url="https://bitbucket.org/atollogy/atlapi",
    author="Atollogy Inc.",
    author_email="eng@atollogy.com",
    license="Commercial License - All rights reserved, Atollogy Inc.",
    classifiers=[
        "Development Status :: 5 - Production/Stable ",
        "Intended Audience :: Internal",
        "Topic :: Core Platform :: REST API",
        "License :: Commercial :: Atollogy",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    keywords="Atollogy data ingest server",
    include_package_data=True,
    packages=[path.replace('/', '.') for path in
              sp.check_output(['find', 'atlapi', '-type', 'd']).decode().split('\n')[:-1]
              if '__' not in path and '.' not in path],
    data_files=[
        ("atlapi", ["atlapi/version.json", "atlapi/atlapi.service"]),
        ("atlapi/static", ["atlapi/static/atl_favicon.ico"])
    ],
    install_requires=install_reqs,
    dependency_links=dep_links
)
