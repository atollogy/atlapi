# Atl API

## Quick Setup
* Clone this repo

```

Usage: setup\_env.py \[-h\] \[-H HELP\] \[-v [VENV\]\] \[-s \[S3WRITE\]\] \[-b \[BCMPATH\]\] \[-r \[RESET\]\] \[-c \[CUSTOMERMODEL\]\]

Environment setup for Atlapi. Sets up environment variables and ensures dependencies are installed. Prefer to use this script rather than manually setting up environment.

Optional arguments:
  -h, --help            	show this help message and exit
  -H HELP, --HELP HELP  	Display extended help documentation.
  
  -v [VENV], --venv [VENV]
                        	Enable the virtual environment. Creates the python virtualenv in folder .venv, defaults to NO
  
  -b [BCMPATH], --bcmPath [BCMPATH]
                        	Path to custom bcm file. Defaults to /etc/kvhandler/bcm.json
  
  -r [RESET], --reset [RESET]
                        	Reset the virtual environment if it was installed previously. Defaults to FALSE
  
  -c [CUSTOMERMODEL], --customerModel [CUSTOMERMODEL]
                        	Pull in the customer model from prd consul into /etc/atollogy/overrides/

Note:
TRUE values - y, 1, true, TRUE, YES, yes, t
FALSE values - n, 0, false, FALSE, NO, no, f

```

* Once a new shell starts with given configuration run `python3 atlapi.py`.

* Environment variables used by atlapi:
** atlapi_conf (/etc/kvhandler/bcm.json)
** atlapi_home (home/path/of/env_setup/script)

### Good to know
  * The systemctl service instance of atlapi resides at location /opt/atlapi
  * Chef runs every 20 mins and configures the atlapi service to conform to whatever configuration defined in consul
  * The brnach can be managed at consul path /versions/[ENV]/[CGR]/atlapi. The commit and install command are not read from consul.
  * The log level can be managed from consul path /envs/[ENV]/svcs/atlapi
  * If you stop the system service the next chef run is going to restart the service. To not have chef not to touch your manual configuration set commit to NONE in consul.
  * atlapi writes to rawdata and data bucket. If buckets does not exist or environmnet differs then expect S3ResponseError or S3AccessError.
  * If you want to setup your own instance of atlapi stop the systemd service and set commit to NONE, then clone to seperate location and manually start your instance

## Atl API configuration

### BCM file path (bcm.py)
BCM file is searched in following order
* Environment variable `atlapi_conf`
* Path `/etc/kvhandler/bcm.json`
* Path `/etc/kvmanager/bcm.json`
* Kvhandler and consul cache applies overrides from path /etc/atollogy/overrides, after loading data from consul. Any json file under that path will be read and used as override.

### Server Port (atlapi.py)
Atl API looks for BCM.endpoints.atlapi.twistd_port and if not found than runs at port 8080

### Logs (log.py)
Usually logs are saved at `/var/log/atollogy/atlapi.log`. The file name comes from BCM.svcs.atlapi.log_name and log level is set from BCM.svcs.atlapi.log_level

## Batch processing (batch.py)
This file can be used to batch process the image processing or replay the image data.
### Command line arguments that should be given
* Gateway name - Gateway names as it appears in s3 path.
* Camera ID - Id of camera to be processed.
* Start Date - Datehour as it appear in s3 path. Start and End date are inclusive.
* End Date - Datehour as it appear in s3 path. Start and End date are inclusive.
* API URL - URL of API server with API version. If not loadbalancer managed URL than add the port also. example: http://localhost:8080/7
For more details do `batch.py --HELP`

### Image processing path
* Batch process sends request to API server
* API server looks in BCM for list of processes to run for that gateway
* API server does the Opencv based processing and sends image to recog server for object detection if it is in process list.
* Recog server sends back the detected objects bounding boxes and annotated images for atlapi to save to s3
* API server stores the json output to s3 for each process and inserts a image_step table 

## Notes
* API server reads recog URL from bcm file - bcm.endpoints.mvapi.url
* API server inserts data into the db from bcm.endpoints.db
* The machine vision config for a gateway can be found in consul at path \_customerModel/CGR/devices/gateways/GATEWAY\_ID/mv_routing

