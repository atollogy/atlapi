#!/usr/bin/env python3.6

from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.plate_key import Plate_Key
import base64
import cv2
from datetime import datetime
from fnmatch import fnmatch
import functools
import logging
import os
logger = logging.getLogger('atlapi')
import math
import numpy as np
import pandas as pd
import re
import requests
from typing import Optional, List, Dict, Tuple, Union
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue


####################################################################################
FASTpixelSensitivity = 20
lpra_imageComplexityThreshold = 42
LPR_IMAGECOMPLEXITYTHRESHOLD = 10
####################################################################################

class LPR(Core):
    def __init__(self):
        Core.__init__(self, 'func_lpr')
        if 'db' in self.svcs:
            self.db = self.svcs.db
        else:
            self.db = None
        self.exclusions = AD()
        self.FLIPPED_REGEX = re.compile(r'.+[XY1AI]{2,3}[B873OR]$')
        self.NOT_FLIPPED_REGEX = re.compile(r'^[B873OR][XY1AI]{2,3}.+')
        self.CAUTION_REGEX = re.compile(r'[VTQI97NYMHA4WKCUDO]{5,8}')
        self.FLIPPED_CAUTION_REGEX = re.compile(r'[1ILNOQUV]{3,5}')
        self.NL_TRAILER_PLATES_REGEX = re.compile(r'^(D|G|J|S|0)\w{3,8}$')

    def amend_bbox_label(self, add_text, bbox, atype='crop'):
        if not isinstance(bbox, Zone):
            bbox = Zone.create(add_text, bbox, atype=atype)
        if add_text not in bbox.label:
            bbox.label = bbox.label + ' - {}'.format(add_text)
        return bbox

    def annotate_lpr_image(self, sc, rv):
        annotationZones = []
        sc.origins = [sc.filelist[0]] +  sc.origins
        toAnnotate = load_image(
            sc.filedata[sc.filelist[0]],
            color_space=sc.color_space,
            crop=None
        )

        if sc.input_crop:
            annotationZones.append(Zone.create(f"{sc.step_name} - {'|'.join(unique(rv.modifications))}", sc.input_crop[:4], atype='crop', rtype='input_crop'))
            save_annotation(rv, annotationZones[-1], sc.origins, sc.color_space)

        try:
            if 'identifier' in rv:
                plate = rv.identifier
            elif 'fullResponse.results' in rv and len(rv.fullResponse.results) and 'plate' in rv.fullResponse.results[0]:
                plate = rv.fullResponse.results[0].plate
            else:
                plate = None

            if plate is not None and 'coordinates' in rv:
                self.generate_plate_corrdinates(sc, rv)
                if rv.plate_coordinates:
                    if rv.modifier is not None:
                        plate_annotation = f'license_plate: {rv.original_plate}-{rv.modifier}->{plate}'
                    else:
                        plate_annotation = f"license_plate: {plate}"
                    annotationZones.append(
                        Zone.create(
                            plate_annotation,
                            rv.plate_coordinates,
                            atype="subject",
                            result=plate,
                            rtype='plate',
                            notwh=True,
                        )
                    )
                    save_annotation(rv, annotationZones[-1], sc.origins, sc.color_space)
        except Exception as err:
            logger.info(f"lpr.annotate_lpr_image exception: {err}")

        annotated_image = annotate(toAnnotate, annotationZones, color_space=sc.color_space)
        rv.filedata = [(sc.filelist[0][:-4] + "_annotated.jpg", "bio", annotated_image)]

    def block_result(self, rv, blocker, cause={}):
        original_result = AD(rv)
        rv.original_result = original_result
        rv.modifier = rv.reason = blocker
        rv.modifications.append(blocker)
        rv.is_whitelist = False
        blocked = AD({
            'modified_by': blocker,
            'cause': cause,
            'attrs': {}
        })
        logger.info(f"lpr.block_result blocking plate: {blocker}")
        for k in ['candidates', 'confidence', 'coordinates', 'fullResponse', 'identifier',
                  'license_plate', 'plate', 'matches_template', 'original_plate','skew_metrics']:
            if k in rv:
                blocked.attrs[k] = rv[k]
                del rv[k]
        self.qc_prop(rv, kv=(blocker, blocked))

    def calculate_skew_metrics(self,
                               coordinates: Optional[List[Dict]],
                               max_size_ratio: float=0.20,
                               shape: Union[List, Tuple]=None,
                               theshold_aspect_ratio: float=0.35) -> AD:
        metrics = AD({
            'aspect_ratio': 0.5625,
            'bad_plate_dimensions': False,
            'bad_aspect_ratio': False,
            'max_height': 144,
            'max_width': 256
        })
        if coordinates is None:
            return metrics
        if len(coordinates) == 4:
            x1 = coordinates[0].get('x')
            x2 = coordinates[1].get('x')
            x3 = coordinates[2].get('x')
            x4 = coordinates[3].get('x')
            y1 = coordinates[0].get('y')
            y2 = coordinates[1].get('y')
            y3 = coordinates[2].get('y')
            y4 = coordinates[3].get('y')

            # Length
            dxTop = x2 - x1
            dyTop = y2 - y1
            dxBottom = x3 - x4
            dyBottom = y3 - y4
            metrics.width_top = math.hypot(dxTop, dyTop)
            metrics.width_bottom = math.hypot(dxBottom, dyBottom)
            metrics.width = (metrics.width_top + metrics.width_bottom)/2
            # multiply by -1 to flip the angle direction (more intuitive)
            metrics.angle_top = None if dxTop == 0 else -1 * math.degrees(math.atan(dyTop /dxTop))
            metrics.angle_bottom = None if dxBottom == 0 else -1 * math.degrees(math.atan(dyBottom /dxBottom))

            # Width
            dxLeft = x1 - x4
            dyLeft = y1 - y4
            dxRight = x2 - x3
            dyRight = y2 - y3
            metrics.angle_left = None if dxLeft == 0 else -1 * math.degrees(math.atan(dyLeft/dxLeft))
            metrics.angle_right = None if dxRight == 0 else -1 * math.degrees(math.atan(dyRight/dxRight))
            metrics.height_left = math.hypot(dxLeft, dyLeft)
            metrics.height_right = math.hypot(dxRight, dyRight)
            metrics.height = (metrics.height_left + metrics.height_right)/2

            # Angle & Area
            metrics.rect_area = metrics.height * metrics.width
            metrics.area = self.polygon_area([(x1, y1), (x2, y2), (x3, y3), (x4, y4)])  # vertices in clockwise order

            # dimensional checks
            metrics.aspect_ratio = metrics.height/metrics.width
            metrics.max_height = shape[0] * max_size_ratio
            metrics.max_width = shape[1] * max_size_ratio
            if (metrics.height > metrics.max_height) or (metrics.width > metrics.max_width):
                metrics.bad_plate_dimensions = True
            if (theshold_aspect_ratio * 2)  < metrics.aspect_ratio < theshold_aspect_ratio:
                metrics.bad_aspect_ratio = True
        return metrics

    def fast_corner_detection(self, image):
        blur_img = cv2.GaussianBlur(image, (5, 5), 0)
        fast = cv2.FastFeatureDetector_create(threshold=FASTpixelSensitivity)
        keyPoints = fast.detect(blur_img, None)
        return keyPoints

    def fix_atollogy_plate(self, rv):
        def fix_flip_plate(plate):
            new_plate = '8XXX'
            for c in plate.upper()[::-1][-3:]:
                if c in 'DOQ':
                    new_plate += '0'
                elif c in 'TI':
                    new_plate += '1'
                elif c in 'Z4':
                    new_plate += '2'
                elif c in 'CE':
                    new_plate += '3'
                elif c in '257Y':
                    new_plate += '4'
                elif c in 'S9G':
                    new_plate += '5'
                elif c in 'L':
                    new_plate += '7'
                elif c in 'RB':
                    new_plate += '8'
                elif c in 'U6':
                    new_plate += '9'
                else:
                    new_plate += c
            return new_plate.upper()

        def fix_ocr_flips(plate):
            new_plate = '8XXX'
            for c in plate.upper()[-3:]:
                if c in 'DOQ':
                    new_plate += '0'
                elif c in 'XI':
                    new_plate += '1'
                elif c in 'Z':
                    new_plate += '2'
                elif c in 'SG':
                    new_plate += '5'
                elif c in 'U':
                    new_plate += '9'
                else:
                    new_plate += c
            return new_plate.upper()

        old_plate = rv.fullResponse.results[0].plate
        if len(old_plate) < 5:
            return
        elif self.flipped.match(old_plate):
            new_plate =  fix_flip_plate(old_plate)
            rv.modifications.append(f"flipped_plate_correction: {old_plate}->{new_plate}")
            rv.fullResponse.results[0].plate = new_plate
            rv.plate = rv.identifier = rv.license_plate = new_plate
            self.qc_prop(rv, kv=('fix_flip_plate', AD({
                            'new_plate': new_plate,
                            'original_plate': old_plate
                        }))
                    )
        elif self.NOT_FLIPPED_REGEX.match(old_plate):
            new_plate =  fix_ocr_flips(old_plate)
            rv.modifications.append(f"ocr_flip_correction: {old_plate}->{new_plate}")
            rv.fullResponse.results[0].plate = new_plate
            rv.plate = rv.identifier = rv.license_plate = new_plate
            self.qc_prop(rv, kv=('ocr_flip_correction', AD({
                            'new_plate': new_plate,
                            'original_plate': old_plate
                        }))
                    )

    def fix_md_plates(self, rv):
        def fix_md_plate(plate):
            # if (plate[-3] in ['8'] and len(plate) == 6 and
            #     plate[0:3].isnumeric() and plate[-2:].isnumeric()):
            #     return f"{plate[:3]}ED{plate[-2:]}"
            if len(plate) == 6:
                if plate[0:3].isnumeric():
                    prefix =  int(plate[:3])
                    if (prefix < 400 and plate[-2:].isnumeric() and plate[3] not in ['F', 'Z']):
                        return f"{plate[:3]}ED{plate[-2:]}"
                    if (prefix >= 600 and not plate[3].isnumeric() and
                        plate[3] not in ['F', 'Z'] and plate[-2:].isnumeric()):
                        return f"{plate[:3]}F{plate[-2:]}"
            elif len(plate) == 7:
                if (plate[0] == 'E' and plate[1:6].isnumeric() and plate[-1] != 'D'):
                    return f"{plate[:-1]}D"
                if (plate[0:2] == 'ES' and plate[2:6].isnumeric()):
                    return f"E5{plate[2:-1]}D"
                if (plate[0:3] == 'ESZ' and plate[3:6].isnumeric()):
                    return f"E52{plate[2:-1]}D"
            return plate.upper()

        old_plate = rv.fullResponse.results[0].plate
        new_plate =  fix_md_plate(rv.fullResponse.results[0].plate)
        if new_plate != old_plate:
            rv.modifications.append(f"md_plate_correction: {old_plate}->{new_plate}")
            rv.fullResponse.results[0].plate = new_plate
            rv.plate = rv.identifier = rv.license_plate = new_plate
            self.qc_prop(rv, kv=('md_plate_fix', AD({
                            'new_plate': new_plate,
                            'original_plate': old_plate
                        }))
                    )

    def fix_nl_plates(self, rv):
        def fix_nl_plate(plate):
            plate = plate.upper()
            if self.NL_TRAILER_PLATES_REGEX.match(plate):
                plate = 'O' + plate[1:]
            return plate
        old_plate = rv.fullResponse.results[0].plate
        new_plate =  fix_nl_plate(rv.fullResponse.results[0].plate)
        if new_plate != old_plate:
            rv.modifications.append(f"nl_plate_correction: {old_plate}->{new_plate}")
            rv.fullResponse.results[0].plate = new_plate
            rv.plate = rv.identifier = rv.license_plate = new_plate
            self.qc_prop(rv, kv=('nl_plate_fix', AD({
                            'new_plate': new_plate,
                            'original_plate': old_plate
                        }))
            )

    def fix_uk_plates(self, rv):
        def fix_uk_plate(plate):
            plate = plate.upper()
            fixed_plate = (plate[-7:-5].replace('0', 'O').replace('5', 'S').replace('9', 'P').replace('2', 'Z') +
                           plate[-5:-3].replace('O', '0').replace('Z', '2').replace('S', '5').replace('I', '1').replace('P', '9') +
                           plate[-3:].replace('0', 'O').replace('2', 'Z').replace('5', 'S').replace('1', 'I')
                        )
            return fixed_plate.upper()

        old_plate = rv.fullResponse.results[0].plate
        new_plate =  fix_uk_plate(rv.fullResponse.results[0].plate)
        if new_plate != old_plate:
            rv.modifications.append(f"uk_plate_correction: {old_plate}->{new_plate}")
            rv.fullResponse.results[0].plate = new_plate
            rv.plate = rv.identifier = rv.license_plate = new_plate
            self.qc_prop(rv, kv=('uk_plate_fix', AD({
                            'new_plate': new_plate,
                            'original_plate': old_plate
                        }))
                    )

    def generate_plate_corrdinates(self, sc, rv):
        rv.plate_coordinates = None
        if 'coordinates' in rv and rv.coordinates:
            coords = sorted(rv.coordinates[:], key=lambda item: item["x"] + item["y"])
            boxPadding = 20
            if sc.input_crop:
                rv.plate_coordinates = [
                    sc.input_crop.x + coords[0]["x"] - boxPadding,
                    sc.input_crop.y + coords[0]["y"] - boxPadding,
                    sc.input_crop.x + coords[-1]["x"] + 2 * boxPadding,
                    sc.input_crop.y + coords[-1]["y"] + 2 * boxPadding,
                ]
            else:
                rv.plate_coordinates = [
                    coords[0]["x"] - boxPadding,
                    coords[0]["y"] - boxPadding,
                    coords[-1]["x"] + boxPadding,
                    coords[-1]["y"] + boxPadding,
                ]

    def generate_plate_key(self, candidates):
        pk = Plate_Key(candidates)
        return pk.pkey, pk.pkey_ranking

    def is_blurry_or_dirty(self, image, rv, threshold=100.0):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        rv.bd_factor = cv2.Laplacian(image, cv2.CV_64F).var()
        rv.is_blurry_or_dirty = rv.bd_factor < threshold

    def is_side_read(self, rv, min_angle=-5, max_angle=45.0):
        try:
            if 'plate_angle' in rv and isinstance(rv.plate_angle, (int, float)):
                if min_angle < rv.plate_angle < max_angle:
                    rv.is_side_read = False
                else:
                    rv.is_side_read = True
        except Exception as err:
            logger.exception(f'is_side_read exception {err}')

    @inlineCallbacks
    def lpr(self, sc):
        """LPR using openALPR API"""
        rv = AD({'annotations': {},
                 'is_side_read': False,
                 'modifier': None,
                 'modifications': [],
                 'plate_angle': None,
                 "reason": None,
                 "status": "success"
                })
        if not len(sc.filelist):
            rv.status = "No api call"
            rv.reason = "No image"
            return rv

        # update exclusion list if unpopulated or the timer has expired
        res = yield self.update_exclusion_list(sc.customer_id)

        # option flags, params and defaults
        sc.brightness_threshold = 75.0 if 'brightness_threshold' not in sc else float(sc.brightness_threshold)
        sc.confidence_threshold = 0 if 'confidence_threshold' not in sc else float(sc.confidence_threshold)

        sc.is_whitelist = False if 'is_whitelist' not in sc else sc.is_whitelist
        sc.vehicle_type_blocking = True if 'vehicle_type_blocking' not in sc else sc.vehicle_type_blocking
        sc.vehicle_type_filtering = True if 'vehicle_type_filtering' not in sc else sc.vehicle_type_filtering
        sc.vehicle_type_inclusion_list = ['tractor-trailer', 'truck-standard', 'trailer-utility'] if 'vehicle_type_inclusion_list' not in sc else sc.vehicle_type_inclusion_list

        # set collection interval
        rv.collection_interval = 4 if 'collection_interval' not in sc else int(sc.collection_interval)

        # side read angle block cfg
        rv.side_read_angle_block_enabled = sc.side_read_angle_block if 'side_read_angle_block' in sc else False
        plate_orientation = sc.plate_orientation if 'plate_orientation' in sc else 'positive'
        if rv.side_read_angle_block_enabled and sc.plate_orientation == 'positive':
            side_read_angle_min = -10.0 if 'side_read_angle_min' not in sc else float(sc.side_read_angle_min)
            side_read_angle_max = 40.0 if 'side_read_angle_max' not in sc else float(sc.side_read_angle_max)
        else:
            side_read_angle_min = -40.0 if 'side_read_angle_min' not in sc else float(sc.side_read_angle_min)
            side_read_angle_max = 10.0 if 'side_read_angle_max' not in sc else float(sc.side_read_angle_max)

        # atollogy plates
        rv.atollogy_plates = True if 'flipped_plates' in sc or 'atollogy_plates' in sc else False

        # blurry or dirty
        rv.bd_threshold = 250.0 if 'bd_threshold' not in sc else float(sc.bd_threshold)
        # openalpr regional cfg
        rv.us_plates = True if ("country" in sc and sc.country == 'us') or ('country' not in sc) else False
        rv.uk_plates = True if (not rv.us_plates and
                             ("country" in sc and sc.country == 'gb') or
                             ('region' in sc and sc.region == 'gb')) else False
        rv.nl_plates = True if (not rv.us_plates and
                             ("country" in sc and sc.country == 'nl') or
                             ("region" in sc and sc.region == 'nl')) else False

        if not rv.us_plates:
            sc.max_size_ratio = 0.20 if 'max_size_ratio' not in sc else float(sc.max_size_ratio)
            sc.theshold_aspect_ratio = 0.17 if 'theshold_aspect_ratio' not in sc else float(sc.theshold_aspect_ratio)
        else:
            sc.max_size_ratio = 0.20 if 'max_size_ratio' not in sc else float(sc.max_size_ratio)
            sc.theshold_aspect_ratio = 0.35 if 'theshold_aspect_ratio' not in sc else float(sc.theshold_aspect_ratio)

        top_correlation = fuzzy_correlation = False

        params = {
            "secret_key": self.tree.endpoints.lpr.key,
            "recognize_vehicle": True,
            "country": sc.country if "country" in sc else "us",
            "return_image": 0,
            "topn": 10,
        }
        if 'plate_region' in sc:
            params['region'] = sc.plate_region

        try:
            if sc.input_crop and not isinstance(sc.input_crop, Zone):
                sc.input_crop = Zone.create("input_crop", sc.input_crop, atype="crop")
            color_space_image = load_image(
                sc.filedata[sc.filelist[0]],
                crop=sc.input_crop
            )
            check_image(color_space_image, sc, rv)
            img_shape = color_space_image.shape

            try:
                rv.fullResponse = yield self._post_external_api(
                        params=params,
                        payload=pack_image(color_space_image, b64=True, color_space=sc.color_space),
                        url=self.tree.endpoints.lpr.url
                    )
            except Exception as err:
                logger.info(f"LPR.lpr exception - openalpr api call error: {err}")
                return self.return_condition(sc,
                                             rv,
                                             reason=f"openalpr api call error: {err}",
                                             status='error'
                                        )

            if "results" in rv.fullResponse and len(rv.fullResponse.results):
                rv.fullResponse.results = [AD(r) for r in rv.fullResponse.results]

                # pick highest confidence result
                for res in rv.fullResponse.results:
                    res.candidates = [AD(c) for c in res.candidates]
                    res.candidates = sorted(res.candidates,
                                            key=lambda candidate: candidate.confidence,
                                            reverse=True)

                if len(rv.fullResponse.results) > 1:
                    rv.fullResponse.results = sorted(rv.fullResponse.results,
                                                     key=lambda res: (res.candidates[0].confidence * res.region_confidence),
                                                     reverse=True)
                    rv.secondary_identifier = rv.fullResponse.results[1].plate if 'plate' in rv.fullResponse.results[1] else None
                    rv.secondary_confidence = rv.fullResponse.results[1].confidence if 'confidence' in rv.fullResponse.results[1] else None

                rv.fullResponse.results[0].update(rv.fullResponse.results[0].candidates[0])
                rv.original_plate = rv.fullResponse.results[0].plate
                rv.candidates = [AD(r) for r in rv.fullResponse.results[0].candidates]
                rv.coordinates = rv.fullResponse.results[0].coordinates
                rv.skew_metrics = self.calculate_skew_metrics(rv.coordinates,
                                                              max_size_ratio=sc.max_size_ratio,
                                                              shape=img_shape,
                                                              theshold_aspect_ratio=sc.theshold_aspect_ratio)

                rv.plate_aspect_ratio_is_incorrect = rv.skew_metrics.bad_aspect_ratio
                rv.plate_aspect_ratio = rv.skew_metrics.aspect_ratio
                if rv.plate_aspect_ratio_is_incorrect:
                    return self.return_condition(sc,
                                                 rv,
                                                 block=f'plate_aspect_ratio_is_incorrect'
                                                )

                rv.plate_dimensions_are_incorrect = rv.skew_metrics.bad_plate_dimensions
                rv.plate_dimensions = (rv.skew_metrics.width, rv.skew_metrics.height)
                rv.plate_dimensions_max = (rv.skew_metrics.max_width, rv.skew_metrics.max_height)
                if rv.plate_dimensions_are_incorrect:
                    return self.return_condition(sc,
                                                 rv,
                                                 block=f'plate_dimensions_are_incorrect'
                                                )

                if self.db is not None:
                    rv.is_blocked =yield self.db.asset_attribute.is_blocked(sc.cgr, rv.fullResponse.results[0].plate)
                    rv.fullResponse.results[0].is_identifier = yield self.db.asset_attribute.is_identifier(sc.cgr, rv.fullResponse.results[0].plate)
                    rv.fullResponse.results[0].has_identifier = yield self.db.asset_attribute.has_identifier(sc.cgr, rv.fullResponse.results[0].plate)
                    if rv.fullResponse.results[0].has_identifier:
                        msg = f'identifier_swap: {rv.fullResponse.results[0].plate} --> {rv.fullResponse.results[0].has_identifier}'
                        rv.modifications.append(msg)
                        logger.info(msg)
                        if isinstance(rv.fullResponse.results[0].has_identifier, tuple):
                            rv.fullResponse.results[0].has_identifier = rv.fullResponse.results[0].has_identifier[0]
                        rv.fullResponse.results[0].plate = rv.fullResponse.results[0].has_identifier

                # dirty or out-of-focus image check using variance_of_laplacian value
                self.is_blurry_or_dirty(color_space_image, rv, threshold=rv.bd_threshold)

                # standard plate blocks
                rv.all_zeros = all([c == '0' for c in rv.fullResponse.results[0].plate])
                rv.all_ones = all([c == '1' for c in rv.fullResponse.results[0].plate])
                rv.all_numeric = all([c.isdigit() for c in rv.fullResponse.results[0].plate])
                if rv.is_blocked:
                    return self.return_condition(sc,
                                                 rv,
                                                 block=f'blocked plate'
                                                )
                if rv.all_zeros:
                    return self.return_condition(sc,
                                                 rv,
                                                 block=f'all zero response block'
                                                )
                if rv.all_ones:
                    return self.return_condition(sc,
                                                 rv,
                                                 block=f'all one response block'
                                                )
                if rv.us_plates and 'DOT' in rv.fullResponse.results[0].plate:
                    return self.return_condition(sc,
                                                 rv,
                                                 block=f'US DOT side read block'
                                                )
                # caution plate block
                if (rv.atollogy_plates and
                    (self.CAUTION_REGEX.match(rv.original_plate) or
                     self.FLIPPED_CAUTION_REGEX.match(rv.original_plate))):
                    return self.return_condition(sc,
                                                 rv,
                                                 block=f'atollogy plates - caution plate block'
                                                )

                # vehicle_type filtering
                if all(['vehicle.body_type' in rv.fullResponse.results[0], len(rv.fullResponse.results[0].vehicle.body_type)]):
                    rv.fullResponse.results[0]['vehicle']['body_type'] = [AD(bt) for bt in rv.fullResponse.results[0].vehicle.body_type]
                    rv.vehicle_type = rv.fullResponse.results[0].vehicle.body_type[0].name
                    rv.vehicle_type_confidence = rv.fullResponse.results[0].vehicle.body_type[0].confidence

                    if sc.vehicle_type_filtering and rv.vehicle_type in sc.vehicle_type_inclusion_list:
                        sc.is_whitelist = True
                    elif rv.brightness >= sc.brightness_threshold and sc.vehicle_type_blocking and rv.vehicle_type not in sc.vehicle_type_inclusion_list:
                        return self.return_condition(sc,
                                                     rv,
                                                     block=f'vehicle_type_block: {rv.vehicle_type}',
                                                     reason=AD({'inclusion_list': sc.vehicle_type_inclusion_list})
                                                    )

                # side-read angle check
                coords = sorted(rv.coordinates, key=lambda item: (item["x"], item["y"]))
                rv.plate_angle = angle([coords[0], coords[2]])
                if rv.side_read_angle_block_enabled:
                    self.is_side_read(rv,
                                      min_angle=side_read_angle_min,
                                      max_angle=side_read_angle_max
                                    )
                    if rv.is_side_read:
                        self.return_condition(sc,
                                              rv,
                                              block=f'side_read_block',
                                              reason=AD({
                                                'plate': rv.fullResponse.results[0].plate,
                                                'plate_angle': rv.plate_angle,
                                                'min_angle': side_read_angle_min,
                                                'max_angle': side_read_angle_max,
                                                'plate_orientation': plate_orientation
                                              })
                                        )


                # populate results finally
                self.populate_results(rv, rv.fullResponse.results[0])
                rv.pkey, rv.pkey_ranking = self.generate_plate_key(rv.candidates)

                # Adjust plates for regional patterns
                if rv.us_plates and 'plate_region' in sc and sc.plate_region == 'md':
                    self.fix_md_plates(rv)
                if rv.uk_plates:
                    self.fix_uk_plates(rv)
                if rv.nl_plates:
                    self.fix_nl_plates(rv)
                if rv.atollogy_plates:
                    self.fix_atollogy_plate(rv)

                if self.svcs.db:
                    top_correlation = yield self.svcs.db.lpr_correlation.get_correlation(
                                            sc.customer_id,
                                            rv.plate,
                                            rv.confidence
                                        )

                    if top_correlation and not top_correlation.block:
                        logger.info(f"LPR CORRELATION FOUND {sc.customer_id} existing plate/confidence: {rv.plate}/{rv.confidence} -> correlation:{top_correlation}")
                        self.populate_results(rv, top_correlation, modifier='correlation')
                        return self.return_condition(sc,
                                                     rv,
                                                     modification=f"correlated_result: {rv.original_plate}->{top_correlation.plate}",
                                                     reason=f"{rv.reason} - with correlated_result: {top_correlation.plate}"
                                                   )
                    elif top_correlation and top_correlation.block:
                        return self.return_condition(sc,
                                                     rv,
                                                     block=f'correlation_block',
                                                     reason=AD({'correlation_entry': top_correlation})
                                                )
                return self.return_condition(sc,
                                             rv,
                                             reason=f"Standard result - no modifications"
                                           )
            else:
                return self.return_condition(sc,
                                             rv,
                                             reason="No License plate identified"
                                           )
        except Exception as err:
            logger.exception(f"lpr exception: {err} {sc}")
            raise

    def polygon_area(self, corners: List[Tuple[int, int]]) -> float:
        # Shoelace formula implementation for area of a polygon
        # - vertices need to be supplied in clockwise order
        n = len(corners)  # of corners
        corners = order_points(corners[:])
        area = 0.0
        for i in range(n):
            j = (i + 1) % n
            area += corners[i][0] * corners[j][1]
            area -= corners[j][0] * corners[i][1]
        area = abs(area) / 2.0
        return area

    def populate_results(self, rv, result_set, modifier=None):
        if not isinstance(result_set, AD):
            result_set = AD(result_set)
        if 'plate' in rv:
            rv.plate = rv.identifier = rv.license_plate = rv.plate.upper()
        if 'plate' in result_set:
            result_set.plate = result_set.plate.upper()
            if modifier is not None:
                original_result = AD(rv)
                rv.original_result = original_result
                rv.modifier = modifier
                self.qc_prop(rv, kv=(modifier, AD({
                                        'modified_by': modifier,
                                        'new_plate': result_set.plate.upper(),
                                        'new_confidence': result_set.confidence,
                                        'original_confidence': rv.original_result.confidence if 'confidence' in rv.original_result else None,
                                        'original_plate': rv.original_result.plate if 'plate' in rv.original_result else None
                                        })
                                    )
                            )
                result_set.candidates = rv.fullResponse.results[0].candidates
                result_set.candidates.append(AD({'plate': result_set.plate,
                                                 'confidence':  result_set.confidence,
                                                 'matches_template': 1}))
                result_set.candidates = sorted([AD(c) for c in result_set.candidates], key=lambda x: x.confidence, reverse=True)
            rv.plate = rv.identifier = rv.license_plate = result_set.plate
            for k in ['candidates', 'confidence', 'matches_template', 'coordinates', 'vehicle_type']:
                if k in result_set:
                    rv[k] = result_set[k]

    def return_condition(self, sc, rv, block=None, modification=None, reason=None, status=None):
        if modification is not None:
            rv.modifications.append(modification)
        if status is not None:
            rv["status"] = status
            if status == 'error':
                logger.error(f"lpr process error: {reason}")
            elif status == 'exception':
                logger.exception(f"lpr process exception: {reason}")
            else:
                logger.info(f"lpr process success: {reason}")

        if sc.is_whitelist:
            rv.is_whitelist = True
        else:
            rv.is_whitelist = False

        rv["reason"] = reason
        if block is not None:
            logger.info(f"blocking lpr result because {block} and {reason}")
            self.block_result(rv, block, cause=reason)
        elif 'identifier' in rv and sc.customer_id in self.exclusions:
            if self.exclusions[sc.customer_id].has_patterns:
                for p in self.exclusions[sc.customer_id].patterns:
                    if fnmatch(rv.identifier, p):
                        self.block_result(rv, 'pattern match block', cause=f'pattern match block - "{p}"')
            if rv.identifier in self.exclusions[sc.customer_id].excluded_plates:
                self.block_result(rv, 'exclusion list block', cause='exclusion list block')

        self.annotate_lpr_image(sc, rv)
        return rv

    def qc_prop(self, rv, kv=None, tag=None):
        if not 'qc_img_props' in rv:
            rv.qc_img_props = AD({'tags': [tag]})
        if rv.qc_img_props and not isinstance(rv.qc_img_props, AD):
            rv.qc_img_props = AD(rv.qc_img_props)
        if kv is not None and isinstance(kv, tuple):
            rv.qc_img_props[kv[0]] = kv[1]
        if tag is not None:
            if 'tags' not in rv.qc_img_props:
                rv.qc_img_props.tags = [tag]
            else:
                rv.qc_img_props.tags.append(tag)

    @inlineCallbacks
    def update_exclusion_list(self, cgr):
        try:
            now = datetime.utcnow().timestamp()
            if not self.svcs.db:
                return
            if cgr not in self.exclusions:
                self.exclusions[cgr] = AD({
                    'last_updated': None,
                    'excluded_plates': []
                    })
            if (cgr in self.exclusions and
                self.exclusions[cgr].last_updated is None or (now - self.exclusions[cgr].last_updated) > 14400):
                self.exclusions[cgr].excluded_plates = yield self.db.asset_attribute.get_exclusion_list(cgr)
                self.exclusions[cgr].last_updated = now
                self.exclusions[cgr].patterns = [p.replace('%', '*') for p in self.exclusions[cgr].excluded_plates if '%' in p]
                self.exclusions[cgr].has_patterns = len(self.exclusions[cgr].patterns)
        except Exception as err:
            logger.exception(f'LPR.update_exclusion_list exception: {err}')
            pass


LPR_Autocrop = LPR
