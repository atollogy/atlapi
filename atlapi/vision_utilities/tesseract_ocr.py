from atlapi.attribute_dict import AD
from atlapi.image.utilities import *
from atlapi.image.transformation import rotate

import logging
import re

import pandas as pd
import pytesseract
import cv2

logger = logging.getLogger('atlapi')

WORD_COLOR = (0, 255, 0)
WORD_BOX_THICKNESS = 4
LINE_COLOR = (0, 255, 255)
LINE_BOX_THICKNESS = 4
PARAGRAPH_COLOR = (255, 255, 0)
PARAGRAPH_BOX_THICKNESS = 4
BLOCK_COLOR = (0,0,255)
BLOCK_BOX_THICKNESS = 4

def tesseract_ocr(step_cfg):
    draw = step_cfg.get('draw', ["words", "blocks"])
    draw_words = "words" in draw
    draw_lines = "lines" in draw
    draw_paragraphs = "paragraphs" in draw
    draw_blocks = "blocks" in draw

    confidence_threshold = step_cfg.get('threshold', 0.4)
    filter_spaces = step_cfg.get('filter_spaces', True)
    return_data = step_cfg.get('return_data', False)

    ocr_engine_model = step_cfg.get('ocr_engine_model', 3)
    page_segmentation_mode = step_cfg.get('page_segmentation_mode', 1)

    return_value = {
        "status": "success", 
        "reason": None
    }

    # prepare config string
    tesseract_config_string = f'--oem {ocr_engine_model} --psm {page_segmentation_mode}'
    tconfig = step_cfg.get('config', None)
    if tconfig is not None:
        return_value['warning'] = 'custom config parameters, ensure you know what you are doing: {tconfig}'
        if isinstance(tconfig, str):
            tesseract_config_string += f' tconfig'
        elif isinstance(tconfig, [dict, AD]):
            tesseract_config_string += f' -c '
            tesseract_config_string += " ".join([f'{k}={v}' for k, v in tconfig.items()])

    # load image and perform usual checks
    img = load_image(
        step_cfg.filedata[step_cfg['filelist'][0]],
        color_space=step_cfg.get('color_space', 'rgb'),
        crop=step_cfg.get('input_crop', None)
    )
    # check_image(img, step_cfg, return_value)
    
    # TODO: Preprocess Steps
    # NOTE: currently applied with image transforms as preprocessing, except rotate
    if 'rotate' in step_cfg:
        img = rotate(img, step_cfg.rotate)

    try:
        data = pytesseract.image_to_data(
            img,
            config=tesseract_config_string,
            output_type=pytesseract.Output.DATAFRAME
        )
    except Exception as err:
        logger.error(f'tesseract_ocr failed with error: {err}')
        return_value['status'] = 'failure'
        return_value['reason'] = f'{err}'
        return return_value

    image_drawing = img
    confidence_filtered_data = data[data.conf >= int(confidence_threshold*100)]

    if filter_spaces:
        confidence_filtered_data = confidence_filtered_data[confidence_filtered_data.text != ' ']

    # output
    full_text = ""
    text = " ".join(confidence_filtered_data.text)
    blocks = []
    paragraphs = []
    lines = []
    words = list(confidence_filtered_data.text)

    # TODO: handle multipage documents?
    for block_number in confidence_filtered_data.block_num.unique():
        bf = confidence_filtered_data[confidence_filtered_data.block_num == block_number]

        for paragraph_number in bf.par_num.unique():
            if len(bf.par_num.unique()) > 1:
                full_text += "\t"
            pf = bf[bf.par_num == paragraph_number]

            for line_number in pf.line_num.unique():
                lf = pf[pf.line_num == line_number]

                for word_number in lf.word_num.unique():
                    wf = lf[lf.word_num == word_number]
                                    
                    if draw_words:
                        x1, y1, x2, y2 = wf.left.min(), wf.top.min(), (wf.left + wf.width).max(), (lf.top + lf.height).max()
                        image_drawing = cv2.rectangle(
                            image_drawing, (x1,y1), (x2, y2), 
                            WORD_COLOR, WORD_BOX_THICKNESS
                        )
                line = " ".join(lf.text)
                lines.append(line)
                full_text += line + "\n"
                        
                if draw_lines:
                    x1, y1, x2, y2 = lf.left.min(), lf.top.min(), (lf.left + lf.width).max(), (lf.top + lf.height).max()
                    image_drawing = cv2.rectangle(
                        image_drawing,
                        (x1 - WORD_BOX_THICKNESS, y1 - WORD_BOX_THICKNESS),
                        (x2 + WORD_BOX_THICKNESS, y2 + WORD_BOX_THICKNESS), 
                        LINE_COLOR, LINE_BOX_THICKNESS
                    )

            paragraphs.append(" ".join(pf.text))
            
            if draw_paragraphs:
                x1, y1, x2, y2 = pf.left.min(), pf.top.min(), (pf.left + pf.width).max(), (pf.top + pf.height).max()
                image_drawing = cv2.rectangle(
                    image_drawing,
                    (x1 - LINE_BOX_THICKNESS, y1 - LINE_BOX_THICKNESS),
                    (x2 + LINE_BOX_THICKNESS, y2 + LINE_BOX_THICKNESS), 
                    PARAGRAPH_COLOR, PARAGRAPH_BOX_THICKNESS
                )

        blocks.append(" ".join(bf.text))
        full_text += "\n"
                
        if draw_blocks:
            x1, y1, x2, y2 = bf.left.min(), bf.top.min(), (bf.left + bf.width).max(), (bf.top + bf.height).max()
            image_drawing = cv2.rectangle(
                image_drawing, 
                (x1 - PARAGRAPH_BOX_THICKNESS, y1 - PARAGRAPH_BOX_THICKNESS),
                (x2 + PARAGRAPH_BOX_THICKNESS, y2 + PARAGRAPH_BOX_THICKNESS), 
                BLOCK_COLOR, BLOCK_BOX_THICKNESS
            )

    return_value['filedata'] = [(
        step_cfg.filelist[0][:-4] + "_ocr.jpg", 
        "bio", 
        pack_image(image_drawing)
    )]

    text_data = {
        "text": text,
        "full_text": full_text,
        "blocks": blocks,
        "paragraphs": paragraphs,
        "lines": lines,
        "words": words
    }

    if return_data:
        return_value['data'] = text_data
    else:
        return_value['full_text'] = full_text

    if not 'subjects' in step_cfg:
        return return_value
    # maintain compatibility with ocr module
    return_value['fields_found'] = []
    # Handle results and search for subjects
    for subject_name, subject in step_cfg.subjects.items():
        try:
            search_in = subject.get('search_in', 'lines')
            # make sure we have a list to go through
            search_list = text_data.get(search_in, [])
            if isinstance(search_list, str):
                search_list = [search_list]

            if 'search' in subject:
                compiled_expression = re.compile(subject['search'])
                for item in search_list:
                    match = compiled_expression.search(item)
                    if match:
                        return_value[subject_name] = match.group()
                        return_value[f'{subject_name}_context'] = item
                        return_value['fields_found'].append(subject_name)
                        break # TODO: return all?
            elif 'match' in subject: 
                compiled_expression = re.compile(subject['match'])
                for item in search_list:
                    match = compiled_expression.match(item)
                    if match:
                        return_value[subject_name] = match.group()
                        return_value['fields_found'].append(subject_name)
                        break # TODO: return all?

        except Exception as err:
            logger.warning(f'tesseract_ocr encountered error in search: {err}')
            return_value['message'] = f'tesseract_ocr error in search: {err}'
        
    return return_value
