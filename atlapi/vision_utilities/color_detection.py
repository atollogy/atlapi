from atlapi.attribute_dict import AD
from atlapi.image.utilities import (
    Zone,
    color_space_zone,
    load_image,
    check_image,
    default_zones,
    Color_Range,
)
from atlapi.common import *
import logging
import numpy as np
import cv2
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')


def huePercentage(image, hsvRange):
    minHSVColor_range = np.array(hsvRange.min)
    maxHSVColor_range = np.array(hsvRange.max)

    maskHSV = cv2.inRange(image, minHSVColor_range, maxHSVColor_range)
    filteredHSV = cv2.bitwise_and(image, image, mask=maskHSV)

    huePercent = np.around(
        (float(np.count_nonzero(filteredHSV[:, :, 0])) / (image.shape[0] * image.shape[1]) * 100),
        decimals=1,
    )

    if huePercent > hsvRange.threshold:
        return hsvRange.label
    else:
        return False

def colorDetection(stepCfg):
    sc = stepCfg
    return_value = AD({"status": "success", "reason": None})
    try:
        color_space_image = load_image(
            stepCfg.filedata[stepCfg.filelist[0]],
            color_space=sc.color_space,
            crop=sc.input_crop
        )
        check_image(color_space_image, sc, return_value)
        for subject_name, subject in sc.subjects.items():
            if subject_name in return_value:
                raise MVError("Subject name {} already present in results".format(subject_name))
            color_space = Color_Range.create(
                subject.color_space, subject.color_space, subject.colors
            )

            if "zone" not in subject or not subject.zone:
                subject.zone = default_zones(color_space_image).image
            else:
                subject.zone = Zone.create(subject_name, subject.zone)

            zonecolor_space_image = color_space_zone(
                color_space_image, sc.color_space, subject.color_space, subject.zone
            )

            return_value[subject_name] = "off"
            for cName, cRange in color_space.items():
                detected = huePercentage(zonecolor_space_image, cRange)
                if detected:
                    return_value[subject_name] = detected
                    break

    except Exception as err:
        msg = "color_detection exception: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value
