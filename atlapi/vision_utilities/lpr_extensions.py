from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.image.utilities import *
import base64
import cv2
import logging
import re
import io

from twisted.internet.defer import inlineCallbacks
import numpy as np
import requests

LOGGER = logging.getLogger("atlapi")

# TODO: this should be come a dataclass in Python 3.7
class LPRResult:
    def __init__(self, plate=None, confidence=0.0, region=None):
        self.plate = plate
        self.confidence = confidence
        self.region = region


class BaseLPR(Core):
    LPR_IMAGE_COMPLEXITY_THRESHOLD = 30
    LPR_IMAGE_BRIGHTNESS_THRESHOLD = 0
    FAST_PIXEL_SENSITIVITY = 20
    BOX_PADDING = 20

    def __init__(self, endpoints=None):
        Core.__init__(self, endpoints=endpoints)

    def lpr_error(self, msg, result, exceptionType=False):
        if exceptionType:
            LOGGER.exception(msg)
        else:
            LOGGER.error(msg)
        result.status = "error"
        result.reason = msg

    def prepare_image(self, step_cfg, return_value):
        number_images = len(step_cfg.filelist)

        # Handle Errors on Image Input
        if number_images == 0:
            return_value.status = "No api call"
            return_value.reason = "No image"
            return None

        try:
            # Ensure input_crop is a Zone object
            if not isinstance(step_cfg.input_crop, Zone):
                step_cfg.input_crop = Zone.create("input_crop", step_cfg.input_crop, atype="crop")

            # Load image
            image =  load_image(
                step_cfg.filedata[step_cfg.filelist[0]], 
                color_space=step_cfg.color_space, 
                crop=step_cfg.input_crop
            )
            return image
        except Exception as err:
            self.lpr_error("lpr error preparing image: {} {}".format(err, step_cfg), return_value, True)

    def pre_lpr_checks(self, step_cfg, return_value, image):
        # brightness and resolution in return_value, also produces warnings
        # for overexposed/underexposed images
        check_image(image, step_cfg, return_value)

        # We may want to replace image complexity calculation using corner detection with
        # brightness checks per discussion with Aarti. Right now this code won't add any performance
        # cost since it should short circuit with current LPR_IMAGE_BRIGHTNESS_THRESHOLD = 0
        if "image_brightness_threshold" in step_cfg:
            image_brightness_threshold = step_cfg.image_brightness_threshold
        else:
            image_brightness_threshold = self.LPR_IMAGE_BRIGHTNESS_THRESHOLD

        if (image_brightness_threshold > 0) and (np.mean(image) < image_brightness_threshold):
            return_value["reason"] = "Low brightness - {} < {}".format(np.mean(image), image_brightness_threshold)
            self.amend_bbox_label(return_value["reason"], step_cfg.input_crop)
            return False


        # Set image complexity threshold from config or default
        if "image_complexity_threshold" in step_cfg:
            image_complexity_threshold = step_cfg.image_complexity_threshold
        else:
            image_complexity_threshold = self.LPR_IMAGE_COMPLEXITY_THRESHOLD

        # Calculate keypoint metrics
        keypoints = self.fast_corner_detection(image)
        number_keypoints = len(keypoints)

        # Handle complexity check / checks
        if number_keypoints < image_complexity_threshold:
            return_value["reason"] = "Low complexity filter block - {} < {}".format(number_keypoints, image_complexity_threshold)
            self.amend_bbox_label(return_value["reason"], step_cfg.input_crop)
            return False

        return True

    def lpr_configuration(self, step_cfg):
        ''' Process step_cfg to produce configuration. May return a dict containing configurations
            to be passed to perform_lpr. lpr_configuration should perform process the configuration
            into the format needed by each LPR vendor/provider'''
        raise NotImplementedError('LPR must implement lpr_configuration')

    @inlineCallbacks
    def perform_lpr(self, step_cfg, return_value, image, config):
        ''' Perform the LPR action on a given image based on the config. In the case of an error,
            perform_lpr may indicate the error through the return_value.'''
        raise NotImplementedError('LPR must implement perform_lpr')

    def post_lpr_processing(self, step_cfg, return_value, response_data):
        ''' Perform post processing on LPR results, including filtering and confidence thresholding,
            returning plate coordinate and license plate.'''
        raise NotImplementedError('LPR must implement post_lpr_processing')

    @inlineCallbacks
    def lpr(self, stepCfg):
        """Implements LPR through interfaces provided by the class"""
        plate_coordinates = None
        return_value = AD({"status": "success", "reason": None})

        image = self.prepare_image(stepCfg, return_value)
        if image is None:
            return return_value

        try:
            # Complexity and Brightness checks
            if not self.pre_lpr_checks(stepCfg, return_value, image):
                self.annotate_lpr_image(stepCfg, return_value, None)
                return return_value

            # TODO: Validate Configuration for LPR Vendor
            config = self.lpr_configuration(stepCfg)

            response_data = yield self.perform_lpr(stepCfg, return_value, image, config)

            # Handle API Error
            if "api_error" in response_data:
                self.lpr_error("lpr error: {}".format(response_data["api_error"]), return_value)
                self.amend_bbox_label('api_error', stepCfg.input_crop)
            else:
                LOGGER.debug("lpr response data: {}".format(response_data))
                return_value["fullResponse"] = response_data

                # TODO: Add confidence across LPR Services
                confidence = 0
                plate_coordinates, license_plate = self.post_lpr_processing(stepCfg, return_value, response_data)

                # Set outputs for LPR Step
                # TODO: Add in confidence to outputs
                if license_plate is not None:
                    return_value["license_plate"] = license_plate
                    return_value["identifier"] = license_plate
                    return_value["confidence"] = confidence

            self.annotate_lpr_image(stepCfg, return_value, plate_coordinates)

        except Exception as err:
            self.lpr_error("lpr exception: {} {}".format(err, stepCfg), return_value, True)

        return return_value

    def amend_bbox_label(self, add_text, bbox, atype='crop'):
        if not isinstance(bbox, Zone):
            bbox = Zone.create(add_text, bbox, atype=atype)
        bbox.label = bbox.label + ' - {}'.format(add_text)
        return bbox

    def detect_license_plate(self, output, result, confidence_threshold=None):
        try:
            if confidence_threshold is not None and result.confidence < confidence_threshold:
                return None
        except Exception as err:
            LOGGER.exception(err)
            output["reason"] = "Invalid type for Confidence Threshold, acceptable types (int, float)"

        if result.region is not None and len(result.region) > 0:
            output["region"] = result.region

        if result.plate is not None and len(result.plate) > 0:
            if re.compile(r"^\d{1,5}$").match(result.plate):
                output["reason"] = (
                    "Detected Licence plate - {}. Not recording it as an identifier. Most likely a side-read."
                ).format(result.plate)
            elif len(result.plate) < 4:
                output["reason"] = (
                    "Detected Licence plate - {}. Not recording it as an identifier. Most likely a side-read."
                ).format(result.plate)
            else:
                return result.plate.upper()

    def annotate_lpr_image(self, stepCfg, output, plate_coordinates=None):
        annotationZones = []
        toAnnotate = load_image(
            stepCfg.filedata[stepCfg.filelist[0]], 
            color_space=stepCfg.color_space, 
            crop=None
        )
        annotationZones.append(stepCfg.input_crop)
        if plate_coordinates:
            annotationZones.append(
                Zone.create(
                    "license_plate: {}".format(output.identifier),
                    plate_coordinates,
                    atype="subject",
                    notwh=True,
                )
            )
        annotatedImage = annotate(toAnnotate, annotationZones, color_space=stepCfg.color_space)
        output.filedata = [(stepCfg.filelist[0][:-4] + "_annotated.jpg", "bio", annotatedImage)]

    def fast_corner_detection(self, image):
        blur_img = cv2.GaussianBlur(image, (5, 5), 0)
        fast = cv2.FastFeatureDetector_create(threshold=self.FAST_PIXEL_SENSITIVITY)
        keyPoints = fast.detect(blur_img, None)
        return keyPoints


class OpenALPR(BaseLPR):
    def __init__(self, endpoints=None):
        # Adds compatibility with current openalpr configuration
        # Specific openalpr endpoint will override lpr endpoint though
        if "lpr" in endpoints and not "openalpr" in endpoints:
            endpoints.openalpr = endpoints.lpr
        BaseLPR.__init__(self, endpoints=endpoints)

    def lpr_configuration(self, step_cfg):
        # set recognize_vehicle (OPENALPR specific)
        recognize_vehicle = 1 if "recognize_vehicle" in step_cfg else 0

        # Country (OPENALPR specific)
        country = step_cfg.country if "country" in step_cfg else "us"

        # Configure parameters (OPENALPR)
        params = {
            "secret_key": self.ENDPOINTS.openalpr.key,
            "recognize_vehicle": recognize_vehicle,
            "country": country,
            "return_image": 0,
            "topn": 10,
        }

        return params

    @inlineCallbacks
    def perform_lpr(self, step_cfg, return_value, image, config):
        # Send image to endpoint
        payload = base64.b64encode(cv2.imencode(".jpg", image)[1]).decode("utf-8")
        response_data = yield self._call_external_api(
            payload=payload, url=self.ENDPOINTS.openalpr.url, params=config
        )
        return response_data

    def post_lpr_processing(self, step_cfg, return_value, response_data):
        # pick license plate from results
        plate_coordinates = None
        license_plate = None
        if "results" in response_data and len(response_data["results"]) > 0:
            results = response_data["results"]
            resultCandidates = []
            for result in results:
                resultCandidate = max(
                    result["candidates"], key=lambda candidate: candidate["confidence"]
                )
                resultCandidate["coordinates"] = result["coordinates"]
                resultCandidates.append(resultCandidate)
            output = max(
                resultCandidates, key=lambda candidate: candidate["confidence"]
            )

            confidenceThreshold = None
            if "confidence_threshold" in step_cfg and step_cfg["confidence_threshold"]:
                confidenceThreshold = step_cfg.confidence_threshold

            lpr_result = LPRResult(output["plate"], output["confidence"], output.get("region", None))

            license_plate = self.detect_license_plate(return_value, lpr_result, confidenceThreshold)
        else:
            return_value["reason"] = "No License plate identified"


        if license_plate is not None:
            coordinates = output["coordinates"]
            for item in coordinates:
                item["tot"] = item["x"] + item["y"]
            coordinates = sorted(coordinates, key=lambda each_coord: each_coord["tot"])
            if step_cfg.input_crop:
                plate_coordinates = [
                    step_cfg.input_crop.x + coordinates[0]["x"] - self.BOX_PADDING,
                    step_cfg.input_crop.y + coordinates[0]["y"] - self.BOX_PADDING,
                    step_cfg.input_crop.x + coordinates[-1]["x"] + self.BOX_PADDING,
                    step_cfg.input_crop.y + coordinates[-1]["y"] + self.BOX_PADDING,
                ]
            else:
                plate_coordinates = [
                    coordinates[0]["x"] - self.BOX_PADDING,
                    coordinates[0]["y"] - self.BOX_PADDING,
                    coordinates[-1]["x"] + self.BOX_PADDING,
                    coordinates[-1]["y"] + self.BOX_PADDING,
                ]
        else:
            self.amend_bbox_label("No License plate identified", step_cfg.input_crop)

        return plate_coordinates, license_plate


class PlateRecognizerLPR(BaseLPR):
    def lpr_configuration(self, step_cfg):
        params = {
            "header": {
                "Authorization": "Token {}".format(self.ENDPOINTS.platerecognizer.token) \
                    if "token" in self.ENDPOINTS.platerecognizer else "",
            },
            "regions": step_cfg.country if "country" in step_cfg else None
        }
        return params

    @inlineCallbacks
    def perform_lpr(self, step_cfg, return_value, image, config):
        # Send image to endpoint
        payload = io.BytesIO(cv2.imencode(".jpg", image)[1].tobytes())
        # TODO: Adapt Core to allow such a request
        response_data = yield requests.post(
            url=self.ENDPOINTS.platerecognizer.url,
            headers=config['header'],
            files={'upload': payload}
            ).json()
        return response_data

    def post_lpr_processing(self, step_cfg, return_value, response_data):
        # pick license plate from results
        plate_coordinates = None
        license_plate = None
        if "results" in response_data and len(response_data["results"]) > 0:
            results = response_data["results"]
            output = max(
                results, key=lambda x: x["score"]
            )

            confidence_threshold = None
            if "confidence_threshold" in step_cfg and step_cfg["confidence_threshold"]:
                confidence_threshold = step_cfg.confidence_threshold

            # Mapping to OpenALPR Results
            lpr_result = LPRResult(output["plate"], (output["score"] + output["dscore"]) / 2)

            license_plate = self.detect_license_plate(return_value, lpr_result, confidence_threshold)
        else:
            return_value["reason"] = "No License plate identified"


        if license_plate is not None:
            coordinates = output["box"]
            # plate_coordinates should be given as [xmin, ymin, xmax, ymax]
            if step_cfg.input_crop:
                plate_coordinates = [
                    step_cfg.input_crop.x + coordinates["xmin"] - self.BOX_PADDING,
                    step_cfg.input_crop.y + coordinates["ymin"] - self.BOX_PADDING,
                    step_cfg.input_crop.x + coordinates["xmax"] + self.BOX_PADDING,
                    step_cfg.input_crop.y + coordinates["ymax"] + self.BOX_PADDING,
                ]
            else:
                plate_coordinates = [
                    coordinates["xmin"] - self.BOX_PADDING,
                    coordinates["ymin"] - self.BOX_PADDING,
                    coordinates["xmax"] + self.BOX_PADDING,
                    coordinates["ymax"] + self.BOX_PADDING,
                ]
        else:
            self.amend_bbox_label("No License plate identified", step_cfg.input_crop)

        return plate_coordinates, license_plate
