import datetime
import logging

from atlapi.attribute_dict import AD
import atlapi.video_handler as vh
from atlapi.image.color import ColorLookup
from atlapi.image.shapes import Zone
from atlapi.image.annotation import annotate, color_space_convert
from atlapi.common import to_datetime

import numpy as np
import cv2
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')

DEFAULT_GP_COLOR_LOOKUP = ColorLookup([
    ("tan", (105, 100,  97)),
    ("tan", (191, 166, 148)),
    ("white", (226, 229, 233)),
    ("grey", (76, 79, 87)),
    ("green", (150, 200, 200)),
], rgb=True)

# NOTE: step config looks like:
# {
#    "threshold": 128,   # int between 0-255
#    "region": None,     # [x,y,w,h]
#    "colors": None,     # <color lookup definition>
#    "minimum_color_time": 0.5,
# }

def check_nan(val, res):
    if np.isnan(val):
        return res

    return val

def video_color_event(step_cfg: AD) -> AD:
    minimum_color_time = datetime.timedelta(seconds=step_cfg.get('minimum_color_time', 0.5))
    collection_time = to_datetime(step_cfg.collection_time)
    if 'region' in step_cfg and isinstance(step_cfg.region, Zone):
        # TODO: merge ffmpeg crop support into Zone class
        step_cfg.region = (step_cfg.region.x, step_cfg.region.y, step_cfg.region.w, step_cfg.region.h)


    events = []
    video_data = step_cfg['filedata'][step_cfg['filelist'][0]]

    with vh.load_video(video_data) as video_file:
        if 'region' in step_cfg:
            frame_data, _ = vh.frame_metadata_extraction(video_file.name, region=step_cfg.region)
        else:
            frame_data, _ = vh.frame_metadata_extraction(video_file.name)

    metric = np.array([d.yavg for d in frame_data])
    times = np.array([d.time for d in frame_data])
    color_data = np.array([d.color for d in frame_data])

    if 'colors' in step_cfg:
        color = ColorLookup(step_cfg.colors, rgb=True).color_lookup(color_data)
    else:
        color = DEFAULT_GP_COLOR_LOOKUP.color_lookup(color_data)

    # get lists of indexes where we cross the threshold
    indexes = np.where(np.diff(metric > step_cfg.threshold) >= 1.0)[0]

    # handle starting with an activation
    if metric.shape[0] > 1 and metric[0] > step_cfg.threshold:
        indexes = np.concatenate((np.array((0,)), indexes))

    for i in range(0, indexes.shape[0], 2):
        start_index = indexes[i]
        end_index = indexes[i+1] if i+1 < indexes.shape[0] else (metric.shape[0] - 1)

        color_indexes = np.where(np.diff(np.unique(color[start_index:end_index], return_inverse=True)[1]) != 0)[0]

        if len(color_indexes) == 0:
            start_timestamp = datetime.timedelta(seconds=times[start_index])
            end_timestamp = datetime.timedelta(seconds=times[end_index])

            events.append({
                "start_time":   collection_time+start_timestamp,
                "end_time":     collection_time+end_timestamp,
                "duration":     end_timestamp - start_timestamp,
                "color_name":   color[(start_index+end_index)//2],
                "color":        color_data[(start_index+end_index)//2].astype(int).tolist(),
                "brightness":   check_nan(metric[start_index:end_index].mean(),0),
                "frame":        start_index
            })
        else:
            last_offset = 0
            for offset in list(color_indexes) + [end_index-start_index]:
                start_timestamp = datetime.timedelta(seconds=times[start_index+last_offset])
                end_timestamp = datetime.timedelta(seconds=times[start_index+offset])
                duration = end_timestamp - start_timestamp

                if not duration < minimum_color_time:
                    events.append({
                        "start_time":   collection_time+start_timestamp,
                        "end_time":     collection_time+end_timestamp,
                        "duration":     end_timestamp - start_timestamp,
                        "color_name":   color[start_index+(last_offset+offset)//2],
                        "color":        color_data[start_index+(last_offset+offset)//2].astype(int).tolist(),
                        "brightness":   check_nan(metric[start_index+last_offset:start_index+offset].mean(), 0),
                        "frame":        start_index+last_offset
                    })
                    last_offset = offset

    color_duration, color_events = {}, {}
    for i, event in enumerate(events):
        if event['color_name'] in color_duration:
            color_duration[event['color_name']] += event['duration']
        else:
            color_duration[event['color_name']] = event['duration']

        if event['color_name'] in color_events:
            color_events[event['color_name']].append(i)
        else:
            color_events[event['color_name']] = [i]

    return AD({
        "events": events,
        "color_duration": color_duration,
        "color_events": color_events
    })

def video_color_event_wrapper(step: AD) -> list:
    records = []

    try:
        result = video_color_event(step)
        step.output = result
        records.append(step)

        # load video a second time to extract frames
        with vh.load_video(step['filedata'][step['filelist'][0]]) as video_file:
            video = cv2.VideoCapture(video_file.name)
            # create records for each color event occuring on the collection period
            # this exists to make loading data into datamart easier
            for color, duration in result.color_duration.items():
                record = AD(step)
                record.bundle = list(record.bundle)
                record.bundle[-1] = f'{record.step_name}_{color}'
                record.scid = ':'.join(record.bundle)
                record.step_name = f'{record.step_name}_{color}'
                record.function = f'{record.function}_color'
                record.output = AD({
                    "duration": duration.total_seconds(),   # convert timedelta to seconds
                    "color": color,
                    "count": len(result.color_events[color])
                })

                # TODO: optimize for single pass; I don't like this code but it isn't too slow
                # and I couldn't come up with something better at the moment
                try:
                    # extract one image per color as proof
                    video.set(cv2.CAP_PROP_POS_FRAMES, result.events[result.color_events[color][0]]['frame']+1)
                    ret, frame = video.read()
                    record.output.filedata = [(
                            f"{color}-event-image.jpg",
                            "bio",
                            annotate(
                                color_space_convert(frame, 'bgr', 'rgb'),
                                [Zone.create(f'event zone', step.region, atype="region")],
                                color_space='rgb'
                            )
                    )]
                except Exception as err:
                    logger.exception(f'video_color_event_wrapper error extracting frame: {err}')

                records.append(record)
            video.release()
    except Exception as err:
        logger.exception(f'video_color_event_wrapper: {err}')

    return records
