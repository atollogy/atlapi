from atlapi.attribute_dict import AD
from atlapi.image.utilities import *
from atlapi.common import *
from atlapi.image.visualizer import *
from func_timeout import func_timeout, func_set_timeout, FunctionTimedOut
import logging
import numpy as np
import cv2
import mahotas as mh
from skimage import measure

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

from pydantic import BaseModel
from twisted.internet import task, reactor, threads
from twisted.internet.threads import deferToThread, blockingCallFromThread
from twisted.internet.defer import ensureDeferred, inlineCallbacks, maybeDeferred, waitForDeferred, Deferred
from typing import Tuple, Dict, List, Optional, Any, Union

logger = logging.getLogger('atlapi')

### color range definition
yellow_minColor_range = np.array([15, 10, 20])
yellow_maxColor_range = np.array([35, 255, 255])

green_minColor_range = np.array([35, 10, 20])
green_maxColor_range = np.array([90, 255, 255])

blue_minColor_range = np.array([90, 10, 10])
blue_maxColor_range = np.array([150, 255, 255])

### red has two range 0~10, 150~180 because hue express by 180 degree.
red_minColor_range1 = np.array([150, 10, 10])
red_maxColor_range1 = np.array([180, 255, 255])

red_minColor_range2 = np.array([0, 10, 10])
red_maxColor_range2 = np.array([15, 255, 255])

class Position(BaseModel):
    x: int       # pyre-ignore[13]
    y: int       # pyre-ignore[13]

class StickerPositionResult(BaseModel):
    position:  Position
    confidence_score: float

_VERBOSE = False


##@execution_time
@inlineCallbacks
def StickerBase(stepCfg: AD, StickerResult: object) -> AD:
    global _VERBOSE

    _VERBOSE = stepCfg.verbose

    return_value = AD({"status": "success", "reason": None, "filedata": []})
    sc = stepCfg
    anonymize = sc.anonymize if 'anonymize' in sc else False
    try:
        color_space_image = load_image(
            stepCfg.filedata[stepCfg.filelist[0]],
            color_space=sc.color_space,
            crop=sc.input_crop
        )
        check_image(color_space_image, sc, return_value)

        return_value.location = {}

        for subject_name, subject in sc.subjects.items():
            if subject_name in return_value:
                raise MVError(
                    "Subject name {} already present in results".format(subject_name)
                )
            if "zone" not in subject or not subject.zone:
                # default is entire image
                h, w, _ = color_space_image.shape
                subject.zone = Zone.create(subject_name, [0, 0, w, h], atype="subject")
            else:
                subject.zone = Zone.create(subject_name, subject.zone, atype="subject")

            zonecolor_space_image = color_space_zone(
                color_space_image.copy(), sc.color_space, 'bgr', subject.zone
            )

            if not len(zonecolor_space_image) or not all(zonecolor_space_image.shape[:2]):
                continue

            ### default parameters for fast_sticker
            color_number_threshold = (
                subject.color_number_threshold
                if "color_number_threshold" in subject
                else 4
            )

            minimum_sticker_size = 40

            sticker_count = subject.sticker_count if "sticker_count" in subject else 2

            sticker_size = subject.sticker_size if "sticker_size" in subject else minimum_sticker_size

            line_detection = (
                subject.line_detection if "line_detection" in subject else True
            )

            if sticker_size < minimum_sticker_size:
                # interpolate, resize image

                interpolated_img = yield image_resize(zonecolor_space_image.copy(), sticker_size, minimum_sticker_size)
                sticker_size = minimum_sticker_size
            else:
                interpolated_img = zonecolor_space_image.copy()

            region_thresh = int(sticker_size * sticker_size / 2)

            color_img = yield colorFilter(interpolated_img)

            color_mask = yield colorHeatmap(
                color_img,
                color_number=color_number_threshold,
                dilate_size=(sticker_size, sticker_size),
                low_threshold=sticker_size,
                return_value=return_value,
            )
            color_filter = sizeFilter(color_mask, region_thresh)

            imgLabels, nr_objects = yield mh.label(color_filter)
            sticker_result = yield sticker_center_result(color_filter, imgLabels, nr_objects, subject.zone)

            if line_detection:
                # if found number of objects more or less than sticker number, then excute line detection
                line_length = int(sticker_size / 2)
                step_size = int(sticker_size / 10)

                color_mask = yield colorHeatmap(
                    color_img,
                    color_number=color_number_threshold - 1,
                    dilate_size=(sticker_size, sticker_size),
                    low_threshold=sticker_size,
                    return_value=return_value,
                )
                color_filter = sizeFilter(color_mask, region_thresh)

                gray = cv2.cvtColor(interpolated_img, cv2.COLOR_BGR2GRAY)
                blurred = cv2.GaussianBlur(gray, (3, 3), 0)
                edge = auto_canny(blurred)
                mask_edge = edge * color_filter

                heatmap_edge = yield deferToThread(
                    functools.partial(
                        SeparatePatch,
                            color_filter,
                            mask_edge,
                            window_size=sticker_size,
                            step=step_size,
                            length=line_length,
                            angle_number_threshold=2,
                        )
                    )

                heatmap_result = (heatmap_edge >= 10) * 1
                heatmap_filter = sizeFilter(heatmap_result, int(region_thresh / 2))

                imgLabels, nr_objects = yield mh.label(heatmap_filter)
                sticker_result = yield sticker_center_result(heatmap_filter, imgLabels, nr_objects, subject.zone)

            subject.zone.label, return_value[subject_name] = yield StickerResult(
                sticker_count, nr_objects
            )

            return_value.location[subject_name] = sticker_result

            annotated_image = anonymized_image = None
            annotated_image = annotate(color_space_image.copy(),
                                             [subject.zone],
                                             color_space=sc.color_space)
            if anonymize:
                anonymized_image = yield anonymize_image(color_space_image, sc)
                if anonymized_image is not None:
                    anonymized_image = annotate(anonymized_image,
                                                      [subject.zone],
                                                      color_space=sc.color_space)

            if annotated_image is not None:
                return_value.filedata.append(
                    (
                        sc.filelist[0][:-4] + "_annotated.jpg",
                        "bio",
                        annotated_image,
                    )
                )
            if anonymized_image is not None:
                return_value.filedata.append(
                    (
                        sc.filelist[0][:-4] + "_anonymized.jpg",
                        "bio",
                        anonymized_image,
                    )
                )
        return return_value
    except Exception as err:
        msg = "FastSticker exception: {}".format(err)
        logger.exception(msg)
        raise

#@execution_time
@inlineCallbacks
def sticker_center_result(filter_image: np.ndarray, imgLabels: np.ndarray, nr_objects: int, zone: AD) -> List[Dict]:
    """
    output_example:
        json format
        [{
            "position": {
                "x": 541,
                "y": 407
            },
            "confidence_score": 1.0
        },
        {
            "position": {
                "x": 630,
                "y": 409
            },
            "confidence_score": 1.0
        }],
        [...]

        final output:
        {
            "status": "success",
            "reason": null,
            "brightness": 102.65983412000868,
            "resolution": "(768, 1024)",
            "location": {
                "region_1": [{
                    "position": {
                        "x": 541,
                        "y": 407
                    },
                    "confidence_score": 1.0
                },
                {
                    "position": {
                        "x": 630,
                        "y": 409
                    },
                    "confidence_score": 1.0
                }],
                "region_2": [...]
            },
            "region_1": "closed"
            "region_2"; ...
        }
    """

    # get center points from imgLabels
    # https://mahotas.readthedocs.io/en/latest/api.html?highlight=center_of_mass#mahotas.center_of_mass

    centers = yield mh.center_of_mass(filter_image, imgLabels)[1:]

    output_list = []
    for i in range(nr_objects):
        center = yield Position(x=round(centers[i][1] + zone[0]), y=round(centers[i][0] + zone[1]))
        sticker_result = yield StickerPositionResult(position=center, confidence_score=1.0)
        output_list.append(dict(sticker_result))

    return output_list

# interpolate image by suggested sticker size
#@execution_time
def image_resize(img: np.ndarray, sticker_size: int, minimum_sticker_size: int) -> np.ndarray:
    minimum_sticker_size += 10
    h, w, _ = img.shape
    size_time = round(minimum_sticker_size / sticker_size)
    if size_time > 1:
        width = size_time * w
        height = size_time * h
        resized = cv2.resize(img, (width, height))
        return resized
    else:
        return img


# Colordetection functions
#@execution_time
def filterImg(mask: np.ndarray, img: np.ndarray) -> np.ndarray:

    result_img = np.zeros(img.shape, dtype=np.uint8)
    result_img[:, :, 0] = img[:, :, 0] * mask
    result_img[:, :, 1] = img[:, :, 1] * mask
    result_img[:, :, 2] = img[:, :, 2] * mask

    return result_img

#@execution_time
def grayMask(img: np.ndarray, mask: np.ndarray, percentage_diff: float) -> np.ndarray:

    np_int16 = np.array(img, dtype="int16")

    dark_1 = (
        abs(np_int16[:, :, 0] - np_int16[:, :, 1])
        < (np_int16[:, :, 0] * percentage_diff)
    ) * 1
    dark_1 = dark_1 * mask

    dark_2 = (
        abs(np_int16[:, :, 1] - np_int16[:, :, 2])
        < (np_int16[:, :, 1] * percentage_diff)
    ) * 1
    dark_2 = dark_2 * mask

    dark_3 = (
        abs(np_int16[:, :, 2] - np_int16[:, :, 0])
        < (np_int16[:, :, 2] * percentage_diff)
    ) * 1
    dark_3 = dark_3 * mask

    return dark_1, dark_2, dark_3

#@execution_time
@inlineCallbacks
def colorFilter(img: np.ndarray) -> np.ndarray:

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    if gray is None:
        return []

    # less than 90 brightness
    first_dark = (gray <= 70) * 1
    first_diff_percentage = 0.3

    # between 90 and 200
    second_dark = ((gray > 70) * (gray <= 200)) * 1
    second_diff_percentage = 0.15

    # between 200 and 220
    third_dark = ((gray > 200) * (gray <= 220)) * 1
    third_diff_percentage = 0.08

    # bigger than 200
    fourth_dark = (gray > 220) * 1
    fourth_diff_percentage = 0.04

    first_1, first_2, first_3 = yield grayMask(img, first_dark, first_diff_percentage)
    second_1, second_2, second_3 = yield grayMask(img, second_dark, second_diff_percentage)
    third_1, third_2, third_3 = yield grayMask(img, third_dark, third_diff_percentage)
    fourth_1, fourth_2, fourth_3 = yield grayMask(img, fourth_dark, fourth_diff_percentage)

    total_1 = first_1 + second_1 + third_1 + fourth_1
    total_2 = first_2 + second_2 + third_2 + fourth_2
    total_3 = first_3 + second_3 + third_3 + fourth_3

    total_gray = total_1 * total_2 * total_3

    # invert mask
    color_mask = np.where(total_gray > 0.5, 0, 1)
    color_pixels = filterImg(color_mask, img)

    return color_pixels

#@execution_time
def sizeFilter(mask: np.ndarray, low_threshold: float) -> np.ndarray:

    labeled, nr_objects = mh.label(mask)
    sizes = mh.labeled.labeled_size(labeled)
    too_small = np.where(sizes < low_threshold)
    labeled = (mh.labeled.remove_regions(labeled, too_small) > 0) * 1

    return labeled

#@execution_time
@inlineCallbacks
def color_rangeMask(
    hsv_img: np.ndarray,
    dilate_size: Tuple[int, int],
    minpixel: float,
    color_title: str,
    color_pick: List[int],
    return_value: AD,
    minColor_range: np.ndarray,
    maxColor_range: np.ndarray,
    minColor_range2: np.ndarray = None,
    maxColor_range2: np.ndarray = None,
) -> np.ndarray:

    imageMask = cv2.inRange(hsv_img, minColor_range, maxColor_range)
    if minColor_range2 is not None:
        imageMask = imageMask + cv2.inRange(hsv_img, minColor_range2, maxColor_range2)

    save_imageMask = imageMask.copy()
    save_imageMask[save_imageMask != 0] = 1

    imageMask = sizeFilter(imageMask, minpixel)
    draw_mask = save_imageMask + imageMask

    maskpatch = yield cv2.dilate(
        imageMask.astype(np.float32), np.ones(dilate_size, np.uint8), iterations=1
    )

    return (maskpatch > 0) * 1

#@execution_time
@inlineCallbacks
def colorHeatmap(
    img: np.ndarray,
    color_number: int,
    dilate_size: Tuple[int, int],
    low_threshold: int,
    return_value: AD,
) -> np.ndarray:

    low_threshold = 1.5 * low_threshold
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    yellow_img = yield color_rangeMask(
        hsv_img,
        dilate_size,
        low_threshold,
        "yellow-patch-image",
        [255, 255, 0],
        return_value,
        yellow_minColor_range,
        yellow_maxColor_range,
    )
    green_img = yield color_rangeMask(
        hsv_img,
        dilate_size,
        low_threshold,
        "green-patch-image",
        [0, 255, 0],
        return_value,
        green_minColor_range,
        green_maxColor_range,
    )
    ### blue patch has smaller number pixel on sticker
    blue_img = yield color_rangeMask(
        hsv_img,
        dilate_size,
        int(low_threshold / 2),
        "blue-patch-image",
        [0, 0, 255],
        return_value,
        blue_minColor_range,
        blue_maxColor_range,
    )
    red_img = yield color_rangeMask(
        hsv_img,
        dilate_size,
        low_threshold,
        "red-patch-image",
        [255, 0, 0],
        return_value,
        red_minColor_range1,
        red_maxColor_range1,
        red_minColor_range2,
        red_maxColor_range2,
    )

    color_heatmap = yellow_img + green_img + blue_img + red_img
    filtered_mask = (color_heatmap >= color_number) * 1

    return filtered_mask


def angleCount(lines: List[List[float]], theta_threshold: float) -> int:
    theta = [t[0][1] for t in lines]
    theta_sort = sorted(theta)

    standard = 0
    count = 1
    for i in theta_sort:
        if standard == 0:
            standard = i
        if abs(i - standard) >= theta_threshold:
            count += 1
            standard = i

    return count


# Window sliding function
@func_set_timeout(15)
def windowSlidingLine(
    arr: np.ndarray,
    window_size: int,
    step: int,
    length: int,
    angle_number_threshold: int,
) -> np.ndarray:
    logger.debug(f"windowSlidingLine.debug: shape: {arr.shape}, window_size: {window_size}, step: {step}, length: {length}, angle_number_threshold: {angle_number_threshold}")
    row = 0
    col = 0
    max_row, max_col = arr.shape
    heat_map = np.zeros(arr.shape, dtype=np.uint8)
    while row < max_row:
        while col < max_col:
            patch = np.zeros(
                arr[row : row + window_size, col : col + window_size].shape,
                dtype=np.uint8,
            )
            patch = arr[row : row + window_size, col : col + window_size]
            patch = np.array(patch, dtype=np.uint8)

            lines = cv2.HoughLines(patch, 1, np.pi / 180, length)
            if lines is not None:
                diff_angle = angleCount(
                    lines, 0.4
                )  # theta_threshold 0.4 is default value for angle difference
                if diff_angle >= angle_number_threshold:
                    heat_map[row : row + window_size, col : col + window_size] += 1

            col += step
        row += step
        col = 0

    return heat_map

# @inlineCallbacks
def SeparatePatch(
    mask: np.ndarray,
    img: np.ndarray,
    window_size: int,
    step: int,
    length: int,
    angle_number_threshold: int,
) -> np.ndarray:
    try:
        sticker_heat = np.zeros(mask.shape, dtype="uint8")
        imgLabels, nr_objects = mh.label(mask)

        for label in np.unique(imgLabels):
            if label == 0:
                continue
            labelMask = np.zeros(mask.shape, dtype="uint8")
            labelMask[imgLabels == label] = 1

            y_list, x_list = np.where(labelMask == 1)
            sliced_image = img[min(y_list) : max(y_list), min(x_list) : max(x_list)]

            hist_map = windowSlidingLine(
                                sliced_image,
                                window_size,
                                step,
                                length,
                                angle_number_threshold
                            )

            sticker_heat[min(y_list) : max(y_list), min(x_list) : max(x_list)] = hist_map

        return sticker_heat
    except Exception as err:
        logger.exception("FastSticker.SeparatePatch exception: " + repr(err))
        raise
