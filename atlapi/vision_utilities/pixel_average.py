from atlapi.attribute_dict import AD
from atlapi.image.utilities import (
    Zone,
    color_space_zone,
    load_image,
    check_image,
    default_zones,
    image_rotation,
)
import logging

logger = logging.getLogger('atlapi')
import numpy as np
import cv2
from PIL import Image
from io import BytesIO
from atlapi.common import *
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

def rotate_image(mat, angle):
    # angle in degrees

    height, width = mat.shape[:2]
    image_center = (width/2, height/2)

    rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

    abs_cos = abs(rotation_mat[0,0])
    abs_sin = abs(rotation_mat[0,1])

    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    rotation_mat[0, 2] += bound_w/2 - image_center[0]
    rotation_mat[1, 2] += bound_h/2 - image_center[1]

    rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))

    return rotated_mat

def pixelAverage(stepCfg):
    """Finds average pixel values in given zone or image"""
    return_value = AD({"status": "success", "reason": None})
    sc = stepCfg
    if 'input_crop' not in sc:
        sc.input_crop = None

    try:
        color_space_image = load_image(
            stepCfg.filedata[stepCfg.filelist[0]],
            color_space=sc.color_space,
            crop=sc.input_crop
        )
        check_image(color_space_image, sc, return_value)
        for subject_name, subject in sc.subjects.items():
            if subject_name in return_value:
                raise MVError("Subject name {} already present in results".format(subject_name))
            if "zone" not in subject or not subject.zone:
                subject.zone = default_zones(color_space_image).center
            else:
                subject.zone = Zone.create(subject_name, subject.zone)

            return_value[subject_name] = list(
                np.mean(color_space_image[subject.zone.y_slice, subject.zone.x_slice], axis=(0, 1)).astype(int)
            )

            if "angle" in subject and subject.angle:
                rotated_img = rotate_image(color_space_image[subject.zone.y_slice, subject.zone.x_slice], subject.angle)
                rishape = rotated_img.shape
                pil_img = Image.fromarray(rotated_img)
                export_img = BytesIO()
                pil_img.save(export_img, format="JPEG")
                export_img = np.asarray(bytearray(export_img.getvalue()), dtype=np.uint8).reshape(rishape)
                return_value.filedata = [
                    (stepCfg.filelist[0][:-4] + "_rotated.jpg", "bio", export_img)
                ]

    except Exception as err:
        msg = "pixelAverage exception: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value
