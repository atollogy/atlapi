from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.vision_utilities.anchor_detection import AnchorDetectionV1

import cv2
import functools
import logging
import numpy as np
import mahotas as mh
import math
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os
from skimage import filters
from skimage.morphology import disk
from typing import Tuple, Dict, List, Optional, Any, Union
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue


logger = logging.getLogger('atlapi')

# configuration parameter candidates
# digit numbers seven segment table
DIGITS_LOOKUP = {
    # {TOP, TOP_LEFT, TOP_RIGHT, CENTER, BOTTOM_LEFT, BOTTOM_RIGHT, BOTTOM}
    (0, 0, 0, 0, 0, 0, 0): "",
    (1, 1, 1, 0, 1, 1, 1): "0",
    (0, 0, 1, 0, 0, 1, 0): "1",
    (1, 0, 1, 1, 1, 0, 1): "2",
    (1, 0, 1, 1, 0, 1, 1): "3",
    (0, 1, 1, 1, 0, 1, 0): "4",
    (1, 1, 0, 1, 0, 1, 1): "5",
    (1, 1, 0, 1, 1, 1, 1): "6",
    (0, 1, 0, 1, 1, 1, 1): "6",
    (1, 0, 1, 0, 0, 1, 0): "7",
    (1, 1, 1, 1, 1, 1, 1): "8",
    (1, 1, 1, 1, 0, 1, 1): "9",
    ## Additional mappings for broken/extra segments
    (1, 1, 1, 1, 1, 0, 1): "2",
    (1, 1, 1, 0, 0, 1, 0): "7",
    (1, 0, 1, 1, 1, 1, 1): "3",
    (0, 1, 1, 1, 0, 0, 0): "4",
    (1, 1, 1, 1, 0, 1, 0): "9",
}

# each digit crop width and height threshold, used for squeezeImg function.
crop_width_threshold = 0.8
crop_height_threshold = 0.8

_VERBOSE = False
_DEFAULT_VERBOSE = False

"""
1. Transform digit field into a rectangle
2. Test for overexposure
3. Select color channels
4. parametrized Otsu filter for binary color quantization
5. find top of digit field (crop top of image)
6. split digits
    a. noise filter (remove small regions)
    b. dilate digit pixels
    c. shrink width and height around digit
7. seven segment recognition
"""

class SevenSegment(Core):
    def __init__(self):
        Core.__init__(self, 'func_seven_segment')

    @inlineCallbacks
    def seven_segment(self, stepCfg: AD) -> AD:
        self._VERBOSE = stepCfg.get("verbose", _DEFAULT_VERBOSE)

        sc = stepCfg
        return_value = AD({"status": "success", "reason": None, "filedata": [], "warning": None})
        try:
            if "anchor" in stepCfg and "anchor_point_list" in stepCfg.anchor:
                yield self.svcs.func_anchor_detection_v1.run_anchor_detection(sc, return_value) # check camera movement by anchor
        except Exception as err:
            logger.exception(f'seven_segment anchor_detection error {err}')
        res = yield deferToThread(
                    functools.partial(
                        self.process_seven_segment,
                        sc,
                        return_value
                    )
                )
        return res

    def process_seven_segment(self, sc: AD, return_value: AD) -> AD:
        try:
            color_space_image = load_image(
                sc.filedata[sc.filelist[0]], color_space="bgr", crop=sc.input_crop
            )
            check_image(color_space_image, sc, return_value)  # check image brightness

            for subject_name, subject in sc.subjects.items():
                if subject_name in return_value:
                    raise APIError(
                        f"seven_segment.process_seven_segment subject name {subject_name} already present in results"
                    )

                if "color_channels" in subject:
                    channel_list = subject.color_channels
                    if not isinstance(channel_list, list):
                        channel_list = [channel_list]
                else:
                    channel_list = ["RGB"]

                pixel_dilation = (
                    1 + subject.extra_dilation if "extra_dilation" in subject else 1
                )
                min_connected = (
                    subject.connected_pixels if "connected_pixels" in subject else 10
                )

                # padding_factor dialates four coordinates ploygon by ratio
                padding_factor = (
                    subject.padding_factor if "padding_factor" in subject else 10
                )
                # glaring_ratio range is between 0.1 ~ 2.0
                glaring_ratio = (
                    subject.glaring_ratio if "glaring_ratio" in subject else 1.0
                )
                brightness_lower_range = (
                    subject.brightness_lower_range
                    if "brightness_lower_range" in subject
                    else 0
                )
                brightness_upper_range = (
                    subject.brightness_upper_range
                    if "brightness_upper_range" in subject
                    else 255
                )
                total_digits = (
                    subject.total_digits
                    if "total_digits" in subject and subject.total_digits
                    else 6
                )

                # transform skewed image to rectangle image.
                transform_img = self.perspectiveTransform(
                    color_space_image,
                    subject.top_left,
                    subject.top_right,
                    subject.bottom_left,
                    subject.bottom_right,
                    padding_factor,
                    total_digits,
                    return_value,
                )

                # shift image brightness, if overexposure
                adjust_brightness_img = self.reduce_brightness_from_overexposure(
                    transform_img, brightness_lower_range, brightness_upper_range, return_value
                )

                # threshold image by lower and upper range
                brightness_range_img = self.brightness_range_filter(
                    adjust_brightness_img,
                    brightness_lower_range,
                    brightness_upper_range,
                    channel_list,
                    return_value,
                )

                # calculate digit size
                digit_height = self.get_mean_distance(
                    subject.top_left,
                    subject.bottom_left,
                    subject.top_right,
                    subject.bottom_right,
                )

                total_width = int(
                    self.get_mean_distance(
                        subject.top_left,
                        subject.top_right,
                        subject.bottom_left,
                        subject.bottom_right,
                    )
                )

                digit_gap_ratio = 0.25
                digit_width = int(
                    total_width
                    / (total_digits + (total_digits - 1) * digit_gap_ratio)
                )
                digit_gap = int(digit_width * digit_gap_ratio)

                # get binary image from each channel.
                for channel_string in channel_list:
                    if len(channel_string) != 3:
                        channel_string = self.overExposed(transform_img, channel_string)

                    channel_array = self.channel_selection(channel_string.upper())
                    stretch_img = self.stretchImg(brightness_range_img, channel_array)

                    # threshold binary image
                    binary_img = self.binarySegmentationImg(
                        stretch_img, channel_string, digit_width, glaring_ratio, return_value
                    )

                    if (
                        np.count_nonzero(binary_img)
                        / (binary_img.shape[0] * binary_img.shape[1])
                        >= 0.01
                    ):
                        break

                # crop image from top to bottom
                cropped_img = self.crop_height(binary_img, digit_width, digit_height)

                # each digit number auto crop result
                each_digit = self.getDigit(
                    cropped_img,
                    digit_width,
                    digit_height,
                    digit_gap,
                    min_connected,
                    pixel_dilation,
                    total_digits,
                    return_value,
                )

                # Post process on digit number recognition
                # Failing case send warning message and default value depend on the order of magnitude.
                digit_recog_text = ""
                predicted = ""
                for i in range(total_digits):
                    segments, on, glares = self.segments_zone(each_digit[i])
                    result_digit = self.digit_recog(segments, each_digit[i], on, glares, i + 1, return_value)
                    predicted = result_digit + predicted

                    result_digit = self.each_text_postprocess(
                        digit_recog_text, result_digit, total_digits, return_value
                    )
                    digit_recog_text = result_digit + digit_recog_text

                    if result_digit == "":
                        break

                digit_recog_text = self.entire_text_postprocess(digit_recog_text, return_value)

                # # save visualizer images
                # if self._VERBOSE:
                #     logger.info("Recognition result is: {}".format(predicted))
                #     save_visualized_img(
                #         transform_img[:, :, (2, 1, 0)],
                #         "{}, warning: {}".format(predicted, return_value.warning),
                #         "verbose/" + "7-result-img.jpg",
                #         return_value,
                #     )

                # save result
                return_value[subject_name] = digit_recog_text
                return_value.predicted = predicted

                # annotate result, make label on the original image.
                boundary_zone = self.boundaryCoordinates(
                    subject.top_left,
                    subject.top_right,
                    subject.bottom_left,
                    subject.bottom_right,
                )

                subject.zone = Zone.create(subject_name, boundary_zone, atype="subject")
                subject.zone.label = digit_recog_text
                annotated_image = annotate(color_space_image, [subject.zone], color_space="bgr")
                return_value.filedata.append(
                    (sc.filelist[0][:-4] + "_annotated.jpg", "bio", annotated_image)
                )

        except Exception as err:
            msg = "sevenSegment exception: {}".format(err)
            logger.exception(msg)
            return_value.status = "error"
            return_value.reason = msg

        return return_value


    def reduce_brightness_from_overexposure(
        self,
        img: np.ndarray,
        brightness_lower_range: int,
        brightness_upper_range: int,
        return_value: AD,
    ) -> np.ndarray:
        """
        input: numpy image
        output: numpy image
        decrease brightness by subtracting pixel value.
        overexposure_threshold default was 150
        unit8_limit_mask is need for unit8 range. negative number become 0 value.
        """
        overexposure_threshold = 150
        middle_brightness = 128
        result_img = img
        avg_brightness = int(np.mean(img))

        if avg_brightness > overexposure_threshold:
            shift_range = avg_brightness - middle_brightness
            unit8_limit_mask = img >= shift_range
            result_img = img.copy() - shift_range
            result_img = result_img * unit8_limit_mask

        # # save visualizer images
        # if self._VERBOSE:
        #     save_histogram(
        #         result_img,
        #         brightness_lower_range,
        #         brightness_upper_range,
        #         avg_brightness,
        #         overexposure_threshold,
        #         "verbose/" + "2-histogram-img.jpg",
        #         return_value,
        #     )

        return result_img


    def overExposed(self, img: np.ndarray, channel_string: str) -> str:
        """
        check over exposed image using counting pixel number above over_exposed_threshold.
        otsu filter is borken when choose not exposed color channel on over exposed image.
        """
        channel_list = "BGR"
        channel_selection = ""
        over_exposed_threshold = 180
        over_exposed_ratio = (
            0.07
        )  # usually number "00" only take 7% on the 6 digit number length image
        img_size = img.shape[0] * img.shape[1]

        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ovep_pixel_number = np.sum(gray_img > over_exposed_threshold)

        if (ovep_pixel_number / img_size) >= over_exposed_ratio:
            for i in range(len(channel_list)):
                ovep_ch_pixel = np.sum(img[:, :, i] > over_exposed_threshold)
                if ovep_ch_pixel >= ovep_pixel_number * 0.70:
                    channel_selection += channel_list[i]

        if len(channel_selection):
            channel_string = channel_selection

        return channel_string


    def brightness_range_filter(
        self, img: np.ndarray, lower_range: int, upper_range: int, channel_list: str, return_value: AD
    ) -> np.ndarray:
        # image value be limited by between lower_range value and upper_range value
        result_img = img.copy()
        for i in range(3):
            result_img[:, :, i][result_img[:, :, i] < lower_range] = 0
            result_img[:, :, i][result_img[:, :, i] > upper_range] = 0

        # # save visualizer images
        # if self._VERBOSE:
        #     save_color_channel(
        #         result_img, channel_list, "verbose/" + "3-brightness-range.jpg", return_value
        #     )

        return result_img


    def channel_selection(self, color: str) -> List:
        # result of channel array is [BGR] such as [1,1,1] (BGR) or [1,0,1] (BG)
        channel = []
        channel.append(1 * ("B" in color))
        channel.append(1 * ("G" in color))
        channel.append(1 * ("R" in color))

        return channel


    def get_mean_distance(self, CD_1: List, CD_2: List, CD_3: List, CD_4: List) -> int:
        """
        CD is coordinate [x,y]
        distance_1 is the distance between CD_1 and CD_2
        distance_2 is the distance between CD_3 and CD_4
        """
        distance_1 = math.hypot(CD_1[0] - CD_2[0], CD_1[1] - CD_2[1])
        distance_2 = math.hypot(CD_3[0] - CD_4[0], CD_3[1] - CD_4[1])

        return int(np.mean([distance_1, distance_2]))


    def unit_vector(self, src: List, dest: List) -> np.array:
        """
        calculate unit vector from two points
        """
        # math.hypot is calcultion of distance between two points
        distance = math.hypot(src[0] - dest[0], src[1] - dest[1])
        vector = np.array(src) - np.array(dest)

        unit_vector = vector / distance

        return unit_vector

    def padding_corner(self, src: List, crd_1: List, crd_2: List, padding_factor: int) -> np.array:
        """
        move corner coordinates realted to two other connected points.
        combine two unit vectors and move corner coordinate with padding factor.

        src: List[x,y] coordinates
        crd_1: List[x,y] coordinates
        crd_2: List[x,y] coordinates
        padding_factor: 0 ~ 50 integer value
        """
        uv_1 = self.unit_vector(src, crd_1)
        uv_2 = self.unit_vector(src, crd_2)

        moving_corner = src + (uv_1 + uv_2) * padding_factor

        return moving_corner


    def padding_coordinates(
        self,
        top_left: List,
        top_right: List,
        bottom_left: List,
        bottom_right: List,
        padding_factor: float,
        image_numpy: np.ndarray,
        total_digits: int,
        return_value: AD,
    ) -> List:
        """
        padd four coordinate with padding_factor ratio
        four coordinate be beggier from center point
        """
        sign_border = np.array(
            [top_left, top_right, bottom_right, bottom_left], dtype=np.int32
        )

        pd_tl = self.padding_corner(top_left, top_right, bottom_left, padding_factor)
        pd_tr = self.padding_corner(top_right, top_left, bottom_right, padding_factor)
        pd_br = self.padding_corner(bottom_right, top_right, bottom_left, padding_factor)
        pd_bl = self.padding_corner(bottom_left, top_left, bottom_right, padding_factor)

        # # save visualizer images
        # if self._VERBOSE:
        #     padded_border = np.array([pd_tl, pd_tr, pd_br, pd_bl], dtype=np.int32)
        #     save_padding_area(
        #         image_numpy, sign_border, padded_border, total_digits, return_value
        #     )

        return pd_tl, pd_tr, pd_bl, pd_br


    def crop_height(self, img: np.ndarray, digit_width: int, digit_height: int) -> np.ndarray:
        """
        This function crop top height and from top point adding digit_height.
        pixel_thickness is that threshold value(number of pixel) to decide crop
        """
        (h, w) = img.shape
        pixel_thickness = 10

        digit_height = int(digit_height + (digit_height * 0.05))
        for r in range(h):
            raw_pixels = np.count_nonzero(img[r : r + 1, 0:w])
            if raw_pixels > digit_width / 3:
                thickness_pixel = np.count_nonzero(img[r : r + 1 + pixel_thickness, 0:w])
                if thickness_pixel > digit_width * (pixel_thickness - 6):
                    if r + 1 + digit_height > h * 1.3:
                        return img
                    else:
                        return img[r : r + 1 + digit_height, 0:w]
        return img


    def perspectiveTransform(
        self,
        img: np.ndarray,
        top_left: List,
        top_right: List,
        bottom_left: List,
        bottom_right: List,
        padding_factor: float,
        total_digits: int,
        return_value: AD,
    ) -> np.ndarray:
        """
        deskew, skwed four coordinate to rectangle.
        """
        # padding for all four corner points
        pd_tl, pd_tr, pd_bl, pd_br = self.padding_coordinates(
            top_left,
            top_right,
            bottom_left,
            bottom_right,
            padding_factor,
            img,
            total_digits,
            return_value,
        )

        # calculate transform shape that we will modify
        tf_height = int(self.get_mean_distance(pd_tl, pd_bl, pd_tr, pd_br))
        tf_width = int(self.get_mean_distance(pd_tl, pd_tr, pd_bl, pd_br))

        transform_top_left = [0, 0]
        transform_top_right = [tf_width, 0]
        transform_bottom_left = [0, tf_height]
        transform_bottom_right = [tf_width, tf_height]

        pts1 = np.float32([pd_tl, pd_tr, pd_bl, pd_br])
        pts2 = np.float32(
            [
                transform_top_left,
                transform_top_right,
                transform_bottom_left,
                transform_bottom_right,
            ]
        )

        # getPerspectiveTransform require np.float32 type
        M = cv2.getPerspectiveTransform(pts1, pts2)
        dst = cv2.warpPerspective(img, M, (tf_width, tf_height))

        return dst


    def stretchImg(self, img: np.ndarray, channel: List) -> np.ndarray:
        # Adding each channel and creat as an one channel
        ch = np.array(img[:, :, :], dtype="uint16") * channel

        return ch[:, :, 0] + ch[:, :, 1] + ch[:, :, 2]


    def binarySegmentationImg(
        self, gray_img: np.ndarray, channel_string: str, digit_width: int, glaring_ratio: float, return_value: AD
    ) -> np.ndarray:
        """
        Threshold_otsu need higher threshold value.
        Thus using channel number increase threshold value.
        """

        h, w = gray_img.shape

        big_radius = digit_width
        small_radius = int(big_radius / 4)

        small_kernel = disk(small_radius)
        big_kernel = disk(big_radius)

        try:
            global_threshold = filters.threshold_otsu(gray_img) * glaring_ratio
            global_binary = gray_img > global_threshold

            # decrease global threshold, if it fails to find any digit number.
            if np.count_nonzero(global_binary) < h * w * 0.01:
                glaring_ratio = 0.9 * glaring_ratio
                global_threshold = filters.threshold_otsu(gray_img) * glaring_ratio
                global_binary = gray_img > global_threshold

            convert_uint8 = (gray_img / len(channel_string)).astype(np.uint8)

            small_local_otsu = filters.rank.otsu(convert_uint8, small_kernel)
            small_local_binary = convert_uint8 > small_local_otsu

            big_local_otsu = filters.rank.otsu(convert_uint8, big_kernel)
            big_local_binary = convert_uint8 > big_local_otsu

            and_binary = (global_binary * small_local_binary * big_local_binary) * 255

        except Exception as e:
            return_value.warning = str(e)
            and_binary = (gray_img > 80) * 255

        return and_binary

    def sizeFilter(self, mask: np.ndarray, lesspixel: int) -> np.ndarray:
        # Remove small noise spot, remve pixels that connected small number of other pixels.
        labeled, nr_objects = mh.label(mask)

        if nr_objects == 0:
            return mask

        sizes = mh.labeled.labeled_size(labeled)
        too_small = np.where(sizes < lesspixel)
        labeled = (mh.labeled.remove_regions(labeled, too_small) > 0) * 255

        return labeled


    def shaveEdges(self, img: np.ndarray, digit_width: int, digit_height: int) -> np.ndarray:
        """
        Find compacted region for each digit to analysis seven segment.
        """
        # input image is 2D scale mask image
        (h, w) = img.shape
        (y_list, x_list) = np.where(img != 0)

        if np.count_nonzero(img) > int(h * w * 0.05):

            x = min(x_list)
            x2 = max(x_list)
            y = min(y_list)
            y2 = max(y_list)

            if (x2 - x) <= (digit_width * crop_width_threshold):
                x = 0
                x2 = digit_width
            if (y2 - y) <= (digit_height * crop_height_threshold):
                y = 0
                y2 = digit_height

            result_img = img[y : y2 + 1, x : x2 + 1]
        else:
            result_img = img
        return result_img

    def getDigit(
        self,
        binary_img: np.ndarray,
        digit_width: int,
        digit_height: int,
        digit_gap: int,
        less_pixel: int,
        dilate_number: int,
        total_digits: int,
        return_value: AD,
    ) -> np.ndarray:
        """
        Read each digit image from seven segment result.
        """
        (h, w) = binary_img.shape
        each_digit = []
        # dilate window kernel
        kernel = np.ones((3, 3), np.uint8)

        # threshold value to avoid small noise spot
        pixel_threshold_ratio = 1 - crop_height_threshold
        point_threshold = int(digit_height * pixel_threshold_ratio)

        # skip value used for empty number
        digit_start_point = w

        # remove noise pixels
        binary_img = self.sizeFilter(binary_img, less_pixel)

        binary_img = np.array(binary_img, dtype=np.uint8)
        # dilate seven segment image
        binary_img = cv2.dilate(binary_img, kernel, iterations=dilate_number)

        right_list = []
        left_list = []

        for i in range(w, 0, -1):
            column_set_pixels = np.count_nonzero(binary_img[0:h, i - 1 : i])

            if column_set_pixels > point_threshold:
                if i <= digit_start_point and i >= digit_width:
                    left_edge = i - digit_width
                    left_list.append(left_edge)
                    right_list.append(i)

                    # crop each digit image
                    digit_img = binary_img[0:h, left_edge:i]

                    # find compacted region on a digit image
                    squeezed_img = self.shaveEdges(digit_img, digit_width, digit_height)

                    each_digit.append(squeezed_img)

                    digit_start_point = left_edge - digit_gap

            if len(each_digit) > 1 and len(each_digit) < total_digits:
                if digit_start_point - i > digit_width - digit_gap:
                    each_digit.append(np.zeros(shape=(digit_height, digit_width)))
                    digit_start_point = digit_start_point - digit_width - digit_gap

            if len(each_digit) >= total_digits:
                break

        while len(each_digit) < total_digits:
            each_digit.append(np.zeros(shape=(digit_height, digit_width)))

        # # save visualizer images
        # if self._VERBOSE:
        #     save_each_digit(
        #         binary_img,
        #         each_digit,
        #         left_list,
        #         right_list,
        #         total_digits,
        #         "verbose/" + "4-dilate-img.jpg",
        #         "verbose/" + "5-each-digit-img.jpg",
        #         return_value,
        #     )

        return each_digit

    def segments_zone(self, digit_img: np.ndarray) -> List:
        """
        assign seven segment bbox region
        """
        (h, w) = digit_img.shape

        (dW, dH) = (int(w * 0.25), int(h * 0.15))
        dHC = int(h * 0.05)

        extra_dh = int(dH * 0.5)

        segments = [
            ((dW, 0), (w - dW, dH)),  # top
            ((0, extra_dh), (dW, h // 2 - extra_dh)),  # top-left
            ((w - dW, extra_dh), (w, h // 2 - extra_dh)),  # top-right
            ((dW, (h // 2) - dHC), (w - dW, (h // 2) + dHC)),  # center
            ((0, h // 2 + extra_dh), (dW, h - extra_dh)),  # bottom-left
            ((w - dW, h // 2 + extra_dh), (w, h - extra_dh)),  # bottom-right
            ((dW, h - dH), (w - dW, h))  # bottom
        ]

        glares = [
            ((dW, dH), (w - dW, (h // 2) - dHC)), # middle-top for glare check
            ((dW, (h // 2) + dHC), (w - dW, h - dH)) # middle-bottom for glare check
        ]

        return segments, [0, 0, 0, 0, 0, 0, 0], glares


    def digit_recog(
        self, segments: List, digit_img: np.ndarray, on: List, glares: List, placement: int, return_value: AD
    ) -> str:
        """
        Read seven segment region and compare with seven segment table list.
        """

        segment_list = [
            "top",
            "top-left",
            "top-right",
            "center",
            "bottom-left",
            "bottom-right",
            "bottom",
        ]

        text_info = []
        percentage_info = []
        on_stage_threshold = 45

        for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
            segROI = digit_img[yA:yB, xA:xB]
            area = (xB - xA) * (yB - yA)
            total = cv2.countNonZero(segROI)

            if total != 0:
                percentage_value = (total / float(area)) * 100
            else:
                percentage_value = 0

            percentage_info.append(percentage_value)
            text_info.append(segment_list[i] + ": " + format(percentage_value, ".2f") + "%")

            if percentage_value >= on_stage_threshold:
                on[i] = 1

        # check whether glare pixels or noise are exist or not.
        for (i, ((xA, yA), (xB, yB))) in enumerate(glares):
            glareROI = digit_img[yA:yB, xA:xB]
            area = (xB - xA) * (yB - yA)
            glare_pixel = cv2.countNonZero(glareROI)
            glare_percentage = glare_pixel / area * 100

            percentage_info.append(glare_percentage)
            text_info.append("glare_{}: {:.2f}%".format(i, glare_pixel / area * 100))

            if glare_percentage > on_stage_threshold:
                return_value.segment_glares = "Image has glare or light reflection."

        #
        # # save visualizer images
        # if self._VERBOSE:
        #     save_segment_region(
        #         digit_img,
        #         segments,
        #         placement,
        #         percentage_info,
        #         on_stage_threshold,
        #         text_info,
        #         return_value,
        #     )

        if tuple(on) in DIGITS_LOOKUP:
            return DIGITS_LOOKUP[tuple(on)]
        else:
            # Fail recognition case. it will return F
            return "F"


    def boundaryCoordinates(
        self, top_left: List, top_right: List, bottom_left: List, bottom_right: List
    ) -> List:
        """
        find rectangle box from four skewd coordinate for annotation image.
        """
        x_list = []
        x_list.append(top_left[0])
        x_list.append(top_right[0])
        x_list.append(bottom_left[0])
        x_list.append(bottom_right[0])

        y_list = []
        y_list.append(top_left[1])
        y_list.append(top_right[1])
        y_list.append(bottom_left[1])
        y_list.append(bottom_right[1])

        return [
            min(x_list),
            min(y_list),
            (max(x_list) - min(x_list)),
            (max(y_list) - min(y_list)),
        ]


    #### business logic post process ####


    def each_text_postprocess(
        self, digit_recog_text: str, result_digit: str, total_digits: int, return_value: AD
    ) -> str:

        if len(digit_recog_text) <= 1:
            if result_digit == "F":
                result_digit = "0"
                return_value.warning = "Digit read failed."
        else:
            if len(digit_recog_text) >= total_digits:
                if result_digit == "F":
                    result_digit = "1"
                    return_value.warning = "Most significant digit read failed."
            else:
                if result_digit == "F":
                    result_digit = "5"
                    return_value.warning = "Digit read failed."

        return result_digit


    def entire_text_postprocess(self, digit_recog_text: str, return_value: AD) -> str:
        if len(digit_recog_text) == 0:
            digit_recog_text = "00"
            return_value.warning = "Digit read failed."
        elif digit_recog_text == "88":
            digit_recog_text = "00"
            return_value.warning = "Digit read failed."

        if digit_recog_text[-1] != "0":
            digit_recog_text = digit_recog_text[:-1] + "0"
            return_value.warning = "Last digit read failed."

        if "segment_glares" in return_value:
            return_value.warning = return_value.segment_glares
            del return_value.segment_glares

        return digit_recog_text
