
from atlapi.attribute_dict import AD
from atlapi.image.utilities import Zone, color_space_zone, load_image, check_image, default_zones
import logging
import numpy as np
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')

def simpleRegion(stepCfg):
    return_value = AD({"status": "success", "regions": {}, "reason": None})
    try:
        sc = stepCfg
        color_space_image = load_image(
            stepCfg.filedata[stepCfg.filelist[0]],
            color_space=sc.color_space,
            crop=sc.input_crop
        )
        check_image(color_space_image, sc, return_value)
        for subject_name, subject in sc.subjects.items():
            if "zone" not in subject or not subject.zone:
                subject.zone = default_zones(color_space_image).image
            else:
                subject.zone = Zone.create(subject_name, subject.zone)

            logger.debug("simpleRegion zone type: {}".format(type(subject.zone)))
            cropBGRImage = color_space_zone(
                color_space_image, sc.color_space, subject.color_space, subject.zone
            )

            if np.mean(cropBGRImage) >= subject.brightness_threshold:
                return_value[subject_name] = "on"
            else:
                return_value[subject_name] = "off"
            del cropBGRImage

    except Exception as err:
        msg = "simple_region exception: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value
