from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *

import functools
import logging
import numpy as np
import cv2
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

#### configurable parameters in Customer Config
### file size threshold default value is 0. it means that will check every file.
file_size_threshold = 0

# ratio where we should ignore the diff as being an environment change
overall_skip_pct = 35

# percentage of changed pixels in a given bounding box.
bbox_diff_pct = 10

# pixel channels must change by at least this to be part of a diff.
pixel_diff_threshold = 7

# region sizes as part of whole
crop_regions = {
    "left": [0.05, 0.0, 0.16, 1.0],
    "center": [0.42, 0.0, 0.16, 1.0],
    "right": [0.79, 0.0, 0.16, 1.0],
}

# number of above regions needed in order to be counted in activity category
activity_threshold = 3

# number of above regions needed in order to be counted in setup category
setup_threshold = activity_threshold - 1

logger = logging.getLogger('atlapi')

@inlineCallbacks
def videoDiffBase(stepCfg, ActivityResult):
    return_value = AD({"status": "success", "reason": None})
    sc = stepCfg
    try:
        ### load lsit of image frames from a video file.
        CSframes = yield deferToThread(
                    functools.partial(
                        load_frames,
                        stepCfg.filedata[stepCfg.filelist[0]]
                    )
                )
        frame_count = len(CSframes)

        for subject_name, subject in sc.subjects.items():
            if subject_name in return_value:
                raise MVError(
                    "Subject name {} already present in results".format(subject_name)
                )

            result = {"frames": frame_count, "activity": 0, "setup": 0}

            local_fs = subject.video_file_size_threshold if "video_file_size_threshold" in subject else file_size_threshold
            local_skip_pct = subject.overall_skip_pct if "overall_skip_pct" in subject else overall_skip_pct
            local_diff_pct = subject.bbox_diff_pct if "bbox_diff_pct" in subject else bbox_diff_pct
            local_pixel_diff = subject.pixel_diff_threshold if "pixel_diff_threshold" in subject else pixel_diff_threshold
            local_blur = subject.gaussian_blur if "gaussian_blur" in subject else None

            ### apply gaussian blur to remove noise
            if local_blur:
                CSframes = gaussian_blur_images(CSframes, local_blur)

            ### default crop regions for motion detection
            h, w, _ = CSframes[0].shape
            if "region_index" in subject:
                local_crops = subject.region_index
            else:
                local_crops = {
                    "left": [int(i*j) for i,j in zip(crop_regions["left"],[w,h,w,h])],
                    "center": [int(i*j) for i,j in zip(crop_regions["center"],[w,h,w,h])],
                    "right": [int(i*j) for i,j in zip(crop_regions["right"],[w,h,w,h])]
                }

            local_config = subject.sticker_config if "sticker_config" in subject else {"zone":[0,0,w,h]}

            ### measure video file size
            if len(stepCfg.filedata[stepCfg.filelist[0]]) > local_fs:
                ### image diff excute
                diffs_frame = 0
                setup_frame = 0
                curr_frame = CSframes[0]
                curr_regions = region_extract(local_crops, curr_frame)
                for i in range(1, len(CSframes)):
                    prev_frame = curr_frame
                    prev_regions = curr_regions
                    curr_frame = CSframes[i]
                    curr_regions = region_extract(local_crops, curr_frame)
                    region_votes = 0
                    if img_diff(prev_frame, curr_frame) < local_skip_pct:
                        for ri in range(len(local_crops)):
                            vote_result = img_diff(prev_regions[ri], curr_regions[ri], local_pixel_diff) > local_diff_pct
                            region_votes = region_votes + vote_result

                    if region_votes >= activity_threshold:
                        diffs_frame = ActivityResult(result, curr_frame, diffs_frame, local_config)
                    elif region_votes >= setup_threshold:
                        setup_frame += 1

                result["activity"] = int((diffs_frame + 1) / 2)
                result["setup"] = setup_frame

            return_value[subject_name] = result

        return_value.filedata = [
            (stepCfg.filelist[0], "bio", stepCfg.filedata[stepCfg.filelist[0]])
        ]

    except Exception as err:
        msg = "pressBreak exception: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value

def gaussian_blur_images(img_list, patch_size):
    """
    blur_rate example is 3,5,7....
    bigger blur_rate be more smooth on image
    """
    blur_imgs = []
    for i in range(len(img_list)):
        blur_imgs.append(cv2.GaussianBlur(img_list[i],(patch_size,patch_size),0))
    return blur_imgs

def region_extract(region_index, img):
    """
    region_list= {
                  'left':[x1,y1,w1,h1],
                  'center':[x2,y2,w2,h2],
                  'right':[x3,y3,w3,h3]
                  }
    """
    region_list = []
    try:
        # python3 dictionary iteration
        for _, value in region_index.items():
            region_list.append(value)
    except:
        # python2 dictionary iteration
        for _, value in region_index.iteritems():
            region_list.append(value)

    region_img = []
    for i in range(len(region_list)):
        ### crop image with sliding index method
        region_img.append(
            img[
                region_list[i][1] : region_list[i][1] + region_list[i][3],
                region_list[i][0] : region_list[i][0] + region_list[i][2],
            ]
        )

    return region_img

def img_diff(img_1, img_2, threshold = pixel_diff_threshold):
    diff_img = cv2.absdiff(img_1, img_2)
    diff_img[diff_img < threshold] = 0

    img_size = img_1.shape[0] * img_1.shape[1] * img_1.shape[2]
    ### unit is percentage
    return (np.count_nonzero(diff_img) / img_size) * 100
