from enum import Enum
from colorsys import rgb_to_hsv
import tempfile
import os
from typing import Tuple

import atlapi.video_handler as vh
from atlapi.attribute_dict import AD

import numpy as np
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue, Deferred

class AndonColor(Enum):
    OFF: str = 'off'
    RED: str = 'red'
    BLUE: str = 'blue'
    AMBER: str = 'amber'
    GREEN: str= 'green'
    WHITE: str = 'white'
    MAGENTA: str = 'magenta'

def hue_to_color_name(hue: float, is_white: bool = False) -> AndonColor:
    '''Expects hue a number in range [0,360].'''
    if (is_white):
        return AndonColor.WHITE

    if 0 <= hue < 27 or 325 <= hue <=360:
        return AndonColor.RED
    elif 27 <= hue < 80:
        return AndonColor.AMBER
    elif 80 <= hue < 175:
        return AndonColor.GREEN
    elif 175 <= hue < 270:
        return AndonColor.BLUE
    elif 270 <= hue < 325:
        return AndonColor.MAGENTA
    else:
        raise ValueError(f"hue={hue} is not a valid color")


def check_white(r: float, g: float, b: float) -> bool:
    '''Expects r,g,b in range [0, 1].'''
    __APOCHROMATIC = 0.753
    low_rgb = min(r,g,b)
    variance = (0.008 + (low_rgb - __APOCHROMATIC)) / 4
    return (low_rgb > __APOCHROMATIC) and (r - low_rgb < variance) \
        and (r - low_rgb < variance) and (b - low_rgb < variance)


def extract_frame_color(r, g, b, value_threshold: float) -> Tuple[str, float, float]:
    '''
    Extracts color from region of frame and returns color, hue, and value
    Expects r,g,b in range [0, 255]
    '''
    r = r / 255
    g = g / 255
    b = b / 255
    h, s, v = rgb_to_hsv(r, g, b)

    color = hue_to_color_name(360*h, check_white(r, g, b)) if v > value_threshold else AndonColor.OFF
    return color.value, 360*h, v

def extract_state(config: dict, hues: list, values: list, colors: list) -> dict:
    colors = np.array(colors)
    condensed_colors = np.unique(colors[colors != AndonColor.OFF.value])
    flashing = np.std(hues) > config['transition_threshold']

    result = {}

    if flashing:
        result = dict(state='flashing')
    else:
        result = dict(state='solid')

    if len(condensed_colors) == 0:
        result['state'] = AndonColor.OFF.value
    else:
        # NOTE: Support multiple colors rather than taking the first one
        result['color'] = condensed_colors[0]

    if config.get('color') is not None and flashing:
        result['color'] = config['color']
    elif len(condensed_colors) > 1:
        result['msg'] = f"Warning: multiple colors given {condensed_colors}"

    if config['return_data']:
        result['data'] = dict(
            hue = hues,
            value = values,
            color = colors.tolist()
        )

    return result

def extract_andon_state(step_cfg: dict) -> dict:
    '''Load the video and perform andon light extraction on it'''
    result = dict(
        collection_interval=step_cfg.get('collection_interval', 60),
        status='success',
    )

    video_data = step_cfg['filedata'][step_cfg['filelist'][0]]

    with vh.load_video(video_data) as video_file:
        if 'input_crop' in step_cfg and step_cfg['input_crop']:
            with tempfile.TemporaryDirectory() as tmp:
                output_path = os.path.join(tmp, 'cropped_video_file.mp4')
                vh.crop_video(video_file.name, output_path, step_cfg['input_crop'])

                with open(output_path, 'rb') as output:
                    result['cropped_video'] = output.read()

        for extraction in step_cfg['extract_andon_state']:
            try:
                frames, _ = vh.frame_metadata_extraction(video_file.name, **extraction)

                colors, hues, values = [], [], []
                for frame in frames:
                    color, hue, value = extract_frame_color(*frame.color, extraction['value_threshold'])
                    colors.append(color)
                    values.append(value)
                    hues.append(hue)

                result[extraction['name']] = extract_state(extraction, hues, values, colors)
            except Exception as e:
                result[extraction['name']] =  dict(
                    status='error',
                    reason=f'extract_andon_error: {extraction["name"]} - {e}'
                )

    return result


def andon_state(step_cfg: dict) -> dict:
    try:
        return extract_andon_state(step_cfg)
    except Exception as e:
        return dict(
            status='error',
            reason=f'andon_state error: {e}'
        )

def andon_state_compatibility(step_cfg: dict) -> dict:
    # expected parameters (mostly compatible with edge andonState):
    #   location: {x,y}
    #   radius: Radius of the detection area. Defaults to 10
    #   vThreshold: Value threshold for on vs off. Defaults to 0.73
    #   onOffRatio: ratio of off to on frames. Defaults to 0.9
    #   minFlashingTransitionCount: Minimum number of state changes to be considered flashing
    EXTRACTION_NAME = 'andon'

    # functions purely for compatibility with edge andon. These shouldn't be used for other contexts
    def __percent_to_pixels(percent: str, max: int):
        return (int(percent[:-1]) / 100) * max

    def __light_state(on, flashing=False):
        if flashing:
            return 'flashing'
        elif on:
            return 'solid'
        return 'off'

    def __return_state(first, transitions, on_count, off_count, min_transitions, on_off_ratio):
        if transitions > min_transitions:
            return __light_state(True, True)
        elif (on_count / (on_count + off_count)) > on_off_ratio:
            return __light_state(True)
        elif (off_count / (on_count + off_count)) > on_off_ratio:
            return __light_state(False)
        else:
            return __light_state(first)

    def __generate_cfg(name: str, cfg: dict ) -> dict:
        # defaults are taken from image collector
        vthreshold = cfg.get('vThreshold', 0.73)
        on_off_ratio = cfg.get('onOffRatio', 0.9)
        min_flashing_transition_count = cfg.get('minFlashingTransitionCount', 4)

        location = cfg.get('location', dict(x='50%', y='50%'))
        radius = cfg.get('radius', 10)
        x, y = location['x'], location['y']

        # TODO: support percentage in location with varying width/height determined from video
        # right now this would require extra calls out to ffmpeg and modification of rest of andon
        # all andon videos currently are produced in 320x240
        width, height = cfg.get('width', 320), step_cfg.get('height', 240)

        if isinstance(x, str):
            x = __percent_to_pixels(x, width)
        if isinstance(y, str):
            y = __percent_to_pixels(y, height)
        if isinstance(radius, str):
            radius = __percent_to_pixels(radius, min(width, height))

        # NOTE: this isnt 100% compatible since the videos have been annotated, color may be messed up
        region = [
            max(0, round(x-radius)),
            max(0, round(y-radius)),
            round(radius*2),
            round(radius*2)
        ]

        return dict(
                name=name,
                fps=0,
                region=region,
                transition_threshold=0.4,    # doesn't matter since we will generate our own transitions
                value_threshold=vthreshold,
                return_data=True,            # we use returned data to actually generate state, etc
                # used to pass along to later processing
                on_off_ratio=on_off_ratio,
                min_flashing_transition_count=min_flashing_transition_count
            )

    def __generate_record(step, output, subject):
        record = AD(step)
        record.bundle = list(record.bundle)
        record.bundle[-1] = f'{record.step_name}_{subject}'
        record.scid = ':'.join(record.bundle)
        record.step_name = f'{record.step_name}_{subject}'
        record.function = f'andonState'
        record.output = AD(output)
        return record

    if 'subjects' in step_cfg:
        step_cfg['extract_andon_state'] = [__generate_cfg(sname, scfg) for sname, scfg in step_cfg['subjects'].items()]
    else:
        step_cfg['extract_andon_state'] = [__generate_cfg(EXTRACTION_NAME, step_cfg)]

    # call to extraction function
    results = extract_andon_state(step_cfg)

    records = []

    # Iterate over the results
    for scfg in step_cfg['extract_andon_state']:
        sname = scfg['name']
        vthreshold = scfg['value_threshold']
        on_off_ratio = scfg['on_off_ratio']
        min_flashing_transition_count = scfg['min_flashing_transition_count']

        # pass errors encountered up through virtual steps
        if 'status' in results[sname] and results[sname]['status'] == 'error':
            records.append(__generate_record(step_cfg, results['sname'], sname))
            continue

        data = results[sname]['data']
        color = results[sname].get('color', None)
        value = np.array(data['value'])

        on = (value >= vthreshold)
        on_count = on.sum()
        off = (value < vthreshold)
        off_count = off.sum()

        # image_collector always counts one transition because it initializes last state to null
        # and checks if the last state == current state
        transitions = np.diff(on).sum() + 1
        last_color = 'on' if on[-1] else 'off'

        state = __return_state(
            on[0], transitions, on_count, off_count,
            min_flashing_transition_count, on_off_ratio
        )

        # not producing meanHsv because it isn't used by the reports and can't be reproduced exactly
        return_value = AD(dict(
            # meanHsv=dict(),
            collection_interval=step_cfg.get('collection_interval', 60),
            counts=dict(
                on= on_count,
                off= off_count,
                transitions=transitions,
                last=last_color
            ),
            state=state,
            status='success',
            reason=None
        ))

        # only output color if state is on; we don't actually use color for the report anyway
        if state != __light_state(False):
            return_value['color'] = color

        # pass video data through; this may not be essential but is needed for compatibility
        if 'cropped_video' in results:
            return_value['filedata'] = [
                (
                    step_cfg['filelist'][0][:-4] + "_annotated.mp4",
                    "bio",
                    results['cropped_video']
                )
            ]
        else:
            return_value['filedata'] = [
                (
                    step_cfg['filelist'][0][:-4] + "_annotated.mp4",
                    "bio",
                    step_cfg['filedata'][step_cfg['filelist'][0]],
                )
            ]

        

        records.append(__generate_record(step_cfg, return_value, sname))

    return records