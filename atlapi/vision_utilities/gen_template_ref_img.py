
import json
from typing import Optional
import sys

import boto3
import cv2

from atlapi.vision_utilities.moving_obj_locate import MovingObjLocationCfg, produce_ref_template_img
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue


@inlineCallback
def gen_template_ref_img(
        cfg_file: str, draw_local_result: Optional[bool]=False) -> None:
    with open(cfg_file, "r") as cfg_fp:
        moving_obj_loc_cfg_dict = json.load(cfg_fp)

    moving_obj_loc_cfg = MovingObjLocationCfg(**moving_obj_loc_cfg_dict)
    if not moving_obj_loc_cfg.ref_orig_s3_loc:
        raise ValueError("ref_orig_s3_loc is not defined.")
    print("ref_orig_s3_loc = {}".format(moving_obj_loc_cfg.ref_orig_s3_loc))

    session = boto3.Session(profile_name="prd")
    s3_client = session.client('s3')

    ref_template_img = yield produce_ref_template_img(moving_obj_loc_cfg, s3_client=s3_client)
    print("'ref_template_img' has been produced and stored at: {}".format(
        moving_obj_loc_cfg.ref_obj_s3_loc))

    if draw_local_result:
        cv2.imshow('ref_template_img', ref_template_img)
        cv2.waitKey(0)

@inlineCallbacks
def gen_template_ref_img_main() -> None:
    if len(sys.argv) < 2:
        print("Usage:")
        print("python -m atlapi.vision_utilities.gen_template_ref_img <path_to_cfg_file> [draw_local_result]")
        exit(1)
    cfg_file = sys.argv[1]
    if len(sys.argv) >= 3:
        draw_local_result_arg = sys.argv[2]
        draw_local_result = (draw_local_result_arg=='draw_local_result')
    else:
        draw_local_result = False
    yield gen_template_ref_img(cfg_file=cfg_file, draw_local_result=draw_local_result)


if __name__ == "__main__":
    gen_template_ref_img_main()

'''
def test_main():
    session = boto3.Session(profile_name="prd")
    s3_client = session.client('s3')

    orig_loc = str('file:///Users/alexyiu/img_sandbox/moving_wheels/'
                   '00007cf95506_video0_1555438606_default_first.jpg')
    """
    orig_loc = str('s3://atl-prd-schneiderelectric-rawdata/image_readings/'
                   'atloverhead08_schneiderelectric_prd/video0/default/2019/04/18/'
                   '20/00007cf95506_video0_1555617610_default_first.jpg')
    """
    ref_out_loc = str('s3://atl-prd-video-sandbox/alex_sand_box/'
                      'wheel_ref/schneiderelectric/'
                      'cam_00007cf95506_wheel_ref_1555438606.jpg')

    json_cfg_file = "cam_00007cf95506_video0_1555438606_moving_obj_cfg.json"

    with open(json_cfg_file, "r") as cfg_fp:
        moving_obj_loc_cfg_dict = json.load(cfg_fp)
    moving_obj_loc_cfg = MovingObjLocationCfg(**moving_obj_loc_cfg_dict)


    """
    ref_template_img = produce_ref_template_img(
        orig_loc, moving_obj_loc_cfg,
        output_img_loc=ref_out_loc,
        s3_client=s3_client)        
    cv2.imshow("orig_loc", open_img(orig_loc, s3_client))
    cv2.imshow("ref_template_img", ref_template_img)
    """

    test_base_dir = '/Users/alexyiu/img_sandbox/moving_wheels/'
    test_img_files = [
        "00007cf95506_video0_1555257860_default_first.jpg",
        "00007cf95506_video0_1555258520_default_first.jpg"
    ]
    """
    for test_img_file in test_img_files:
        test_img_path = os.path.join(test_base_dir, test_img_file)
        test_img = cv2.imread(test_img_path)
        match_template(test_img, moving_obj_loc_cfg, s3_client, draw_result=True)
        cv2.waitKey(0)
    """
'''
