
from atlapi.attribute_dict import AD
from atlapi.image.utilities import (
    Zone,
    color_space_zone,
    load_image,
    check_image,
    default_zones,
    Color_Range,
)
from atlapi.common import *
import logging
import numpy as np
import cv2
from skimage import measure
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')


def find_change_point(histogram, lowpass_threshold, flag):
    startPoint = histogram[0]
    peakStatus = 0
    peakIndex = []
    peakValue = []

    for i in range(len(histogram)):
        if flag == 0:
            if histogram[i] - startPoint < 0:
                peakStatus = 1
                if i == len(histogram) - 1:
                    peakIndex.append(i)
                    peakValue.append(histogram[i])
            else:
                if peakStatus == 1:
                    if startPoint > lowpass_threshold:
                        peakIndex.append(i - 1)
                        peakValue.append(startPoint)
                peakStatus = 0
        else:
            if histogram[i] - startPoint > 0:
                peakStatus = 1
                if i == len(histogram) - 1:
                    peakIndex.append(i)
                    peakValue.append(histogram[i])
            else:
                if peakStatus == 1:
                    if startPoint > lowpass_threshold:
                        peakIndex.append(i - 1)
                        peakValue.append(startPoint)
                peakStatus = 0

        startPoint = histogram[i]

    return peakIndex, peakValue


def shapedHistogramPeakFinder(histogram, flag_convex_concave, std_multiply):
    originPeak = []

    startPoint = 0
    changeSet = {"increase": 0, "decrease": 0}
    sol = {}

    if flag_convex_concave == 0:
        peakIndex, peakValue = find_change_point(histogram, 0, 0)
        if len(peakValue):
            thresholdChange = np.std(peakValue) * std_multiply
            peakPoint = [None, max(peakValue)]
            for i, v in enumerate(peakValue):
                if peakValue[i] - startPoint < 0:
                    changeSet["decrease"] += startPoint - peakValue[i]

                if changeSet["decrease"] > thresholdChange:
                    if peakValue[i] - startPoint > 0:
                        changeSet["increase"] += peakValue[i] - startPoint

                    if peakValue[i] <= peakPoint[1]:
                        peakPoint = [i, peakValue[i]]

                    if changeSet["increase"] > thresholdChange:
                        sol[peakPoint[0]] = peakPoint[1]
                        peakPoint = [None, max(peakValue)]
                        changeSet["increase"], changeSet["decrease"] = 0, 0

                if i == len(peakValue) - 1:
                    if peakValue[i] != 0 and peakPoint is not None:
                        sol[i] = peakValue[i]

                if peakValue[i] == 0:
                    if changeSet["decrease"] < thresholdChange:
                        changeSet["increase"], changeSet["decrease"] = 0, 0

                startPoint = peakValue[i]

            for key, value in sol.items():
                originPeak.append(peakIndex[key])

    else:
        peakIndex, peakValue = find_change_point(histogram, 0, 1)
        if len(peakValue):
            thresholdChange = np.std(peakValue) * std_multiply
            peakPoint = [None, 0]
            for i, v in enumerate(peakValue):
                if peakValue[i] - startPoint > 0:
                    changeSet["increase"] += peakValue[i] - startPoint

                if changeSet["increase"] > thresholdChange:
                    if peakValue[i] - startPoint < 0:
                        changeSet["decrease"] += startPoint - peakValue[i]

                    if peakValue[i] >= peakPoint[1]:
                        peakPoint = [i, peakValue[i]]

                    if changeSet["decrease"] > thresholdChange:
                        sol[peakPoint[0]] = peakPoint[1]
                        peakPoint = [None, 0]
                        changeSet["increase"], changeSet["decrease"] = 0, 0

                    if i == len(peakValue) - 1:
                        if peakPoint[0] is not None:
                            sol[peakPoint[0]] = peakPoint[1]

                if peakValue[i] == 0:
                    changeSet["increase"], changeSet["decrease"] = 0, 0

                startPoint = peakValue[i]

            for key, value in sol.items():
                originPeak.append(peakIndex[key])

    return originPeak


def colorDecision(image, colors):
    results = {}

    for cName, cRange in colors.items():
        results[cName] = 0
        minColor_range = np.array(cRange.min)
        maxColor_range = np.array(cRange.max)

        maskColor = cv2.inRange(image, minColor_range, maxColor_range)
        filteredColorImage = cv2.bitwise_and(image, image, mask=maskColor)

        colorValue = np.count_nonzero(filteredColorImage[:, :, 0])
        if colorValue:
            results[cName] = colorValue

    assert all([isinstance(v, int) for v in results.values()])
    maxColor = max(results, key=results.get)

    if results[maxColor] > colors[maxColor].threshold:
        result = maxColor
    else:
        result = "off"

    return result


def predictBulb(mask_img, crop_img, size, radius_range, colors):
    subjectMask = (mask_img > 0) * 1
    labels = measure.label(subjectMask, neighbors=8, background=0)
    predict = "off"
    center = []
    radius = 0

    for label in np.unique(labels):
        if label == 0:
            continue
        labelMask = np.zeros(subjectMask.shape, dtype="uint8")
        labelMask[labels == label] = 1
        numPixels = np.count_nonzero(labelMask)

        if numPixels > size[0] and numPixels < size[1]:
            predictImage = np.zeros((crop_img.shape), np.uint8)
            cnts, _ = cv2.findContours(
                labelMask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
            )
            (x, y), radius = cv2.minEnclosingCircle(cnts[0])
            radius = int(radius)
            center = (int(x), int(y))

            if radius < radius_range[0]:
                radius = radius_range[1]
            elif radius < radius_range[1]:
                radius = radius + 7
            else:
                radius = radius + 3

            x2, y2 = np.mgrid[0 : len(crop_img[:, :, 2][0]) : 1, 0 : len(crop_img[:, :, 2]) : 1]
            circleMask = ((x2 - center[0]) ** 2 + (y2 - center[1]) ** 2) <= (radius) ** 2
            trMask = circleMask.T

            predictImage[:, :, 0] = crop_img[:, :, 0] * trMask
            predictImage[:, :, 1] = crop_img[:, :, 1] * trMask
            predictImage[:, :, 2] = crop_img[:, :, 2] * trMask

            predict = colorDecision(predictImage, colors)

            if predict != "off":
                break
    return predict, center, radius


def defaultBulb(crop_img, center, radius, size, intensity, colors):
    defaultImage = np.zeros((crop_img.shape), np.uint8)
    x2, y2 = np.mgrid[0 : len(crop_img[:, :, 2][0]) : 1, 0 : len(crop_img[:, :, 2]) : 1]
    circleMask = ((x2 - center[0]) ** 2 + (y2 - center[1]) ** 2) <= (radius) ** 2
    trMask = circleMask.T

    defaultImage[:, :, 0] = crop_img[:, :, 0] * trMask
    defaultImage[:, :, 1] = crop_img[:, :, 1] * trMask
    defaultImage[:, :, 2] = crop_img[:, :, 2] * trMask

    histogram, bins = np.histogram(defaultImage[:, :, 2].ravel(), 256, [0, 256])
    histogram = histogram[intensity:256]
    intensePixelSum = sum(histogram)

    if intensePixelSum > size:
        predict = colorDecision(defaultImage, colors)
    else:
        predict = "off"

    return predict

def bulbDetection(stepCfg):
    sc = stepCfg
    return_value = AD({"status": "success", "reason": None})

    try:
        maxRange = np.array([256, 256, 256])
        color_space_image = load_image(
                                 stepCfg.filedata[stepCfg.filelist[0]],
                                 color_space="bgr",
                                 crop=sc.input_crop)
        check_image(color_space_image, sc, return_value)
        for subject_name, subject in sc.subjects.items():
            if subject_name in return_value:
                raise MVError("Subject name {} already present in results".format(subject_name))
            return_value[subject_name] = "off"
            color_space = Color_Range.create("hsv", "hsv", subject.colors)
            if "zone" not in subject or not subject.zone:
                subject.zone = default_zones(color_space_image).image
            else:
                subject.zone = Zone.create(subject_name, subject.zone)

            zonecolor_space_image = color_space_zone(color_space_image, "bgr", "hsv", subject.zone)

            # histogram for intensity detection
            histogram, _ = np.histogram(zonecolor_space_image[:, :, 2].ravel(), 256, [0, 256])
            bulbHistogram = histogram[subject.intensity : 256]

            bulbIntensityResult = shapedHistogramPeakFinder(bulbHistogram, 1, 1)

            if len(bulbIntensityResult) and min(bulbIntensityResult) > 0:
                bulbHistogramIntensity = min(bulbIntensityResult) + subject.intensity
                minBulbIntensityRange = np.array([0, 0, bulbHistogramIntensity])
                bulbMaskIntensity = cv2.inRange(zonecolor_space_image, minBulbIntensityRange, maxRange)
                filteredBulbIntensity = cv2.bitwise_and(
                    zonecolor_space_image, zonecolor_space_image, mask=bulbMaskIntensity
                )
                return_value[subject_name], center, radius = predictBulb(
                    filteredBulbIntensity[:, :, 2],
                    zonecolor_space_image,
                    subject.size,
                    subject.radius_range,
                    color_space,
                )

                if return_value[subject_name] == "off":
                    kernel = np.ones((2, 2), np.uint8)
                    filteredBulbIntensity[:, :, 2] = cv2.erode(
                        filteredBulbIntensity[:, :, 2], kernel, iterations=3
                    )
                    filteredBulbIntensity[:, :, 2] = cv2.dilate(
                        filteredBulbIntensity[:, :, 2], kernel, iterations=2
                    )

                    return_value[subject_name], center, radius = predictBulb(
                        filteredBulbIntensity[:, :, 2],
                        zonecolor_space_image,
                        subject.size,
                        subject.radius_range,
                        color_space,
                    )
                if return_value[subject_name] != "off":
                    subject["history"]["center"] = center
                    subject["history"]["radius"] = radius
            else:
                if (
                    subject["history"]["center"] is not None
                    and len(subject["history"]["center"]) == 2
                ):
                    return_value[subject_name] = defaultBulb(
                        zonecolor_space_image,
                        subject.history.center,
                        subject.history.radius + 2,
                        subject.size[0],
                        subject.intensity,
                        color_space,
                    )
    except Exception as err:
        msg = "bulbDetection: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value
