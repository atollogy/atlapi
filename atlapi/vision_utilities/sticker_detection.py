from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *
import functools
import logging
import numpy as np
import cv2
import math
from skimage import measure
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')

def stickerDetection(stepCfg):
    return_value = AD({"status": "success", "reason": None})
    sc = stepCfg

    try:
        color_space_image = load_image(stepCfg.filedata[stepCfg.filelist[0]], color_space=sc.color_space)
        check_image(color_space_image, sc, return_value)
        colorSearchOrder = ["green", "blue", "yellow", "red"]
        for subject_name, subject in sc.subjects.items():
            if subject_name in return_value:
                raise MVError("Subject name {} already present in results".format(subject_name))

            if "zone" not in subject or not subject.zone:
                subject.zone = default_zones(color_space_image, atype="subject").image
            else:
                subject.zone = Zone.create(subject_name, subject.zone, atype="subject")
            zonecolor_space_image = color_space_zone(
                color_space_image, sc.color_space, subject.color_space, subject.zone
            )

            stickerRegion = np.zeros((zonecolor_space_image.shape[:2]), np.int64) + 1
            for colorName in colorSearchOrder:
                colorInf = subject.stickerPatches[colorName]

                labels = measure.label(
                    makeMinMaxHSV(colorInf, zonecolor_space_image), connectivity=1, background=0
                )
                pixelMask = np.zeros((zonecolor_space_image.shape[:2]), np.int64)

                for label in np.unique(labels):
                    if label == 0:
                        continue
                    labelMask = np.zeros(pixelMask.shape, dtype="uint8")
                    labelMask[labels == label] = 1
                    numPixels = np.count_nonzero(labelMask)

                    if numPixels > colorInf.sizeRange[0] and numPixels < colorInf.sizeRange[1]:

                        cnts, _ = cv2.findContours(
                            labelMask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
                        )
                        (x, y), radius = cv2.minEnclosingCircle(cnts[0])
                        center = (int(x), int(y))

                        radius = int(radius) + subject.addingBorderRadius

                        xCircle, yCircle = np.mgrid[
                            0 : len(zonecolor_space_image[0]) : 1, 0 : len(zonecolor_space_image) : 1
                        ]
                        circleMask = ((xCircle - center[0]) ** 2 + (yCircle - center[1]) ** 2) <= (
                            radius
                        ) ** 2
                        searchMask = circleMask.T * 1

                        pixelMask += searchMask

                boolMask = pixelMask > 0
                stickerRegion = stickerRegion * boolMask

                zonecolor_space_image[:, :, 0] = zonecolor_space_image[:, :, 0] * stickerRegion
                zonecolor_space_image[:, :, 1] = zonecolor_space_image[:, :, 1] * stickerRegion
                zonecolor_space_image[:, :, 2] = zonecolor_space_image[:, :, 2] * stickerRegion

                if colorName == "yellow" or colorName == "red":
                    stickerList = circularRegions(stickerRegion, subject.circularityThreshold)
                    if len(stickerList) <= 2:
                        break

            if len(stickerList) >= 2:
                result_text = "closed"
            else:
                result_text = "open"

            # add annotation function
            subject.zone.label = result_text
            annotated_image = annotate(color_space_image, [subject.zone], color_space=sc.color_space)
            return_value[subject_name] = result_text
            return_value.filedata = [
                (stepCfg.filelist[0][:-4] + "_annotated.jpg", "bio", annotated_image)
            ]

    except Exception as err:
        msg = "stickerDetection exception: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value


def circularRegions(stickerRegion, detectionCircularity):
    labelsDistance = measure.label(stickerRegion, connectivity=2, background=0)
    distanceMask = np.zeros((stickerRegion.shape[:2]), np.int64)

    centerList = []

    for labelEstimate in np.unique(labelsDistance):
        if labelEstimate == 0:
            continue
        Eachlabel = np.zeros(labelsDistance.shape, dtype="uint8")
        Eachlabel[labelsDistance == labelEstimate] = 1

        cntsD, _ = cv2.findContours(
            Eachlabel.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )
        (xD, yD), radius = cv2.minEnclosingCircle(cntsD[0])
        centerD = [int(xD), int(yD)]

        distanceList = []

        for i in range(len(cntsD[0])):
            dist = math.hypot(centerD[0] - cntsD[0][i][0][0], centerD[1] - cntsD[0][i][0][1])
            distanceList.append(dist)

        if max(distanceList) != 0:
            if (min(distanceList) / max(distanceList)) > detectionCircularity:
                distanceMask += Eachlabel
                centerList.append(centerD)

    return centerList


def transformHueRange(value, diameter, label):
    minRange = None
    minRange2 = None
    maxRange = None
    maxRange2 = None

    if label == "h":
        value = int(value / 2)
        radius = int(diameter / 4)
        limit = 180
    else:
        radius = int(diameter / 2)
        limit = 256

    minRange = value - radius
    if minRange < 0:
        minRange2 = limit + minRange
        minRange = 0
        maxRange2 = limit

    maxRange = value + radius
    if maxRange > limit:
        maxRange2 = maxRange - limit
        maxRange = limit
        minRange2 = 0

    return minRange, maxRange, minRange2, maxRange2


def copyArray(ToCopy, FromCopy):
    for i in range(len(ToCopy)):
        if ToCopy[i] is None:
            ToCopy[i] = FromCopy[i]
    return ToCopy


def makeMinMaxHSV(colorInf, zonecolor_space_image):
    minArray = []
    maxArray = []
    minArray2 = []
    maxArray2 = []

    min1, max1, min2, max2 = transformHueRange(colorInf.hueValue, colorInf.hueDiameter, "h")
    minArray.append(min1)
    maxArray.append(max1)
    minArray2.append(min2)
    maxArray2.append(max2)
    min1, max1, min2, max2 = transformHueRange(
        colorInf.saturationValue, colorInf.saturationDiameter, "s"
    )
    minArray.append(min1)
    maxArray.append(max1)
    minArray2.append(min2)
    maxArray2.append(max2)
    min1, max1, min2, max2 = transformHueRange(
        colorInf.intensityValue, colorInf.intensityDiameter, "v"
    )
    minArray.append(min1)
    maxArray.append(max1)
    minArray2.append(min2)
    maxArray2.append(max2)

    minArray2 = copyArray(minArray2, minArray)
    maxArray2 = copyArray(maxArray2, maxArray)

    overFlag = 0
    for i in minArray2:
        if i is not None:
            overFlag = 1

    minColor_range = np.array(minArray)
    maxColor_range = np.array(maxArray)

    imageMask = cv2.inRange(zonecolor_space_image, minColor_range, maxColor_range)
    maskConvert = (imageMask > 0) * 1

    if overFlag == 1:
        minColor_range2 = np.array(minArray2)
        maxColor_range2 = np.array(maxArray2)

        imageMask2 = cv2.inRange(zonecolor_space_image, minColor_range2, maxColor_range2)
        maskConvert2 = (imageMask2 > 0) * 1
        maskConvert = maskConvert + maskConvert2

    return maskConvert
