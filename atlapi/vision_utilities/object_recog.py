#!/usr/bin/env python3.6

from atlapi.attribute_dict import AD
from atlapi.core import Core
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.queuing import ATLEvent

import base64
import logging
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')

FOUR_HOURS = 4*60*60

class ObjRecog(Core):
    def __init__(self, use_rest=False, late_window=FOUR_HOURS):
        Core.__init__(self, 'func_obj_recog', late_window=late_window)
        self.SQS = self.svcs.aws_sqs
        self.use_rest = use_rest
        if late_window is None:
            logger.info(f"LATE_QUEUE IS DISABLED")

    def is_late(self, stepCfg):
        try:
            if self.LATE_WINDOW is None:
                return False
            creation_time = to_datetime(stepCfg.collection_time).timestamp()
            if int(time.time()) - creation_time > self.LATE_WINDOW:
                return True
            else:
                return False
        except ValueError as err:
            logger.exception("objRecog collection is late check exception: {} converting {}".format(err, stepCfg.collection_time))
            return False

    @inlineCallbacks
    def objRecog(self, stepCfg):
        return_value = AD({"status": "success", "reason": None})

        try:
            sc = stepCfg
            if sc.function not in self.SQS.queues:
                raise APIError("Function {} not known to object recognition".format(sc.function))

            hdrs = {
                "Content-Type": "application/json",
                "gateway_id": sc.gateway_id,
                "nodename": sc.nodename,
                "customer_id": sc.customer_id,
            }

            for k in ["filedata", "filelist", "fData", "input_image_indexes"]:
                del stepCfg[k]

            # sending images encoded in json requires images to first be base64 encodied,
            # then converted to a unicode string to be serialized into json.
            # On the other end, the data coming from the json as a string must be re-converted
            # into bytes before being base64.decoded back into its original format
            # 1) img_int8_bytes -> base64.encode(img_int8_bytes) -> image_b64_bytes
            # 2) image_b64_bytes -> decode_to(utf-8) -> image_unicode_str
            # 3) image_unicode_str -> payload_dict( key: image_unicode_str)
            # 4) json.dumps(payload_dict) -> json_unicode_str
            # 5) send to other host
            # 6) json_unicode_str -> json.parse() -> payload_dict
            # 7) payload_dict[key]=image_unicode_str -> encode_to(bytes)-> image_b64_bytes
            # 8) image_b64_bytes -> base64.decode(image_b64_bytes) = img_int8_bytes

            queued = False

            data = {
                "cid": sc.cid,
                "customer_id": sc.customer_id,
                "headers": hdrs,
                "args": {},
                "records": [sc],
            }

            # add late check to decide queue to deliver to
            if self.is_late(stepCfg):
                resp = yield self.SQS.queues[sc.function + '_late'].put_msg(ATLEvent(data=data))
            else:
                resp = yield self.SQS.queues[sc.function].put_msg(ATLEvent(data=data))

            queued = resp.get("MessageId")

            if queued:
                return_value.status = "queued"
            else:
                return_value.status = "error"
                return_value.reason = f"ERROR objRecog enqueue error: {stepCfg.scid}"
                logger.error(return_value.reason)

        except Exception as err:
            return_value.status = "error"
            return_value.reason = f"objRecog exception: {stepCfg.scid}".upper() + repr(err)
            logger.exception(return_value.reason)
        return return_value
