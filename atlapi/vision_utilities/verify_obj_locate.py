
import boto3
import cv2
import functools
import json
import sys
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

from atlapi.vision_utilities.moving_obj_locate \
    import MovingObjLocationCfg, open_img, match_loaded_template


def _curtime_epoch_milli() -> int:
    return int(round(time.time() * 1000))


def _verify_obj_locate_main(cfg_file: str, s3_links_file: str) -> None:
    pass
#     with open(cfg_file, "r") as cfg_fp:
#         moving_obj_loc_cfg_dict = json.load(cfg_fp)
#
#     location_cfg = MovingObjLocationCfg(**moving_obj_loc_cfg_dict)
#
#     with open(s3_links_file, "r") as f:
#         content = f.readlines()
#     content = [x.strip() for x in content]
#
#     for s3_link in content:
#         print("s3_link={}".format(s3_link))
#         t1 = _curtime_epoch_milli()
#         test_img = open_img(s3_link, s3_client=s3_client)
#         t2 = _curtime_epoch_milli()
#         print("s3 image fetching time={}".format((t2-t1)))
#         ref_img = open_img(location_cfg.ref_obj_s3_loc, conv_gray=True, s3_client=s3_client)
#         # cv2.imshow(s3_link, test_img)
#         t1 = _curtime_epoch_milli()
#         result = match_loaded_template(test_img, ref_img, location_cfg, draw_result=True)
#         t2 = _curtime_epoch_milli()
#         print("match_template time={}".format((t2 - t1)))
#         print("result={}".format(result))
#         t2 = _curtime_epoch_milli()
#         cv2.waitKey(0)
#
#
# if __name__ == "__main__":
#     if len(sys.argv) < 3:
#         raise ValueError("Usage: python -m atlapi.vision_utilities.verify_obj_locate ./sample-cfg.json")
#     _verify_obj_locate_main(sys.argv[1], sys.argv[2])
