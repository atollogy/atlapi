
from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.vision_utilities.anchor_detection import AnchorDetectionV1

import base64
import cv2
import functools
import logging
# from pyzbar.pyzbar import decode
import re
from typing import Optional, List, Dict, Tuple
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')

_BEACON_ID = "beacon_id"
_POST_PREFIX_BEACON_REGEX = r"\w{4}#[a-fA-F0-9]{12}#[a-fA-F0-9]{20}"

# Examples:
# full beacon id = "ATLtest#k24z#395642764e64#17826da6bc5b71e0893e"
# post prefix beacon id = "k24z#395642764e64#17826da6bc5b71e0893e"

class OCR(Core):
    def __init__(self):
        Core.__init__(self, 'func_ocr')

    @inlineCallbacks
    def ocr(self, stepCfg):
        """OCR using google vision API"""

        def ocr_error(msg, exceptionType=False):
            if exceptionType:
                logger.exception(msg)
            else:
                logger.error(msg)
            return_value.status = "error"
            return_value.reason = msg

        return_value = AD({"status": "success", "fields_found": [], "reason": None})
        try:
            sc = stepCfg
            color_space_image = load_image(
                stepCfg.filedata[stepCfg.filelist[0]],
                color_space=sc.color_space,
                crop=sc.input_crop
            )
            check_image(color_space_image, sc, return_value)

            weight_thresholds = AD({
                'low': 200,
                'high': 150000
            })
            if 'weight_thresholds' in sc:
                if 'low' in sc.weight_thresholds:
                    weight_thresholds.low = sc.weight_thresholds.low
                if 'high' in sc.weight_thresholds:
                    weight_thresholds.high = sc.weight_thresholds.high

            # try:
            #     res = yield self.svcs.atlapi.vision.anchor_detection_v1.run_anchor_detection(sc, return_value) # check camera movement by anchor
            # except Exception as err:
            #     logger.warning(f'ocr anchor_detection error {err}')

            ocr_type = "DOCUMENT_TEXT_DETECTION"
            if "ocr_type" in sc and sc.ocr_type == "TEXT_DETECTION":
                ocr_type = "TEXT_DETECTION"
            req = {
                "requests": [
                    {
                        "image": {
                            "content": pack_image(color_space_image, b64=True, color_space=sc.color_space)
                        },
                        "imageContext": {"languageHints": ["en"]},
                        "features": [{"type": "{}".format(ocr_type)}],
                    }
                ]
            }

            respData = yield self._post_external_api(url=self.tree.endpoints.ocr.url, payload=req)

            if "api_error" not in respData:
                if 'responses' not in respData:
                    return_value.status = 'error'
                    return return_value

                # logger.debug("ocr response data: {}".format(respData))

                captured_text = [
                    respDict["textAnnotations"]
                    for respDict in respData["responses"]
                    if "textAnnotations" in respDict
                ]
                if (
                    len(captured_text)
                    and captured_text[0][0]
                    and captured_text[0][0]["description"]
                ):
                    return_value["fullResponse"] = captured_text[0][0]["description"]
                    chunks = captured_text[0][0]["description"].splitlines()

                    excludebox = []
                    found = False
                    for subject_name, subject in sc.subjects.items():
                        return_value[subject_name] = {}
                        # TODO: turn this prefix matching into a fuzzy match
                        if "prefixes" in subject:
                            found = return_value[subject_name] = self.find_by_prefix(subject["prefixes"], chunks)
                            return_value.fields_found.append(subject_name)
                            excludebox = self.find_bounding_box(
                                subject["prefixes"], captured_text[0], excludebox
                            )
                        elif "postfixes" in subject:
                            found = return_value[subject_name] = self.find_by_postfix(subject["postfixes"], chunks)
                            return_value.fields_found.append(subject_name)
                            excludebox = self.find_bounding_box(
                                subject["postfixes"], captured_text[0], excludebox
                            )

                        if len(excludebox) == 4:
                            excludebox = [
                                excludebox[0],
                                excludebox[1],
                                excludebox[2] * 8,
                                excludebox[3] * 2,
                            ]
                        else:
                            excludebox = [0, 0, 0, 0]

                        if not found and "values" in subject:
                            states_detected = self.detect_state(subject["values"], chunks)
                            if states_detected:
                                check_states = self.check_excludebox(
                                    excludebox, states_detected, captured_text[0]
                                )
                                if len(check_states) > 0:
                                    return_value[subject_name] = self.find_top_state(
                                        captured_text[0][1:], check_states
                                    )
                                    return_value.fields_found.append(subject_name)
                        elif not found and "match" in subject:
                            compiled_expression = re.compile(subject["match"])
                            matches, pos = self.find_by_match(compiled_expression, chunks)

                            if matches:
                                return_value[subject_name] = matches
                                return_value.fields_found.append(subject_name)

                            if ("position" in subject) and (subject["position"] != pos):
                                element = self.find_by_position(subject["position"], chunks, compiled_expression)
                                if element and not compiled_expression.match(element):
                                    return_value[subject_name] = element
                                    return_value.fields_found.append(subject_name)
                                else:
                                    return_value.warning = "Match found but not at specified position"

                        elif not found and "position" in subject:
                            element = self.find_by_position(subject["position"], chunks)
                            if element:
                                return_value[subject_name] = element
                                return_value.fields_found.append(subject_name)


                    if 'qrcode_scanner' in sc and sc.qrcode_scanner:
                        qrcode_prefix = sc.qrcode_prefix if "qrcode_prefix" in sc else None
                        # an example of qrcode_prefix is "ATLTEST#"
                        # where "#" is a delimiter used in bluetooth beacon ID
                        qrcode = self.scan_qrcode(color_space_image, qrcode_prefix)
                        # Use Google OCR response as a fallback to qrcode_scanner
                        if not qrcode and qrcode_prefix:
                            qrcode = self.locate_qrcode_from_ocr(chunks, qrcode_prefix)

                        if qrcode:
                            return_value[_BEACON_ID] = qrcode

                    if len(chunks) == 1 and chunks[0].isdigit():
                        # Assuming is to be the scale reader
                        # as there are only digits in the output
                        # And assigning the numbers to 'weight'
                        return_value["weight"] = int(chunks[0])
                        self.curate_weights(return_value, weight_thresholds)
            else:
                ocr_error("ocr error: {}".format(respData["api_error"]))

            return_value.filedata = [
                (stepCfg.filelist[0][:-4] + "_sourceimg.jpg", "bio", stepCfg.filedata[stepCfg.filelist[0]])
            ]

        except Exception as err:
            ocr_error("ocr exception: {} {}".format(err, stepCfg), True)

        return return_value

    def curate_weights(self, return_value, weight_thresholds):
        if return_value.weight < weight_thresholds.low:
            logger.debug("Weight lower than the threshold: {} < {}..assigning 0 weight".format(return_value.weight, weight_thresholds.low))
            return_value.weight = 0
        elif return_value.weight > weight_thresholds.high:
            logger.debug("Weight higher than the threshold: {} < {}..deleting the weight".format(return_value.weight, weight_thresholds.high))
            del return_value.weight

    def find_by_prefix(self, prefix_list, chunks):
        for item in chunks:
            for prefix in prefix_list:
                if item.startswith(prefix):
                    return item.split(prefix, 1)[1].strip()

    def find_by_postfix(self, postfix_list, chunks):
        for item in chunks:
            for postfix in postfix_list:
                if item.endswith(postfix):
                    result = item.split(postfix, 1)[0].strip()
                    if " " in result:
                        result = result.split(" ")[-1].strip()

                    sign_check = result.replace("-", "")
                    if sign_check.isnumeric():
                        return result
                    else:
                        return "Fail to detect number"

        return "Fail to detect number"

    def detect_state(self, card_state_list, chunks):
        state_list = []
        for item in chunks:
            for state in card_state_list:
                if state in item:
                    state_list.append(state)
        return state_list

    def find_by_match(self, match_regex, chunks):
        # NOTE: This will not work across newlines because of how chunk is divided
        # returns the first match and its position
        for pos, item in enumerate(chunks):
            match = match_regex.match(item)
            if match:
                return match.group(), pos

        return None, 0

    def find_by_position(self, position, chunks, match_regex=None):
        if position < len(chunks):
            if match_regex:
                match = match_regex.match(chunks[position])
                if match:
                    return match.group()
            else:
                return chunks[position]

        return None

    def find_top_state(self, annotations, states_arr):
        if len(states_arr) == 1:
            return states_arr[0]

        state_y = {}
        for v in annotations[1:]:
            if any(v["description"] in s for s in states_arr) and v["description"] != "In":
                values = min([coordinate["y"] for coordinate in v["boundingPoly"]["vertices"]])
                key_state = v["description"]
                state_y[key_state] = values
        dict_state = {}
        for key, value in state_y.items():
            matching = [s for s in states_arr if key in s]
            dict_state[matching[0]] = value

        sorted_state = sorted(dict_state, key=lambda x: dict_state[x])
        return sorted_state[0]

    def find_bounding_box(self, prefix_list, captured_text, excludebox):
        if excludebox == []:
            x_list = []
            y_list = []
        else:
            x_list = [excludebox[0], excludebox[0] + excludebox[2]]
            y_list = [excludebox[1], excludebox[1] + excludebox[3]]

        for i in range(len(captured_text)):
            if captured_text[i]["description"] in prefix_list:
                for coordinate in captured_text[i]["boundingPoly"]["vertices"]:
                    x_list.append(coordinate["x"])
                    y_list.append(coordinate["y"])

        if len(x_list) == 0 or len(y_list) == 0:
            result = []
        else:
            min_x = min(x_list)
            max_x = max(x_list)
            min_y = min(y_list)
            max_y = max(y_list)
            result = [min_x, min_y, max_x - min_x, max_y - min_y]

        return result

    def check_excludebox(self, excludebox, states_detected, captured_text):
        result = []
        exclude_list = []

        for i in range(len(captured_text)):
            if captured_text[i]["description"] in states_detected:
                for j in range(4):
                    if (
                        captured_text[i]["boundingPoly"]["vertices"][j]["x"] > excludebox[0]
                        and captured_text[i]["boundingPoly"]["vertices"][j]["x"]
                        < excludebox[0] + excludebox[2]
                    ) and (
                        captured_text[i]["boundingPoly"]["vertices"][j]["y"] > excludebox[1]
                        and captured_text[i]["boundingPoly"]["vertices"][j]["y"]
                        < excludebox[1] + excludebox[3]
                    ):

                        exclude_list.append(captured_text[i]["description"])

        for state in states_detected:
            if state not in exclude_list:
                result.append(state)

        return result

    def scan_qrcode(self, color_space_image, qrcode_prefix=None):
        return None
        # qr_scan_results = decode(color_space_image)
        # if qr_scan_results and len(qr_scan_results[0]) > 0:
        #     for qr_scan_result in qr_scan_results:
        #         qrcode = str(qr_scan_result[0].decode("utf-8"))
        #         if qrcode_prefix:
        #             if qrcode.startswith(qrcode_prefix):
        #                 post_prefix_qrcode = qrcode[len(qrcode_prefix):]
        #                 pattern = re.compile(_POST_PREFIX_BEACON_REGEX)
        #                 match_obj = pattern.match(post_prefix_qrcode)
        #                 if match_obj:
        #                     return qrcode
        #                 else:
        #                     logger.info("ocr.py: post prefix qrcode scanning result '%s' does not match regular expression: '%s' ",
        #                                 post_prefix_qrcode, _POST_PREFIX_BEACON_REGEX)
        #                     return None
        #             else:
        #                 logger.info("ocr.py: qrcode scanning result '%s' does not match qrcode_prefix: '%s' ", qrcode,
        #                             qrcode_prefix)
        #                 return None
        #         else:
        #             logger.info("ocr.py: 'qrcode_prefix' is not passed in, returning qrcode: '%s'", qrcode)
        #             return qrcode

    def locate_qrcode_from_ocr(self, chunks: List[str], qrcode_prefix: str) -> Optional[str]:
        chunks_len = len(chunks)
        _MAX_NUM_CHUNKS_FOR_RE = 5
        for i in range(0, chunks_len):
            if chunks[i].startswith(qrcode_prefix):
                pattern = re.compile(_POST_PREFIX_BEACON_REGEX)
                chunk_range_limit = min(chunks_len, i+_MAX_NUM_CHUNKS_FOR_RE)
                tmp_str = ""
                for j in range(i, chunk_range_limit):
                    tmp_str = tmp_str + chunks[j]
                tmp_str = tmp_str[len(qrcode_prefix):]
                match_obj = pattern.match(tmp_str)
                if match_obj:
                    return qrcode_prefix+match_obj.group()
                else:
                    return None
        return None
