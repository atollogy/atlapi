from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *

try:
    # handle different versions of scikit-image
    from skimage.metrics import structural_similarity
except:
    from skimage.measure import compare_ssim as structural_similarity

import cv2
import logging
import numpy as np
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')

_TOO_DARK = 'Too Dark'
_TOO_BRIGHT = 'Too Bright'


class ImageSimilarity(Core):
    def __init__(self):
        Core.__init__(self, 'func_image_similariity')
        self.AWS = self.svcs.aws

    def mse(self, a: np.array, b: np.array) -> float:
        err = np.sum((a.astype("float") - b.astype("float")) ** 2)
        err /= float(a.shape[0] * a.shape[1])
        return err

    def mse_to_sim(self, mse: float, shape) -> float:
        entries = shape[0] * shape[1]

        if len(shape) >= 3:
            entries *= shape[2]

        max_mse = ((255**2)*entries) / (shape[0]*shape[1])

        # NOTE: This won't be normally distributed and may have low variation
        return 1 - (mse/max_mse)

    def ssim(self, a: np.array, b: np.array) -> float:
        # if passed a color image, set the multichannel flag
        multichannel = len(a.shape) > 2 and a.shape[2] == 3
        res = structural_similarity(a, b, multichannel=multichannel)
        return res

    @inlineCallbacks
    def load_patch(self, nodename: str, subject: AD, key: str, image_path: str, input_crop: Zone, color_space: str) -> np.ndarray:
        patch = None
        if self.AWS.S3.has_state(nodename, key):
            try:
                patch = yield self.AWS.S3.get_image_state(nodename, key)
            except Exception as err:
                patch = None
                logger.exception(f"image_similarity failed to get patch: {nodename}, {key}")

        if not isinstance(patch, np.ndarray) or patch.shape[:2] != (subject.zone.w, subject.zone.h):
            try:
                #logger.info(f'loading of original image to create patch: {image_path} - {zone}')
                image = yield self.AWS.S3.get_image(image_path, color_space=color_space, crop=input_crop, cacheable=True)
                if 'equalize' in subject and subject.equalize:
                    image = equalize_image(image, color_space)

                patch = image[subject.zone.y_slice, subject.zone.x_slice]
                res = yield self.AWS.S3.set_image_state(nodename, key, patch)
                # logger.info(f"ImageSimilarityload_patch aws.s3.set_image_state result: " + str(res))
                #logger.info(f'Loaded patch: {subject.image_path}')
            except Exception as err:
                logger.exception(f"loading of original image to create path returned None: {image_path}: " + repr(err))
                raise
        return patch

    def autocrop_patches(self, subject: AD, image: np.ndarray, patch: np.ndarray) -> (np.ndarray, np.ndarray):
        if 'autocrop_ratio_threshold' not in subject:
            subject.autocrop_ratio_threshold = 0.6  # allows 4x3 -> 3x4

        if not isinstance(subject.autocrop, (AD, dict)):
            subject.autocrop = AD()
        if 'low' not in subject.autocrop:
            subject.autocrop.low = int(image[0:10, 0:10].mean())

        _patch = boundary_auto_crop(patch, **subject.autocrop)
        _image = boundary_auto_crop(image, **subject.autocrop)

        if _patch.shape != _image.shape:
            px, py = _patch.shape[:2]
            cx, cy = _image.shape[:2]

            if py == 0 or cy == 0:
                return image, patch

            # check if same ratio
            if abs((px/py) - (cx/cy)) > subject.autocrop_ratio_threshold:
                # Handle completely different ratios
                # This likely occurs when we have only a partial sticker detection
                # Default to original patches
                return image, patch

            patch = resize(_patch, px, py)
            image = resize(_image, px, py)
        else:
            patch, image = _patch, _image

        return image, patch

    @inlineCallbacks
    def image_similarity(self, step_cfg: AD) -> AD:
        '''
        {
            "color_space": ...
            "input_crop": ...
            "return_patches": bool
            "subjects": {
                "<subject_name>": {
                    "blur_radius": int
                    "equalize": bool
                    "autocrop": bool
                    "autocrop_ratio_threshold": float
                    "aggregate": str [max, min, avg]
                    "ssim_ratio": float [0,1]
                    "mse_ratio": float [0,1-ssim_ratio]
                    "threshold": float [0,1]
                    "zone": Zone (x, y, x. y)
                    "image_path": <s3 path>
                }
            }
        }
        '''
        try:
            return_value = AD({
                'status': 'success',
                'filedata': [],
                'reason': None,
            })


            color_space = step_cfg.color_space if 'color_space' in step_cfg else 'bgr'
            anonymize = step_cfg.anonymize if 'anonymize' in step_cfg else False

            boxes = [] # list to store boxes for annotation
            patch_filedata = []

            image = load_image(
                step_cfg.filedata[step_cfg.filelist[0]],
                color_space=color_space,
                crop=step_cfg.input_crop,
            )
            check_image(image, step_cfg, return_value)

            for subject_name, subject in step_cfg.subjects.items():
                try:
                    subject = AD(subject)
                    #logger.info(f'image_similarity step_cfg: {step_cfg}')
                    if 'threshold' not in subject:
                        subject.threshold = 0.65

                    if 'ssim_ratio' not in subject:
                        subject.ssim_ratio = 0.5

                    if 'mse_ration' not in subject:
                        subject.mse_ratio = 1 - subject.ssim_ratio
                        subject.mse_ration = 0 if subject.mse_ratio < 0 else subject.mse_ratio

                    if "zone" not in subject or not subject.zone:
                        subject.zone = default_zones(image).image
                    else:
                        subject.zone = Zone.create(subject_name, subject.zone, atype="subject")

                    if not isinstance(subject.image_path, list):
                        subject.image_path = [subject.image_path]

                    if 'equalize' in subject and subject.equalize:
                        image = equalize_image(image, color_space)

                    data = AD({
                        'detection': [],
                        'ssim_score': [],
                        'mse_score': [],
                        'mse': [],
                        'similarity': [],
                    })

                    for i, image_path in enumerate(subject.image_path):
                        # Redo crop because each multiple iterations may crop/resize differently
                        cropped_image = image[subject.zone.y_slice, subject.zone.x_slice]

                        subject_name_key = subject_name.replace('_', '-')
                        if len(subject.image_path) > 1:
                            subject_name_key = f'{subject_name_key}:{i}'

                        key = f'image_similarity/{subject_name}-{i}_{subject.zone.x}_{subject.zone.y}_{subject.zone.w}_{subject.zone.h}.jpg'


                        patch = yield self.load_patch(
                            step_cfg.nodename, subject, key, image_path, step_cfg.input_crop, color_space
                        )

                        patch_filedata.append(
                            (
                                f'{step_cfg.filelist[0][:-4]}_{subject_name_key}-patch.jpg',
                                "bio",
                                pack_image(np.hstack([cropped_image, patch]))
                            )
                        )

                        # perform autocroping
                        if 'autocrop' in subject and subject.autocrop:
                            cropped_image, patch = self.autocrop_patches(subject, cropped_image, patch)

                            patch_filedata.append(
                                (
                                    f'{step_cfg.filelist[0][:-4]}_{subject_name_key}-autocrop.jpg',
                                    "bio",
                                    pack_image(np.hstack([cropped_image, patch]))
                                )
                            )

                        if 'blur_radius' in subject and subject.blur_radius:
                            kernel = np.ones((subject.blur_radius,subject.blur_radius), np.float32) / (subject.blur_radius**2)
                            cropped_image = cv2.filter2D(cropped_image,-1,kernel)
                            patch = cv2.filter2D(patch,-1,kernel)

                        ssim_similarity = self.ssim(patch, cropped_image)
                        mean_square_error = self.mse(patch, cropped_image)
                        mse_similarity = self.mse_to_sim(mean_square_error, cropped_image.shape)
                        similarity = float(subject.mse_ratio*mse_similarity + subject.ssim_ratio*ssim_similarity)

                        data.detection.append(similarity > subject.threshold)
                        data.similarity.append(similarity)
                        data.ssim_score.append(float(ssim_similarity))
                        data.mse_score.append(float(mse_similarity))
                        data.mse.append(int(mean_square_error))

                    similarity = 0
                    if 'aggregate' not in subject or subject.aggregate == 'max':
                        similarity = max(data.similarity)
                    elif subject.aggregate == 'min':
                        similarity = min(data.similarity)
                    elif subject.aggregate == 'avg':
                        similarity = sum(data.similarity)/len(data.similarity)

                    detection = np.bool_(similarity > subject.threshold) # TODO: use normal python bools
                    annotation = 'present' if detection else 'absent'


                    # brightness key generated by check_image
                    if return_value.brightness < step_cfg.brightness.low_threshold:
                        detection = _TOO_DARK
                        annotation = _TOO_DARK
                    elif return_value.brightness > step_cfg.brightness.high_threshold:
                        detection = _TOO_BRIGHT
                        annotation = _TOO_BRIGHT

                    return_value[subject_name] = {
                        'similarity': float(similarity),
                        'detection': detection,
                        'data': data,
                    }

                    boxes.append(
                        Zone.create(f'{subject_name}: {annotation}', subject.zone.list, atype="subject")
                    )
                except Exception as err:
                    logger.exception(f'image_similarity subject {subject_name} exception: ' + repr(err))
                    raise

            annotated_image = anonymized_image = None
            annotated_image = annotate(image,
                                        boxes)
            # if anonymize:
            #     anonymized_image = yield anonymize_image(image, sc)
            #     if anonymized_image is not None:
            #         anonymized_image = annotate(anonymized_image,
            #                                     boxes,
            #                                     color_space=step_cfg.color_space)

            if annotated_image is not None:
                return_value.filedata.append(
                    (
                        step_cfg.filelist[0][:-4] + "_annotated.jpg",
                        "bio",
                        annotated_image,
                    )
                )
            if anonymized_image is not None:
                return_value.filedata.append(
                    (
                        step_cfg.filelist[0][:-4] + "_anonymized.jpg",
                        "bio",
                        anonymized_image,
                    )
                )

            if 'return_patches' in step_cfg and step_cfg.return_patches:
                return_value.filedata.extend(patch_filedata)

        except Exception as err:
            logger.exception(f'image_similarity exception: ' + repr(err))
            return_value.status = "error"
            return_value.reason = repr(err)

        return return_value
