from atlapi.attribute_dict import AD
from atlapi.image.utilities import *
from atlapi.vision_utilities.pixel_average import pixelAverage, MVError

import colorsys
import logging
from math import atan2, sqrt, pi
import numpy
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue
from typing import Tuple, Dict, List, Optional, Any

logger = logging.getLogger('atlapi')


def conv_rgb_to_hue_radian(input_rgb_tuple: Tuple[int, int, int]) -> float:
    """

    :param input_rgb_tuple: a Tuple of RGB color in 3 numeric components
    :return: the Hue Vector Angle in Radians, i.e. the result is between -pi and pi
    """
    (input_r, input_g, input_b) = input_rgb_tuple
    # See: https://en.wikipedia.org/wiki/Hue
    return atan2(sqrt(3)*(input_g - input_b), 2*input_r - input_g - input_b)


def compare_radian_angle(angle_01: float, angle_02: float) -> float:
    diff = angle_01 - angle_02
    # radian range:
    # -pi <= angle <= pi
    # if expressed in degree range:
    # -180 <= angle <= 180
    if diff > pi:
        return diff - pi*2
    elif diff < -pi:
        return diff + pi*2
    else:
        return diff


def conv_rgb_to_hsv(input_rgb_tuple: Tuple[int, int, int],
                    max_value: Optional[int]=255) -> Tuple[float, float, int]:
    [input_r, input_g, input_b] = input_rgb_tuple
    # using max_value to normalize RGB to the range between 0 and 1
    return colorsys.rgb_to_hsv(input_r/max_value, input_b/max_value, input_g/max_value)


class BlackWhiteLabelConfig:
    white_saturation_threshold: Optional[float]
    white_hsv_value_threshold: Optional[float]
    black_saturation_threshold: Optional[float]
    black_hsv_value_threshold: Optional[float]

    def __str__(self) -> str:
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )

    def __repr__(self) -> str:
        return self.__str__()


class HueConfig:
    label: str
    rgb: Tuple[int, int, int]
    hue_radian: float

    def __str__(self) -> str:
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )

    def __repr__(self) -> str:
        return self.__str__()


class HueResult(HueConfig):
    hue_diff: float

    def __init__(self, hue_config: HueConfig) -> None:
        self.label = hue_config.label
        self.rgb = hue_config.rgb
        self.hue_radian = hue_config.hue_radian


class SubjectHueResult:
    subject_rgb: Tuple[int, int, int]
    subject_hue: float
    subject_saturation: Optional[float]         # saturation as in HSV
    subject_value: Optional[float]              # value as in HSV
    inner_outer_zone_diff: Optional[float]
    subject_hue_label: str
    subject_hue_details: Optional[List[HueResult]]

    def __str__(self) -> str:
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )

    def __repr__(self) -> str:
        return self.__str__()


# nothing once we adopt Pydantic or move away from AD (AttrDict)
# all these "_conv_*" functions may not be necessary anymore.
_KNOWN_OBJ_TYPES = (SubjectHueResult, HueResult,)
_KNOWN_SIMPLE_TYPES = (int, float, str, bool,)
_KNOWN_NUMPY_TYPES = (numpy.int64, )
_KNOWN_LIST_TYPES = (list, tuple,)
_KNOWN_DICT_TYPES = (dict, )
_KNOWN_TYPES = _KNOWN_OBJ_TYPES + _KNOWN_SIMPLE_TYPES + _KNOWN_LIST_TYPES + _KNOWN_DICT_TYPES


def _conv_from_numpy_to_builtin(obj):
    if isinstance(obj, numpy.int64):
        return int(obj)
    else:
        raise ValueError(
            "Unsupported numpy type: {} ".format(
                type(obj)))


def _conv_from_obj_list_to_dict_list(obj_list: List):
    result_list = []
    for obj in obj_list:
        if obj is None or isinstance(obj, _KNOWN_TYPES):
            result_list.append(_conv_from_obj_to_dict(obj))
        elif isinstance(obj, _KNOWN_NUMPY_TYPES):
            result_list.append(_conv_from_numpy_to_builtin(obj))
        else:
            raise ValueError(
                "Unsupported type: {} ".format(
                    type(obj)))
    return result_list


def _conv_from_obj_dict_to_dict_dict(obj_dict: Dict):
    result_dict = AD()
    for key, val in obj_dict.items():
        if val is None or isinstance(val, _KNOWN_TYPES):
            result_dict[key] = _conv_from_obj_to_dict(val)
        elif isinstance(val, _KNOWN_NUMPY_TYPES):
            result_dict[key] = _conv_from_numpy_to_builtin(val)
        else:
            raise ValueError(
                "Unsupported type: {} under dict key {}".format(
                    type(val), key))
    return result_dict


def _conv_from_obj_to_dict(obj):
    if obj is None:
        return None
    elif isinstance(obj, _KNOWN_SIMPLE_TYPES):
        return obj

    result_dict = AD()
    attr_key_list = [a for a in dir(obj) if not a.startswith('__') and
                     not callable(getattr(obj, a))]
    for attr_key in attr_key_list:
        val = getattr(obj, attr_key)
        if val is None:
            result_dict[attr_key] = None
        elif isinstance(val, _KNOWN_SIMPLE_TYPES):
            result_dict[attr_key] = val
        elif isinstance(val, _KNOWN_NUMPY_TYPES):
            result_dict[attr_key] = _conv_from_numpy_to_builtin(val)
        elif isinstance(val, _KNOWN_LIST_TYPES):
            result_dict[attr_key] = _conv_from_obj_list_to_dict_list(val)
        elif isinstance(val, _KNOWN_DICT_TYPES):
            result_dict[attr_key] = _conv_from_obj_dict_to_dict_dict(val)
        elif isinstance(val, _KNOWN_OBJ_TYPES):
            result_dict[attr_key] = _conv_from_obj_to_dict(val)
        else:
            raise ValueError(
                "Unsupported type: {} under field {} of container type {}".format(
                    type(val), attr_key, type(obj)))
    return result_dict


def _conv_rgb_vector_label_config(
        label_input_config: Dict[str, List[int]]) -> List[HueConfig]:
    result_list = []
    for key, value in label_input_config.items():
        if isinstance(value, (list,)):
            if len(value) == 3:
                for entry in value:
                    if not isinstance(entry, int):
                        raise ValueError("expecting a list of 3 integers: {}".format(value))
            else:
                raise ValueError("expecting a list of 3 integers: {}".format(value))
        else:
            raise ValueError("value is not a list: {}".format(value))
        hue_config = HueConfig()
        hue_config.label = key
        hue_config.rgb = tuple(value)
        hue_config.hue_radian = conv_rgb_to_hue_radian(hue_config.rgb)
        result_list.append(hue_config)
    return result_list


def _conv_black_white_config(black_white_enabled_dict: Optional[Dict[str, Any]]) -> Optional[BlackWhiteLabelConfig]:
    if not  black_white_enabled_dict or black_white_enabled_dict is None or len(black_white_enabled_dict) == 0:
        return None
    else:
        config = BlackWhiteLabelConfig()
        for v in ['white_saturation_threshold', 'white_hsv_value_threshold', 'black_hsv_value_threshold', 'black_saturation_threshold']:
            setattr(config, v, black_white_enabled_dict[v] if v in black_white_enabled_dict else None)
        return config


def _select_hue_label(target_hue: float,
                      hue_config_list: List[HueConfig]) -> List[HueResult]:
    result_list = []
    for hue_config in hue_config_list:
        hue_result = HueResult(hue_config)
        hue_result.hue_diff = abs(compare_radian_angle(target_hue, hue_config.hue_radian))
        result_list.append(hue_result)
    result_list = sorted(result_list, key=lambda hue_entry: hue_entry.hue_diff)
    return result_list


def _cal_avg_rgb_outer_zone(step_cfg: AD, subj_name: str,
                            outer_zone: Tuple[int, int, int, int]) -> List[int]:
        img_subj_zone = Zone.create(subj_name, outer_zone)
        img_obj = load_image(step_cfg.filedata[step_cfg.filelist[0]],
                            color_space=step_cfg.color_space, crop=step_cfg.input_crop)
        return list(numpy.mean(img_obj[img_subj_zone.y_slice, img_subj_zone.x_slice],
                               axis=(0, 1)).astype(int))


def _cal_simple_rgb_diff_percent(rgb_1: Tuple[int, int, int],
                                 rgb_2: Tuple[int, int, int]) -> float:
    diff_percentage_acc = 0.0
    for i, input_1_color_component in enumerate(rgb_1):
        input_2_color_component = rgb_2[i]
        diff_percentage_acc = (diff_percentage_acc +
                               (input_2_color_component - input_1_color_component)
                               / input_1_color_component)
    return diff_percentage_acc

WARNING_CODE_IMG_TOO_DARK = "img_too_dark"
WARNING_CODE_IMG_TOO_BRIGHT = "img_too_bright"

def hue_vector_label(stepCfg):
    # note: stepCfg is an instance of "AD".
    # we are considering moving way from "AD" to Python Data Class
    # with support from Pydantic: https://pydantic-docs.helpmanual.io/
    # Example of 'pixel_avg_result':
    # {
    #   'status': 'success', 'reason': None,
    #   'brightness': 155.28095238095239, 'resolution': '(70, 50)',
    #   'card_hue': [190.9471153846154, 144.75480769230768, 172.76923076923077]
    # }
    #
    # pixel_avg_result.brightness is the average brightness of the whole image
    # NOT average brightness of the specified crop
    #
    # In the case of machine status wheel,
    # it would include white "cover" of other parts of the wheel.
    #

    ret_value = AD({"status": "success", "hue_result": AD(), "reason": None, "warning": None})
    sc = stepCfg
    pixel_avg_result = pixelAverage(sc)

    if ('brightness.low_threshold' in sc and
           isinstance(sc.brightness.low_threshold, (int, float)) and
            pixel_avg_result.brightness < sc.brightness.low_threshold):
        ret_value.warning = 'warning : Image is very dark {} < {}'.format(
            pixel_avg_result.brightness, sc.brightness.low_threshold)
        ret_value.warning_code = WARNING_CODE_IMG_TOO_DARK
        ret_value.status = "error"
        ret_value.reason = ret_value.warning
    elif ('brightness.high_threshold' in sc and
             isinstance(sc.brightness.high_threshold, (int, float)) and
             pixel_avg_result.brightness > sc.brightness.high_threshold):
        ret_value.warning = 'warning : Image is very bright {} > {}'.format(
            pixel_avg_result.brightness, sc.brightness.high_threshold)
        ret_value.warning_code = WARNING_CODE_IMG_TOO_BRIGHT
        ret_value.status = "error"
        ret_value.reason = ret_value.warning

    try:
        for subj_name, subject in sc.subjects.items():
            ret_value.hue_result[subj_name] = AD()
            subject_hue_result = SubjectHueResult()

            if ('warning_code' in ret_value and
                ret_value.warning_code in [WARNING_CODE_IMG_TOO_DARK, WARNING_CODE_IMG_TOO_BRIGHT]):
                subject_hue_result.subject_hue_label = "_nondeterministic"
                ret_value.hue_result[subj_name].update(_conv_from_obj_to_dict(subject_hue_result))
                continue
            elif len(subject.rgb_vector_labels) <= 1:
                ret_value.status = ret_value.status = "error"
                ret_value.reason = "Expected more than 1 entry under 'rgb_vector_labels': {}".format(
                        subject.rgb_vector_labels)
                subject_hue_result.subject_hue_label = "_nondeterministic"
                ret_value.hue_result[subj_name].update(_conv_from_obj_to_dict(subject_hue_result))
                continue
            else:
                hue_config_list = _conv_rgb_vector_label_config(subject.rgb_vector_labels)
                black_white_config = _conv_black_white_config(subject.black_white_enablement) if 'black_white_enablement' in subject else None

                subject_hue_result.subject_rgb = pixel_avg_result.get(subj_name)
                subject_hue_result.subject_hue = conv_rgb_to_hue_radian(subject_hue_result.subject_rgb)

                labelled = False
                subject_hue_result.subject_saturation, subject_hue_result.subject_value = conv_rgb_to_hsv(input_rgb_tuple=subject_hue_result.subject_rgb)[1:]
                if black_white_config:
                    if (hasattr(black_white_config, 'white_hsv_value_threshold') and
                        black_white_config.white_hsv_value_threshold and
                        subject_hue_result.subject_value > black_white_config.white_hsv_value_threshold and
                        subject_hue_result.subject_saturation < black_white_config.white_saturation_threshold):
                        labelled = subject_hue_result.subject_hue_label = "white"
                    elif (hasattr(black_white_config, 'black_hsv_value_threshold') and
                        black_white_config.black_hsv_value_threshold and
                        subject_hue_result.subject_value < black_white_config.black_hsv_value_threshold and
                        subject_hue_result.subject_saturation < black_white_config.black_saturation_threshold):
                        labelled = subject_hue_result.subject_hue_label = "black"
                    if (not labelled and 'outer_white_zone' in subject and
                        isinstance(subject.outer_white_zone, (list, Zone))):
                        # if the outer_white_zone is defined and the configuration tells
                        # that "white" color is white label that we are looking for.
                        outer_zone_rgb = _cal_avg_rgb_outer_zone(stepCfg, "outer_white_zone_"+subj_name, subject.outer_white_zone)
                        subject_hue_result.inner_outer_zone_diff = _cal_simple_rgb_diff_percent(subject_hue_result.subject_rgb, outer_zone_rgb)
                        # if the outer zone is not brighter then inner zone
                        # For Flex as we are using Flip-charts there's no outer-white-zone so
                        # Disabling this check
                        # if subject_hue_result.inner_outer_zone_diff < 0.05:
                        #     # TBD this threshold value for subject_hue_result.inner_outer_zone_diff
                        #     # whether we should do:
                        #     # (zone_diff < 0.05), (zone_diff < 0.0) or (zone_diff < -0.05)
                        #     labelled = subject_hue_result.subject_hue_label = "white"

                if not labelled:
                    subject_hue_result.subject_hue_details = _select_hue_label(subject_hue_result.subject_hue, hue_config_list)
                    subject_hue_result.subject_hue_label = subject_hue_result.subject_hue_details[0].label
                    if subject_hue_result.subject_hue_details[0].hue_diff > 1.6:  # 1.6 ~= pi / 2
                        ret_value.warning = "Hue of target area may be too far away from the selected hue: {} ".format(
                                subject_hue_result.subject_hue_details[0].label)
                    elif len(subject_hue_result.subject_hue_details) >= 2:
                        hue_diffs_compare = abs(subject_hue_result.subject_hue_details[0].hue_diff -
                                                subject_hue_result.subject_hue_details[1].hue_diff)
                        if hue_diffs_compare < 0.09:
                            ret_value.warning = "The Hue of target area is ambiguous. It is in between: '{}' and '{}' ".format(
                                    subject_hue_result.subject_hue_details[0].label,
                                    subject_hue_result.subject_hue_details[1].label)

                ret_value.hue_result[subj_name].update(_conv_from_obj_to_dict(subject_hue_result))

    except Exception as err:
        logger.exception("hue_vector_label exception: " + repr(err))
        raise

    return ret_value
