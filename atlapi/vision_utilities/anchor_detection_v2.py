#! /usr/bin/env python3.6
from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *

import json
import enum
import math
import cv2
import time
import numpy as np
from pydantic import BaseModel
from typing import Tuple, Dict, List, Optional, Any, Union
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue


class AnchorPointDef(BaseModel):
    """
    Definition of a Single Anchor Point
    """

    s3_url_origin: str
    """
    anchor image original s3 url path
    """

    coordinates: Tuple[int, int]
    """
    This coordinates field, having x, y value Tuple defines
    the center point of the Anchor Image
    """

    anchor_width: Optional[int] = 40
    """
    Anchor Width defines with the Width of Anchor Image (by default square shape).
    And, the Anchor Image will have the above coordinates as its center point.
    Default is 40
    """


class AnchorDetectionCfg(BaseModel):
    """
    Configuration that supports the logic that locates one or more anchor points
    and calculate their shifting distance.
    Initial version supports one 1 anchor point only.
    """

    anchor_point_list: List[AnchorPointDef]
    """
    List of Anchor Points.
    Initial version supports one 1 anchor point only.
    """

    ignore_anchor_movement_threshold: Optional[int] = 5
    """
    After anchor point detection, it will estimate how many pixels the anchor has moved.
    If the movement is larger than the threshold,
    it will pass value movement vector to other parts of system.
    For example, shifting coordinates used in 7-Segment CV function,
    or, alerting Atollogy users.
    If the movement is smaller than then threshold, the movement will be ignored.
    Default is 5
    """

    region_multiple: Optional[int] = 5
    """
    For template match, the anchor image will be located and compared
    within a bigger region area.
    This value indicates how many times this comparison region is bigger
    than the anchor image. Default is 5
    """

    template_meth: Optional[str] = "cv2.TM_CCORR_NORMED"
    """
    The method of opencv template match. default is "cv2.TM_CCORR_NORMED"
    """

    sns_target_arn: Optional[str] = None
    """
    AWS alert message target ARN
    """

    similarity_threshold: Optional[float] = None
    """
    if AnchorResult's similarity is less than the similarity_threshold value, the anchor will be 'failed_to_detect' status
    """


class Point(BaseModel):
    """
    x,y coordinates on the image
    """

    x: int
    y: int


class AnchorStateEnum(str, enum.Enum):
    """
    Show anchor status
    'too_dark': image is too dark to find anchor on the image
    'stationary': on the image, anchor is not moved.
    'moved': on the image, anchor is moved.
    'failed_to_detect': anchor is out of image. anchor disapear totally from the image view.
    """

    too_dark: str = "too_dark"
    """
    image is too dark to find anchor on the image
    """

    stationary: str = "stationary"
    """
    on the image, anchor is not moved. camera is not moved
    """

    moved: str = "moved"
    """
    on the image, anchor is moved. detect new anchor point
    """

    failed_to_detect: str = "failed_to_detect"
    """
    anchor is out of the image. anchor disapear totally from the image view.
    """


class AnchorResult(BaseModel):
    """
    Anchor point result output format.
    """

    anchor_state: AnchorStateEnum
    """
    Indicate anchor status, result is among these list "too_dark" |  "stationary" | "moved" | "failed_to_detect"
    """

    origin: Optional[Point]
    """
    Anchor image's origin center coordinates before movement.
    origin = {"x": coordinate int, "y": coordinate int}
    """

    to: Optional[Point]
    """
    Anchor image's center coordinates after movement.
    to = {"x": coordinate int, "y": coordinate int}
    """

    similarity: Optional[float]
    """
    template match similarity score.
    Map of comparison results. It must be single-channel 32-bit floating-point. If image is W x H and templ is w x h , then result is (W-w+1) x (H-h+1).
    """


class CameraPayload(BaseModel):
    """
    Camera information
    """

    cameraID: str
    """
    camera node name, for example: atlview13_slateinc_prd
    """

    collection_time: float
    """
    collection_time is when shooting the image from camera
    """


######### Anchor point code #########
class AnchorRecognition(Core):
    def __init__(self):
        Core.__init__(self, 'func_anchor_recognition')
        self.AWS = self.svcs.aws
        self.s3_bucket = f"atl-{BCM.env}-anchorpoint-data"
        self.anchor_dir = "anchor_v2"

    def _conv_obj_list_to_json_list(self, obj_list: List[BaseModel]) -> List[Dict]:
        return [json.loads(obj.json()) for obj in obj_list]

    def crop_by_radius(self, img: np.ndarray, center_point: List[int], radius: int) -> np.ndarray:
        """
        crop region from center point with the radius
        crop shape is a square
        padding increase width (only right side) and hieght (only bottom side) by ratio
        """

        # avoid negative values before crop
        x1 = max(0, center_point[0] - radius)
        x2 = max(0, center_point[0] + radius)
        y1 = max(0, center_point[1] - radius)
        y2 = max(0, center_point[1] + radius)

        crop_img = img[y1:y2, x1:x2]

        return crop_img

    def template_match(self, template_img: np.ndarray, interest_region: np.ndarray, template_meth: str) -> List[int]:
        w, h = template_img.shape[::-1]
        method = getattr(cv2, template_meth.split('.')[-1])

        # Apply template Matching
        res = cv2.matchTemplate(interest_region, template_img, method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            top_left = min_loc
            similarity = min_val
        else:
            top_left = max_loc
            similarity = max_val

        bottom_right = (top_left[0] + w, top_left[1] + h)

        # calculate center point from top left and bottom right
        center = [
            top_left[0] + round((bottom_right[0] - top_left[0]) / 2),
            top_left[1] + round((bottom_right[1] - top_left[1]) / 2),
        ]

        return center, similarity

    @inlineCallbacks
    def check_anchor_template(self, cameraId: str, anchor_point_list: AnchorPointDef) -> List[str]:
        """
        cameraId: camera node name, for example: atlview13_slateinc_prd
        """
        anchor_node_dir = f"{self.anchor_dir}/{gateway}"
        template_s3_list = yield self.AWS.S3.list_objects(self.s3_bucket, prefix=anchor_node_dir)

        for anchor in anchor_point_list:
            s3_url = anchor.s3_url_origin
            f_name = s3_url.split("/")[-1]
            tmeplate_parameters = "_cx_{}_cy_{}_aw_{}_template.jpg".format(
                anchor.coordinates[0],
                anchor.coordinates[1],
                anchor.anchor_width
            )
            template_file_name = f_name[:-4] + tmeplate_parameters
            template_s3 = (
                "s3://"
                + self.s3_bucket
                + "/"
                + anchor_node_dir
                + "/"
                + template_file_name
            )

            if template_s3 not in template_s3_list:
                image_np = self.AWS.S3.get_image(s3_url, cacheable=True)
                processed_img = self.crop_by_radius(
                    image_np,
                    anchor_point_list[i].coordinates,
                    int(anchor_point_list[i].anchor_width / 2),
                )
                self.AWS.S3.put_path(template_s3, processed_img)
                template_s3_list.append(template_s3)

        return template_s3_list

    @inlineCallbacks
    def anchor_detection(self, stepCfg: AD) -> AD:
        """
        Detect anchor using opencv template match.

        https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_template_matching/py_template_matching.html

        Also, simply compare two image that are anchor image and anchor region from current image by image subtraction.

        https://docs.opencv.org/master/dd/d4d/tutorial_js_image_arithmetics.html

        input example):

        "subject":{
            "anchor": {
                "anchor_point_list": [
                    {
                    "s3_url_origin": "s3://atl-prd-trimac-rawdata/image_readings/db8cam15_trimac_prd/video0/heartbeat/2020/05/19/04/38af294f1452_video0_1589861636_heartbeat_20200519041356432285_scheduled.jpg",
                    "coordinates": [450, 105],
                    "anchor_width": 50
                    },
                    {
                    "s3_url_origin": "s3://atl-prd-trimac-rawdata/image_readings/db8cam17_trimac_prd/video0/atlftpd/2020/05/15/21/38af294f1323_video0_1589578177_atlftpd_20200515212937161072_capture.jpg",
                    "coordinates": [452, 107],
                    "anchor_width": 50
                    },
                    {
                    "s3_url_origin": "s3://atl-prd-trimac-rawdata/image_readings/db8cam17_trimac_prd/video0/atlftpd/2020/05/15/21/38af294f1323_video0_1589578177_atlftpd_20200515212937161082_capture.jpg",
                    "coordinates": [453, 108],
                    "anchor_width": 50
                    }
                ],
                "region_multiple": 5,
                "template_meth": "cv2.TM_CCOEFF_NORMED",
                "ignore_anchor_movement_threshold": 10,
                "similarity_threshold": 0.65,
                "sns_target_arn": "arn:aws:sns:us-west-2:434432432932:ANCHOR_POINT"
                },
        }
        "brightness": {
          "alert": 3,
          "high_threshold": 190,
          "low_threshold": 8,
          "normalize": false,
          "track": false,
          "track_history_length": 2
        }

        output example:

        {
            "status": "success",
            "reason": null,
            "warning": null,
            "brightness": 119.37065321180556,
            "resolution": {"w": 1280, "h": 720},
            "anchor_state_list": [
                {
                    "anchor_state": "moved",
                    "origin": {"x": 809, "y": 149},
                    "to": {"x": 828, "y": 90},
                    "similarity": 0.70
                }
            ]
        }
        """
        sc = stepCfg
        return_value = AD(
            {"status": "success", "reason": None, "filedata": [], "warning": None}
        )
        try:
            # image read by gray scale for template match
            annotation_image = load_image(
                stepCfg.filedata[stepCfg.filelist[0]],
                color_space=sc.color_space,
                crop=sc.input_crop,
            )
            img = load_image(
                stepCfg.filedata[stepCfg.filelist[0]], color_space="gray", crop=sc.input_crop
            )
            check_image(annotation_image, sc, return_value)  # check image brightness

            anchor_state_list = []
            bbox_annotation = []

            for subject_name, subject in sc.subjects.items():
                if subject_name in return_value:
                    raise APIError(
                        "Subject name {} already present in results".format(
                            subject_name
                        )
                    )
                if "zone" not in subject or not subject.zone:
                    # default is entire image
                    h, w, _ = annotation_image.shape
                    subject.zone = Zone.create(
                        subject_name, [0, 0, w, h], atype="subject"
                    )
                else:
                    subject.zone = Zone.create(
                        subject_name, subject.zone, atype="subject"
                    )

                anchor_parameter = AnchorDetectionCfg(**subject)

                ###### parameter set up for anchor detection
                ignore_anchor_movement_threshold = (
                    anchor_parameter.ignore_anchor_movement_threshold
                )
                region_multiple = anchor_parameter.region_multiple
                template_meth = anchor_parameter.template_meth
                similarity_threshold = anchor_parameter.similarity_threshold

                # check image brightness. if lower than the threshold, it will skip the process
                brightness_threshold = (
                    stepCfg.brightness.low_threshold
                    if stepCfg.brightness.low_threshold
                    else 30
                )
                ############

                anchor_result = AnchorResult(
                    anchor_state=AnchorStateEnum.failed_to_detect,
                    origin=None,
                    to=None,
                    similarity=None,
                )

                # brightness calcualted by average value
                brightness = np.mean(img)
                bbox = None

                if brightness < brightness_threshold:
                    template_s3_list = yield self.check_anchor_template(
                        stepCfg.nodename, anchor_parameter.anchor_point_list
                    )

                    for tmpl in template_s3_list:
                        # load achor image from s3
                        template_img = yield self.AWS.S3.get_image(tmpl, color_space='gray', cacheable=True)
                        template_file_name = tmpl.rsplit("/", 1)[-1]

                        # read image process parameter from template file name
                        cx_index = template_file_name.find("_cx_")
                        cy_index = template_file_name.find("_cy_")
                        aw_start_index = template_file_name.find("_aw_")
                        aw_end_index = template_file_name.find("_template.jpg")

                        center_x = int(template_file_name[cx_index + 4 : cy_index])
                        center_y = int(
                            template_file_name[cy_index + 4 : aw_start_index]
                        )
                        anchor_width = int(
                            template_file_name[aw_start_index + 4 : aw_end_index]
                        )
                        radius = int(anchor_width / 2)

                        # load anchor image origin center points
                        anchor_result.origin = Point(x=center_x, y=center_y)

                        # template match only occurs from small region of area
                        interest_region = self.crop_by_radius(
                            img, [center_x, center_y], radius * region_multiple
                        )

                        # template match
                        detected_anchor, similarity = self.template_match(
                            template_img, interest_region, template_meth
                        )

                        anchor_result.similarity = similarity

                        # calculate original coordinate for entire image
                        detected_center = (
                            max(0, center_x - radius * region_multiple)
                            + detected_anchor[0],
                            max(0, center_y - radius * region_multiple)
                            + detected_anchor[1],
                        )

                        # bbox for annotation
                        bbox = [
                            detected_center[0] - radius,
                            detected_anchor[1] - radius,
                            detected_center[0] + radius,
                            detected_anchor[1] + radius,
                        ]

                        # calculate distance between orignial point to changed point
                        anchor_distance = math.hypot(
                            center_x - detected_center[0],
                            center_y - detected_center[1],
                        )

                else:
                    anchor_result.anchor_state = AnchorStateEnum.too_dark

                if bbox:
                    subject.zone = Zone.create(
                        subject_name, bbox, atype="subject"
                    )
                    subject.zone.label = str(anchor_result.anchor_state.value)
                    bbox_annotation.append(subject.zone)

                anchor_state_list.append(anchor_result)

            return_value.anchor_state_list = self._conv_obj_list_to_json_list(
                anchor_state_list
            )

            annotated_image = annotate(
                annotation_image, bbox_annotation, color_space=sc.color_space
            )

            return_value.filedata.append(
                (
                    stepCfg.filelist[0][:-4] + "_annotated.jpg",
                    "bio",
                    annotated_image,
                )
            )

        except Exception as err:
            msg = "anchor_detection exception: {}".format(err)
            logger.exception(msg)
            return_value.status = "error"
            return_value.reason = msg

        return return_value
