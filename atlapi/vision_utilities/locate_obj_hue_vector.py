from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.vision_utilities.hue_vector_label import hue_vector_label
from atlapi.vision_utilities.moving_obj_locate \
    import MovingObjLocationCfg, BoundingRectCfg, match_loaded_template


import cv2
from enum import Enum
from expiringdict import ExpiringDict
import functools
import json
import imutils
import numpy as np
from pydantic import BaseModel
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue
from typing import Tuple, Optional

_DRAW_LOCAL = False

bgr_color = Tuple[int, int, int]

_COLOR_BGR_GREEN: bgr_color = (0, 255, 0)


def _draw_rect(img: np.ndarray, zone: Tuple[int, int, int, int], color: bgr_color):
    cv2.rectangle(
        img,
        (zone[0], zone[1]),
        (zone[0]+zone[2], zone[1]+zone[3]), color=color)


class LocateHueCfg:
    color_space: str
    locate_cfg: MovingObjLocationCfg
    hue_cfg: AD


class LocateTargetHue(Core):
    def __init__(self):
        Core.__init__(self, 'func_locate_target_hue')
        self.AWS = self.svcs.aws

    @inlineCallbacks
    def locate_target_hue(self, step_cfg: AD) -> AD:
        input_img = load_image(
            step_cfg.filedata[step_cfg.filelist[0]],
            color_space=step_cfg.color_space,
            crop=step_cfg.input_crop
        )
        if _DRAW_LOCAL:
            cv2.imshow("input_img", input_img)

        locate_hue_cfg = LocateHueCfg()
        locate_hue_cfg.color_space = step_cfg.color_space
        if 'gray_conv' not in step_cfg.locate_cfg:
            step_cfg.locate_cfg['gray_conv'] = False
        locate_hue_cfg.locate_cfg = MovingObjLocationCfg(**step_cfg.locate_cfg.as_dict())
        locate_hue_cfg.hue_cfg = step_cfg.hue_cfg
        template_img = yield self.AWS.S3.get_image(locate_hue_cfg.locate_cfg.ref_obj_s3_loc, color_space='gray')
        ret_val = self.locate_and_cal_hue(input_img, template_img, locate_hue_cfg)
        return ret_val

    def locate_and_cal_hue(self, input_img: np.ndarray, template_img: np.ndarray, locate_hue_cfg: LocateHueCfg) -> AD:
        locate_cfg = locate_hue_cfg.locate_cfg
        subject_rect: BoundingRectCfg = match_loaded_template(
            input_img,
            template_img,
            locate_cfg,
            draw_result=_DRAW_LOCAL
            )

        if subject_rect is None:
            return  AD({
                "status": "error",
                "reason": "Cannot detect target object",
                "match_template_result": {
                    "pattern_detected": False
                }
            })

        if locate_cfg.moving_range_rect:
            target_crop_rect = BoundingRectCfg(
                x=locate_cfg.moving_range_rect.x + subject_rect.x,
                y=locate_cfg.moving_range_rect.y + subject_rect.y,
                w=subject_rect.w,
                h=subject_rect.h,
            )
        else:
            target_crop_rect = subject_rect
        target_crop_img = input_img[
                          target_crop_rect.y:target_crop_rect.y+target_crop_rect.h,
                          target_crop_rect.x:target_crop_rect.x+target_crop_rect.w]
        return self._invoke_hue_step(target_crop_img, locate_hue_cfg.hue_cfg, target_crop_rect)


    def _invoke_hue_step(self, target_crop_img: np.ndarray, hue_cfg_ad: AD, target_crop_rect: BoundingRectCfg) -> AD:
        hue_cfg_ad_copy = AD(hue_cfg_ad)
        hue_cfg_ad_copy.filelist = [ "target_crop.jpg" ]
        hue_cfg_ad_copy.filedata = {
            "target_crop.jpg": pack_image(target_crop_img)
        }

        ret_val =  hue_vector_label(hue_cfg_ad_copy)
        ret_val.filedata = []
        ret_val.match_template_result = AD({})
        ret_val.match_template_result.pattern_detected = True
        ret_val.match_template_result.matched_pattern_cordinates = target_crop_rect.dict()

        for subject_name in hue_cfg_ad_copy.subjects.keys():
            target_crop_annotated_img = target_crop_img.copy()
            _draw_rect(target_crop_annotated_img,
                       hue_cfg_ad.subjects[subject_name].zone,
                       _COLOR_BGR_GREEN)
            _draw_rect(target_crop_annotated_img,
                       hue_cfg_ad.subjects[subject_name].outer_white_zone,
                       _COLOR_BGR_GREEN)

            ret_val.filedata.append(
                ("{}_annotated.jpg".format(subject_name), "npa",
                 pack_image(target_crop_annotated_img))
            )

            if _DRAW_LOCAL:
                cv2.imshow("{}_annotated.jpg => hue={}".format(
                    subject_name,
                    ret_val.hue_result[subject_name].subject_hue_label),
                    target_crop_annotated_img)

        return ret_val

#
# def _test_main_simple():
#     sample_cfg_path = \
#         "./samples/cfg/wheels/cam_000031ee53c1_locate_hue_cfg_sample.json"
#     sample_input_img_path_red = \
#         "/Users/alexyiu/img_sandbox/moving_wheels/000031ee53c1_video0_1555438635_default_first.jpg"
#     sample_input_img_path_green = \
#         "/Users/alexyiu/img_sandbox/moving_wheels/000031ee53c1_video0_1555585415_default_first.jpg"
#     sample_input_img_path = sample_input_img_path_green
#     with open(sample_input_img_path, "rb") as file:
#         data = file.read()
#     sample_input_img = np.frombuffer(data, np.uint8)
#
#     with open(sample_cfg_path, "r") as cfg_fp:
#         cfg_dict = json.load(cfg_fp)
#         cfg_ad = AD(cfg_dict)
#         cfg_ad.filelist = ["test.jpg"]
#         cfg_ad.filedata = {"test.jpg": sample_input_img}
#
#     session = boto3.Session(profile_name="prd")
#     s3_client = session.client('s3')
#
#     locate_hue_handler = LocateTargetHue(boto_s3_client=s3_client)
#
#     ret_val = locate_hue_handler.locate_target_hue(cfg_ad)
#     print("t01: ret_val={}".format(ret_val))
#     if _DRAW_LOCAL:
#         cv2.waitKey(0)
#
#
# def _curtime_epoch_milli() -> int:
#     return int(round(time.time() * 1000))

#
# def _test_main_multiple() -> None:
#     cfg_file = "./samples/cfg/wheels/cam_000031ee53c1_locate_hue_cfg_sample.json"
#     s3_links_file = "./samples/imgs/wheels/s3_links/wheel_s3_b_01.txt"
#
#     cfg_ad = AD.load(cfg_file)
#
#     with open(s3_links_file, "r") as f:
#         content = f.readlines()
#     content = [x.strip() for x in content]
#
#     session = boto3.Session(profile_name="prd")
#     s3_client = session.client('s3')
#
#     locate_hue_handler = LocateTargetHue(boto_s3_client=s3_client)
#
#     for s3_link in content:
#         print("s3_link={}".format(s3_link))
#
#         t1 = _curtime_epoch_milli()
#         np_img = open_img(s3_link, s3_client=s3_client)
#         step_file_data = _conv_np_img_to_step_file_data(np_img)
#         cfg_ad.filelist = ["test.jpg"]
#         cfg_ad.filedata = {"test.jpg": step_file_data}
#         t2 = _curtime_epoch_milli()
#         print("s3 image fetching time={}".format((t2-t1)))
#
#         t1 = _curtime_epoch_milli()
#         ret_val = locate_hue_handler.locate_target_hue(cfg_ad)
#         t2 = _curtime_epoch_milli()
#         print("ret_val={}".format(ret_val))
#         print("locate_and_cal_hue_step time={}".format((t2 - t1)))
#         if _DRAW_LOCAL:
#             cv2.waitKey(0)
#
#
# if __name__ == "__main__":
#     print("======> __main__")
#     _test_main_simple()
#     # _test_main_multiple()
