

from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.image.transformation import crop_pixels
import cv2
import functools
import math
import numpy as np

class MotionDirection(object):
    _HEMISPHERES = ['north',  'south', 'south', 'north']
    _NORTH_HEMISHERE, _SOUTH_HEMISHPERE = _HEMISPHERES[:2]
    _QUADRANTS = ['northeast', 'southeast', 'southwest', 'northwest']
    _NE_QUADRANT, _SE_QUADRANT, _SW_QUADRANT,  _NW_QUADRANT, = _QUADRANTS
    _DIRECTIONS = ['north', 'northeast', 'northeast', 'east',
                   'east', 'southeast','southeast', 'south',
                   'south', 'southwest', 'southwest', 'west',
                   'west', 'northwest', 'northwest', 'north']
    (_N_DIRECTION, _NE_DIRECTION, _E_DIRECTION, _SE_DIRECTION,
      _S_DIRECTION, _SW_DIRECTION, _W_DIRECTION, _NW_DIRECTION) = _DIRECTIONS[::2]

    def __init__(self):
        # Core.__init__(self, 'func_motion_direction')
        pass

    def angle_to_cardinal(self, angle: float) -> str:
        angle = self.normalize_angle(angle)
        index = int(angle/22.5) % 16
        return self._DIRECTIONS[index]

    def angle_to_hemisphere(self, angle: float) -> str:
        angle = self.normalize_angle(angle)
        index = int(angle/90) % 4
        return self._HEMISPHERES[index]

    def angle_to_quadrant(self, angle: float) -> str:
        angle = self.normalize_angle(angle)
        index = int(angle/90) % 4
        return self._QUADRANTS[index]

    def motion_delta(self, prev_frame, next_frame, flow_params):
        # NOTE: Parameters have some tuning, may need to be exposed in step config
        flow = cv2.calcOpticalFlowFarneback(
                    prev_frame,
                    next_frame,
                    None,
                    **flow_params)

        mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1], angleInDegrees=True)
        mag = mag.mean()
        ang = self.normalize_angle(ang.mean())
        logger.info(f"motion delta: angle -> {ang}, magnitude -> {mag}")
        return  (ang, mag)

    def motion_direction(self, img_sequence, crop=None, channel=None, flow_params=None):
        # process frames
        img_sequence = [self.preprocess_frame(img, flow_params.poly_n, crop, channel) for img in img_sequence]

        angle_set = []
        magnitude_set = []
        x,y = (0,0)
        for prev_ing, next_img in zip(img_sequence, img_sequence[1:]):
            angle, magnitude = self.motion_delta(prev_ing, next_img, flow_params)
            angle_set.append(angle)
            magnitude_set.append(magnitude)

        logger.info(f"motion_direction angle_set={angle_set} magnitude_set={magnitude_set}")
        return np.mean(angle_set), np.mean(magnitude_set)

    def normalize_angle(self, angle: float) -> float:
        """normalize angle to range [0, 360]"""
        if angle < 0:
            angle = 360 + angle
        return angle % 360

    def process(self, step_cfg: AD) -> dict:
        frames = []
        result = AD(dict(
                    status='success',
                    reason=None,
                    angle=None,
                    direction=None,
                    quadrant=None,
                    hemisphere=None
                )
            )
        sc = step_cfg

        if 'extracted_video' in sc:
            raise NotImplementedError('MotionDirection cannot handle frame extraction block yet')
        elif 'mp4' not in sc.filelist[0]:
            result.status = 'error'
            result.reason = f'MotionDirection cannot process non-mp4 file {sc.filelist[0]}'
            logger.error(result.reason)
            return result

        flow_params = AD(dict(
            pyr_scale=0.5,
            levels=5,
            winsize=11,
            iterations=3,
            poly_n=5,
            poly_sigma=1.1,
            flags=0
        ))

        if 'flow_params' in sc:
            flow_params.update(sc.flow_params)

        if 'channel' not in step_cfg:
            step_cfg.channel = None

        if sc.input_crop and not isinstance(step_cfg.input_crop, Zone):
            step_cfg.input_crop = Zone.create('input_crop', step_cfg.input_crop, atype='region')

        # Load frames from a video file if frame extraction didn't happen
        extraction = load_n_frames(
                            sc.filedata[sc.filelist[0]],
                            color_space='bgr',
                            min_fcount=3,
                            max_fcount=5,
                            min_duration=4
                         )

        if extraction is None:
            result.status = 'error'
            result.reason = f"motion_direction frame extraction produced 0 frames for file: {sc.filelist[0]}"
            logger.error(result.reason)
            return result

        frames = extraction[0]
        if 'diff_threshold' in sc and sc.diff_threshold:
            ref_img = frames[0].copy()
            filtered_frames = [ref_img]
            for img in frames[1:]:
                if mmse_compare_images(ref_img, img, lower_threshold=int(sc.diff_threshold)):
                    filtered_frames.append(img)
                ref_image = img.copy()
            frames = filtered_frames

        if len(frames) <= 1:
            result.status = 'error'
            result.reason = f"motion_direction frame extraction with diff filtering yielded {len(frames)} frames for file: {sc.filelist[0]}"
            logger.error(result.reason)
            return result

        # angle from x-axis
        angle, magnitude = self.motion_direction(
            frames, step_cfg.input_crop, step_cfg.channel, flow_params=flow_params
        )

        results = AD(
            dict(
                status='success',
                angle=angle,
                magnitude=magnitude,
                direction=self.angle_to_cardinal(angle),
                quadrant=self.angle_to_quadrant(angle),
                hemisphere=self.angle_to_hemisphere(angle)
            )
        )
        logger.info(f"motion_direction results: {results}")
        # if 'filter' in sc:
        #     name, attr, values = sc.filter
        #     if attr == 'range':
        #         for val in values:
        #             if ':' in val:
        #                 minv, maxv = map(float, val.split(':'))
        #                 if minv < results.angle < maxv:
        #                     results.filedata = [(sc.filelist[0][:-4] + f"_{name}.mp4", "bio", sc.filedata[sc.filelist[0]])]
        #                     break
        #     elif results[attr] in values:
        #         results.filedata = [(sc.filelist[0][:-4] + f"_{name}.mp4", "bio", sc.filedata[sc.filelist[0]])]
        # else:
        results.filedata = [(sc.filelist[0], "bio", sc.filedata[sc.filelist[0]])]
        return results

    def preprocess_frame(self, frame, block_size=16, crop=None, channel=None):
        if channel == 'blue':
            frame = frame[:,:,0]
        elif channel == 'green':
            frame = frame[:,:,1]
        elif channel == 'red':
            frame = frame[:,:,2]
        else:
            frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

        if crop is not None:
            frame = crop_pixels(frame, crop.x, crop.y, crop.w, crop.w)

        # frame = cv2.resize(frame, (frame.shape[0]//block_size, frame.shape[1]//block_size))
        return frame

    def reshape(self, image, target_height=300, target_width=300):
        height, width = image.shape
        if width == height:
            resized_image = cv2.resize(image, (target_height,target_width))
        elif height < width:
            resized_image = cv2.resize(image, (int(width * float(target_height)/height), target_width))
            cropping_length = int((resized_image.shape[1] - target_height) / 2)
            resized_image = resized_image[:,cropping_length:resized_image.shape[1] - cropping_length]
        else:
            resized_image = cv2.resize(image, (target_height, int(height * float(target_width) / width)))
            cropping_length = int((resized_image.shape[0] - target_width) / 2)
            resized_image = resized_image[cropping_length:resized_image.shape[0] - cropping_length,:]

        return cv2.resize(resized_image, (target_height, target_width))
