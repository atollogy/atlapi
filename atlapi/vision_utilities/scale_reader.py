import base64
import logging
from atlapi.image.utilities import *
from atlapi.attribute_dict import AD
from atlapi.core import Core
import cv2
import numpy as np
from skimage import filters, measure
import os
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

logger = logging.getLogger('atlapi')

## find number

DIGITS_LOOKUP = {
    # T, TL, TR, C, BL, BR ,B
    (1, 1, 1, 0, 1, 1, 1): 0,
    (0, 0, 1, 0, 0, 1, 0): 1,
    (1, 0, 1, 1, 1, 0, 1): 2,
    (1, 0, 1, 1, 0, 1, 1): 3,
    (0, 1, 1, 1, 0, 1, 0): 4,
    (1, 1, 0, 1, 0, 1, 1): 5,
    (1, 1, 0, 1, 1, 1, 1): 6,
    (1, 0, 1, 0, 0, 1, 0): 7,
    (1, 1, 1, 1, 1, 1, 1): 8,
    (1, 1, 1, 1, 0, 1, 1): 9,
    ## Additional mappings for broken/extra segments
    (1, 1, 1, 1, 1, 0, 1): 2,
}

## crop each number
def digit_zone(x_list, y_list):
    x = min(x_list)
    x2 = max(x_list)
    y = min(y_list)
    y2 = max(y_list)

    if (x2 - x) < (y2 - y) / 2.3:
        x = int(x2 - ((y2 - y) / 1.7))

    result = [x, x2, y, y2]
    return result


## define each of the 7 segments
def segments_zone(digit_img):
    (h, w) = digit_img.shape

    (dW, dH) = (int(w * 0.15), int(h * 0.15))
    dHC = int(h * 0.05)

    segments = [
        ((0, 0), (w - dW, dH)),  # top
        ((0, 0), (dW, h // 2)),  # top-left
        ((w - dW, 0), (w, h // 2)),  # top-right
        ((dW, (h // 2) - dHC), (w - dW, (h // 2) + dHC)),  # center
        ((0, h // 2), (dW, h)),  # bottom-left
        ((w - dW, h // 2), (w, h)),  # bottom-right
        ((0, h - dH), (w - dW, h)),  # bottom
    ]
    on = [0] * len(segments)

    return segments, on


def digit_recog(segments, digit_img, on):

    for (i, ((xA, yA), (xB, yB))) in enumerate(segments):

        segROI = digit_img[yA:yB, xA:xB]
        total = cv2.countNonZero(segROI)
        area = (xB - xA) * (yB - yA)

        ## if the total number of non-zero pixels is greater than
        ## 50% of the area, mark the segment as "on"

        if area > 0:
            if total / float(area) > 0.5:
                on[i] = 1

    if tuple(on) in DIGITS_LOOKUP:
        return DIGITS_LOOKUP[tuple(on)]
    else:
        return ""

def scaleReader(stepCfg):

    sc = stepCfg
    return_value = AD({"status": "success", "reason": None})

    scaleCoordinates = None

    try:
        kernel = np.ones((3, 3), np.uint8)
        color_space_image = load_image(
            stepCfg.filedata[stepCfg.filelist[0]],
            color_space=sc.color_space,
            crop=sc.input_crop
        )
        rgb_img = cv2.cvtColor(color_space_image, cv2.COLOR_BGR2RGB)

        check_image(color_space_image, sc, return_value)

        ## Perspective Transformation
        if "actual_coordinates" in sc and sc["actual_coordinates"]:
            actual_coordinates = sc["actual_coordinates"]
        else:
            raise ValueError(
                "Invalid format of required parameter scale_coordiantes. Accepted format array of (x,y) for (top_left, top_right, bottom_left, bottom_right"
            )

        if "transform_coordinates" in sc and sc["transform_coordinates"]:
            transform_coordinates = sc["transform_coordinates"]
        else:
            raise ValueError(
                "Invalid format of required parameter transform_coordiantes. Accepted format array of (x,y) for (top_left, top_right, bottom_left, bottom_right"
            )

        if "kernel_size" in sc and sc["kernel_size"]:
            kernel_size = sc["kernel_size"]
        else:
            kernel_size = (8, 5)

        top_left, top_right, bottom_left, bottom_right = actual_coordinates

        padding = 50
        scaleCoordinates = [
            top_left[0] - padding,
            top_left[1] - padding,
            bottom_right[0] + padding,
            bottom_right[1] + padding,
        ]

        pts1 = np.float32([top_left, top_right, bottom_left, bottom_right])

        transform_top_left, transform_top_right, transform_bottom_left, transform_bottom_right = (
            transform_coordinates
        )

        pts2 = np.float32(
            [
                transform_top_left,
                transform_top_right,
                transform_bottom_left,
                transform_bottom_right,
            ]
        )

        M = cv2.getPerspectiveTransform(pts1, pts2)
        dst = cv2.warpPerspective(rgb_img, M, tuple(transform_bottom_right))

        ## subtract number
        gray_image = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)

        kernel = np.ones(tuple(kernel_size), np.uint8)

        dilated_img = cv2.dilate(gray_image, kernel, iterations=2)
        res = cv2.erode(dilated_img, kernel, iterations=1)

        dilated_img = cv2.dilate(res, kernel, iterations=2)
        res = cv2.erode(dilated_img, kernel, iterations=1)

        threshold = filters.threshold_otsu(gray_image)

        filtered_img = (res > threshold) * 255

        ## separate each number
        digit_info = {}

        labels = measure.label(filtered_img, neighbors=8, background=0)

        for label in np.unique(labels):
            if label == 0:
                continue
            labelMask = np.zeros(filtered_img.shape, dtype="uint8")
            labelMask[labels == label] = 1
            numPixels = np.count_nonzero(labelMask)

            if numPixels > 2000:
                cnts, _ = cv2.findContours(
                    labelMask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
                )

                x_list = []
                y_list = []
                for c in cnts[0]:
                    x_list.append(c[0][0])
                    y_list.append(c[0][1])

                digit_coordinate = digit_zone(x_list, y_list)
                digit_img = filtered_img[
                    digit_coordinate[2] : digit_coordinate[3],
                    digit_coordinate[0] : digit_coordinate[1],
                ]
                segments, on = segments_zone(digit_img)
                result_digit = digit_recog(segments, digit_img, on)
                digit_info[min(x_list)] = result_digit

        ## sort digit number
        digit_result = []

        for key in sorted(digit_info):
            digit_result.append(digit_info[key])

        weight = ""
        for item in digit_result:
            weight += str(item)

        return_value["weight"] = weight

        if "weight" in return_value and return_value["weight"]:
            annotate_scale_image(sc, return_value, scaleCoordinates)

    except Exception as err:
        msg = "scale reader exception: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value


def annotate_scale_image(stepCfg, return_value, scaleCoordinates=None):
    annotationZones = []
    if scaleCoordinates != None:
        annotationZones.append(
            Zone.create(
                "Weight: {}".format(return_value.weight), scaleCoordinates, atype="subject", notwh=True
            )
        )
    toAnnotate = load_image(
                stepCfg.filedata[stepCfg.filelist[0]],
                color_space=stepCfg.color_space,
                crop=None)
    annotatedImage = annotate(toAnnotate, annotationZones, color_space=stepCfg.color_space)
    return_value.filedata = [(stepCfg.filelist[0][:-4] + "_annotated.jpg", "bio", annotatedImage)]
