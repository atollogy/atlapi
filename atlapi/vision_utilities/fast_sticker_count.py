from atlapi.common import *
from atlapi.vision_utilities.sticker_base import *
import functools
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue


_DEFAULT_VERBOSE = False

def StickerResult(needs, seen):
    result = {}
    result["needs"] = needs
    result["seen"] = seen
    return "seen: {}/{}".format(seen, needs), result

@exec_timed
@inlineCallbacks
def fastStickerCount(stepCfg):
    if not stepCfg.verbose:
        stepCfg.verbose = _DEFAULT_VERBOSE
    sb = yield StickerBase(stepCfg, StickerResult)
    return sb
