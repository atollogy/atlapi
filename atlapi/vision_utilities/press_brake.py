from atlapi.vision_utilities.video_diff_base import *
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

def ActivityResult(result, file_data, count, subject_config):
    return count + 1

@inlineCallbacks
def pressBrake(stepCfg):
    res = yield videoDiffBase(stepCfg, ActivityResult)
    return res
