from atlapi.common import *
from atlapi.vision_utilities.sticker_base import *
import functools
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

_DEFAULT_VERBOSE = False

def StickerResult(needs, seen):
    result = "open"
    if seen >= needs:
        result = "closed"
    return result, result

@exec_timed
@inlineCallbacks
def fastSticker(stepCfg):
    if not stepCfg.verbose:
        stepCfg.verbose = _DEFAULT_VERBOSE
    sb = yield StickerBase(stepCfg, StickerResult)
    return sb
