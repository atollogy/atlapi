from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *

import cv2
import numpy as np

logger = logging.getLogger('atlapi')

def diff_generator(frames, blur_radius=21):
    last_frame = None
    for frame in frames:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # frame = cv2.medianBlur(frame, blur_radius)
        frame = cv2.blur(frame, (blur_radius,blur_radius))
        if last_frame is not None:
            yield frame - last_frame
        last_frame = frame

def lightswitch_filter(frames, max_change_threshold=0.16):
    for image in frames:
        if not image.mean()/255 > max_change_threshold:
            yield image

def average_frames(frames, abs=True):
    accumulator = None
    frame_count = 0
    for image in frames:
        if accumulator is None:
            accumulator = np.zeros(shape=image.shape, dtype=np.float)
            
        # NOTE: this can be accelerated by accumulating in np.uint64 and waiting to divide by 255
        if abs:
            accumulator += np.abs(image / 255.0)
        else:
            accumulator += image / 255.0
        frame_count += 1
        
    return accumulator / frame_count

class FrameTee():
    def __init__(self):
        self.frame = None
    def __call__(self, iterable):
        for i in iterable:
            if self.frame is None:
                self.frame = i
            yield i

def grid_activation(step_cfg: AD) -> AD:
    frames = []
    result = AD(dict(
        status='success',
        reason=None,
        filedata=[],
        active_subjects=[]
    ))

    if 'extracted_video' in step_cfg:
        result.status = 'error'
        result.status = 'grid activation cannot handle frame extraction block yet'
        logger.error(result.reason)
        return result
    elif 'mp4' not in step_cfg.filelist[0]:
        result.status = 'error'
        result.reason = f'grid_activation cannot process non-mp4 file {step_cfg.filelist[0]}'
        logger.error(result.reason)
        return result

    try:
        num_frames = step_cfg.input_parameters.media_info[step_cfg.filelist[0]].get('total_frames', 0)
        result['total_frames'] = num_frames
        if num_frames > 900:
            result.status = 'error'
            result.reason = f'grid_activation detected excessive frames {num_frames}'
            logger.error(result.reason)
            return result  
    except Exception as err:
        logger.warning(f'failed to get number of frames with {err}')

    ft = FrameTee()

    mean_difference = average_frames(
        lightswitch_filter(
            diff_generator(
                ft(iterate_frames(step_cfg.filedata[step_cfg.filelist[0]])),
                blur_radius= step_cfg.get('blur_radius', 17)
            ),
            max_change_threshold=step_cfg.get('max_change_threshold', 0.16)
        )
    )

    normalized = cv2.normalize(mean_difference, None, 0, 1.0, cv2.NORM_MINMAX)

    if not 'subjects' in step_cfg:
        logger.info('grid_activation: requires subjects')    

    for subject_name, subject in step_cfg.subjects.items():
        # TODO: better error handling for ret
        ret, thresh = cv2.threshold(normalized, subject.get('sensitivity', 0.2), 1.0, cv2.THRESH_BINARY)

        # TODO: better handling for zones (polygon, etc)
        zone = Zone.create(subject_name, subject.region[:4], atype='region')
        roi = thresh[zone.y:zone.y2, zone.x:zone.x2]

        roi_activation_average = float(roi.mean())
        roi_activation = bool(roi_activation_average > subject['threshold'])

        if subject.get('generate_image', True):
            # Generate visualization of frame
            try:
                visualize_frame = cv2.addWeighted(
                    ft.frame, 0.6, 
                    cv2.cvtColor(np.uint8(255*thresh), cv2.COLOR_GRAY2RGB), 0.4,
                    0
                )

                annotated_image = annotate(visualize_frame, [zone])
                result.filedata.append(
                    (
                        step_cfg.filelist[0][:-4] + f"{subject_name.replace('_','-')}.jpg",
                        "bio",
                        annotated_image,
                    )
                )
            except Exception as err:
                logger.warning(f"grid_activation:generate_image failed with error {err}")

        result[subject_name] = {
            'average': roi_activation_average,
            'active': roi_activation
        }

        if roi_activation:
            result.active_subjects.append(subject_name)

    return result



def grid_activation_wrapper(step: AD) -> list:
    records = []

    try:
        results = grid_activation(step)
        step.output = results
        records.append(step)

        for active_subject_name in results.active_subjects:
            # Boilerplate to generate extra step
            record = AD(step)
            record.bundle = list(record.bundle)
            record.bundle[-1] = f'{record.step_name}_active'
            record.scid = ':'.join(record.bundle)
            record.step_name = f'{record.step_name}_{active_subject_name}'
            record.function = f'{record.function}_active'
            record.output = record.output[active_subject_name]

            records.append(record)

    except Exception as err:
        logger.exception(f'grid_activation_wrapper error: {err}')

    return records
