#! /usr/bin/env python3.6
from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *

import json
import enum
import math
import boto3
import cv2
import time
import numpy as np
from pydantic import BaseModel
from typing import Tuple, Dict, List, Optional, Any, Union
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

class AnchorPointDef(BaseModel):
    """
    Definition of a Single Anchor Point
    """

    coordinates: Tuple[int, int]
    """
    This coordinates field, having x, y value Tuple defines
    the center point of the Anchor Image
    """

    anchor_width: Optional[int] = 40
    """
    Anchor Width defines with the Width of Anchor Image (by default square shape).
    And, the Anchor Image will have the above coordinates as its center point.
    Default is 40
    """

class AnchorDetectionCfg(BaseModel):
    """
    Configuration that supports the logic that locates one or more anchor points
    and calculate their shifting distance.
    Initial version supports one 1 anchor point only.
    """

    anchor_point_list: List[AnchorPointDef]
    """
    List of Anchor Points.
    Initial version supports one 1 anchor point only.
    """

    pixel_diff_threshold: Optional[int] = 10
    """
    For image difference process, set up pixel difference threshold value.
    If the difference value is bigger than the threshold, it will be counted as changed pixels. default is 10
    """

    pixel_change_percentage_threshold: Optional[float] = 10.0
    """
    After image difference calculation, estimate how much percentage of pixels be changed.
    With this percentage threshold, it will be decided whether changed or not. default is 10.0
    """

    ignore_anchor_movement_threshold: Optional[int] = 3
    """
    After anchor point detection, it will estimate how many pixels the anchor has moved.
    If the movement is larger than the threshold,
    it will pass value movement vector to other parts of system.
    For example, shifting coordinates used in 7-Segment CV function,
    or, alerting Atollogy users.
    If the movement is smaller than then threshold, the movement will be ignored.
    Default is 5
    """

    region_multiple: Optional[int] = 5
    """
    For template match, the anchor image will be located and compared
    within a bigger region area.
    This value indicates how many times this comparison region is bigger
    than the anchor image. Default is 5
    """

    template_meth: Optional[str] = "cv2.TM_CCORR_NORMED"
    """
    The method of opencv template match. default is "cv2.TM_CCORR_NORMED"
    """

    sns_target_arn: Optional[str] = None
    """
    AWS alert message target ARN
    """

    similarity_threshold: Optional[float] = None
    """
    if AnchorResult's similarity is less than the similarity_threshold value, the anchor will be 'failed_to_detect' status
    """

class Point(BaseModel):
    """
    x,y coordinates on the image
    """
    x: int
    y: int


class AnchorStateEnum(str, enum.Enum):
    """
    Show anchor status
    'too_dark': image is too dark to find anchor on the image
    'stationary': on the image, anchor is not moved.
    'moved': on the image, anchor is moved.
    'failed_to_detect': anchor is out of image. anchor disapear totally from the image view.
    """

    too_dark: str = 'too_dark'
    """
    image is too dark to find anchor on the image
    """

    stationary: str = 'stationary'
    """
    on the image, anchor is not moved. camera is not moved
    """

    moved: str = 'moved'
    """
    on the image, anchor is moved. detect new anchor point
    """

    failed_to_detect: str = 'failed_to_detect'
    """
    anchor is out of the image. anchor disapear totally from the image view.
    """


class AnchorResult(BaseModel):
    """
    Anchor point result output format.
    """

    anchor_state: AnchorStateEnum
    """
    Indicate anchor status, result is among these list "too_dark" |  "stationary" | "moved" | "failed_to_detect"
    """

    origin: Point
    """
    Anchor image's origin center coordinates before movement.
    origin = {"x": coordinate int, "y": coordinate int}
    """

    to: Optional[Point]
    """
    Anchor image's center coordinates after movement.
    to = {"x": coordinate int, "y": coordinate int}
    """

    similarity: Optional[float]
    """
    template match similarity score.
    Map of comparison results. It must be single-channel 32-bit floating-point. If image is W x H and templ is w x h , then result is (W-w+1) x (H-h+1).
    """


class CameraPayload(BaseModel):
    """
    Camera information
    """

    cameraID: str
    """
    camera node name, for example: atlview13_slateinc_prd
    """

    collection_time: float
    """
    collection_time is when shooting the image from camera
    """


######### Anchor point code #########
class AnchorDetectionV1(Core):
    def __init__(self):
        Core.__init__(self, 'func_anchor_detection_v1')
        self.AWS = self.svcs.aws
        self.s3_bucket = f"atl-{self.env}-anchorpoint-data"
        self.anchor_dir = "anchor_template"

    def _conv_obj_list_to_json_list(self, obj_list: List[BaseModel]) -> List[Dict]:
        return [json.loads(jd) for jd in obj_list]

    @inlineCallbacks
    def check_anchor_s3(self, gateway: str) -> Any:
        '''
        search gateway prefix achor image file
        if the image file detected, it will return s3 file path
        and if not, it will return False as a boolean value.
        '''
        obj_key = yield self.AWS.S3.list_objects(self.s3_bucket, prefix=self.anchor_dir, term=gateway)
        return obj_key

    def crop_by_radius(self, img: np.ndarray, center_point: List[int], radius: int) -> np.ndarray:
        '''
        crop region from center point with the radius
        crop shape is a square
        padding increase width (only right side) and hieght (only bottom side) by ratio
        '''

        # avoid negative values before crop
        x1 = max(0,center_point[0] - radius)
        x2 = max(0,center_point[0] + radius)
        y1 = max(0,center_point[1] - radius)
        y2 = max(0,center_point[1] + radius)

        crop_img = img[y1:y2, x1:x2]

        return crop_img

    def anchor_difference(
        self,
        img: np.ndarray,
        template_img: np.ndarray,
        anchor_point: List[int],
        radius: int,
        pixel_diff_threshold: int
        ) -> float:
        '''
        compare pixel difference between anchor template from s3 and current image anchor region
        return total percentage of changed pixel member above the threshold
        '''

        # crop anchor region for current image
        anchor_region = self.crop_by_radius(img, anchor_point, radius)

        # width and height
        H, W = template_img.shape[:2]
        aH, aW = anchor_region.shape[:2]

        if (H*W) != (aH*aW):
            logger.info(f"anchor_difference comparison images do not have the same shape: template[{template_img.shape[:2]}] - anchor[{anchor_region.shape[:2]}] - adjusting")
            if (H*W) > (aH*aW):
                template_img = cv2.resize(template_img, anchor_region.shape[:2], interpolation=cv2.INTER_AREA)
            else:
                anchor_region = cv2.resize(anchor_region, template_img.shape[:2], interpolation=cv2.INTER_AREA)

        img_diff = cv2.absdiff(anchor_region, template_img)
        img_diff[img_diff <= pixel_diff_threshold] = 0

        # count changed pixel number
        diff_count = np.count_nonzero(img_diff)

        return (diff_count / (W * H)) * 100

    def template_match(
        self,
        template_img: np.ndarray,
        interest_region: np.ndarray,
        template_meth: str
        ) -> List[int]:

        w, h = template_img.shape[::-1]
        method = getattr(cv2, template_meth.split('.')[-1])

        # Apply template Matching
        res = cv2.matchTemplate(interest_region, template_img, method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            top_left = min_loc
            similarity = min_val
        else:
            top_left = max_loc
            similarity = max_val

        bottom_right = (top_left[0] + w, top_left[1] + h)

        # calculate center point from top left and bottom right
        center = [top_left[0] + round( (bottom_right[0] - top_left[0])/2 ), top_left[1] + round( (bottom_right[1] - top_left[1])/2 )]

        return center, similarity

    @inlineCallbacks
    def run_anchor_detection(self, stepCfg: AD, return_value: AD) -> None:
        """
        Detect anchor using opencv template match.

        https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_template_matching/py_template_matching.html

        Also, simply compare two image that are anchor image and anchor region from current image by image subtraction.

        https://docs.opencv.org/master/dd/d4d/tutorial_js_image_arithmetics.html
        """

        if "anchor" in stepCfg and "anchor_point_list" in stepCfg.anchor:
            anchor_parameter = AnchorDetectionCfg(**stepCfg.anchor)

            ###### parameter set up for anchor detection
            anchor_center = anchor_parameter.anchor_point_list[0].coordinates
            crop_width = anchor_parameter.anchor_point_list[0].anchor_width
            pixel_diff_threshold = anchor_parameter.pixel_diff_threshold
            pixel_change_percentage_threshold = anchor_parameter.pixel_change_percentage_threshold
            ignore_anchor_movement_threshold = anchor_parameter.ignore_anchor_movement_threshold
            region_multiple = anchor_parameter.region_multiple
            template_meth = anchor_parameter.template_meth
            radius = int(crop_width / 2)
            sns_target_arn = anchor_parameter.sns_target_arn
            # check image brightness. if lower than the threshold, it will skip the process
            brightness_threshold = stepCfg.brightness.low_threshold if stepCfg.brightness.low_threshold else 30
            similarity_threshold = anchor_parameter.similarity_threshold
            ############

            # image read by gray scale for template match
            img = load_image(stepCfg.filedata[stepCfg.filelist[0]], color_space="gray")
            gateway = stepCfg.gateway_id

            anchor_result = AnchorResult(
                anchor_state=AnchorStateEnum.stationary,
                origin=Point(x=anchor_center[0], y=anchor_center[1]),
                to=None,
                similarity=None
                )

            # brightness calcualted by average value
            brightness = np.mean(img)

            if brightness > brightness_threshold:

                # check whether anchor image is exsit on s3 or not
                obj_key = yield self.check_anchor_s3(gateway)

                if obj_key:
                    # load achor image from s3
                    s3_object_key = "s3://" + self.s3_bucket + "/" + obj_key
                    # template image also read by gray scale
                    template_img = yield self.AWS.S3.get_image(s3_object_key, color_space='gray', cacheable=True)

                    # template match only occurs from small region of area
                    interest_region = self.crop_by_radius(img, anchor_center, radius*region_multiple)
                    detected_anchor, similarity = self.template_match(template_img, interest_region, template_meth)
                    anchor_result.similarity = similarity

                    # calculate original coordinate for entire image
                    detected_center = (max(0, anchor_center[0] - radius*region_multiple) + detected_anchor[0] , max(0, anchor_center[1] - radius*region_multiple) + detected_anchor[1])
                    # calculate distance between orignial point to changed point
                    anchor_distance = math.hypot(anchor_center[0] - detected_center[0], anchor_center[1] - detected_center[1])
                    # estimate how much percentage of pixels be changed in the achor image region.
                    diff_percentage = self.anchor_difference(img, template_img, anchor_center, radius, pixel_diff_threshold)

                    # additionary function created for Trimac - WashBay customer usage -- DuckHa 2020/04/14
                    if similarity_threshold:
                        if similarity > similarity_threshold:
                            anchor_result.anchor_state = AnchorStateEnum.stationary
                            anchor_result.to = Point(x=detected_center[0], y=detected_center[1])
                        else:
                            anchor_result.anchor_state = AnchorStateEnum.moved
                            anchor_result.to = Point(x=detected_center[0], y=detected_center[1])
                else:
                    # if the anchor image is not exist, create one and upload to s3
                    template_img = self.crop_by_radius(img, anchor_center, radius)
                    upload_path = "s3://{}/{}/g_{}_cx_{}_cy_{}_r_{}.jpg".format(self.s3_bucket, self.anchor_dir, gateway, center[0], center[1], radius)
                    res = yield self.AWS.S3.put_path(upload_path, img)
            else:
                anchor_result.anchor_state = AnchorStateEnum.too_dark

            return_value.anchor_state_list = [AD(anchor_result)]

        return return_value
