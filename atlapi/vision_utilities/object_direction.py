

from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.data_utilities.vptree import *
from atlapi.image.utilities import *
from atlapi.image.transformation import crop_pixels
import cv2
import functools
import math
import numpy as np

class Object_Direction(object):
    _HEMISPHERES = ['north', 'south']
    _NORTH_HEMISHERE, _SOUTH_HEMISHPERE = _HEMISPHERES
    _QUADRANTS = ['northeast', 'southeast',  'southwest', 'northwest']
    _NE_QUADRANT, _NW_QUADRANT, _SW_QUADRANT, _SE_QUADRANT = _QUADRANTS
    _DIRECTIONS = [ 'north', 'northeast', 'east', 'southeast',
                    'south', 'southwest', 'west', 'northwest']
    (_N_DIRECTION, _NE_DIRECTION, _E_DIRECTION,  _SE_DIRECTION,
    _S_DIRECTION, _SW_DIRECTION, _W_DIRECTION, _NW_DIRECTION) = _DIRECTIONS

    def __init__(self):
        # Core.__init__(self, 'func_motion_direction')
        pass

    def angle_to_cardinal(self, angle: float) -> str:
        angle = self.normalize_angle(angle)
        index = int(round((angle/45) % 8, 0 )) % 8
        return self._DIRECTIONS[index]

    def angle_to_hemisphere(self, angle: float) -> str:
        angle = self.normalize_angle(angle)
        index = int(round((angle/180) % 2, 0 )) % 2
        return self._HEMISPHERES[index]

    def angle_to_quadrant(self, angle: float) -> str:
        angle = self.normalize_angle(angle)
        index = int((angle/90) % 4)
        return self._QUADRANTS[index]

    def convert_hash(self, h):
        # convert the hash to NumPy's 64-bit float and then back to
        # Python's built in int
        return int(np.array(h, dtype="float64"))

    def dhash(self, image, hashSize=8):
        # resize the input image, adding a single column (width) so we
        # can compute the horizontal gradient
        resized = cv2.resize(image, (hashSize + 1, hashSize))
        # compute the (relative) horizontal gradient between adjacent
        # column pixels
        diff = resized[:, 1:] > resized[:, :-1]
        # convert the difference image to a hash
        phash = sum([2 ** i for (i, v) in enumerate(diff.flatten()) if v])
        phash = int(np.array(phash, dtype="float64"))
        return phash

    def object_direction(self, img_sequence, regions, scale=10):
        # process frames
        results = AD()

        for rname, region in regions.items():
            res = results[rname] = AD({'angle': None,
                                       'direction': None,
                                       'hemisphere': None,
                                       'magnitude': None,
                                       'magnitude_set': [],
                                       'quadrant': None,
                                       'region_name': rname,
                                       'status': 'success'})
            res.filter = region.filter
            vbins = AD()
            img_sequence = [self.preprocess_frame(img.copy(), region.channel, region.input_crop, scale) for img in img_sequence]
            if len(img_sequence) < 3:
                res.status = f"Too few images extracted from video sequence: {len(img_sequence)}"
                continue

            ref_frame = None
            tmp_results = AD()
            for img_num, gray in enumerate(img_sequence):
                img_res = tmp_results[img_num] = AD({'angle_set': [], 'bins': {}, 'magnitude_set': [], 'phashes': [], 'vectors': []})
                gray = cv2.GaussianBlur(gray, (11, 11), 3)
                gray = cv2.medianBlur(gray, 5)
                if ref_frame is None:
                    ref_frame = gray.copy()
                    rh, rw = ref_frame.shape[:2]
                    img_area = rh*rw
                    minarea = 500
                    maxarea = img_area*0.50
                    continue

                kernel = np.ones((5,5),np.uint8)
                fgmask = cv2.absdiff(gray.copy(), ref_frame)
                closing = cv2.morphologyEx(fgmask, cv2.MORPH_CLOSE, kernel)
                opening = cv2.threshold(closing, 75, 225, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
                dilation = cv2.dilate(opening, None, iterations=8)
                threshold = cv2.adaptiveThreshold(dilation, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
                # threshold = cv2.threshold(dilation, 200, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
                cnts = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
                fcnts = [c for c in cnts if minarea < cv2.contourArea(c) < maxarea]
                if not len(fcnts):
                    continue
                # contours = [max(fcnts, key=cv2.contourArea)]
                contours = sorted(fcnts, key=cv2.contourArea)
                clen = len(contours)

                if clen == 0:
                    continue
                elif clen in [1, 2]:
                    filtered_contours = contours
                # elif 3 < clen <=5:
                #     filtered_contours = contours[:clen//2]
                else:
                    filtered_contours = contours[clen//2:]
                # else:
                #     logger.info(f"filtered_contour count: {len(filtered_contours)}")

                for cnt in filtered_contours:
                    area = cv2.contourArea(cnt)
                    x, y, w, h = shape = cv2.boundingRect(cnt)
                    px = ((x + w/2)*w/rw)
                    py = ((y + h/2)*h/rh)
                    pt = (px, py)
                    # patch = gray[y:int(y + h):, x:int(x + w):].copy()
                    # phash = self.dhash(patch)
                    img_res.bins[id(cnt)] = AD({
                                        'pt': pt,
                                        # 'phash': phash,
                                        'area': area,
                                        'cnt': cnt,
                                        # 'patch': patch,
                                        'shape': shape
                        })
                    # if pt not in img_res.phashes:
                    #     img_res.phashes.append(phash)

            have_results = [i for i in range(len(img_sequence)) if tmp_results[i].bins.values()]
            for lindex, rindex in zip(have_results, have_results[1:]):
                left = tmp_results[lindex]
                left.img = lindex
                right = tmp_results[rindex]
                if not left.bins.values() or not right.bins.values():
                    continue
                angle_set = left.angle_set
                magnitude_set = left.magnitude_set
                # lpd = sorted(left.bins.values(), key=lambda x: x.area, reverse=True)
                # rpd = sorted(right.bins.values(), key=lambda x: x.area, reverse=True)[:len(lpd)]
                lpd = left.bins.values()
                rpd = right.bins.values()
                rpd_len = len(rpd)
                for i, l in enumerate(lpd):
                    if i >= rpd_len:
                        continue
                    r = rpd[i]
                    lpt = l.pt
                    rpt = r.pt
                    v = [lpt, rpt]
                    angle, magnitude = angle_from_points(v, magnitude=True)
                    if angle and magnitude and angle != np.NaN:
                        angle = self.normalize_angle(angle)
                        abin = int(angle//22.5%16)
                        if abin not in vbins:
                            vbins[abin] = AD({'ang': [], 'imgs': [], 'mag': [], 'vectors': []})
                        if lindex not in vbins[abin].imgs:
                            vbins[abin].imgs.append(lindex)
                        vbins[abin].vectors.append((lindex, v, abin, angle, magnitude))
                        vbins[abin].ang.append(angle)
                        a = np.mean(vbins[abin].ang)
                        if a != np.NaN:
                            vbins[abin].angle = a
                        vbins[abin].mag.append(magnitude)
                        m = np.sum(vbins[abin].mag)
                        if m != np.NaN:
                            vbins[abin].magnitude = m

            # logger.info(f"result_set for {rname}: {tmp_results}")
            if len(vbins) > 0:
                vb = max(vbins.values(), key=lambda v: v.magnitude)
                res.angle_set = vb.ang
                res.magnitude_set = vb.mag
                res.angle = vb.angle
                res.magnitude = vb.magnitude
                res.image_num = vb.imgs[len(vb.imgs)//2]
                if res.angle and res.angle != np.NaN:
                    res.direction = self.angle_to_cardinal(res.angle)
                    res.quadrant = self.angle_to_quadrant(res.angle)
                    res.hemisphere = self.angle_to_hemisphere(res.angle)
                logger.info(f"motion_direction {rname} result: {res}")
            else:
                res.status = f"no result available: {tmp_results}"

        logger.info(f"motion_direction final results: {results}")
        return results

    def normalize_angle(self, angle: float) -> float:
        """normalize angle to range [0, 360]"""
        if angle < 0:
            angle = 360 + angle
        return angle % 360

    def process(self, step_cfg: AD) -> dict:
        frames = []
        result = AD(dict(
                    status='success',
                    reason=None,
                    angle=None,
                    direction=None,
                    quadrant=None,
                    hemisphere=None
                )
            )
        sc = step_cfg

        if 'extracted_video' in sc:
            raise NotImplementedError('MotionDirection cannot handle frame extraction block yet')
        elif 'mp4' not in sc.filelist[0]:
            result.status = 'error'
            result.reason = f'MotionDirection cannot process non-mp4 file {sc.filelist[0]}'
            logger.error(result.reason)
            return result

        if 'channel' not in step_cfg:
            step_cfg.channel = None

        if 'filter' not in sc:
            sc.filter = None

        if sc.input_crop and not isinstance(step_cfg.input_crop, Zone):
            step_cfg.input_crop = Zone.create('input_crop', step_cfg.input_crop, atype='region')

        if 'scale' not in step_cfg:
            step_cfg.scale = 10

        # Load frames from a video file if frame extraction didn't happen
        extraction = load_n_frames(
                            sc.filedata[sc.filelist[0]],
                            color_space='bgr',
                            # erate=1,
                            min_fcount=10,
                            max_fcount=20,
                            min_duration=4,
                            scene_threshold=20
                         )

        if extraction is None:
            result.status = 'error'
            result.reason = f"motion_direction frame extraction produced 0 frames for file: {sc.filelist[0]}"
            result.filedata = [(sc.filelist[0], "bio", sc.filedata[sc.filelist[0]])]
            logger.error(result.reason)
            return result

        frames = extraction[0]
        if 'diff_threshold' in sc and sc.diff_threshold:
            ref_img = frames[0].copy()
            filtered_frames = [ref_img]
            for img in frames[1:]:
                if mmse_compare_images(ref_img, img, lower_threshold=int(sc.diff_threshold)):
                    filtered_frames.append(img)
                ref_image = img.copy()
            frames = filtered_frames

        if len(frames) <= 1:
            result.status = 'error'
            result.reason = f"motion_direction frame extraction with diff filtering yielded {len(frames)} frames for file: {sc.filelist[0]}"
            logger.error(result.reason)
            return result

        # angle from x-axis
        if 'regions' in sc:
            results = self.object_direction(
                                        [f.copy() for f in frames],
                                        sc.regions,
                                        scale=sc.scale
                                )
        else:
            results = self.object_direction(
                                        [f.copy() for f in frames],
                                        AD({sc.step_name: sc}),
                                        scale=sc.scale
                                )

        logger.info(f"motion_direction results: {results}")
        for result in results.values():
            # if result.filter is not None:
            #     name, attr, values = result.filter
            #     result.name = name
            #     if attr == 'range':
            #         for val in values:
            #             if ':' in val:
            #                 minv, maxv = map(float, val.split(':'))
            #                 if minv < result.angle < maxv:
            #                     result.filedata = [(sc.filelist[0][:-4] + f"_{name}.mp4", "bio", sc.filedata[sc.filelist[0]])]
                                  # result.filedata.append((sc.filelist[0][:4]+f"_{name}.jpg', "bio", pack_image(frames[result.image_num])))
            #                     break
            #     elif result[attr] in values:
            #         result.filedata = [(sc.filelist[0][:-4] + f"_{name}.mp4", "bio", sc.filedata[sc.filelist[0]])]
            #         result.filedata.append((sc.filelist[0][:4]+f"_{name}.jpg', "bio", pack_image(frames[result.image_num])))
            # else:
            result.filedata = [(sc.filelist[0], "bio", sc.filedata[sc.filelist[0]])]
            # if 'image_num' in result:
            #     result.filedata.append((sc.filelist[0][:4]+'.jpg', "bio", frames[result.image_num]))


        if sc.step_name in results:
            return results[sc.step_name]
        else:
            rlist = []
            for r in results.values():
                tr = AD(sc)
                tr.update(r)
                tr.step_name = r.region_name
                rlist.append(tr)
            return rlist

    def preprocess_frame(self, frame, channel, crop, scale):
        if channel == 'blue':
            frame = frame[:,:,0]
        elif channel == 'green':
            frame = frame[:,:,1]
        elif channel == 'red':
            frame = frame[:,:,2]
        else:
            frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

        if crop is not None:
            frame = crop_pixels(frame, crop.x, crop.y, crop.w, crop.w)

        if scale is not None:
             frame = cv2.resize(frame, (frame.shape[0]//scale, frame.shape[1]//scale))
        return frame

    def reshape(self, image, target_height=300, target_width=300):
        height, width = image.shape
        if width == height:
            resized_image = cv2.resize(image, (target_height,target_width))
        elif height < width:
            resized_image = cv2.resize(image, (int(width * float(target_height)/height), target_width))
            cropping_length = int((resized_image.shape[1] - target_height) / 2)
            resized_image = resized_image[:,cropping_length:resized_image.shape[1] - cropping_length]
        else:
            resized_image = cv2.resize(image, (target_height, int(height * float(target_width) / width)))
            cropping_length = int((resized_image.shape[0] - target_width) / 2)
            resized_image = resized_image[cropping_length:resized_image.shape[0] - cropping_length,:]

        return cv2.resize(resized_image, (target_height, target_width))
