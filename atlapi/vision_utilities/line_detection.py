
from atlapi.attribute_dict import AD
from atlapi.image.utilities import (
    Zone,
    color_space_zone,
    load_image,
    check_image,
    default_zones,
    annotate,
)
from atlapi.common import *
import logging

logger = logging.getLogger('atlapi')
import numpy as np
import cv2
from typing import Tuple, Dict, List, Optional, Any, Union
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

def lineDetection(stepCfg: AD) -> AD:
    sc = stepCfg
    return_value = AD({"status": "success", "reason": None})
    try:
        color_space_image = load_image(
            stepCfg.filedata[stepCfg.filelist[0]],
            color_space=sc.color_space,
            crop=sc.input_crop
        )
        check_image(color_space_image, sc, return_value)
        for subject_name, subject in sc.subjects.items():
            if subject_name in return_value:
                raise MVError("Subject name {} already present in results".format(subject_name))

            if "zone" not in subject or not subject.zone:
                subject.zone = default_zones(color_space_image, atype="subject").image
            else:
                subject.zone = Zone.create(subject_name, subject.zone, atype="subject")

            zonecolor_space_image = color_space_zone(
                color_space_image, sc.color_space, subject.color_space, subject.zone
            )

            result_text = "absent"
            line_result = None

            if "gaussian_blur" in subject:
                if isinstance(subject.gaussian_blur, list):
                    blur_img = cv2.GaussianBlur(zonecolor_space_image, tuple(subject.gaussian_blur), 0)
                elif isinstance(subject.gaussian_blur, bool):
                    if subject.gaussian_blur:
                        blur_img = cv2.GaussianBlur(zonecolor_space_image, (3,3), 0)
                    else:
                        blur_img = zonecolor_space_image
            else:
                blur_img = cv2.GaussianBlur(zonecolor_space_image, (3,3), 0)

            lower, upper = auto_canny(blur_img)

            if "canny_edge" in subject:
                canny_threshold_edge_lower = subject.canny_edge[0]
                canny_threshold_edge_upper = subject.canny_edge[1]
            else:
                canny_threshold_edge_lower = lower
                canny_threshold_edge_upper = upper

            pixel_dilation = (
                1 + subject.extra_dilation if "extra_dilation" in subject else 1
            )

            if "rho_reference" in subject and subject.rho_reference:
                rho_reference = subject.rho_reference
                if "rho_range" in subject and subject.rho_range:
                    rho_range = subject.rho_range
                else:
                    rho_range = 10
            else:
                rho_reference = None

            if "theta_reference" in subject and subject.theta_reference:
                theta_reference = subject.theta_reference
                if "theta_range" in subject and subject.theta_range:
                    theta_range = subject.theta_range
                else:
                    theta_range = 0.04
            else:
                theta_reference = None

            if "quadrant" in subject and subject.quadrant:
                quadrant = subject.quadrant
            else:
                quadrant = 1

            imageEdges = cv2.Canny(
                blur_img,
                canny_threshold_edge_lower,
                canny_threshold_edge_upper,
                apertureSize=3,
            )

            kernel = np.ones((3, 3), np.uint8)

            zoneEdgeDilated = cv2.dilate(imageEdges, kernel, iterations=pixel_dilation)

            if 'line_length_threshold' not in subject:
                subject.line_length_threshold = 200

            houghLines = cv2.HoughLines(zoneEdgeDilated, quadrant, np.pi / 180, subject.line_length_threshold)

            if houghLines is not None:
                for hl in houghLines:
                    rho, theta = hl[0]
                    if rho_reference is None and theta_reference is None:
                        result_text = "present"
                        line_result = (rho, theta)
                        break
                    else:
                        if rho_reference is not None and theta_reference is None:
                            if (rho > rho_reference - rho_range and rho < rho_reference + rho_range):
                                result_text = "present"
                                line_result = (rho, theta)
                                break
                        elif rho_reference is None and theta_reference is not None:
                            if (theta > theta_reference - theta_range and theta < theta_reference + theta_range):
                                result_text = "present"
                                line_result = (rho, theta)
                                break
                        else:
                            if (rho > rho_reference - rho_range and rho < rho_reference + rho_range) and (theta > theta_reference - theta_range and theta < theta_reference + theta_range):
                                result_text = "present"
                                line_result = (rho, theta)
                                break

            # add annotation function
            subject.zone.label = result_text
            return_value[subject_name] = result_text
            return_value.line_result = line_result
            return_value.filedata = [
                (stepCfg.filelist[0][:-4] + "_annotated.jpg", "bio", annotate(color_space_image, [subject.zone], color_space=sc.color_space))
            ]


    except Exception as err:
        msg = "line_detection exception: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value


def auto_canny(image: np.ndarray, sigma: float=0.33) -> Tuple[float, float]:
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))

    return lower, upper
