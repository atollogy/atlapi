from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *

import cv2
from enum import Enum
import functools
import imutils
import numpy as np
from pydantic import BaseModel
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

from typing import Optional, List

class TemplateMatchAlgName(str, Enum):
    TM_CCOEFF = "TM_CCOEFF"
    TM_CCOEFF_NORMED = "TM_CCOEFF_NORMED"
    TM_CCORR = "TM_CCORR"
    TM_CCORR_NORMED = "TM_CCORR_NORMED"
    TM_SQDIFF = "TM_SQDIFF"
    TM_SQDIFF_NORMED = "TM_SQDIFF_NORMED"


TEMPLATE_METHODS = {
    "TM_CCOEFF": cv2.TM_CCOEFF,
    "TM_CCOEFF_NORMED": cv2.TM_CCOEFF_NORMED,
    "TM_CCORR": cv2.TM_CCORR,
    "TM_CCORR_NORMED": cv2.TM_CCORR_NORMED,
    "TM_SQDIFF": cv2.TM_SQDIFF,
    "TM_SQDIFF_NORMED": cv2.TM_SQDIFF_NORMED
}


class BoundingRectCfg(BaseModel):
    x: int
    y: int
    w: int
    h: int


class TemplateMatchCfg(BaseModel):
    alg: List[TemplateMatchAlgName]
    gray_conv: Optional[bool] = False
    auto_edge_conv: bool


class MovingObjLocationCfg(BaseModel):
    gray_conv: Optional[bool] = False
    moving_range_rect: Optional[BoundingRectCfg]
    ref_orig_s3_loc: Optional[str]
    ref_obj_rect: Optional[BoundingRectCfg]
    """
        The x, y coordinates of 'ref_obj_rect' is relatively to 'moving_range_rect'
    """
    ref_obj_s3_loc: str
    template_match: TemplateMatchCfg

"""
Note:
(1) When 'moving_range_rect' is being used, we don't need to separate image step that does cropping
(2) 'ref_orig_s3_loc' and 'ref_obj_rect' are used only at Ref Obj image generation time.
(3) 'ref_orig_s3_loc' specifies the S3 location of the original image used to generate the Ref Obj image
(4) 'ref_obj_s3_loc' must be present at Computer Vision image ingestion time.
"""

def match_loaded_template(
        area_img: np.ndarray,           # pyre-ignore[11]
        template_img: np.ndarray,       # pyre-ignore[11]
        location_cfg: MovingObjLocationCfg,
        draw_result: bool = False
) -> Optional[BoundingRectCfg]:

    if location_cfg.moving_range_rect:
        crop_box = location_cfg.moving_range_rect
        area_img_2 = area_img[
                     crop_box.y: crop_box.y+crop_box.h,
                     crop_box.x: crop_box.x+crop_box.w]
    else:
        area_img_2 = area_img

    if location_cfg.template_match.gray_conv:
        area_img_3 = cv2.cvtColor(area_img_2, cv2.COLOR_BGR2GRAY)
    else:
        area_img_3 = area_img_2

    if location_cfg.template_match.auto_edge_conv:
        area_img_4 = auto_canny(area_img_3)
    else:
        area_img_4 = area_img_3

    template_width, template_height = template_img.shape[::-1]

    # Apply template Matching
    if len(location_cfg.template_match.alg) == 0:
        # TODO to shift this validation logic to Pydantic
        raise ValueError("template_match.alg should have at least one algorithm entry")
    template_method = TEMPLATE_METHODS.get(location_cfg.template_match.alg[0])
    # TODO: to use the first algorithm for now
    if template_method is None:
        raise RuntimeError('template_method cannot be found')

    result = cv2.matchTemplate(area_img_4, template_img, template_method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
    if template_method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        top_left = min_loc
    else:
        top_left = max_loc

    rect = BoundingRectCfg(x=top_left[0], y=top_left[1],
                           w=template_width, h=template_height)
    if draw_result:
        draw_img = area_img_2.copy()
        cv2.rectangle(draw_img,
                      (rect.x, rect.y), (rect.x + rect.w, rect.y + rect.h),
                      color=(0, 255, 0))
        cv2.imshow("detected_object", draw_img)
        # drawing rectangle in Green color (under BGR space)
    return rect


@inlineCallbacks
def produce_ref_template_img(
        location_cfg: MovingObjLocationCfg,
        orig_img_loc: Optional[str] = None,
        output_img_loc: Optional[str] = None,
        aws=None
) -> Optional[np.ndarray]:

    if not orig_img_loc:
        orig_img_loc = location_cfg.ref_orig_s3_loc
    if not output_img_loc:
        output_img_loc = location_cfg.ref_obj_s3_loc

    if location_cfg.template_match.gray_conv:
        orig_ref_img = yield aws.s3.get_image(orig_img_loc, color_space='gray', cacheable=True)
    else:
        orig_ref_img= yield aws.s3.get_image(orig_img_loc, cacheable=True)

    crop_box = location_cfg.moving_range_rect
    if crop_box:
        crop_ref_img =  orig_ref_img.copy()[
                        crop_box.y:crop_box.y+crop_box.h,
                        crop_box.x:crop_box.x+crop_box.w]
    else:
        crop_ref_img = orig_ref_img.copy()

    crop_ref_img_2 = crop_ref_img.copy()

    if location_cfg.template_match.auto_edge_conv:
        crop_ref_img_3 = auto_canny(crop_ref_img_2.copy())
    else:
        crop_ref_img_3 = crop_ref_img_2.copy()

    target_ref_img =  crop_ref_img_3[
                    location_cfg.ref_obj_rect.y:
                    location_cfg.ref_obj_rect.y+location_cfg.ref_obj_rect.h,
                    location_cfg.ref_obj_rect.x:
                    location_cfg.ref_obj_rect.x+location_cfg.ref_obj_rect.w]

    if output_img_loc:
        res = yield aws.s3.put_path(output_img_loc, target_ref_img)

    return target_ref_img
