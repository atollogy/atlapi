from atlapi.attribute_dict import AD
from atlapi.image.utilities import (
    Zone,
    color_space_zone,
    load_image,
    check_image,
    default_zones,
    image_rotation,
    annotate,
)
import logging
logger = logging.getLogger('atlapi')
import numpy as np
from atlapi.common import *


def histogramSplit(stepCfg):
    return_value = AD({"status": "success", "reason": None})
    sc = stepCfg

    try:
        color_space_image = load_image(
            stepCfg.filedata[stepCfg.filelist[0]], color_space=sc.color_space, crop=sc.input_crop
        )
        check_image(color_space_image, sc, return_value)
        for subject_name, subject in sc.subjects.items():
            if subject_name in return_value:
                raise MVError("Subject name {} already present in results".format(subject_name))

            if "zone" not in subject or not subject.zone:
                subject.zone = default_zones(color_space_image, atype="subject").image
            else:
                subject.zone = Zone.create(subject_name, subject.zone, atype="subject")
            zonecolor_space_image = color_space_zone(
                color_space_image, sc.color_space, subject.color_space, subject.zone
            )

            if (
                "skew_adjust" in subject
                and subject.skew_adjust
                and "mask_patch" in subject
                and subject.mask_patch is not None
            ):
                zonecolor_space_image = image_rotation(
                    zonecolor_space_image, subject.skew_adjust, crop=subject.mask_patch
                )
            elif "skew_adjust" in subject and subject.skew_adjust:
                zonecolor_space_image = image_rotation(zonecolor_space_image, subject.skew_adjust)

            histogram, bins = np.histogram(zonecolor_space_image.ravel(), 256, [1, 256])
            smoothHistogram = histogramSmooth(histogram, subject.smooth_config)

            if len(smoothHistogram) > 0:
                objectHistogram = basicHistogramPeakFinder(smoothHistogram, subject.peak_threshold)
                objectHistogram = sorted(objectHistogram)

                if len(objectHistogram) > 0:
                    count_peak_point = 1
                    for j in range(1, len(objectHistogram)):
                        if objectHistogram[j] - objectHistogram[j - 1] > subject.peak_width_range:
                            count_peak_point += 1

                    if count_peak_point <= subject.peak_predict:
                        result_text = "closed"
                    else:
                        result_text = "open"

            # add annotation function
            subject.zone.label = result_text
            annotated_image = annotate(color_space_image, [subject.zone], color_space=sc.color_space)
            return_value[subject_name] = result_text
            return_value.filedata = [
                (stepCfg.filelist[0][:-4] + "_annotated.jpg", "bio", annotated_image)
            ]

            # self.trackSubjectValue(sc, subject_name, subject, return_value[subject_name])

    except Exception as err:
        msg = "histogram_split exception: {}".format(err)
        logger.exception(msg)
        return_value.status = "error"
        return_value.reason = msg

    return return_value


def histogramSmooth(histogram, smooth_threshold):
    for i in range(1, len(histogram) - 1):
        if abs(histogram[i] - histogram[i - 1]) < smooth_threshold:
            if histogram[i] - histogram[i - 1] > 0:
                histogram[i - 1] = histogram[i]
            else:
                histogram[i] = histogram[i - 1]

    return histogram


def basicHistogramPeakFinder(histogram, peak_threshold):
    originPeak = []

    if len(histogram) > 0:
        startPoint = 0
        peakPoint = [None, 0]
        changeSet = {"increase": 0, "decrease": 0}
        sol = {}

        for i in range(len(histogram)):
            if histogram[i] - startPoint > 0:
                changeSet["increase"] += histogram[i] - startPoint

            if changeSet["increase"] > peak_threshold:
                if histogram[i] - startPoint < 0:
                    changeSet["decrease"] += startPoint - histogram[i]

                if histogram[i] >= peakPoint[1]:
                    peakPoint = [i, histogram[i]]

                if changeSet["decrease"] > peak_threshold:
                    sol[peakPoint[0]] = peakPoint[1]
                    peakPoint = [None, 0]
                    changeSet["increase"], changeSet["decrease"] = 0, 0

                if i == len(histogram) - 1:
                    if peakPoint[0] is not None:
                        sol[peakPoint[0]] = peakPoint[1]

            if histogram[i] == 0:
                changeSet["increase"], changeSet["decrease"] = 0, 0

            startPoint = histogram[i]

        for key, value in sol.items():
            originPeak.append(key)

    return originPeak
