from atlapi.attribute_dict  import *
from atlapi.common import *

categories = AD({
    "ppe": {
        "primary": "operator",
        "qualities": {
            "hard_hat": {
                "alert": True,
                "annotation": "no hard hat",
                "conditions": {
                    "hard_hat": False
                },
                "enabled": True,
                "min_count": 6,
                "threshold": 0.6
            },
            "safety_vest": {
                "alert": True,
                "annotation": "no safety vest",
                "conditions": {
                    "safety_vest": False,
                    "safety_garment": False
                },
                "enabled": True,
                "min_count": 6,
                "threshold": 0.6
            },
        },
        "name": "ppe",
        "iou_threshold": 0
    },
    "cage": {
        "primary": "operator",
        "qualities": {
            "safe_cage": {
                "alert": True,
                "annotation": "cage violation",
                "conditions": {
                    "cage_violation": True
                },
                "deployed_cage_bbox": None,
                "deployed_threshold": 0.75,
                "deploying_threshold": 0.5,
                "enabled": True,
                "min_count": 2,
                "output_image": "anonymous.jpg",
                "raised_cage_bbox": None,
                "threshold": 0.16
            }
        },
        "name": "cage",
        "iou_threshold": 0
    }
})

violation =  AD({
          "safe_cage": {
              "correlation": {
                "event_interval": 4,
                "name": "violation_truck",
                "sources": ["38af294f1518"],
                "step_functions": [
                  "lpr"
                ],
                "unknown_image_index": -2
              },
              "categories": {
                "ppe": {
                    "qualities": {
                        "hard_hat": {
                            "alert": True,
                            "enabled": True,
                            "min_count": 6,
                            "threshold": 0.6
                        },
                        "safety_vest": {
                            "alert": True,
                            "enabled": True,
                            "min_count": 6,
                            "threshold": 0.6
                        }
                    }
                },
                "cage": {
                  "qualities": {
                      "safe_cage": {
                          "alert": True,
                          "deployed_cage_bbox": [
                            630,
                            300,
                            256,
                            239
                          ],
                          "deployed_threshold": 0.75,
                          "deploying_threshold": 0.5,
                          "enabled": True,
                          "min_count": 2,
                          "output_image": "anonymous.jpg",
                          "raised_cage_bbox": [
                            461,
                            134,
                            306,
                            237
                          ],
                          "threshold": 0.16
                      }
                  }
                }
              },
              "alert": {
                "active_alert_window": 7200,
                "atollogy_recipients": [
                  "todd@atollogy.com"
                ],
                "use_recipient_field": "ToAddresses"
              }
          }
        }
)

if __name__ == "__main__":
    rd = AD()
    rd.output = AD()

    if 'topics' not in rd.output:
        rd.output.topics = AD()

    if 'evaluations' not in rd.output:
        rd.output.evaluations = AD()

    for topic_name, ccfg in violation.items():
        topic = rd.output.topics[topic_name] = ccfg
        evaluations = rd.output.evaluations[topic_name] = AD()

        for category_name in ccfg.categories:
            if category_name in categories:
                conditions = unique(*[cfg.conditions.keys() for cfg in categories[category_name].qualities.values()])
                print(conditions)
                try:
                    category = AD(categories[category_name])
                    for k, v in category.deep_items():
                        topic.categories[category_name][k] = v
                except Exception as err:
                    pass

    print(rd.jstr())
