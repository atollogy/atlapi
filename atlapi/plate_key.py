
from common import *
from collections import Counter
import numpy as np
import pandas as pd


class Plate_Key(object):
    def __init__(self, candidates):
        self.candidates = sorted([AD(c) for c in candidates], key=lambda x: x['confidence'], reverse=True)
        self.top_plate = max(self.candidates, key=lambda x: x['confidence'])
        self.plate = self.top_plate.plate
        self.confidence = self.top_plate.confidence
        self.plates = [c['plate'] for c in self.candidates]
        pframe = pd.DataFrame(data=[list(p) for p in [c['plate'] for c in self.candidates]])
        [pframe[c].fillna(value=np.nan, inplace=True) for c in pframe.columns]
        self.pkey = '_'.join([''.join(pframe[c].value_counts().keys().to_list()) for c in pframe.columns])
        self.ranking = [Counter(pframe[c].value_counts().to_dict()) for c in pframe.columns]
        self.column_chars = [r.keys() for r in self.ranking]
        self.column_counts = [len(r.values()) for r in self.ranking]
        self.column_count = len(self.column_counts)
        self.column_ranks= [sum(r.values()) if len(r.values()) else 0 for r in self.ranking]
        self.rank = sum([sum(r.values()) for r in self.ranking if len(r.values())])
        self.pkey_ranking = '_'.join([','.join([f"{k}:{str(v)}" for k, v in counter.items()]) for counter in self.ranking])

    def like(self, other):
        score = self.match(other)
        if score >= 1.0:
            return True
        else:
            return False

    def match(self, other):
        my_score = self.score(other)
        their_score = other.score(self)
        the_score = (my_score + their_score)/2
        return the_score

    def __repr__(self):
        return str((self.plate, self.confidence, self.pkey, self.pkey_ranking, self.rank))

    def score(self, other):
        if not isinstance(other, Plate_Key):
            raise TypeError(f"object {repr(other)} is not a Plate_Key")

        if other.pkey ==  self.pkey:
            my_score = 2.0
        else:
            my_score = 0
            column_chars = []
            column_counts= []
            columns_matched = []
            column_scores = []
            for i, chars in enumerate(other.column_chars):
                column_chars.append([c for c in chars if c in self.ranking[i]])
                column_counts.append(len(column_chars[-1]))
                columns_matched.append(1 if column_counts[-1] > 0 else 0)
                column_scores.append([self.ranking[i][c] for c in chars if c in self.ranking[i]])
                my_score += (sum(column_scores[-1]) + column_counts[-1])
            my_score = (my_score * (sum(columns_matched)/self.column_count))/self.rank
        # print(f"{self.plate} - my_score for {other.plate}: {my_score}")
        return my_score

    def __str__(self):
        return self.pkey


class Place_Key(object):
    def __init__(self, location, plate_keys):
        self.location = location
        self.pkeys = AD()
        self.plates = AD()
        self.plate = None
        self.confidence = None
        self.pkey = None
        self.ranking = None
        self.column_chars = [[] for x in range(0, 8)]
        self.column_counts = [[] for x in range(0, 8)]
        self.column_ranks = [Counter() for x in range(0, 8)]
        if isinstance(plate_keys, Place_Key):
            if plate_keys.location == self.location:
                self.merge(plate_keys)
            else:
                raise AttributeError(f"Place_Key location mismatch: {self.location} != {plate_keys.location}")
        else:
            self.integrate(plate_keys)

    def __add__(self, other):
        tpk = Place_Key(self)
        tpk.integrate(other)
        return tpk

    def add_pkey(self, pkey, ranking):
        self.pkeys[pkey] = AD({
                                'column_chars': [r.keys() for r in ranking],
                                'column_counts': [len(r.values()) for r in ranking],
                                'column_ranks': [sum(r.values()) if len(r.values()) else 0 for r in ranking],
                                'max_rank': sum([sum(r.values()) for r in ranking if len(r.values())]),
                                'pkey': pkey,
                                'pkey_ranking': self.serialize_ranking(ranking),
                                'ranking': ranking
                            })

    def calculate_pkey_from_pkey(self, pkey):
        pkg_frame = pd.DataFrame(data=[list(c) for c in pkey.split('_')]).transpose()
        pframe = pd.DataFrame([np.array(pkg_frame[x]) for x in pkg_frame.columns]).transpose()
        pkey = '_'.join([''.join(pframe[c].value_counts().keys().to_list()) for c in pframe.columns])
        ranking = self.calcualte_ranking(pframe)
        return (pkey, ranking)

    def calculate_pkey_from_pkey_block(self):
        pkg_frame = pd.DataFrame(data=[p.split('_') for p in self.pkeys]).transpose()
        pframe = pd.DataFrame(data=[list(''.join([c for c in list(p) if c])) for p in np.array(pkg_frame)]).transpose()
        pkey = '_'.join([''.join(pframe[c].value_counts().keys().to_list()) for c in pframe.columns])
        ranking = self.sum_ranking()
        return (pkey, ranking)

    def calcualte_ranking(self, target):
        if isinstance(target, pd.DataFrame):
            return [Counter(target[c].value_counts().to_dict()) for c in target.columns]
        else:
            # pkey_ranking in the form of 7:10_e:8.r:2_..._char:count.char:count_
            ranking = [Counter() for x in range(0, 8)]
            for c, col in enumerate(target.split('_')):
                rows = [r.split(':') for r in col.split(',')]
                for k, v in rows:
                    ranking[c][k] += int(v)
            return ranking

    def generate(self):
        if len(self.pkeys):
            if (self.pkeys) == 1:
                self.pkey = self.pkeys[0]
                self.ranking = self.pkeys[self.pkey].ranking
            else:
                self.pkey, self.ranking = self.calculate_pkey_from_pkey_block()
                self.pkey_ranking = self.serialize_ranking(self.ranking)
            self.avg_rank = np.mean([pk.max_rank for pk in self.pkeys.values()])
            self.top_rank = max([pk.max_rank for pk in self.pkeys.values()])
            self.column_chars = [r.keys() for r in self.ranking]
            self.column_counts = [len(r.values()) for r in self.ranking]
            self.column_ranks = [sum(r.values()) if len(r.values()) else 0 for r in self.ranking]
            self.max_rank = sum(self.column_ranks)
            print(f"pkey: {self.pkey} avg_rank: {self.avg_rank}")

    def integrate(self, other):
        plates = []
        pkeys = []
        if isinstance(other, Place_Key):
            self.merge(other)
        elif isinstance(other, (AD, dict)) and 'plate' in other:
            pk = Plate_Key
        elif isinstance(other, (list, tuple)):
            for o in other:
                if isinstance(o, Plate_Key):
                    self.merge(o)
                elif isinstance(o, tuple) and len(o) == 2:
                    pkey = o[0]
                    ranking = self.ranking(o[1])
                    self.add_pkey(pkey, ranking)
                elif isinstance(o, (AD, dict)) and 'plate' in o:
                    if o['plate'] in self.plates:
                        self.plates[o['plate']].count += 1
                    else:
                        self.plates[o['plate']] = AD({'count': 1, 'confidence': [], 'plate': o['plate']})
                    if 'confidence' in o:
                        self.plates[o['plate']].confidence.append(o['confidence'])
                elif isinstance(o, str) and '_' in o and '::' not in o:
                    pkey, ranking = self.calculate_pkey_from_pkey(o)
                    self.add_pkey(pkey, ranking)
                elif isinstance(o, str) and '_' in o and '::' in o:
                    pkey, rstr = o.split('::')
                    ranking = self.ranking(rstr)
                    self.add_pkey(pkey, ranking)
                elif isinstance(o, str) and len(o) <= 8:
                    self.plates[o] = AD({'count': 1, 'confidence': [], 'plate': o})
        elif isinstance(other, str) and '_' in other and '::' not in other:
            pkey, ranking = self.calculate_pkey_from_pkey(other)
            self.add_pkey(pkey, ranking)
        elif isinstance(other, str) and '_' in other and '::' in other:
            pkey, rstr = other.split('::')
            ranking = self.ranking(rstr)
            self.add_pkey(pkey, ranking)
        elif isinstance(other, str) and len(other) <= 8:
            self.plates[other] = AD({'count': 1, 'confidence': [], 'plate': other})

        self.generate()

    __iadd__ = integrate

    def merge(self, pk_obj):
        if isinstance(pk_obj, Plate_Key):
            self.plates = unique(self.plates, pk_obj.plates)
            self.pkeys.update(pk_obj.pkeys)
            self.generate()

    def __repr__(self):
        return f"{self.pkey}::{self.pkey_ranking}"

    def serialize_ranking(self, ranking):
        counters = [','.join([f"{k}:{str(v)}" for k, v in counter.items()]) for counter in ranking]
        pkey_ranking = '_'.join(counters)
        return pkey_ranking

    def match(self, other):
        if not isinstance(other, Plate_Key):
            other = Plate_Key(other)
        if other.pkey in self.pkeys:
            score = self.pkeys[other.pkey].max_rank
        else:
            score = 0
            column_chars = []
            column_counts= []
            column_scores = []
            for i, chars in enumerate(other.column_chars):
                column_chars.append([c for c in chars if c in self.ranking[i]])
                column_counts.append(len(column_chars[-1]))
                column_scores.append([self.ranking[i][c] for c in chars if c in self.ranking[i]])
                score += sum(column_scores[-1])

        match = score/self.top_rank
        return (score, match)

    def __str__(self):
        return self.pkey

    def sum_ranking(self):
        ranking = [Counter() for x in range(0, 8)]
        counters = [pk.ranking for pk in self.pkeys.values()]
        for x in range(0, 8):
            for c in counters:
                if x < len(c):
                    ranking[x].update(c[x])
        return ranking



########################################################################################################################
if __name__ == "__main__":

    plate1='R547183'
    conf1=90.48078156
    cand1=[{"matches_template": 0, "plate": "R547183", "confidence": 90.48078156}, {"matches_template": 0, "plate": "R541183", "confidence": 78.51911926}, {"matches_template": 0, "plate": "R54713", "confidence": 77.34101105}, {"matches_template": 0, "plate": "R57183", "confidence": 77.337883}, {"matches_template": 0, "plate": "R5A7183", "confidence": 77.22924805}, {"matches_template": 0, "plate": "R547103", "confidence": 77.22261047}, {"matches_template": 0, "plate": "R54113", "confidence": 65.37934875}, {"matches_template": 0, "plate": "R51183", "confidence": 65.3762207}, {"matches_template": 0, "plate": "R5A1183", "confidence": 65.26759338}, {"matches_template": 0, "plate": "R541103", "confidence": 65.26094818}]
    pk1 = Plate_Key(plate1, conf1, cand1)

    plate2='R537713'
    conf2=93.83930206
    cand2=[{"matches_template": 0, "plate": "R537713", "confidence": 93.83930206}]
    pk2 = Plate_Key(plate2, conf2, cand2)

    plate3='R537714'
    conf3=92.9029541
    cand3=[{"matches_template": 0, "plate": "R537714", "confidence": 92.9029541}, {"matches_template": 0, "plate": "R53714", "confidence": 79.94360352}, {"matches_template": 0, "plate": "R53B714", "confidence": 79.46370697}, {"matches_template": 0, "plate": "R539714", "confidence": 79.44104767}, {"matches_template": 0, "plate": "R531714", "confidence": 79.43972015}, {"matches_template": 0, "plate": "R538714", "confidence": 79.4394989}]
    pk3 = Plate_Key(plate3, conf3, cand3)

    print(pk.pkey, pk.ranking)
