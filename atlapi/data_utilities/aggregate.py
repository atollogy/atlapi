from atlapi.attribute_dict import AD

import jmespath

import logging

logger = logging.getLogger('atlapi')

class AggregateStep:
    def __init__(self):
        pass

    def process(self, step_cfg: AD) -> AD:
        '''
            "subjects": {
                "<subject_name>": {
                    "query": str
                }
            }
        }
        '''
        return_value = AD({
            'status': 'success',
            'reason': None,
        })

        try:
            for subject_name, subject in step_cfg.subjects.items():
                if 'query' not in subject:
                    return_value[subject_name] = dict(
                        status='error',
                        reason='query not specified'
                    )
                    continue

                try:
                    query = jmespath.compile(subject.query)
                    return_value[subject_name] = query.search(
                        step_cfg.input_parameters
                    )
                except Exception as err:
                    return_value[subject_name] = dict(
                        status='error',
                        reason=f'{err}'
                    )

        except Exception as err:
            msg = f'AggregateStep error: {err}'
            logger.exception(msg)
            return_value.status = "error"
            return_value.reason = msg

        return return_value
