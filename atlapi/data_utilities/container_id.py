
from atlapi.core import Core
from stdnum.exceptions import InvalidLength, InvalidFormat, InvalidChecksum
from stdnum.iso6346 import compact,validate,format

from twisted.internet.defer import inlineCallbacks

class ContainerID(Core):
    def __init__(self):
        Core.__init__(self, 'func_container_id')

        if 'db' in self.svcs:
            self.db = self.svcs.db
        else:
            self.db = None
        
    @inlineCallbacks
    def container_id(self, step_cfg):
        return_value = {'status': 'success'}

        try:
            # `key` allows control over the field to get the key from
            search_key = step_cfg.get('key', 'text')
            container_id = step_cfg['input_parameters'].get(search_key)

            # TODO: Reduce duplication of files in s3
            # Copy over image
            return_value['filedata'] = [
                    (
                        step_cfg['filelist'][0],
                        "bio",
                        step_cfg['filedata'][step_cfg['filelist'][0]],
                    )
                ]

            # we sometimes get an empty string
            if container_id is None or len(container_id) == 0:
                return_value['warning'] = 'Container ID Not Found'
                return return_value

            # validation throws errors if invalid
            validate(container_id)
            container_id = format(container_id)

            return_value['container_id'] = container_id
            return_value['checksum_valid'] = True

            serial_number = container_id.split(' ')[1]

            if self.db is not None:
                yield self.db.asset_attribute.add_identifier(
                    step_cfg['cgr'],
                    container_id,
                    serial_number
                )

        except InvalidLength:
            return_value['warning'] = "Invalid Length"

            if len(compact(container_id)) == 10:
                container_id = container_id + '-'
                return_value['warning'] = "Checksum digit not received"

            return_value['container_id'] = format(container_id)
            return_value['checksum_valid'] = False
            
        except InvalidFormat:
            return_value['warning'] = "Invalid Format"

            if len(container_id) == 11:
                return_value['container_id'] = format(container_id)

            return_value['container_id'] = None
            return_value['checksum_valid'] = False
        except InvalidChecksum:
            return_value['warning'] = "Invalid Checksum"
            return_value['container_id'] = format(container_id)
            return_value['checksum_valid'] = False
        except Exception as err:
            #return_value['status'] = "error"
            return_value['message'] = f"container_id error: {err}"


        return return_value