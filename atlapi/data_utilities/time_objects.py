#!/usr/bin/env python3.6

from atlapi.attribute_dict import *
from atlapi.common import *

class TIndex(object):
    dh_names = ['day', 'hour', '15m', '5m', '1m', 'tframe', 'domain', 'tnumber']

    def __init__(self, ts, domain=1):
        if isinstance(ts, TIndex):
            self.dt = ts.dt
        else:
            self.dt = to_datetime(ts)
        self.domain = domain
        self.ts = self.dt.timestamp()
        self.tn = int(self.ts/domain)
        self.m = int(self.ts/60)
        self.p = int(self.m/5)
        self.q = int(self.m/15)
        self.h = int(self.m/30)
        self.H = int(self.m/60)
        self.B = int(self.H/4)
        self.Q = int(self.H/6)
        self.O = int(self.H/8)
        self.D = int(self.H/24)
        if (15 % domain) == 0:
            self.tframe = int(self.tn/(15/domain))
        elif (30 % domain) == 0:
            self.tframe = int(self.tn/(30/domain))
        elif domain < 60:
            self.tframe = self.m
        else:
            self.tframe = int(self.m/(domain/60))
        self.DHIndex = [self.D, self.H, self.q, self.p, self.m, self.tframe, self.domain, self.tn]
        self.DQHIndex = [self.D, self.Q, self.H, self.q, self.p, self.m, self.tframe, self.domain, self.tn]
        self.DBHIndex = [self.D, self.B, self.H, self.q, self.p, self.m, self.tframe, self.domain, self.tn]
        self.DOHIndex = [self.D, self.O, self.H, self.q, self.p, self.m, self.tframe, self.domain, self.tn]

    def __str__(self):
        return '.'.join([str(x) for x in self.DHIndex])

    def __repr__(self):
        return self.__str__()

    def __cmp__(self, other):
        if isinstance(other, TIndex):
            return self.tn == other.tn
        else:
            return self.tn == TIndex(other).tn

    def adjacent(self, other, sash=1):
        return abs(self.distance(other)) <= sash

    def as_dict(self):
        return AD(dict(zip(self.dh_names, self.DHIndex)))

    def distance(self, other):
        if isinstance(other, TIndex):
            otn = int(other.ts/self.domain)
        else:
            otn = int(to_datetime(other).timestamp()/self.domain)
        return (otn - self.tn)

    def frame_distance(self, other):
        if not isinstance(other, TIndex):
            o = TIndex(other)
        else:
            o = other
        return (o.tframe - self.tframe)

    def in_tframe(self, other, sash=0):
        if not isinstance(other, TIndex):
            o = TIndex(other)
        else:
            o = other
        return (abs(self.frame_distance(o)) <= sash)

    def in_wframe(self, other, sash=0):
        if not isinstance(other, TIndex):
            o = TIndex(other)
        else:
            o = other
        return (abs(self.window_distance(o)) <= sash)

    def labels(self):
        return '.'.join(self.dh_names)

    def root(self, other):
        rpath = []
        if isinstance(other, TIndex):
            oidx = other.DHIndex
        else:
            oidx = TIndex(other).DHIndex
        for p in zip(self.DHIndex, oidx):
            if (p[0] - p[1]) != 0:
                break
            else:
                rpath.append(p[0])
        if len(rpath):
            return '.'.join([str(x) for x in rpath])
        else:
            return None

    def timestamp(self):
        return self.ts

    def window_distance(self, other):
        if not isinstance(other, TIndex):
            o = TIndex(other)
        else:
            o = other
        return (o.m - self.m)

