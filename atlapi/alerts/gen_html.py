#!/usr/bin/env python3.6
from safe_cage import *
from atlapi.common import *

if __name__ == "__main__":
    with open('alerts/alert.html', 'w') as fh:
        fh.write(render_safe_cage_alert_email('seventh level of hell',
                                     'hell',
                                     datetime.utcnow(),
                                     datetime.utcnow(),
                                     AD({'safe_rack_violation': {'image_link': 'media.atollogy.com/<cgr>/<gwid>/<camera_id>/<ctime>/<step_name>/<fname>'},
                                                                 'violation_truck': {'identifier': 'oh fuck', 'image_link': 'media.atollogy.com/<cgr>/<gwid>/<camera_id>/<ctime>/<step_name>/<fname>'}}
                                        )))
