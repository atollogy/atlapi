#!/usr/bin/env python3

from atlapi.common import *
from atlapi.attribute_dict import *
from atlapi.consul_kvs import CAD_Cache
from atlapi.core import Core
from atlapi.aws import Aws
from atlapi.database.db import DB
from atlapi.routes import CoreServer

import argparse
import json
import os
import pyconsul as consul
import random
import sys

from twisted.internet import reactor
from twisted.conch import manhole, manhole_ssh
from twisted.conch.ssh.keys import Key
from twisted.cred import portal, checkers
from twisted.internet.defer import ensureDeferred, inlineCallbacks, maybeDeferred, waitForDeferred


FUNCTION_VERSION = MODULE_VERSION = (
    shcmd('git log -n1 -p -- {}| grep "^commit"'.format(os.path.abspath(__file__)))[0][0]
    .split(" ")[-1]
    .strip("\n")[0:8]
)
print("{} version: {}".format(__file__, MODULE_VERSION))


# --------------------------------------------------------------------------------------------------------------


def get_manhole_factory(namespace, **passwords):

    def get_manhole(arg):
        return manhole.ColoredManhole(namespace)

    realm = manhole_ssh.TerminalRealm()
    realm.chainedProtocolFactory.protocolFactory = get_manhole
    p = portal.Portal(realm)
    p.registerChecker(checkers.InMemoryUsernamePasswordDatabaseDontUse(**passwords))
    f = manhole_ssh.ConchFactory(p)
    f.publicKeys = {"ssh-rsa": Key.fromFile("keys/manhole.pub")}
    f.privateKeys = {"ssh-rsa": Key.fromFile("keys/manhole")}
    return f


def start(start=True):
    parser = argparse.ArgumentParser(description="atollogy api server")
    parser.add_argument("-l", "--log_level", default=None, help="set logging level (ex: 10, 20)")
    parser.add_argument("-c", "--console", default=False, action='store_true', help="start interactive debugging console")
    parser.add_argument("-H", "--HELP", help="Display extended help documentation")
    ARGS = parser.parse_args()

    ### Set up the main service objects
    ### aws module
    aws = Aws()
    ### Database Access
    database = DB(version=FUNCTION_VERSION)

    ### API server
    atlapi_server = CoreServer(
        aws=aws,
        db=database,
        fver=FUNCTION_VERSION
    )

    # atlapi_server.APP.listenTCP(2222, get_manhole_factory(globals(), admin='admin'))

    # set app resource for twistd/klein
    resource = atlapi_server.APP.resource
    if ARGS.console:
        import manhole
        manhole.install(
            locals=dict(globals(), **locals()),
            strict=False
        )
    if start:
        try:
            atlapi_server.APP.run("0.0.0.0", BCM.endpoints.atlapi.twistd_port)
        except Exception as err:
            logger.exception(f"atlapi.server exception: " + repr(err))
    else:
        return atlapi_server, aws, database


if __name__ == "__main__":
    start()
