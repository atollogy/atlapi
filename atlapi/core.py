#!/usr/bin/env python3

from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.consul_kvs import CAD_Cache
from atlapi.image.utilities import *

import boto3
import botocore
import base64
import concurrent.futures
import pyconsul as consul
from datadog import initialize
from datadog import api as ddapi
from datetime import datetime, date, timedelta, timezone
from dateutil.parser import parse as dateutil_parser
import email
import email.message
from email import policy
from email.parser import BytesParser, Parser
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart, MIMEBase
from email.mime.application import MIMEApplication
from func_timeout import func_timeout, func_set_timeout, FunctionTimedOut
import functools
import gzip
import io
import json
from klein import Klein
import logging
import os
import mimetypes
from multiprocessing import Pool
import numpy as np
import re
import requests
import requests.exceptions
import sys
import tarfile
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread, deferToThreadPool
from twisted.internet.defer import inlineCallbacks, ensureDeferred, maybeDeferred, returnValue, DeferredQueue
from twisted.python.threadpool import ThreadPool

import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from urllib3.util.retry import Retry
import zipfile

logger = logging.getLogger('atlapi')

### load the version metadata
with open(os.path.dirname(__file__) + "/version.json") as vf:
    VERSION = json.load(vf)["version"]

_klein_app = Klein()
reactor.suggestThreadPoolSize(256)


dd_options = AD({
    'api_key': BCM.endpoints.datadog.core_api_key,
    'app_key': BCM.endpoints.atlapi.dd_app_key
    })
initialize(**dd_options)
metric_prefix = f'atl.{BCM.env}.atlapi'

core_keys = ['_customerModel', 'active_cgrs', 'aws_region', 'brq', 'carq', 'cfg', 'cgr', 'edrq', 'env',
             'descriptorq', 'irq', 'metricq', 'mvapi_models', 'mvrq', 'nodename', 'scode', 'svcs', 'vpq']

def _initProcessPool(reactor, *args, **kwargs):
    if not hasattr(reactor, 'processpool') or reactor.processpool is None:
        reactor.processpool = Pool(*args, **kwargs)
        reactor.processpoolShutdownID = reactor.addSystemEventTrigger(
            'during', 'shutdown', _stopProcessPool, reactor)

def _stopProcessPool(reactor):
    reactor.processpoolShutdownID = None
    pool = getattr(reactor,'processpool',None)
    if pool:
        pool.close()
        pool.join()
        reactor.processpool = None

class Core(object):
    """Atollogy REST Core Class """

    APP = _klein_app  # Klein App Resource
    API_VERSION = VERSION

    icon_fname = f"{os.path.dirname(os.path.realpath(__file__))}/static/atl_favicon.ico"
    with open(icon_fname, "rb") as _ico:
        favicon = _ico.read()

    def __init__(self, svc_name, *args, **kwargs):
        _initProcessPool(reactor)
        if svc_name:
            if svc_name in BCM.svcs.deep_keys():
                raise APIError(f"api.initialization exception service name already initialized: " + svc_name)
            else:
                logger.info(f"Core.__init__ service name: " + svc_name)
                BCM.svcs[svc_name] = self

        self.__dict__['tree'] = BCM
        for k in core_keys:
            self.__dict__[k] = BCM[k]

        self.args = args
        for k, v in kwargs.items():
            if k != "APP" and k[0] != "_" and k not in self.__dict__:
                self.__dict__[k.upper()] = v

    # def __call__(self, key):
    #     return self.tree.get(key)

    def _json_safe(self, obj):
        """JSON dumper for objects not serializable by default json code"""
        try:
            msg = json.dumps(obj, cls=JSONEncoder)
        except Exception:
            msg = repr(obj)
            raise
        return msg

    def requests_retry_session(self, backoff_factor=0.3, status_forcelist=(500, 502, 504), retries=3, session=None):
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
            status_forcelist=status_forcelist,
        )
        adapter = requests.adapters.HTTPAdapter(max_retries=retry)
        session.mount("http://", adapter)
        session.mount("https://", adapter)
        return session

    def _http_get(self, *args, **kwargs):
        if "url" not in kwargs:
            raise APIError("_http_get_json Error: URL is a required parameter")
        elif len(kwargs["url"]) < 10:
            raise APIError("_http_get_json Error: Invalid URL passed: {}".format(kwargs["url"]))
        else:
            if "backoff_factor" not in kwargs:
                kwargs["backoff_factor"] = 0.3
            if "headers" not in kwargs or kwargs["headers"] == None:
                kwargs["headers"] = {}
            if "params" not in kwargs:
                kwargs["params"] = {}
            if "retries" not in kwargs:
                kwargs["retries"] = 3
            if "status_forcelist" not in kwargs:
                kwargs["status_forcelist"] = (500, 502, 504)
            if "timeout" not in kwargs or kwargs["timeout"] == None:
                kwargs["timeout"] = 30
            if "verify" not in kwargs:
                kwargs["verify"] = None
            try:
                resp = self.requests_retry_session(
                            backoff_factor=kwargs["backoff_factor"],
                            retries=kwargs["retries"],
                            status_forcelist=kwargs["status_forcelist"]).get(
                                url=kwargs["url"],
                                headers=kwargs["headers"],
                                params=kwargs["params"],
                                timeout=kwargs["timeout"],
                                verify=kwargs["verify"],
                            )
                resp.raise_for_status()
            except requests.exceptions.HTTPError as err:
                logger.exception(f"_http_post: failure response " + resp(err))
                raise
            else:
                logger.info(f"_http_post: {kwargs['url']} - response {resp}")
                return resp

    def _http_post(self, *args, **kwargs):
        if "payload" not in kwargs:
            raise APIError("_http_post_json Error: Empty payload")
        elif "url" not in kwargs:
            raise APIError("_http_post_json Error: URL is a required parameter")
        elif len(kwargs["url"]) < 10:
            raise APIError("_http_post_json Error: Invalid URL passed: {}".format(kwargs["url"]))
        else:
            if "backoff_factor" not in kwargs:
                kwargs["backoff_factor"] = 0.3
            if "headers" not in kwargs or kwargs["headers"] == None:
                kwargs["headers"] = {}
            if "params" not in kwargs:
                kwargs["params"] = {}
            if "retries" not in kwargs:
                kwargs["retries"] = 3
            if "status_forcelist" not in kwargs:
                kwargs["status_forcelist"] = (500, 502, 504)
            if "timeout" not in kwargs or kwargs["timeout"] == None:
                kwargs["timeout"] = 30
            if "verify" not in kwargs:
                kwargs["verify"] = None
            try:
                resp = self.requests_retry_session(
                                    backoff_factor=kwargs["backoff_factor"],
                                    retries=kwargs["retries"],
                                    status_forcelist=kwargs["status_forcelist"]).post(
                                        url=kwargs["url"],
                                        headers=kwargs["headers"],
                                        params=kwargs["params"],
                                        json=kwargs["payload"],
                                        timeout=kwargs["timeout"],
                                        verify=kwargs["verify"],
                                    )
                resp.raise_for_status()
            except requests.exceptions.HTTPError as err:
                logger.exception(f"_http_post: failure response " + resp(err))
                raise
            else:
                logger.info(f"_http_post: {kwargs['url']} - response {resp}")
                return resp

    @inlineCallbacks
    def _get_external_api(self, url, headers=None, params=None):
        try:
            resp = yield deferToThread(
                        functools.partial(
                            self._http_get,
                            url=url,
                            headers=headers,
                            params=params,
                            timeout=30,
                            verify=True,
                        )
                    )
            return resp.json()
        except Exception as err:
            return json_error("External API Call Exception: {}".format(err), True)

    @inlineCallbacks
    def _post_external_api(self, url, payload, headers=None, params=None):
        try:
            resp = yield deferToThread(
                        functools.partial(
                            self._http_post,
                            url=url,
                            payload=payload,
                            headers=headers,
                            params=params,
                            timeout=30,
                            verify=True,
                        )
                    )
            return AD(resp.json())
        except Exception as err:
            return json_error("External API Call Exception: {}".format(err), True)

    @inlineCallbacks
    def _req_data(self, func_name, request):
        try:
            def itemize(d):
                if hasattr(d, "items"):
                    _td = {}
                    for k, val in d.items():
                        k = k.lower()
                        if isinstance(k, bytes):
                            k = k.decode("utf-8")
                        if isinstance(val, list) and len(val) == 1:
                            if isinstance(val[0], bytes):
                                _td[k] = val[0].decode("utf-8")
                            else:
                                _td[k] = val[0]
                        elif isinstance(val, list):
                            _td[k] = itemize(val)
                        else:
                            _td[k] = val
                    return _td
                elif isinstance(d, list):
                    _tl = []
                    for i in d:
                        if hasattr(i, "items"):
                            _tl.append(itemize(i))
                        elif isinstance(i, bytes):
                            _tl.append(i.decode("utf-8"))
                        else:
                            _tl.append(i)
                    return _tl
                elif isinstance(d, bytes):
                    return d.decode("utf-8")
                else:
                    return d

            hdrs = AD(itemize(request.requestHeaders._rawHeaders))
            args = AD(itemize(request.args) if len(request.args) else {})

            if 'customer_id' in hdrs:
                customer_id = hdrs.customer_id
            else:
                customer_id = None

            if 'gateway_id' in hdrs:
                gateway_id = hdrs.gateway_id
            else:
                gateway_id = None

            if 'content-type' in args:
                hdrs["content-type"] = args["content-type"]

            if "mv_routing" in hdrs:
                hdrs["mv_routing"] = AD.loads(gzip.decompress(base64.b64decode(hdrs["mv_routing"].encode())))

            if "content-encoding" not in hdrs:
                hdrs["content-encoding"] = "identity"

            if 'nodename' not in hdrs:
                if 'db' in self.svcs:
                    try:
                        db = self.svcs.db
                        hdrs.nodename = yield db.resolver.get_nodename(customer_id, gateway_id)
                    except Exception as err:
                        logger.exception(f"api._req_data get nodename from resolver error: {err}")
                        hdrs.nodename = get_nodename(customer_id, gateway_id)
                else:
                    hdrs.nodename = gateway_id

            nodename = hdrs.nodename

            if request.method in [b"POST", b"PUT"]:
                raw_data = request.content.read()
                if "content-encoding" in hdrs and hdrs["content-encoding"] == "gzip":
                    semi_raw_data = gzip.decompress(raw_data)
                elif "content-encoding" in hdrs and hdrs["content-encoding"] == "zip":
                    semi_raw_data = zipfile.ZipFile(io.BytesIO(raw_data))
                else:
                    semi_raw_data = raw_data

                if "content-type" in hdrs and hdrs["content-type"] == "application/json":
                    data =AD.loads(semi_raw_data)
                elif "content-type" in hdrs and hdrs["content-type"] == "application/zip":
                    data = AD({"filelist": semi_raw_data.namelist()})
                    for fname in data.filelist:
                        if fname == "data.json":
                            data["metadata"] = AD.loads(semi_raw_data.read(fname))
                        else:
                            data[fname] = semi_raw_data.read(fname)
                elif "content-type" in hdrs and hdrs["content-type"] == "application/x-tar":
                    semi_raw_data = tarfile.open(io.BytesIO(raw_data), "r")
                    data = AD({"filelist": semi_raw_data.getnames()})
                    for fname in data.filelist:
                        if fname == "data.json":
                            data["metadata"] = AD.loads(semi_raw_data.extractfile(fname))
                        else:
                            data[fname] = semi_raw_data.extractfile(fname)
                elif "content-type" in hdrs and "multipart" in hdrs["content-type"]:
                    data = AD({"order": []})
                    try:
                        mver = hdrs["mime-version"].encode() if "mime-version" in hdrs else b"1.0"
                        semi_raw_data = (
                            b"Mime-Version: "
                            + mver
                            + b"\r\nContent-Type: "
                            + hdrs["content-type"].encode()
                            + b"\r\n"
                            + semi_raw_data
                        )
                        msg = email.message_from_bytes(semi_raw_data)
                        for p in msg.walk():
                            pc_type = p.get_content_type()
                            if pc_type == "multipart/related" or "Content-ID" not in p.keys():
                                continue

                            cid_parts = tuple(p["Content-ID"][1:-1].split("@")[0].split("/"))
                            # logger.info("cid parts: {}".format(cid_parts))

                            if len(cid_parts) == 4 and pc_type == "application/json":
                                gateway_id, camera_id, ctime, step_name = cid_parts
                                filename = "index.json"
                                fpath = "index/index.json"
                            else:
                                gateway_id, camera_id, ctime, step_name, filename = cid_parts
                                fpath = "{}/{}".format(step_name, filename)

                            ctime = f"{to_datetime(ctime).timestamp():.6f}"
                            hdrs.collection_time = ctime

                            camera_id = f"video{camera_id}"
                            bundle_id = tuple([str(i) for i in [
                                    camera_id,
                                    ctime,
                                    hdrs["customer_id"],
                                    gateway_id,
                                    hdrs.nodename]]
                                )
                            body = yield p.get_payload(decode=True)
                            if bundle_id not in data.order:
                                logger.info(f"bundle_id: {bundle_id}")
                                data.order.append(bundle_id)
                                data[bundle_id] = AD()
                                data[bundle_id].stepData = AD()

                                metadata = AD.loads(body)
                                metadata["steps"] = [AD(s) for s in metadata["steps"]]
                                metadata.collection_time = metadata.collectionTime = ctime
                                metadata.startTime = metadata.start_time = to_datetime(metadata["steps"][0]["startTime"]).isoformat()
                                if 'endTime' in metadata.steps[0] and metadata.steps[0].endTime:
                                    metadata.endTime = metadata.end_time = to_datetime(metadata.steps[0].endTime).isoformat()
                                else:
                                    metadata.endTime = metadata.end_time = metadata.start_time

                                bundle = tuple([str(i) for i in [camera_id,
                                                            ctime,
                                                            hdrs.customer_id,
                                                            gateway_id,
                                                            hdrs.nodename]])
                                bundle_dict = AD(dict(
                                    zip(
                                        [
                                            "camera_id",
                                            "collection_time",
                                            "customer_id",
                                            "gateway_id",
                                            "nodename"
                                        ],
                                        bundle,
                                    )
                                ))

                                data[bundle_id].update(bundle_dict)
                                data[bundle_id].update(metadata)
                                data[bundle_id].bundle = bundle
                                data[bundle_id].cid = ":".join(bundle)

                                base_event = data[bundle_id].stepData.base_event = AD(data[bundle_id].deep_items())
                                base_event.args = args
                                base_event.headers = hdrs
                                base_event.update(parse_fqdn(hdrs.nodename))
                                base_event.filedata = AD({"metadata.json": metadata})
                                base_event.filelist = ["metadata.json"]
                                base_event.function = "metadata"
                                base_event.function_version = 0
                                base_event.images = []
                                base_event.inputParameters = AD()
                                base_event.metadata = metadata
                                base_event.output = AD({"status": "success", "reason": "", "media_info": {}})
                                base_event.step_config_version = 0
                                base_event.name = base_event.step_name = "base_event"

                                for step in metadata.steps:
                                    step = AD(step)
                                    step.args = args
                                    step.headers = hdrs
                                    step.update(parse_fqdn(hdrs.nodename))
                                    step.filedata = AD()
                                    step.filelist = []
                                    step.images = []
                                    if step.name == 'None' or step.name is None:
                                        step.name = step.step_name = 'default'
                                    if 'video' in step_name and step_name.split('_')[0] == step.name:
                                        step.name = step.step_name= step_name
                                    if 'function' not in step or step.function == 'None' or step.function is None:
                                        step.function = 'default'

                                    step.collection_time = step.collectionTime = to_datetime(ctime)
                                    step.startTime = step.start_time = to_datetime(step.collection_time).isoformat()
                                    if 'endTime' in step and step.endTime:
                                        step.endTime = step.end_time = to_datetime(step.endTime).isoformat()
                                    else:
                                        step.endTime = step.end_time = step.start_time

                                    bundle = tuple(list(bundle) + [step.name])
                                    bundle_dict = AD(dict(
                                        zip(
                                            [
                                                "camera_id",
                                                "collection_time",
                                                "customer_id",
                                                "gateway_id",
                                                "nodename",
                                                'step_name'
                                            ],
                                            bundle
                                        )
                                    ))

                                    data[bundle_id].stepData[step.name] = step
                                    data[bundle_id].stepData[step.name].update(bundle_dict)
                                    data[bundle_id].stepData[step.name].bundle = bundle
                                    data[bundle_id].stepData[step.name].cid = ":".join(bundle[:-1])
                                    data[bundle_id].stepData[step.name].scid = ":".join(bundle)
                                    data[bundle_id].stepData[step.name].step_name = step.name
                                    data[bundle_id].stepData[step.name].nodename = hdrs.nodename
                                    data[bundle_id].stepData[step.name].cgr = hdrs["customer_id"]
                                    data[bundle_id].stepData[step.name].processor_role = hdrs.nodename.split("_")[0][:-2]
                                    if 'output' not in data[bundle_id].stepData[step.name]:
                                        data[bundle_id].stepData[step.name].output =  AD({"status": "success", "reason": "", "media_info": {}})
                                    if 'media_info' not in data[bundle_id].stepData[step.name].output:
                                        data[bundle_id].stepData[step.name].output.media_info = AD()
                                # data[bundle_id].stepData = AD({k: AD(d) for k, d in data[bundle_id].stepData.items() if isinstance(d, (dict, AD))})
                                data[bundle_id].steps = data[bundle_id].stepData.keys()
                            else:
                                try:
                                    if step_name not in data[bundle_id].stepData:
                                        data[bundle_id].stepData[step_name] = AD(data[bundle_id].stepData.base_event)
                                        data[bundle_id].stepData[step_name].name = data[bundle_id].stepData[step_name].step_name = step_name
                                        bundle = data[bundle_id].stepData[step_name].bundle = tuple(list(data[bundle_id].stepData[step_name]) + [step_name])
                                        data[bundle_id].stepData[step_name]["cid"] = ":".join(bundle[:-1])
                                        data[bundle_id].stepData[step_name]["scid"] = ":".join(bundle)
                                        data[bundle_id].stepData[step_name].update(parse_fqdn(hdrs.nodename))
                                        data[bundle_id].stepData[step_name].filedata = AD()
                                        data[bundle_id].stepData[step_name].filelist = []
                                        data[bundle_id].stepData[step_name].output.media_info = AD()
                                    if 'filedata' not in data[bundle_id].stepData[step_name]:
                                        data[bundle_id].stepData[step_name].filedata = AD()
                                    if 'filelist' not in data[bundle_id].stepData[step_name]:
                                        data[bundle_id].stepData[step_name].filelist = []
                                    if 'media_info' not in data[bundle_id].stepData[step_name].output:
                                        data[bundle_id].stepData[step_name].output.media_info = []
                                    data[bundle_id].stepData[step_name].filelist.append(filename)
                                    data[bundle_id].stepData[step_name].filedata[filename] = np.asarray(bytearray(body), dtype=np.uint8)
                                    get_media_metadata(filename, body, data[bundle_id].stepData[step_name].output.media_info)
                                except Exception:
                                    logger.exception(f"_req_data: bundle structure malformed: {data[bundle_id].stepData[step_name]}")
                                    data[bundle_id].stepData[step_name]["filedata"][filename] = body
                        # upack bundle dictionary into array of values
                        data = [data[k] for k in data.order]
                    except Exception as err:
                        logger.exception("_req_data: multipart decode EXCEPTION - headers: {} - {}".format(request.requestHeaders._rawHeaders, err))
                        raise
                else:
                    data = [semi_raw_data.decode("utf-8")]
            else:
                raw_data = None
                data = []

            cgr = hdrs["customer_id"] if "customer_id" in hdrs else None
            return (cgr, hdrs,args, data, raw_data)

        except Exception as e:
            logger.exception(' '.join([
                func_name.upper(),
                "API [",
                request.method.upper().decode("utf-8"),
                "]: EXCEPTION in _req_data - headers: ",
                request.requestHeaders._rawHeaders,
                repr(e)]))
            raise

    def _return_error(self, func_name, request, status=500, err=None, msg=None):
        request.setResponseCode(status)
        request.setHeader("Content-Type", "application/json")
        out = {
            "apiVersion": Core.API_VERSION,
            "error_source": func_name,
            "error": err,
            "message": msg,
            "requestHeaders": repr(request.requestHeaders._rawHeaders),
            "resultCode": status,
            "resultType": "ERROR",
            "uri": request.uri.decode(),
        }

        logger.exception(out)
        return self._json_safe(out)

    def _return_success(
        self,
        func_name,
        request,
        status=200,
        msg="Success",
        payload=None,
        return_headers={},
    ):
        request.setResponseCode(status)
        request.setHeader("Content-Type", "application/json")
        for k, v in return_headers.items():
            request.setHeader(k, v)

        out = {"status": "ok"}

        # logger.debug(out)
        return self._json_safe(out)

    # @inlineCallbacks
    # def run_in_process(self, partial_func, timeout=30):
    #     future = yield self.process_workers.submit(partial_func)
    #     return future
    #
    # @inlineCallbacks
    # def run_in_threadpool(self, partial_func, timeout=30):
    #     future = self.thread_workers.submit(partial_func)
    #     result = yield future.result(timeout=timeout)
    #     return result

    @inlineCallbacks
    def track_activity(self, params):
        nodename, metrics = params
        self.metricq.get().addCallback(self.track_activity)
        try:
            if not nodename or '_' not in nodename:
                return

            fqdnd = parse_fqdn(nodename)

            if fqdnd is None:
                return

            tags = [
                    f"env:{BCM.env}",
                    f"cgr:{fqdnd.cgr}",
                    f"scode:{fqdnd.scode}",
                    f"nodename:{nodename}"
                ]

            series = [
                    dict(
                        host=BCM.fqdn,
                        metric=mtv[0],
                        points=[[time.time(), mtv[2]]],
                        tags=unique(tags + mtv[1]),
                        type='count'
                    )
                    for mtv in metrics if len(mtv) == 3]

            series.append(
                dict(
                        host=BCM.fqdn,
                        metric='.'.join(['atl'] + [fqdnd[i] for i in ['env', 'cgr', 'site'] if fqdnd[i]] + ['ds', 'activity']),
                        points=[[time.time(), 1]],
                        tags=tags,
                        type='count'
                    )
                )

            resp = yield reactor.callInThread(
                        functools.partial(
                            self.svcs.ddapi.Metric.send,
                            series
                        ),
                        timeout=30
                    )
            # logger.debug(f"track_activity {nodename} - {metric} result: " + repr(resp))

        except Exception as err:
            logger.exception(f"track_activity exception: {nodename} {metrics} - " + repr(err))
