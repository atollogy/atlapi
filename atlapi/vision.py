#!/usr/bin/env python3

from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.image.utilities import apply_transforms, load_image
from atlapi.data_utilities.aggregate import AggregateStep
from atlapi.data_utilities.container_id import ContainerID
from atlapi.vision_utilities.anchor_detection import AnchorDetectionV1
from atlapi.vision_utilities.anchor_detection_v2 import AnchorRecognition
from atlapi.vision_utilities.andon import andon_state, andon_state_compatibility
from atlapi.vision_utilities.bulb_detection import bulbDetection
from atlapi.vision_utilities.color_detection import colorDetection
from atlapi.vision_utilities.diff_filter import DiffFilter
from atlapi.vision_utilities.fast_sticker import fastSticker
from atlapi.vision_utilities.fast_sticker_count import fastStickerCount
from atlapi.vision_utilities.grid_activation import grid_activation_wrapper
from atlapi.vision_utilities.histogram_split import histogramSplit
from atlapi.vision_utilities.hue_vector_label import hue_vector_label
from atlapi.vision_utilities.image_similarity import ImageSimilarity
from atlapi.vision_utilities.line_detection import lineDetection
from atlapi.vision_utilities.locate_obj_hue_vector import LocateTargetHue
from atlapi.vision_utilities.lpr import LPR, LPR_Autocrop
from atlapi.vision_utilities.motion_direction import MotionDirection
from atlapi.vision_utilities.object_direction import Object_Direction
from atlapi.vision_utilities.ocr import OCR
from atlapi.vision_utilities.object_recog import ObjRecog
from atlapi.vision_utilities.pixel_average import pixelAverage
from atlapi.vision_utilities.press_brake import pressBrake
from atlapi.vision_utilities.scale_reader import scaleReader
from atlapi.vision_utilities.seven_segment import SevenSegment
from atlapi.vision_utilities.simple_region import simpleRegion
from atlapi.vision_utilities.sticker_detection import stickerDetection
from atlapi.vision_utilities.tesseract_ocr import tesseract_ocr
from atlapi.vision_utilities.video_color_event import video_color_event_wrapper

import base64
import cv2
from datetime import datetime, timedelta, timezone
import functools
from glob import glob
import io
import json
import logging
import os
logger = logging.getLogger('atlapi')

import math
import numpy as np
import operator
import pandas as pd
import psycopg2
import requests
from skimage import measure
import subprocess as _sp
import time
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

CV2_BGR_CONVERTERS = {c[10:].lower(): c for c in cv2.__dict__.keys() if c.startswith("COLOR_BGR2")}
logger = logging.getLogger('atlapi')

FOUR_HOURS = 4*60*60

class MachineVision(Core):
    def __init__(self, fver=None):
        Core.__init__(self, 'vision_processor', function_version=fver)
        self.vpq.get().addCallback(self.process)
        self.late_window = FOUR_HOURS
        self.anchor_detection_v1 = AnchorDetectionV1()
        self.anchor_detection = AnchorRecognition().anchor_detection
        self.andon_state = andon_state
        self.andonState = andon_state_compatibility # mostly compatible with edge andon state function
        self.color_detection = colorDetection
        self.container_id = ContainerID().container_id
        self.diff_filter = DiffFilter().run_DiffFilter
        self.fast_sticker = fastSticker
        self.fast_sticker_count = fastStickerCount
        self.grid_activation = grid_activation_wrapper
        self.histogram_split = histogramSplit
        self.hue_vector_label = hue_vector_label
        self.image_similarity = ImageSimilarity().image_similarity
        self.jmespath = AggregateStep().process
        self.line_detection = lineDetection
        self.locate_target_hue = LocateTargetHue().locate_target_hue
        self.lpr = LPR().lpr
        self.motion_direction = MotionDirection().process
        self.object_direction = Object_Direction().process
        self.obj_recog = ObjRecog(late_window=self.late_window).objRecog
        self.ocr = OCR().ocr
        self.pixel_average = pixelAverage
        self.press_brake = pressBrake
        self.scale_reader = scaleReader
        self.seven_segment = SevenSegment().seven_segment
        self.simple_region = simpleRegion
        self.sticker_detection = stickerDetection
        self.tesseract_ocr = tesseract_ocr
        self.video_color_event = video_color_event_wrapper

        self.not_deferred = [
            'andonState', 'andon_state',
            'bulbDetection',
            'color_detection',
            'diff_filter',
            'histogram_split', 'hue_vector_label',
            'jmespath',
            'line_detection',
            'pixel_average',
            'scale_reader',
            'simple_region',
            'sticker_detection',
            'subject_filter',
            ### temporary
            'grid_activation',
            'motion_direction',
            'object_direction',
            'video_color_event',
            'tesseract_ocr'
        ]

        self.defer_to_process = [
            # 'grid_activation',
            # 'motion_direction',
            # 'object_direction',
            # 'video_color_event'
        ]

    def build_steps(self, sourceEvent, routing):
        try:
            if not sourceEvent or not routing:
                logger.info(
                    "build_steps empty input parameters: sourceEvent - {} ".format(
                        sourceEvent
                    )
                )
                return None
            now = datetime.utcnow()
            now = now.replace(tzinfo=timezone.utc)
            bundle = sourceEvent.bundle
            step_set = AD({"bundle": bundle, "cfgs": {}, "order": [], "ordered": sourceEvent.stepData.keys()})
            available_steps = sourceEvent.stepData
            step_cfgs = AD({step: AD(routing.stepCfgs[step]) for step in routing.steps})
            available_steps.update(step_cfgs)
            step_list = routing.steps

            if len(bundle) == 5:
                camera_id, collection_time, customer_id, gateway_id, nodename = bundle
            else:
                camera_id, collection_time, customer_id, gateway_id, nodename, _ = bundle

            for sname in step_list:
                if not sname:
                    continue
                step_cfgs[sname].step_name = sname

                input_step = None
                if 'input_step' in step_cfgs[sname]:
                    if '||' in step_cfgs[sname].input_step:
                        for instep in step_cfgs[sname].input_step.split('||'):
                            if instep in available_steps:
                                input_step = step_cfgs[sname].input_step = instep
                                break
                    else:
                        input_step = step_cfgs[sname].input_step
                else:
                    input_step = step_cfgs[sname].input_step = 'default'

                if input_step in available_steps:
                    try:
                        step_set.cfgs[sname] = make_image_utility_objects(step_cfgs[sname])

                        step_set.cfgs[sname].update(
                            {
                                "bundle": tuple(
                                    [camera_id, collection_time, customer_id, gateway_id, nodename, sname]
                                ),
                                "cid": ":".join(
                                    [camera_id, collection_time, customer_id, gateway_id, nodename]
                                ),
                                "scid": ":".join(
                                    [camera_id, collection_time, customer_id, gateway_id, nodename, sname]
                                ),
                                "camera_id": camera_id,
                                # time values
                                "collection_time": to_datetime(collection_time).timestamp(),
                                "start_time": now.isoformat(),
                                "startTime": now.isoformat(),
                                "end_time": None,
                                "endTime": None,
                                "cgr": customer_id,
                                "customer_id": customer_id,
                                "enabled": True,
                                "function_version": self.FUNCTION_VERSION,
                                "gateway_id": gateway_id,
                                "history": AD({"brightness": [], "past_values": []}),
                                "input_step": input_step,
                                "processor_role": 'mvapi' if step_set.cfgs[sname].function in self.mvapi_models else self.tree.shost,
                                "nodename": nodename,
                                'origins': available_steps[input_step].images if 'images' in available_steps[input_step] else [],
                                "output": {"status": "success", "reason": ""},
                                "step_name": sname,
                                "stored": False,
                                "recorded": False
                            }
                        )
                        if "brightness" not in step_set.cfgs[sname]:
                           step_set.cfgs[sname].brightness = AD(
                                {
                                    "alert": 3,
                                    "low_threshold": 9,
                                    "high_threshold": 197,
                                    "normalize": False,
                                    "track": False,
                                    "track_history_length": 2,
                                }
                            )
                        collection_intervals = self.tree.defaults.collection_interval.by_function
                        if "collection_interval" not in step_set.cfgs[sname]:
                            if step_set.cfgs[sname].function in collection_intervals:
                                step_set.cfgs[sname].collection_interval = collection_intervals[step_set.cfgs[sname].function]
                            else:
                                step_set.cfgs[sname].collection_interval = collection_intervals._default
                        if "color_space" not in step_set.cfgs[sname]:
                            step_set.cfgs[sname].color_space = "bgr"
                        if "input_crop" not in step_set.cfgs[sname]:
                            step_set.cfgs[sname].input_crop = None
                        if "input_image_indexes" not in step_set.cfgs[sname]:
                            step_set.cfgs[sname].input_image_indexes = [0]
                        if "subjects" not in step_set.cfgs[sname]:
                            step_set.cfgs[sname].subjects = AD()
                        if "verbose" not in step_set.cfgs[sname]:
                            step_set.cfgs[sname].verbose = False
                        if 'function' in step_set.cfgs[sname]:
                            available_steps[sname] = step_set.cfgs[sname]
                        else:
                            msg = f"Machine Vision build_stepsfunction name not provided {sname}"
                            logger.exception(msg)
                    except Exception as err:
                        msg = f"Machine Vision build_steps failed for {sname} - {repr(err)}"
                        logger.exception(msg)

            to_order = step_set.cfgs.keys()
            cnt = 0
            while len(to_order) > 0 and cnt <= 25:
                cnt += 1
                for step_name in to_order[:]:
                    input_step = step_set.cfgs[step_name].input_step
                    if step_set.cfgs[step_name].function in ["process", "metadata", "buildSteps", "load_results"]:
                        to_order.pop(to_order.index(step_name))
                        continue
                    if input_step in step_set.ordered:
                        step_set.order.append(step_name)
                        step_set.ordered.append(step_name)
                        to_order.pop(to_order.index(step_name))

            step_set.order = unique(step_set.order)
            step_set.ordered = unique(step_set.ordered)

            return step_set
        except Exception as err:
            logger.exception(f"vision.build_steps processing exception"  + repr(err))
            raise

    def load_results(self, stepCfg, input_step_data):
        if 'filelist' in input_step_data and 'filedata' in input_step_data:
            stepCfg.filelist = input_step_data.filelist[:]
            stepCfg.filedata = {fName: input_step_data.filedata[fName] for fName in stepCfg.filelist}
            stepCfg.input_parameters = input_step_data.output if 'output' in input_step_data else AD()
        elif 'output' in input_step_data and 'filedata' in input_step_data.output:
            if isinstance(input_step_data.output.filedata, list):
                stepCfg.filelist = [ft[0] for ft in input_step_data.output.filedata if isinstance(ft, tuple)]
                stepCfg.filedata = AD({ft[0]: ft[-1] for ft in input_step_data.output.filedata if isinstance(ft, tuple)})
            else:
                stepCfg.filelist = input_step_data.output.filelist[:]
                stepCfg.filedata = AD({fName: input_step_data.output.filedata[fName] for fName in stepCfg.filelist})

            stepCfg.input_parameters = AD()
            if 'next_step_input_parameters' in input_step_data:
                for src, dst in input_step_data.next_step_input_parameters.deep_items():
                    stepCfg.input_parameters[dst] = input_step_data[src]
                if ('input_filelist_filters' in stepCfg.input_parameters and
                    isinstance(stepCfg.input_parameters.input_filelist_filters, list)):
                    stepCfg.filelist = [f for f in stepCfg.filelist
                                        if f and any([f.endswith(ffn) for ffn in stepCfg.input_parameters.input_filelist_filters])]
        else:
            msg = f'vision.load_results source data cannot be found in input_step_data'
            raise APIError(msg)

        if 'input_image_filters' in stepCfg and isinstance(stepCfg.input_image_filters, list) and 'images' in input_step_data:
            stepCfg.input_images = stepCfg.origin_links = [f for f in input_step_data.images
                                                           if f and any([f.endswith(ffn) for ffn in stepCfg.input_image_filters])]
            if not len(stepCfg.input_images):
                msg = f'vision.load_results input_images is empty for step: {stepCfg.step_name} defaulting to all images'
                raise APIError(msg)
        else:
            stepCfg.input_images = stepCfg.origin_links = input_step_data.images[:] if 'images' in input_step_data else []

        self.transform_images(stepCfg)

    def transform_images(self, stepCfg):
        if 'adjustments' in stepCfg:
            # logger.info(f'stepCfg.adjustments: {stepCfg.adjustments}')
            for index in stepCfg.input_image_indexes:
                try:
                    if stepCfg.filelist[index].endswith('jpg'):
                        element = load_image(
                            stepCfg.filedata[stepCfg.filelist[index]], color_space=stepCfg.color_space
                        )

                        image, msg = apply_transforms(element, stepCfg.adjustments)

                        if msg != "":
                            logger.warning(f"apply_transforms failed with error {msg} for {stepCfg.scid}")

                        stepCfg.filedata[stepCfg.filelist[index]] = cv2.imencode(".jpg", image)[1]
                except Exception as err:
                    logger.exception(f"transform_images failed for {stepCfg.scid} with error " + repr(err))
                    raise

    @inlineCallbacks
    def process(self, params):
        headers, sourceEvent = params
        self.vpq.get().addCallback(self.process)
        results = []
        metrics = []
        if len(sourceEvent.bundle) == 5:
            camera_id, collection_time, customer_id, gateway_id, nodename = sourceEvent.bundle
        else:
            camera_id, collection_time, customer_id, gateway_id, nodename, _ = sourceEvent.bundle
        try:
            if "mv_routing" in headers:
                mv_routing = AD(headers["mv_routing"])
            elif (customer_id in self.tree._customerModel and
                  f"{customer_id}.devices.gateways.{nodename}.mv_routing" in self.tree._customerModel):
                mv_routing = AD(self.tree._customerModel[customer_id].devices.gateways[nodename].mv_routing)
            else:
                logger.debug(
                    "vision_process: customer_id[{}] or nodename[{}] not found in cmcache".format(
                        customer_id, nodename
                    )
                )
                return

            if 'steps' not in mv_routing or not len(mv_routing.steps):
                logger.debug(f"no steps configured for {sourceEvent.bundle}")
                return None

            step_set = self.build_steps(sourceEvent, mv_routing)
            result_set = sourceEvent.stepData
            to_remove = result_set.keys()
            for step_name in step_set.order:
                origin_step = AD(step_set.cfgs[step_name])
                logger.info(f"atlapi.vision.process processing {nodename}:{step_name}:{origin_step.function}")
                if origin_step.input_step in result_set:
                    try:
                        if isinstance(result_set[origin_step.input_step], list):
                            runs = result_set[origin_step.input_step]
                        else:
                            runs = [result_set[origin_step.input_step]]
                        for run_data in runs:
                            step = AD(origin_step)
                            self.load_results(step, run_data)
                            start = time.time()
                            if step.function in self.mvapi_models:
                                res = yield self.run_in_mainloop(nodename, step_name, self.obj_recog, step)
                                metrics.append((f"atlapi.vision.function",
                                                [f'step_name:{step_name}', f'step_function:{step.function}'],1))
                                duration = time.time() - start
                                if duration > 10:
                                    logger.info(f"atlapi.vision.process {step.nodename} " +
                                                f"- {step.step_name}:{step.function} execution time was: {duration}")
                                continue
                            else:
                                if step.function in self.defer_to_process:
                                    output = yield self.run_in_process(nodename, step_name, getattr(self, step.function), step)
                                elif step.function in self.not_deferred:
                                    output = yield self.run_in_thread(nodename, step_name, getattr(self, step.function), step)
                                else:
                                    output = yield self.run_in_mainloop(nodename, step_name, getattr(self, step.function), step)
                                if 'filelist' in step:
                                    del step['filelist']
                                if 'filedata' in step:
                                    del step['filedata']

                                # output with a list type signifies forked steps being returned
                                # these are called "f_steps". An f_step should be treated as a
                                # distinct step variant derived from step which created it.
                                # An f_step MUST be returned as a full step_cfg bundle with an "output" result
                                # f_steps should have differentiated step names from there origin,
                                # but are not required to. If the step_name is not differentiated,
                                # the output will be treated as a v_step and collected
                                if isinstance(output, list):
                                    for f_step in [AD(d) for d in output]:
                                        f_step.end_time = f_step.endTime = datetime.utcnow().isoformat()
                                        if 'collection_interval' in f_step.output:
                                            f_step.collection_interval = f_step.output.collection_interval
                                        if f_step.step_name in result_set:
                                            if isinstance(result_set[f_step.step_name], list):
                                                result_set[f_step.step_name].append(f_step)
                                            else:
                                                result_set[f_step.step_name] = [result_set[f_step.step_name], f_step]
                                        else:
                                            result_set[f_step.step_name] = f_step
                                        res = yield self.mvrq.put((step.customer_id, headers, (), [f_step]))
                                        metrics.append((f"atlapi.vision.function",
                                                        [f'step_name:{f_step.step_name}', f'step_function:{f_step.function}'], 1))
                                    duration =  time.time() - start
                                    if duration > 10:
                                        logger.info(f"atlapi.vision.process {f_step.nodename} " +
                                                    f"- {f_step.step_name}:{f_step.function} execution time was: {duration}")
                                    continue
                                # output with a tuple type signifies virtual steps being returned
                                # these are called "v_steps"
                                # a virtual step represents the iterative application of a step over
                                # multiple data inputs each having a unique result
                                # these results are collected under a common step-name in results
                                # any downstream steps will also be iteratively applied to these outputs
                                # a v_steps should be returned as only the step_output (not the full step_cfg bundle)
                                # this is to signal the results coming from a common cfg source
                                elif isinstance(output, tuple):
                                    v_step_output = []
                                    for o in [AD(d) for d in output]:
                                        v_step = AD(step)
                                        if 'output' in o:
                                            v_step.ouput = o.output
                                        else:
                                            v_step.output = o
                                        v_step.end_time = v_step.endTime = datetime.utcnow().isoformat()
                                        if 'collection_interval' in v_step.output:
                                            v_step.collection_interval = v_step.output.collection_interval
                                        v_step_output.append(v_step)
                                        res = yield self.mvrq.put((step.customer_id, headers, (), [v_step]))
                                        metrics.append((f"atlapi.vision.function",
                                                        [f'step_name:{v_step.step_name}', f'step_function:{v_step.function}'], 1))
                                    if step.step_name in result_set:
                                        if isinstance(result_set[step.step_name], list):
                                            result_set[step.step_name].extend(v_step_output)
                                        else:
                                            v_step_output.append(result_set[step.step_name])
                                            result_set[step.step_name] = v_step_output
                                    else:
                                        result_set[step.step_name] = v_step_output
                                    duration =  time.time() - start
                                    if duration > 10:
                                        logger.info(f"atlapi.vision.process {v_step.nodename} " +
                                                    f"- {v_step.step_name}:{v_step.function} execution time was: {duration}")
                                else:
                                    step.output = output
                                    step.end_time = step.endTime = datetime.utcnow().isoformat()
                                    if 'collection_interval' in output:
                                        step.collection_interval = output.collection_interval
                                    result_set[step_name] = step
                                    res = yield self.mvrq.put((step.customer_id, headers, (), [step]))
                                    metrics.append((f"atlapi.vision.function",
                                                    [f'step_name:{step_name}', f'step_function:{step.function}'],1))
                                    duration =  time.time() - start
                                    if duration > 10:
                                        logger.info(f"atlapi.vision.process {step.nodename} " +
                                                    f"- {step.step_name}:{step.function} execution time was: {duration}")
                    except Exception as err:
                        logger.exception(f"bundle - {step.bundle} error: not run  " + repr(err))
            res = yield self.metricq.put((nodename, metrics))
            return True
        except Exception as err:
            logger.exception("Machine Vision  processing failed: ".upper() + repr(err))
            raise

    @exec_timed
    @inlineCallbacks
    def run_in_mainloop(self, nodename, step_name, func, step):
        start = time.time()
        logger.info(f"running step {step_name}:{func.__name__} for: {nodename}")
        res = yield func(step)
        exec_time = time.time() - start
        if exec_time > 10:
            logger.debug(f"EXCESSIVE EXECUTION TIME RUNNING STEP IN MAINLOOP {step_name}:" +
                         f"{func.__name__.upper()} for {nodename} = {exec_time} SEC")
        else:
            logger.info(f"atlapi.vision.process {nodename}:{step.step_name}.{step.function}: execution time was: {time.time() - start}")
        return res

    @exec_timed
    @inlineCallbacks
    def run_in_process(self, nodename, step_name, func, step):
        start = time.time()
        logger.info(f"running step {step_name}:{func.__name__} for: {nodename}")
        if 'input_crop' in step and step.input_crop and len(step.input_crop):
            step.input_crop = list(step.input_crop)
        res = yield deferToProcess(func, step)
        exec_time = time.time() - start
        if exec_time > 10:
            logger.debug(f"EXCESSIVE EXECUTION TIME RUNNING STEP IN PROCESS {step_name}:{func.__name__.upper()} for {nodename} = {exec_time} SEC")
        else:
            logger.info(f"atlapi.vision.process {nodename}:{step.step_name}.{step.function}: execution time was: {time.time() - start}")
        return res

    @exec_timed
    @inlineCallbacks
    def run_in_thread(self, nodename, step_name, func, step):
        start = time.time()
        logger.info(f"running step {step_name}:{func.__name__} for: {nodename}")
        res = yield deferToThread(func, step)
        exec_time = time.time() - start
        if exec_time > 10:
            logger.debug(f"EXCESSIVE EXECUTION TIME RUNNING STEP IN THREAD {step_name}:{func.__name__.upper()} for {nodename} = {exec_time} SEC")
        else:
            logger.info(f"atlapi.vision.process {nodename}:{step.step_name}.{step.function}: execution time was: {time.time() - start}")
        return res
