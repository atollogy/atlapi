#!/usr/bin/env python3.6

from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.image.utilities import *

import asyncio
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
import os
from pprint import pprint as pp
import time
from twisted.internet import task, reactor
from twisted.internet.defer import ensureDeferred, inlineCallbacks, maybeDeferred, waitForDeferred, returnValue


@inlineCallbacks
def correct_result(event, result, fields, aws):
    try:
        if 'step_output' not in result:
            if 'output' in result:
                result.step_output = result.output
                del result['output']
            else:
                result.update(new_result(fields, result.images))

        if 'annotations.plate' in result.step_output:
            result.step_output.annotations.plate = AD()

        result.step_output.original_result = None
        result.step_output.original_result = AD(result.step_output)
        result.step_output.modifier = fields.modifier

        if 'identifier' in result.step_output:
            result.step_output.original_plate =  result.step_output.identifier
        else:
            result.step_output.original_plate = 'None'

        if ('fullResponse' not in result.step_output or
            'fullResponse.results' not in result.step_output or
            not isinstance(result.step_output.fullResponse.results, list) or
            len(result.step_output.fullResponse.results) == 0 or
            'candidates' not in result.step_output.fullResponse.results[0]):
            result.step_output['fullResponse.results'] = [AD({
                        "candidates": [{"confidence": 100, "matches_template": 0, "plate": fields.new_plate}],
                        "confidence": 100,
                        "coordinates": [],
                        "matches_template": 1,
                        "plate": fields.new_plate,
                        "plate_index": 0,
                        "processing_time_ms": 10.0,
                        "region": fields.modifier,
                        "region_confidence": 100.0,
                        "requested_topn": 1,
                        "vehicle": {
                            "body_type": [
                                {"confidence": 100, "name": "tractor-trailer"}
                            ]
                        }
                    })]
        else:
            result.step_output.fullResponse.results[0]['candidates'].append(AD({
                 'plate': fields.new_plate,
                 'matches_template': 0,
                 'confidence': 100
            }))


        for k in ['identifier', 'plate', 'license_plate']:
            result.step_output[k] = fields.new_plate

        result.step_output.confidence = 100.0

        if 'qc_img_prop' not in result.step_output:
            result.step_output.qc_img_prop = AD()

        result.step_output.qc_img_prop['hitl_correction_annotation'] = {
            'identifier': fields.new_plate,
            'created_at_date': fields.collection_time.isoformat()
        }

        result = yield reannotate(result, fields, aws)
        return result
    except Exception as err:
        logger.exception(f"atlapi.results.lpr.correct_result: {err} ")
        raise

def new_result(fields, images, confidence=100.0):
    bundle = ('video0', str(fields.collection_time.timestamp()), fields.cgr, fields.gateway_id, fields.nodename, fields.step_name)
    now = datetime.utcnow()
    tmpl = AD({
        "brightness": {
            "alert": 3,
            "low_threshold": 9,
            "high_threshold": 197,
            "normalize": False,
            "track": False,
            "track_history_length": 2
        },
        "brightness_threshold": 45,
        "bundle": bundle,
        "camera_id": "video0",
        "cgr": fields.cgr,
        "cid": ':'.join(bundle[:-1]),
        "collection_interval": 4,
        "collection_time": fields.collection_time.timestamp(),
        "color_space": "rgb",
        "confidence_threshold": 0,
        "config_version": None,
        "customer_id": fields.customer_id,
        "enabled": True,
        "end_time": None,
        "function": "lpr",
        "function_version": None,
        "gateway_id": fields.gateway_id,
        "history": {
            "brightness": [],
            "past_values": []
        },
        "images": [],
        "input_crop": None,
        "input_image_indexes": [0],
        "input_parameters": {},
        "input_step": "default",
        "is_whitelist": True,
        "nodename": fields.nodename,
        "origins": [],
        "origin_links": [],
        "step_output": {
            "all_numeric": True,
            "all_ones": False,
            "all_zeros": False,
            "annotations": {
            },
            "atollogy_plates": False,
            "bd_factor": 1000,
            "brightness": 128,
            "candidates": [{"confidence": confidence, "matches_template": 1, "plate": fields.new_plate}],
            "cause": fields.modifier,
            "collection_interval": 4,
            "confidence": confidence,
            "coordinates": [],
            "fullResponse": {
                "candidates": [{"confidence": confidence, "matches_template": 1, "plate": fields.new_plate}],
                "credits_monthly_total": 50000000,
                "credits_monthly_used": 1,
                "data_type": "alpr_results",
                "epoch_time": fields.collection_time.timestamp(),
                "error": False,
                "has_identifier": True if 'vid' in fields.new_plate else False,
                "is_identifier": 1 if 'vid' in fields.new_plate else 0,
                "img_height": 720,
                "img_width": 1280,
                "plate": fields.new_plate,
                "plate_index": 0,
                "processing_time": {"plates": 10.0, "total": 100.0, "vehicles": 10.0},
                "regions_of_interest": [{"height": 720, "width": 1280, "x": 0, "y": 0}],
                "results": [
                    {
                        "candidates": [{"confidence": confidence, "matches_template": 0, "plate": fields.new_plate}],
                        "confidence": confidence,
                        "coordinates": [],
                        "matches_template": 1,
                        "plate": fields.new_plate,
                        "plate_index": 0,
                        "processing_time_ms": 10.0,
                        "region": fields.modifier,
                        "region_confidence": 100.0,
                        "requested_topn": 1,
                        "vehicle": {
                            "body_type": [
                                {"confidence": confidence, "name": "tractor-trailer"}
                            ]
                        }
                    }
                ],
                "uuid": "",
                "version": 2
            },
            "identifier": fields.new_plate,
            "is_blurry_or_dirty": False,
            "is_side_read": False,
            "is_whitelist": True,
            "license_plate": fields.new_plate,
            "matches_template": 1,
            "modifier": fields.modifier.lower(),
            "origins": images,
            "plate": fields.new_plate,
            "plate_angle": 0,
            "qc_img_props": {
                fields.modifier: {
                    'identifier': fields.new_plate,
                    'created_at_date': fields.collection_time.isoformat()
                }
            },
            "reason": f"Success - {fields.modifier.lower()}",
            "resolution": {"h": 720, "w": 1280},
            "side_read_angle_block": False,
            "skew_metrics": {},
            "status": "success",
            "vehicle_type": "trailer-utility" if fields.trailer else "tractor-trailer",
            "vehicle_type_confidence": confidence
        },
        "plate_orientation": None,
        "processor_role": BCM.nodename,
        "region_filter": False,
        "recorded": False,
        "scid": ':'.join(bundle),
        "side_read_angle_block": False,
        "start_time": fields.collection_time.isoformat(),
        "step_name": fields.step_name,
        "stored": False,
        "subjects": {},
        "vehicle_type_blocking": False,
        "vehicle_type_filtering": True,
        "vehicle_type_inclusion_list": ["tractor-trailer", "trailer-rv", "trailer-utility", "truck-standard"],
        "verbose": False,
    })
    return tmpl


@inlineCallbacks
def reannotate(result, fields, aws):
    try:
        annotation = None
        if 'identifer' in result.step_output:
            new_plate = result.step_output.identifier
        else:
            new_plate = result.step_output.identifier = fields.new_plate
        if 'original_plate' in result.step_output:
            original_plate = result.step_output.original_plate
        else:
            original_plate = result.step_output.original_plate = None

        result.images = unique(result.images)
        fname = [fn for fn in unique(result.images) if fn.endswith('annotated.jpg')]
        if len(fname):
            fname = fname[0]
            ofname = fname[:-4] + '_original.jpg'
            if ofname not in result.images:
                result.images.append(ofname)

            # collect image
            bucket, akey = fname[5:].split('/', 1)
            okey = ofname[5:].split('/', 1)[-1]
            original = yield aws.S3.get(bucket, akey)
            original = load_image(original)

            # save original
            res = yield aws.S3.put(bucket, okey, pack_image(original))
            if res and res not in result.images:
                result.images.append(res)

            # figure out annotation zone to use
            ats = datetime.utcnow().timestamp()
            if 'annotations.plate' in result.step_output:
                for ats, a in result.step_output.annotations.plate.items():
                    if 'origins' in result.step_output.annotations.plate[ats]:
                        try:
                            result.step_output.annotations.plate[ats].origins = unique(result.step_output.annotations.plate[ats].origins)
                        except Exception:
                            result.step_output.annotations.plate[ats].origins = []
                    if hasattr(a, 'items') and 'result' in a and a.result == original_plate:
                        annotation = a
                        del result.step_output.annotations.plate[ats]
                        break
            if annotation:
                azone = Zone.create(
                        f"corrected license plate: {original_plate} -> {new_plate}",
                        annotation.coords,
                        atype="correction",
                        result=new_plate,
                        rtype='plate'
                    )
            else:
                h, w = original.shape[:2]
                azone = Zone.create(
                        f"corrected license plate: {new_plate}",
                        [w*0.2, h*0.2, w*0.8, h*0.8],
                        atype="correction",
                        result=new_plate,
                        rtype='plate'
                    )
            if 'annotations.plate' not in result.step_output or not isinstance(result.step_output.annotations, (AD, dict)):
                result.step_output.annotations = AD({
                        'plate': {
                            ats: azone.annotation
                        }
                    })
            else:
                result.step_output.annotations.plate[ats] = azone.annotation

            # re-annotate result
            to_annotate = original.copy()
            annotated_image = annotate(to_annotate, [azone])

            # save newly annotated image
            res = yield aws.S3.put(bucket, akey, pack_image(annotated_image))
            if res and res not in result.images:
                result.images.append(res)

            # save new annotation details
            result.step_output.annotations.plate[ats] = azone.annotation
            result.step_output.annotations.plate[ats].origins = result.images
            result.step_output.annotations.plate[ats].color_space = 'bgr'
        return result
    except Exception as err:
        logger.exception(f"atlapi.results.lpr.reannotate: {err} - {result}")
        raise


########################################################################################
    # step_output example
    # {
    #     "status": "success",
    #     "reason": "Standard result - no modifications",
    #     "brightness": 4.911083984375,
    #     "resolution": {
    #         "w": 1280,
    #         "h": 720
    #     },
    #     "warning": "warning : Image is very dark 4.911083984375 < 9",
    #     "fullResponse": {
    #         "uuid": "",
    #         "data_type": "alpr_results",
    #         "epoch_time": 1586856948246,
    #         "processing_time": {
    #             "plates": 61.8330078125,
    #             "total": 66.07000000076368
    #         },
    #         "img_height": 720,
    #         "img_width": 1280,
    #         "results": [{
    #                 "plate": "972F28",
    #                 "confidence": 90.60073852539062,
    #                 "region_confidence": 58,
    #                 "vehicle_region": {
    #                     "y": 94,
    #                     "x": 864,
    #                     "height": 416,
    #                     "width": 416
    #                 },
    #                 "region": "md",
    #                 "plate_index": 0,
    #                 "processing_time_ms": 18.802385330200195,
    #                 "candidates": [{
    #                         "matches_template": 0,
    #                         "plate": "972F28",
    #                         "confidence": 90.60073852539062
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "92F28",
    #                         "confidence": 75.89857482910156
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "972F2",
    #                         "confidence": 75.87266540527344
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "972F26",
    #                         "confidence": 75.79547119140625
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "972F29",
    #                         "confidence": 75.59844207763672
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "972F20",
    #                         "confidence": 75.48986053466797
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "972F23",
    #                         "confidence": 75.48950958251953
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "992F28",
    #                         "confidence": 75.34713745117188
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "922F28",
    #                         "confidence": 75.33201599121094
    #                     }, {
    #                         "matches_template": 0,
    #                         "plate": "902F28",
    #                         "confidence": 75.3118667602539
    #                     }
    #                 ],
    #                 "coordinates": [{
    #                         "y": 370,
    #                         "x": 1088,
    #                         "tot": 1458
    #                     }, {
    #                         "y": 350,
    #                         "x": 1156,
    #                         "tot": 1506
    #                     }, {
    #                         "y": 400,
    #                         "x": 1152,
    #                         "tot": 1552
    #                     }, {
    #                         "y": 425,
    #                         "x": 1084,
    #                         "tot": 1509
    #                     }
    #                 ],
    #                 "matches_template": 0,
    #                 "requested_topn": 10
    #             }
    #         ],
    #         "credits_monthly_used": 843941,
    #         "version": 2,
    #         "credits_monthly_total": 50000000,
    #         "error": False,
    #         "regions_of_interest": [{
    #                 "y": 0,
    #                 "x": 0,
    #                 "height": 720,
    #                 "width": 1280
    #             }
    #         ],
    #         "credit_cost": 1
    #     },
    #     "is_blurry_or_dirty": "True",
    #     "bd_factor": 38.50276758688351,
    #     "is_side_read": False,
    #     "plate_angle": "16.389540334034784 < 60.0",
    #     "plate": "972F28",
    #     "identifier": "972F28",
    #     "license_plate": "972F28",
    #     "candidates": [{
    #             "matches_template": 0,
    #             "plate": "972F28",
    #             "confidence": 90.60073852539062
    #         }, {
    #             "matches_template": 0,
    #             "plate": "92F28",
    #             "confidence": 75.89857482910156
    #         }, {
    #             "matches_template": 0,
    #             "plate": "972F2",
    #             "confidence": 75.87266540527344
    #         }, {
    #             "matches_template": 0,
    #             "plate": "972F26",
    #             "confidence": 75.79547119140625
    #         }, {
    #             "matches_template": 0,
    #             "plate": "972F29",
    #             "confidence": 75.59844207763672
    #         }, {
    #             "matches_template": 0,
    #             "plate": "972F20",
    #             "confidence": 75.48986053466797
    #         }, {
    #             "matches_template": 0,
    #             "plate": "972F23",
    #             "confidence": 75.48950958251953
    #         }, {
    #             "matches_template": 0,
    #             "plate": "992F28",
    #             "confidence": 75.34713745117188
    #         }, {
    #             "matches_template": 0,
    #             "plate": "922F28",
    #             "confidence": 75.33201599121094
    #         }, {
    #             "matches_template": 0,
    #             "plate": "902F28",
    #             "confidence": 75.3118667602539
    #         }
    #     ],
    #     "confidence": 90.60073852539062,
    #     "matches_template": 0,
    #     "coordinates": [{
    #             "y": 370,
    #             "x": 1088,
    #             "tot": 1458
    #         }, {
    #             "y": 350,
    #             "x": 1156,
    #             "tot": 1506
    #         }, {
    #             "y": 425,
    #             "x": 1084,
    #             "tot": 1509
    #         }, {
    #             "y": 400,
    #             "x": 1152,
    #             "tot": 1552
    #         }
    #     ],
    #     "skew_metrics": {
    #         "length_top": 70.8801805866774,
    #         "length_bottom": 72.44998274671983,
    #         "angle_top": 16.389540334034784,
    #         "angle_bottom": 20.18580300946485,
    #         "angle_left": 85.84035770628736,
    #         "angle_right": 85.42607874009914,
    #         "width_left": 55.14526271584895,
    #         "width_right": 50.15974481593781,
    #         "rect_area": 3908.706179799142,
    #         "area": 3480.0
    #     }
    # }
