#!/usr/bin/env python3
import os
import sys
sys.path.insert(0, os.path.join('..', 'atlapi'))
# sys.path.insert(0, os.path.join('..', '..', 'atlapi'))
#
# import pathlib
# parameter_path = pathlib.Path.cwd().joinpath('parameters')

from atlapi.attribute_dict import *
from atlapi.consul_kvs import CAD_Cache

import pyconsul as consul

import asyncio
import base64
import boto3
import botocore
import cv2
# from datadog_api_client.v1 import CoreClient, CoreException, Configuration
# from datadog_api_client.v1.api import metrics_api
# from datadog_api_client.v1.models import *
from datadog import initialize
from datadog import api as ddapi
from dateutil.parser import parse as date_parser
from datetime import datetime, date, timedelta, timezone
import functools
import gzip
import iso8601
import io
import json
import logging
from logging.handlers import RotatingFileHandler, QueueListener, QueueHandler
from multiprocessing import Pool
import multiprocessing_logging
multiprocessing_logging.install_mp_handler()
import mimetypes
import numpy as np
import os
from queue import Queue
import pandas as pd
from pandas.tseries.frequencies import to_offset
import re
import requests
import signal
import subprocess as _sp
import sys
import time
import timeframe
from twisted.internet import task, reactor, threads
from twisted.internet.threads import deferToThread, deferToThreadPool
from twisted.internet.defer import inlineCallbacks, ensureDeferred, maybeDeferred, returnValue, DeferredQueue
import urllib3
from urllib3.util.retry import Retry
import zipfile

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logger = logging.getLogger('atlapi')

LOG_PATH = "/var/log/atollogy"
LOG_NAME = "{}/atlapi.log".format(LOG_PATH)
LOG_SIZE = 20000000  # 20MB
LOG_ROTATION_COUNT = 10
LOG_LEVEL = 10

# Create log folder
if not os.path.exists(LOG_PATH):
    os.makedirs(LOG_PATH)

_log_queue = Queue()
formatter = logging.Formatter(
    '{"timestamp": "%(asctime)s", "thread": "%(threadName)s", "syslog.appname": "%(name)s", "level": "%(levelname)s", "message": "%(message)s"}'
)
floghandler = RotatingFileHandler(LOG_NAME, maxBytes=LOG_SIZE, backupCount=LOG_ROTATION_COUNT)
floghandler.setLevel(LOG_LEVEL)
floghandler.setFormatter(formatter)

qlistener = QueueListener(_log_queue, floghandler).start()
qhandler = QueueHandler(_log_queue)
logger.addHandler(qhandler)
logger.setLevel(LOG_LEVEL)


### array de-duplication
def unique(*args):
    temp = []
    for l in args:
        if isinstance(l, list):
            for i in l:
                if isinstance(i, list):
                    for x in unqiue(i):
                        if x not in temp:
                            temp.append(x)
                elif i not in temp:
                    temp.append(i)
    return temp

# Write out queued logs to a file:

BCM = None
def get_bcm():
    global BCM
    cfg = None
    local = False
    if "local_bcm" in os.environ:
        cfg = os.environ["local_bcm"]
        local = True
    elif os.path.exists("/etc/kvhandler/bcm.json"):
        cfg = "/etc/kvhandler/bcm.json"
    elif os.path.exists("/etc/kvmanager/bcm.json"):
        cfg = "/etc/kvmanager/bcm.json"

    if cfg is not None:
        BCM = AD.load(cfg)
        BCM.is_local = local
        return BCM
    else:
        print("BCM cannot be found - exiting")
        sys.exit(1)

BCM = get_bcm()
# print(BCM)
seen = []
def watch_cgrs():
    if 'active_cgrs' not in BCM:
        BCM.active_cgrs = []
    cgrs = BCM.svcs.ckv.get(f'cgrsActive/{BCM.env}')
    BCM.active_cgrs = unique(BCM.active_cgrs, cgrs)
    logger.info(f"cgr_watcher.update: {BCM.active_cgrs}")
    for cgr in BCM.active_cgrs:
        if cgr not in BCM._customerModel:
            BCM._customerModel[cgr] = AD({'devices': {'gateways': {}}})
        if f'devices.gateways' not in BCM._customerModel[cgr]:
            BCM._customerModel[cgr]['devices.gateways'] = AD()

BCM._customerModel = CAD_Cache('_customerModel')
logger.info(f"_customerModel top level: {BCM._customerModel.keys()}")
BCM.gwid = CAD_Cache(f"_nodes")
BCM.active_cgrs = BCM.cgrsActive
BCM.brq = DeferredQueue()
BCM.cache = AD()
BCM.carq = DeferredQueue()
BCM.cfg = AD()
BCM.cfg.boto = botocore.client.Config(signature_version='s3v4', max_pool_connections=50)
BCM.cgrMap = CAD_Cache("cgrMap", env=BCM.env)
BCM.descriptorq = DeferredQueue()
BCM.edrq = DeferredQueue()
BCM.env_models = CAD_Cache(f"_models/envs/{BCM.env}/svcs/mvapi/load_models", env=BCM.env)
BCM.irq = DeferredQueue()
BCM.metricq = DeferredQueue()
BCM.mvapi_models = BCM.env_models.keys()
BCM.mvrq = DeferredQueue()
BCM._resources = CAD_Cache(f'_resources/{BCM.env}', env=BCM.env)
BCM.svcs = AD()
BCM.svcs.ddapi = ddapi

if 'at0l' in _sp.check_output(['hostname']).decode():
    BCM.svcs.ckv = consul.Consul(
                                host=f"localhost",
                                port=8543,
                                scheme="https",
                                verify=False).kv
else:
    BCM.svcs.ckv = consul.Consul(host='consul.prd.at0l.io',
                                 port=8543,
                                 scheme="https",
                                 verify=False).kv

BCM.vpq = DeferredQueue()

CGR_WATCHER = task.LoopingCall(watch_cgrs)
CGR_WATCHER.start(300, now=True)

def get_nodename(cgr, gwid):
    if cgr in BCM.gwid:
        if gwid in BCM.gwid[cgr].by_gwid and 'nodename' in BCM.gwid[cgr].by_gwid[gwid]:
            return BCM.gwid[cgr].by_gwid[gwid].nodename
        elif gwid in BCM.gwid.by_gwid and 'nodename' in BCM.gwid.by_gwid[gwid]:
            return BCM.gwid.by_gwid[gwid].nodename
        else:
            return gwid
    else:
        return gwid

######### Context Managers ####################################################
class GracefulInterruptHandler(object):
    def __init__(self, signals=(signal.SIGINT, signal.SIGTERM), funcs=[]):
        self.signals = signals
        self.original_handlers = {}
        self.funcs = funcs

    def __enter__(self):
        self.interrupted = False
        self.released = False

        for sig in self.signals:
            self.original_handlers[sig] = signal.getsignal(sig)
            signal.signal(sig, self.handler)

        return self

######### Exception classes ####################################################
class Atl_Exception(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv

class APIError(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class CFGError(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class DBError(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class MetricError(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class MVError(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class TimeError(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class Image_Utility_Exception(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

API_Exception= API_Error = APIError
CFG_Exception = CFG_Error = CFGError
DB_Exception = DB_Error = DBError
Metric_Exception = Metric_Error = MetricError
MV_Exception = MV_Error = MVError
Time_Exception = Time_Error = TimeError

######### Common Utility Functions ############################################
### JSON Serialization Helpers
class JSONEncoder(json.JSONEncoder):
    """JSONEncoder to handle ``datetime`` and other problematic object values"""

    class EncodeError(Exception):
        """Raised when an error occurs during encoding"""

    def default(self, obj):
        try:
            if isinstance(obj, datetime):
                return obj.timestamp()
            elif isinstance(obj, bytes):
                return self.default(obj.decode("utf-8"))
            elif isinstance(obj, io.BytesIO):
                return "removed binary data"
            elif hasattr(obj, "toJSON"):
                return obj.toJSON()
            elif hasattr(obj, "jstr"):
                return obj.jstr()
            else:
                try:
                    encoded_obj = json.JSONEncoder.default(self, obj)
                except TypeError as err:
                    encoded_obj = repr(obj)
                return encoded_obj
        except Exception as err:
            raise JSONEncoder.EncodeError("JSON Encoder Error: {}".format(repr(err)))

### json object serializer
def json_safe(obj):
    """JSON dumper for objects not serializable by default json code"""
    try:
        return json.dumps(obj, cls=JSONEncoder, default=str, indent=4, separators=(",", ": "), sort_keys=True)
    except Exception:
        return repr(obj)

class ProxyMetaObject(object):
    proxies = AD()
    def __init__(self, obj, **kwargs):
        ProxyMetaObject.proxies[id(self)] = AD({'_obj': obj, '_meta': kwargs})

    def __getattribute__(self, name):
        if name == '_meta':
            return ProxyMetaObject.proxies[id(self)]._meta
        elif name.startswith('_meta.'):
            return ProxyMetaObject.proxies[id(self)]._meta[name[5:]]
        else:
            return getattr(ProxyMetaObject.proxies[id(self)]._obj, name)

    def __delattr__(self, name):
        if name.startswith('_meta.'):
            del ProxyMetaObject.proxies[id(self)]._meta[name[5:]]
        else:
            delattr(ProxyMetaObject.proxies[id(self)]._obj, name)

    def __setattr__(self, name, value):
        if name.startswith('_meta.'):
            ProxyMetaObject.proxies[id(self)]._meta[name[5:]] = value
        else:
            setattr(ProxyMetaObject.proxies[id(self)]._obj, name, value)

    def __nonzero__(self):
        return bool(ProxyMetaObject.proxies[id(self)]._obj)

    def __str__(self):
        return str(ProxyMetaObject.proxies[id(self)]._obj)

    def __repr__(self):
        return repr(ProxyMetaObject.proxies[id(self)]._obj)

    def __hash__(self):
        return hash(ProxyMetaObject.proxies[id(self)]._obj)

def repopulate_mvres_from_step_record(mvres, s3link):
    lparts = s3link[4:].split('/')
    fparts = lparts[-1].split('_')
    mvres.nodename = lparts[3]
    mvres.cgr = mvres.customer_id = mvres.nodename.split('_')[1]
    mvres.function = mvres.step_function
    fname = lparts[-1].rsplit('_', 1)[-1]
    mvres.gateway_id = fparts[0]
    mvres.camera_id = fparts[1]
    new_datetime = to_datetime(mvres.collection_time)
    mvres.collection_time = new_datetime.timestamp()
    mvres.collection_interval = mvres.collection_interval.seconds
    mvres.start_time = to_datetime(mvres.step_start_time).isoformat()
    mvres.end_time = to_datetime(mvres.step_end_time).isoformat()
    mvres.color_space = 'rgb'
    mvres.headers = mvres.bundle = AD({
        'camera_id': mvres.camera_id,
        'collection_time': mvres.collection_time,
        'customer_id': mvres.cgr,
        'gateway_id': mvres.gateway_id,
        'nodename': mvres.nodename
    })


######### datetime handler ############################################
# this regex parses '20201111120151' and '20201111120151.123456'
DATETIME_RE = re.compile(r'(?P<year>(\d{4}))(?P<month>(\d{2}))(?P<day>(\d{2}))(?P<hour>(\d{2}))(?P<minute>(\d{2}))(?P<second>(\d{2}))\.{0,1}(?P<microsecond>(\d{0,6}))')
def to_datetime(rec_time):
    if isinstance(rec_time, str):
        try:
            dt = iso8601.parse_date(rec_time)
            dt = dt.replace(tzinfo=timezone.utc)
            return dt
        except Exception:
            pass

        try:
            dmatch = DATETIME_RE.match(rec_time)
            return datetime(**{k: int(v) for k, v in dmatch.groupdict().items() if v.isnumeric()},  tz=timezone.utc)
        except Exception:
            pass

        if '.' in rec_time:
            try:
                return datetime.fromtimestamp(float('.'.join(rec_time.split('.')[:2])), tz=timezone.utc)
            except Exception as err:
                return datetime.fromtimestamp(int(rec_time.split('.')[0]), tz=timezone.utc)
        elif rec_time.isnumeric():
            if len(rec_time) == 13:
                return datetime.fromtimestamp((int(rec_time)/1000), tz=timezone.utc)
            elif len(rec_time) == 16:
                return datetime.fromtimestamp((int(rec_time)/1000000), tz=timezone.utc)
            elif len(rec_time) == 10:
                return datetime.fromtimestamp(int(rec_time), tz=timezone.utc)
            else:
                raise ValueError(f"to_datetime was passed an integer string with an invalid length: {len(rec_time)}")
        else:
            raise ValueError(f"to_datetime was passed an invalid string: {rec_type}")
    elif isinstance(rec_time, (int, np.int32)):
        if len(str(rec_time)) == 13:
            return datetime.fromtimestamp((rec_time/1000), tz=timezone.utc)
        elif len(str(rec_time)) == 16:
            return datetime.fromtimestamp((rec_time/1000000), tz=timezone.utc)
        elif len(str(rec_time)) == 10:
            return datetime.fromtimestamp(int(rec_time), tz=timezone.utc)
        else:
            return datetime.fromtimestamp(rec_time, tz=timezone.utc)
    elif isinstance(rec_time, (float, np.float, np.float64)):
        return datetime.fromtimestamp(rec_time, tz=timezone.utc)
    else:
        return datetime.fromtimestamp(rec_time.timestamp(), tz=timezone.utc)

def mtime_int(tm):
    return int(to_datetime(tm).timestamp() * 1000)

def mtime_str(tm):
    return str(int(to_datetime(tm).timestamp() * 1000))

def timeline(*args, precision='ms'):
    return np.array(args, dtype=f'datetime64[{precision}]')


def ts_round(t, freq):
    freq = to_offset(freq)
    return pd.Timestamp((t.value // freq.delta.value) * freq.delta.value)

######### REST helpers #################################
def _httpGet(*args, **kwargs):
    if "url" not in kwargs:
        raise Atl_Error("_http_get_json Error: URL is a required parameter")
    elif len(kwargs["url"]) < 10:
        raise Atl_Error("_http_get_json Error: Invalid URL passed: {}".format(kwargs["url"]))
    else:
        if "backoff_factor" not in kwargs:
            kwargs["backoff_factor"] = 0.3
        if "headers" not in kwargs or kwargs["headers"] == None:
            kwargs["headers"] = {}
        if "params" not in kwargs:
            kwargs["params"] = {}
        if "retries" not in kwargs:
            kwargs["retries"] = 3
        if "status_forcelist" not in kwargs:
            kwargs["status_forcelist"] = (500, 502, 504)
        if "timeout" not in kwargs or kwargs["timeout"] == None:
            kwargs["timeout"] = 10
        if "verify" not in kwargs:
            kwargs["verify"] = None
        resp = _requests_retry_session(
                    backoff_factor=kwargs["backoff_factor"],
                    retries=kwargs["retries"],
                    status_forcelist=kwargs["status_forcelist"]
                ).get(
                url=kwargs["url"],
                headers=kwargs["headers"],
                params=kwargs["params"],
                timeout=kwargs["timeout"],
                verify=kwargs["verify"]
            )
        resp.raise_for_status()
        return resp

def _httpPost( *args, **kwargs):
    if "payload" not in kwargs:
        raise Atl_Error("_http_post_json Error: Empty payload")
    elif "url" not in kwargs:
        raise Atl_Error("_http_post_json Error: URL is a required parameter")
    elif len(kwargs["url"]) < 10:
        raise Atl_Error("_http_post_json Error: Invalid URL passed: {}".format(kwargs["url"]))
    else:
        if "backoff_factor" not in kwargs:
            kwargs["backoff_factor"] = 0.3
        if "headers" not in kwargs or kwargs["headers"] == None:
            kwargs["headers"] = {}
        if "params" not in kwargs:
            kwargs["params"] = {}
        if "retries" not in kwargs:
            kwargs["retries"] = 3
        if "status_forcelist" not in kwargs:
            kwargs["status_forcelist"] = (500, 502, 504)
        if "timeout" not in kwargs or kwargs["timeout"] == None:
            kwargs["timeout"] = 10
        if "verify" not in kwargs:
            kwargs["verify"] = None
        resp = _requests_retry_session(
                    backoff_factor=kwargs["backoff_factor"],
                    retries=kwargs["retries"],
                    status_forcelist=kwargs["status_forcelist"],
                ).post(
                url=kwargs["url"],
                headers=kwargs["headers"],
                params=kwargs["params"],
                json=kwargs["payload"],
                timeout=kwargs["timeout"],
                verify=kwargs["verify"],
            )
        resp.raise_for_status()
        return resp

def _requests_retry_session(backoff_factor=0.3, status_forcelist=(500, 502, 504), retries=3, session=None):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = requests.adapters.HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    return session

mdata_endpoint = f"https://mdata.{BCM.env}.at0l.io"
def get_mdata(key):
    try:
        if '.' in key:
            key = key.replace('.', '/')
        if key[0] == '/':
            key = key[1:]
        res = get_rest_api(f"{mdata_endpoint}/kvq/tree/{key}").json()
        try:
            return AD(res)[key]
        except Exception as err:
            return res
    except Exception as err:
        logger.info(f"atlc-base.api.get_mdata (key: {key}) requests GET error: {err}")
        return None

def get_rest_api(url, headers=None, params=None, retries=3, timeout=10, verify=None):
    resp = _httpGet(
        url=url,
        headers=headers,
        params=params,
        timeout=timeout,
        verify=verify,
    )
    if resp.status_code < 300:
        return resp
    else:
        logger.error(resp)
        return None

def post_rest_api(url, payload, headers=None, params=None, retries=3, timeout=10, verify=None):
    if not isinstance(payload, str):
        payload = json_safe(payload)
    resp = _httpPost(
        url=url,
        payload=payload,
        headers=headers,
        params=params,
        timeout=timeout,
        verify=verify,
    )

    if resp.status_code < 300:
        return resp
    else:
        logger.error(resp)
        return resp

######### FQDN and Nodename Parser with Cache #################################
FQDND_STORE = AD()
host_re = re.compile(r'(?P<scode>(^(\w){4,8})(?=(?P<inst>(\d{2,4}$))))')
def parse_fqdn(hostname):
    if hostname is None or not isinstance(hostname, str) or len(hostname) < 3:
        logger.info(f"parse_fqdn: cannot deal with - {hostname}")
        return None

    if '_' in hostname:
        cgr, env = hostname.split('_')[1:]
        hostname = f"{hostname.replace('_', '.')}.at0l.io"
    else:
        cgr, env = hostname.split('.')[1:3]

    if hostname in FQDND_STORE and isinstance(FQDND_STORE[hostname], AD):
        return FQDND_STORE[hostname]
    else:
        FQDND_STORE[hostname] = fqdnd = AD()

    nparts = hostname.split('.')
    fqdnd.fqdn = fqdnd.hostname = hostname
    fqdnd.cgr = fqdnd.customer_id = cgr
    fqdnd.dom = nparts[-2]
    fqdnd.domain = '.'.join(nparts[-2:])
    fqdnd.env = env
    fqdnd.host = nparts[0]
    fqdnd.nodename = '_'.join(nparts[:-2])
    fqdnd.nparts = nparts
    fqdnd.shost = '.'.join(nparts[:-3])
    fqdnd.site = None
    fqdnd.subdom = '.'.join(nparts[1:])
    fqdnd.tld = nparts[-1]
    fqdnd.scode = fqdnd.host
    fqdnd.inst = 0
    fqdnd.inum = 0
    fqdnd.slot = 0

    labels = ['host', 'site', 'cgr', 'env', 'dom', 'tld']
    splabels = labels[-(len(nparts) - 1):]
    nsubparts = nparts[-(len(nparts) - 1):]
    for idx, np in enumerate(nsubparts):
        fqdnd[splabels[idx]] = np
    if fqdnd.site is not None:
        fqdnd.cgr_path = f'{fqdnd.site}_{fqdnd.cgr}'
        fqdnd.cgr_parts = f'{fqdnd.site}_{fqdnd.cgr}'
    else:
        fqdnd.cgr_path = fqdnd.cgr
    try:
        res = host_re.match(fqdnd.host)
        if hasattr(res, 'groupdict'):
            hre_res = res.groupdict()
            fqdnd.scode = hre_res['scode']
            fqdnd.inst = hre_res['inst']
            fqdnd.inum = int(hre_res['inst'])
            fqdnd.slot = int(hre_res['inst'][-1])
    except Exception as err:
        logger.info(f'parse_fqdn exception: {err}')
    return fqdnd

def shcmd(cmd, stdin=None, stdout=_sp.PIPE, stderr=_sp.PIPE, shell=True, split=True):
    """Execute command using subprocess and returns a tuple (cmd output, cmd stderr, result code)"""

    rawcmd = []
    if not isinstance(cmd, list):
        cmd = [cmd]

    for c in cmd:
        rawcmd.append("%s" % (c))

    cmdobj = _sp.Popen(rawcmd, stdin=stdin, stdout=_sp.PIPE, stderr=_sp.PIPE, shell=shell)

    try:
        (cmdout, cmderr) = cmdobj.communicate()
        if split:
            cmdout = cmdout.decode().split("\n")
        return  (cmdout, cmderr, int(cmdobj.returncode))
    except OSError as e:
        return (None, e, cmdobj.returncode)

def shout(cmd, split=True):
    try:
        out, err, err_code = shcmd(cmd, shell=True, split=split)
        if err_code:
            return None
        else:
            return out
    except Exception as err:
        logger.exception(f'shout exception: {err}')
        return None

########## metric helpers ###############################################

def find_value(*args, _value_name='nodename', _hint={}, **kwargs):
    if not _value_name:
        return None

    hints = {
        'nodename': ['args', 'steps', 'requestHeaders._rawHeaders'],
        'step_name': ['records', 'steps']
    }
    hints.update(_hint)
    value = None

    def get_value_by_name(tgt, value_name):
        if not tgt or not value_name:
            return None
        value = None
        if hasattr(tgt, value_name):
            value = getattr(tgt, value_name)
        elif hasattr(tgt, 'keys') and value_name in tgt:
            value =  tgt[value_name]
        elif hasattr(tgt, 'values'):
            value = find_value(*tuple(tgt.values()), _value_name=value_name)
        elif hasattr(tgt, 'deepKeys'):
            for k in tgt.deepKeys():
                if k.rsplit('.', 1)[-1] == value_name:
                    value =  tgt[k]
        elif isinstance(tgt, (list, tuple)):
            value = find_value(*tuple(tgt), _value_name=value_name)
        return value

    for a in [v for v in (list(args) + list(kwargs.values()))]:
        value = get_value_by_name(a, _value_name)
        if value:
            return value
        elif _value_name in hints:
            for lf in hints[_value_name]:
                value = get_value_by_name(get_value_by_name(a, lf), _value_name)
                if value:
                    return value
    return  value

### setup datadog agent
if BCM is not None:
    dd_options = AD({
        'api_key': BCM.endpoints.datadog.core_api_key,
        'app_key': BCM.endpoints.atlapi.dd_app_key
        })
    initialize(**dd_options)
    metric_prefix = f'atl.{BCM.env}.atlapi'
else:
    ddapi = None
    metric_prefix = f'atl.prd.atlapi'

# def send_metric(nodename, metric, exec_time=None, mtype='gauge', options=None, tags=None, value=1):
#     try:
#
#         print(payload)
#         cmd = f"""curl -X POST -H "Content-type: application/json" -d '{payload}' https://app.datadoghq.com/api/v1/series?api_key={BCM.endpoints.datadog.core_api_key}'"""
#         resp = shcmd(cmd)
#         return resp
#     except Exception as err:
#         logger.exception(f"send_metric.datadog_api {nodename} - {metric}: exception calling MetricsCore->submit_metrics -  " + repr(err))

async def send_metric(nodename, metrics):
     try:
        fqdnd = parse_fqdn(nodename)
        if not exec_time:
            exec_time = 1

        tags = [
                f"env:{BCM.env}",
                f"cgr:{fqdnd.cgr}",
                f"scode:{fqdnd.scode}",
                f"nodename:{nodename}"
            ]

        series = [
                dict(
                    host=BCM.fqdn,
                    metric=mtv[0],
                    points=[[time.time(), mtv[2]]],
                    tags=unique(tags + mtv[1]),
                    type='count'
                )
                for mtv in metrics if len(mtv) == 3]

        series.append(dict(
                    host=BCM.fqdn,
                    metric = '.'.join(['atl'] + [fqdnd[i] for i in ['env', 'cgr', 'site'] if fqdnd[i]] + ['ds', 'activity']),
                    points=[[time.time(), value]],
                    tags=tags,
                    type='count'
                )
            )

        resp = await deferToThread(
            functools.partial(
                ddapi.Metric.send,
                series
                )
            )
        logger.debug(f"send_metric response: " + repr(resp))
        return resp

     except Exception as err:
        logger.exception(f"send_metric.datadog_api {nodename} - {metric}: exception calling MetricsCore->submit_metrics -  " + repr(err))


######### decorators ############################################
def execution_time(func, _ftype='sync', threshold=10):
    @functools.wraps(func)
    async def async_timer(*args, **kwargs):
        start = asyncio.get_event_loop().time()
        result = await func(*args, **kwargs)
        duration = time.time() - start
        if duration > threshold:
            logger.debug(f"EXCESSIVE EXECUTION TIME FOR {func.__name__.upper()} = {duration} SEC")
        return result

    @functools.wraps(func)
    @inlineCallbacks
    def deferred_timer(*args, **kwargs):
        start = time.time()
        result = yield func(*args, **kwargs)
        duration = time.time() - start
        if duration > threshold:
            logger.debug(f"EXCESSIVE EXECUTION TIME FOR {func.__name__.upper()} = {duration} SEC")
        return result

    @functools.wraps(func)
    def sync_timer(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        duration = time.time() - start
        if duration > threshold:
            logger.debug(f"EXCESSIVE EXECUTION TIME FOR {func.__name__.upper()} = {duration} SEC")
        return result

    if logger.level < 30:
        if _ftype == 'async':
            return async_timer
        elif _ftype == 'defer':
            return deferred_timer
        else:
            return sync_timer
    else:
        return func

def exec_timed(func, threshold=10):
    return execution_time(func, _ftype='defer', threshold=threshold)

def exec_time_sync(func, threshold=10):
    return execution_time(func, _ftype='sync', threshold=threshold)



def track_metrics(func, _ftype='sync', _metric=None, _mtype='count', _nodename=None, _tags=[], _value=1):
    def sync_metric_decorator(func):
        metric = _metric if _metric else func.__name__
        failure = f"{metric}.failure"
        __mtype = _mtype
        __nodename = _nodename
        __tags = _tags
        __value = _value

        @functools.wraps(func)
        def sync_metric_wrap(*args, **kwargs):
            tags = []
            duration = None
            start = time.time()
            nodename = __nodename if __nodename else find_value(args, kwargs)
            try:
                res = func(*args, **kwargs)
                duration = (time.time() - start)
                if not nodename:
                    logger.error(f"sync_metric_wrap {metric} ERROR: nodename is empty - aborting metric collection")
                    return res
                else:
                    if len(__tags):
                        for t in __tags:
                            if ':{}' in t:
                                tags.append(t.format(find_value(res, _value_name=t.split(':')[0])))
                            else:
                                tags.append(t)
            except Exception as err:
                send_metric(nodename, failure, exec_time=duration, mtype=__mtype, tags=tags, value=__value)
                logger.exception(f"sync_metric_wrap {metric} EXCEPTION: " + repr(err))
                raise
            else:
                send_metric(nodename, metric, exec_time=duration, mtype=__mtype, tags=tags, value=__value)
            return res
        return sync_metric_wrap

    def deferred_metric_decorator(func):
        metric = _metric if _metric else func.__name__
        failure = f"{metric}.failure"
        __mtype = _mtype
        __nodename = _nodename
        __tags = _tags
        __value = _value

        @functools.wraps(func)
        @inlineCallbacks
        def deferred_metric_wrap(*args, **kwargs):
            tags = []
            duration = None
            start = time.time()
            nodename = __nodename if __nodename else find_value(args, kwargs)
            try:
                res = yield func(*args, **kwargs)
                duration = (time.time() - start)
                if not nodename:
                    logger.error(f"deferred_metric_wrap {metric} ERROR: nodename is empty - aborting metric collection")
                    return res
                else:
                    if len(__tags):
                        for t in __tags:
                            if ':{}' in t:
                                tags.append(t.format(find_value(res, _value_name=t.split(':')[0])))
                            else:
                                tags.append(t)
            except Exception as err:
                logger.exception(f"deferred_metric_wrap {metric} EXCEPTION: " + repr(err))
                res  = yield deferToThread(
                            functools.partial(
                                send_metric,
                                nodename,
                                failure,
                                exec_time=duration,
                                mtype=__mtype,
                                tags=tags,
                                value=__value
                            )
                        )
                raise
            else:
                res = yield deferToThread(
                            functools.partial(
                                send_metric,
                                nodename,
                                metric,
                                exec_time=duration,
                                mtype=__mtype,
                                tags=tags,
                                value=__value
                            )
                        )
            return res
        return deferred_metric_wrap

    def async_metric_decorator(func):
        metric = _metric if _metric else func.__name__
        failure = f"{metric}.failure"
        __mtype = _mtype
        __nodename = _nodename
        __tags = _tags
        __value = _value

        @functools.wraps(func)
        async def async_metric_wrap(*args, **kwargs):
            tags = []
            duration = None
            start = asyncio.get_event_loop().time()
            nodename = __nodename if __nodename else find_value(args, kwargs)
            try:
                res = await func(*args, **kwargs)
                duration = (asyncio.get_event_loop().time() - start)
                if not nodename:
                    logger.error(f"async_metric_wrap {metric} ERROR: nodename is empty - aborting metric collection")
                    return res
                else:
                    if len(__tags):
                        for t in __tags:
                            if ':{}' in t:
                                tags.append(t.format(find_value(res, _value_name=t.split(':')[0])))
                            else:
                                tags.append(t)
            except Exception as err:
                send_metric(nodename, failure, exec_time=duration, mtype=__mtype, tags=tags, value=__value)
                logger.exception(f"async_metric_wrap {metric} EXCEPTION: " + repr(err))
                raise
            else:
                send_metric(nodename, metric, exec_time=duration, mtype=__mtype, tags=tags, value=__value)
            return res
        return async_metric_wrap

    if _ftype == 'defer':
        return deferred_metric_decorator(func)
    elif _ftype == 'async':
        return async_metric_decorator(func)
    else:
        return sync_metric_decorator(func)

def smetric(func, *args, _nodename=None, _metric=None, _tags=[], _value=1, **kwargs):
    frapper = track_metrics(func, _ftype='sync', _nodename=_nodename, _metric=_metric, _tags=_tags, _value=_value)
    return frapper(*args, **kwargs)

def dmetric(func, *args, _nodename=None, _metric=None, _tags=[], _value=1, **kwargs):
    frapper = track_metrics(func, _ftype='defer', _nodename=_nodename, _metric=_metric, _tags=_tags, _value=_value)
    return frapper(*args, **kwargs)

def ametric(func, *args, _nodename=None, _metric=None, _tags=[], _value=1, **kwargs):
    frapper = track_metrics(func, _ftype='async', _nodename=_nodename, _metric=_metric, _tags=_tags, _value=_value)
    return frapper(*args, **kwargs)

####### deferToProcess
def callFromProcess(reactor, f, *args, **kw):
    assert callable(f), "%s is not callable" % (f,)
    # lists are thread-safe in CPython, but not in Jython
    # this is probably a bug in Jython, but until fixed this code
    # won't work in Jython.
    reactor.processCallQueue.append((f, args, kw))
    reactor.wakeUp()

def _initProcessPool(reactor, *args, **kwargs):
    if not hasattr(reactor, 'processpool') or reactor.processpool is None:
        reactor.processpool = Pool(*args, **kwargs)
        reactor.processpoolShutdownID = reactor.addSystemEventTrigger(
            'during', 'shutdown', _stopProcessPool, reactor)

def _stopProcessPool(reactor):
    reactor.processpoolShutdownID = None
    pool = getattr(reactor, 'processpool',None)
    if pool:
        pool.close()
        # Block until worker processes are finished
        pool.join()
        reactor.processpool = None

def deferToProcessPool(reactor, pool, f, *args, **kwargs):
    results = pool.apply_async(f, args, kwargs)
    return deferToThread(results.get)


def deferToProcess(f, *args, **kwargs):
    if getattr(reactor,'processpool',None) is None:
        _initProcessPool(reactor)
    return deferToProcessPool(reactor, reactor.processpool,
                              f, *args, **kwargs)
