#!/usr/bin/env python3

from atlapi.attribute_dict import *
from atlapi.core import Core
from atlapi.common import *
from atlapi.image.utilities import *
# from atlapi.observers.observatory import Observatory
# from atlapi.pipes import Plumber
from atlapi.queuing import ATLEvent
# from atlapi.registry import Registry
from atlapi.violations import Violation_Processor
from atlapi.vision import MachineVision

import asyncio
from asyncio import ensure_future
import base64
from collections import namedtuple
from datetime import datetime, timezone
import functools
import io
import iso8601

import logging
import os
logger = logging.getLogger('atlapi')

import psycopg2
import threading
import time
import json
import threading
from twisted.internet import task, reactor, threads
from twisted.internet.threads import deferToThread, blockingCallFromThread
from twisted.internet.defer import ensureDeferred, inlineCallbacks, maybeDeferred, waitForDeferred, Deferred, DeferredQueue


# String constants for (input) function_names
CA_RESULT_FUNCTIONS = ["lpr",  "ocr"]

class CoreServer(Core):
    def __init__(self, aws=None, db=None, fver=None):
        try:
            Core.__init__(self, 'atlapi', aws=aws, db=db, function_version=fver)
            self.mv = MachineVision(fver=fver)
            # self.obx = Observatory()
            # self.pipes = Plumber()
            # self.registry = Registry()
            self.vp = Violation_Processor()
            logger.info(f"atlapi.svcs: {self.svcs.deep_keys()}")
            self.increment = 10

            self.brq.get().addCallback(self.beacon_reading_handler)
            self.carq.get().addCallback(self.ca_result_handler)
            self.edrq.get().addCallback(self.ca_result_handler)
            self.irq.get().addCallback(self.image_reading_handler)
            self.metricq.get().addCallback(self.track_activity)
            self.mvrq.get().addCallback(self.mv_result_handler)
            self.vpq.get().addCallback(self.mv.process)

            self.mv_result_queue = self.AWS.SQS('mv_results')
            self.ca_result_queue = self.AWS.SQS("ca_results")

            self.mv_result_task1 = task.LoopingCall(self.check_for_mv_results)
            self.mv_result_task1.start(2, now=False)
            self.mv_result_task2 = task.LoopingCall(self.check_for_mv_results)
            self.mv_result_task2.start(2, now=False)
            self.mv_result_task3 = task.LoopingCall(self.check_for_mv_results)
            self.mv_result_task3.start(2, now=False)
            self.ca_result_task = task.LoopingCall(self.check_for_customer_attr_results)
            self.ca_result_task.start(2, now=False)

            logger.info("############# Starting atlapi {} #############".format(str(datetime.utcnow())))
        except Exception as err:
            logger.exception(f"atlapi.__init__ exception: " + repr(err))

    ############### Background Queue Workers ############################

    ### mv_results queue task
    @inlineCallbacks
    def check_for_mv_results(self):
        try:
            results = yield self.mv_result_queue.pop(mcount=self.increment)
            if not results or results is None:
                # logger.info("MV result queue {} - no results".format(time.time()))
                return
            if not isinstance(results, list):
                results = [results]

            req_fields = ["customer_id", "gateway_id", "records"]
            for r in results:
                try:
                    if isinstance(r, ATLEvent):
                        r = r.data
                    if isinstance(r, dict):
                        r = AD(r)
                    if 'headers' not in r:
                        r.headers = AD()
                    res = yield self.mvrq.put((r.customer_id, r.headers, (), r.records))
                except Exception as err:
                    logger.exception(f"atlapi.routes.check_for_mv_results handler exception: " + repr(err))
        except Exception as err:
            logger.exception(f"atlapi.routes.check_for_mv_results exception: " + repr(err))

    ### customer attribute result task
    @inlineCallbacks
    def check_for_customer_attr_results(self):
        try:
            results = yield self.ca_result_queue.pop(mcount=self.increment)
            if not results or results is None:
                # logger.info("CA result queue {} - no results".format(time.time()))
                return
            if not isinstance(results, list):
                results = [results]

            req_fields = ["customer_id", "gateway_id", "records"]
            for r in results:
                try:
                    # print("CA results consumer result {}: {}".format(i, r))
                    if isinstance(r, ATLEvent):
                        r = r.data
                    if isinstance(r, dict):
                        r = AD(r)
                    if 'headers' not in r:
                        r.headers = AD()
                    res = yield self.carq.put((r.customer_id, r.headers, (), r.records))
                except Exception as err:
                    logger.exception(f"atlapi.routes.check_for_customer_attr_results handler exception: " + repr(err))
        except Exception as err:
            logger.exception(f"atlapi.routes.check_for_customer_attr_results exception: " + repr(err))

    ############### Endpoints & Handlers ############################

    @Core.APP.route("/")
    @Core.APP.route("/{}".format(Core.API_VERSION))
    @inlineCallbacks
    def index(self, request):
        res = yield self._return_success("index", request)
        return res

    @Core.APP.route("/{}/favicon.ico".format(Core.API_VERSION))
    @inlineCallbacks
    def favicon(self, request):
        res = yield self._return_success("favicon.ico", request, payload=self.favicon)
        return res

    @Core.APP.route("/health")
    @inlineCallbacks
    def health_alias(self, request):
        resp = yield self.health_check(request)
        return resp

    @Core.APP.route("/{}/health".format(Core.API_VERSION))
    @inlineCallbacks
    def health_check(self, request):
        try:
            result = yield self.DB.customer_ready('atl')
            if result:
                return self._return_success(
                    "health_check",
                    request,
                    status=200,
                    payload={"server_status": "ok", "database_health": result},
                )
            else:
                return self._return_error(
                    "health_check",
                    request,
                    status=404,
                    payload={"server_status": "degraded", "database_health": result},
                )
        except Exception as err:
            logger.exception(f"api_server.health_check exception: " + repr(err))


    @inlineCallbacks
    def andon_data_handler(self, customer_id, headers, args, rec):
        try:
            other = AD({sdn: sdr for sdn, sdr in rec.stepData.items() if sdr.function not in ['andonState']})
            andon_records = AD({sdn: AD(sdr.deep_items()) for sdn, sdr in rec.stepData.items() if sdr.function == 'andonState'})
            andon_base = None
            for step_name, step in andon_records.items():
                if andon_base is None:
                    rec.stepData.andon_base = andon_base = AD(step)
                    andon_base.step_name = andon_base.name = andon_base.function = 'andon_base'
                    andon_base.update(headers)
                    andon_base.filedata = AD()
                    andon_base.filelist = []
                andon_base.filelist = unique(andon_base.filelist, step.filelist)
                if 'filedata' in step:
                    andon_base.filedata.update(step.filedata)
                    step.output.filedata = step.filedata.items()
                    del step['filedata']
                else:
                    step.output.filedata = []
                step.end_time = datetime.utcnow().isoformat()
                logger.info(f"andon_data_handler {step_name} data: {step}")

                res = yield self.mvrq.put((customer_id, headers, args, [step]))
                res = yield self.metricq.put((
                                headers.nodename,
                                [(f"atl.edge.function", [f'step_name:{step.step_name}', f'step_function:{step.function}'], 1)]
                        ))
        except Exception as err:
            logger.exception(f'andon_data_handler exception: ' + repr(err))


    @Core.APP.route("/{}/beacon_readings".format(Core.API_VERSION), methods=["post", "put"])
    @inlineCallbacks
    def beacon_readings(self, request):
        try:
            customer_id, headers, args, records, raw_data = yield self._req_data(
                "beacon_readings", request
            )
            res = yield self.brq.put((customer_id, headers, args, records, raw_data))
            res = yield self.metricq.put((headers.nodename, [("atlapi.route.beacon_readings", [f'cgr:{customer_id}'], len(records))]))
            return self._return_success("beacon_readings", request)
        except Exception as err:
            logger.exception(f"beacon_readings EXCEPTION: " + repr(err))

    @inlineCallbacks
    def beacon_reading_handler(self, params):
        metrics = []
        customer_id, headers, args, records, raw_data = params
        self.brq.get().addCallback(self.beacon_reading_handler)
        try:
            start = time.time()
            now = int(time.time())
            category = "beacons"
            recId = "{}".format(headers["gateway_id"])
            fname = "{}_{}.json.gz".format(headers["gateway_id"], now)
            bucket, key, path = self.AWS.S3.create_key(customer_id, category, recId, now, fname)
            for rec_data in records:
                rec_data["provenance"].append(self.nodename)
                rec_data["asset_key"] = path
            try:
                res = yield self.AWS.S3.put_customer_data(bucket, key, records, rec_type= "json", compress= False)
                metrics.append((f"atlapi.beacon_reading_handler.s3.rawdata",["step_name:beacon_reading_capture", "step_function:beacon_readng_ingest"],1))
            except Exception as err:
                logger.exception(f"beacon_reading_handler s3 persistence EXCEPTION: " + repr(err))

            try:
                res = yield self.DB.beacon_reading.insert(customer_id, headers, records)
                metrics.append((f"atlapi.db.beacon_reading", [f'step_name:beacon_readings', f'step_function:ble_capture'], 1))
                logger.info(f"routes.beacon_reading_handler persisting raw beacon data for {headers.nodename}:beacon_readings.ble_capture took: {time.time() - start} ")
            except Exception as err:
                logger.exception(f"beacon_reading_handler db persistence exception: " + repr(err))
            res = yield self.metricq.put((headers.nodename, metrics))
        except Exception as err:
            logger.exception(f"beacon_reading_handler:  " + repr(err))
        return True

    @inlineCallbacks
    def ca_result_handler(self, params):
        customer_id, headers, args, records = params
        self.carq.get().addCallback(self.ca_result_handler)
        if not isinstance(records, list):
            records = [records]
        start = time.time()
        for ca_result in records:
            try:
                ca_result = AD(ca_result)
                if "end_time" not in ca_result:
                    ca_result.end_time = datetime.utcnow().isoformat()

                ca_result.collection_time = f"{to_datetime(ca_result.collection_time).timestamp():.6f}"

                _recId = "%(nodename)s/%(camera_id)s/%(step_name)s" % (ca_result)
                prefix = "%(gateway_id)s_%(camera_id)s_%(collection_time)s_%(step_name)s_" % (
                    ca_result
                )

                if 'output' in ca_result:
                    if (ca_result.function in ['lpr'] and "identifier" in ca_result.output and ca_result.output.identifier != 'unknown'):
                        logger.info(f"routes.ca_result_handler {headers.nodename}:{ca_result.step_name}.{ca_result.function} - lpr identifier: {ca_result.output.identifier}")
                        ca_result.unique_id = ca_result.output.identifier
                    elif ca_result.function in ['ocr'] and "workorder_id" in ca_result.output:
                        logger.info(f"routes.ca_result_handler {headers.nodename}:{ca_result.step_name}.{ca_result.function} - workorder_id: {ca_result.output.workorder_id}")
                        ca_result.unique_id = ca_result.output.workorder_id
                    else:
                        continue
                    ca_result.output = self.generate_asset_attribute_records(ca_result)
                    fName = prefix + "metadata.json"
                    bucket, key, _ = self.AWS.S3.create_key(
                        customer_id, ca_result.function, _recId + "/state", ca_result.collection_time, fName
                    )
                    res = yield self.AWS.S3.put_customer_data(bucket, key, ca_result, rec_type="json", compress=False)
                    try:
                        res = yield self.DB.asset_attribute.insert(customer_id, headers, ca_result.output)
                    except psycopg2.IntegrityError as err:
                        logger.exception(f"ca_result_handler dupe key ERROR: " + repr(err))
            except Exception as err:
                logger.exception( f"ca_result_handler EXCEPTION: " + repr(err))
        # logger.info(f"routes.ca_result_handler processed {len(records)} records for {headers} in {time.time() - start}")

    @Core.APP.route("/cm_update", methods=["post"])
    @inlineCallbacks
    def cm_update(self, request):
        try:
            _, headers, args, rec_data, raw_data = yield self._req_data("cm_update", request)
            rdata = CAD(rec_data)
            if 'group' in args and args['group'] == '_customerModel':
                group = args["group"]
                for _cgr in rdata:
                    if _cgr in self.CACHE[group]:
                        logger.debug(f"cm_update skipping cache_update for cgr: {_cgr} with data: {rdata}")
                        # self.CACHE[group][_cgr]._check_update(force=True)
            return self._return_success("cm_update", request)
        except Exception as err:
            logger.exception( f"cm_update EXCEPTION: " + repr(err))

    @Core.APP.route("/{}/external_data".format(Core.API_VERSION), methods=["post", "put"])
    @inlineCallbacks
    def external_data(self, request):
        try:
            customer_id, headers, args, records, raw_data = yield self._req_data("external_data", request)

            res = yield self.external_data_handler((customer_id, headers, args, records))
            if res:
                yield self.metricq.put((headers.nodename, [(f"atlapi.route.external_data", [f'cgr:{customer_id}'], len(records))]))
                return self._return_success("external_data", request)
            else:
                return self._return_error("external_data", request)
        except Exception as err:
            logger.exception(f"api.routes.external_data endpoint exception: " + repr(err))

    @exec_timed
    @inlineCallbacks
    def external_data_handler(self, params):
        metrics = []
        customer_id, headers, args, records = params
        # self.edrq.get().addCallback(self.external_data_handler)
        try:
            if not isinstance(records, list):
                records = [records]
            metrics = []
            for edres in records:
                edres = AD(edres)
                edres.camera_id = 'ed0'
                metadata = AD(edres.as_dict())
                metadata.images  = []
                if edres.function == 'external_data' and edres.step_name == 'connex_event_data':
                    ed_records = self.prepare_connex_data_records(edres)
                elif edres.function == 'external_data' and edres.step_name == 'webform_event_data':
                    ed_records = self.prepare_webform_data_records(edres)
                else:
                    ed_records = []

                for edrec in ed_records:
                    try:
                        edrec.collection_time = f"{to_datetime(edrec.collection_time).timestamp():.6f}"
                        prefix = f"{edrec.gateway_id}_ed0_{edrec.collection_time}_{edrec.step_name}"
                        bucket, key, path = self.AWS.S3.create_key(customer_id,
                                                                   edrec.step_name,
                                                                   f"{edrec.nodename}/{edrec.camera_id}/{edrec.function}",
                                                                   edrec.collection_time,
                                                                   prefix + '.json'
                                                    )
                        res = yield self.AWS.S3.put_customer_data(bucket, key, edrec, rec_type="json")
                        logger.info(f"routes.external_data_handler put record_data result: " + str(res))
                        metadata.images.append(res)
                        edrec.images = [res]
                        metrics.append(
                                (
                                    f"atlapi.external_data_handler.s3.data",
                                    [f'step_name:{edrec.step_name}',f'step_function:{edrec.function}'],
                                    1
                                )
                            )

                        res = yield self.DB.image_step.insert(customer_id, headers, edrec)
                    except Exception as err:
                        logger.exception(f"routes.external_data_handler s3 persistence EXCEPTION: " + repr(err))

                metadata.collection_time = f"{to_datetime(metadata.collection_time).timestamp():.6f}"
                prefix = f"{metadata.gateway_id}_ed0_{metadata.collection_time}_{metadata.step_name}_"
                bucket, key, path = self.AWS.S3.create_key(customer_id,
                                                           metadata.function,
                                                           f"{metadata.nodename}/{metadata.camera_id}/{metadata.step_name}",
                                                           metadata.collection_time,
                                                           prefix + 'metadata.json'
                                             )
                res = yield self.AWS.S3.put_customer_data(bucket, key, metadata, rec_type="json")
                logger.info(f"routes.external_data_handler put metadata result: " + str(res))
                metrics.append(
                        (
                            f"atlapi.external_data_handler.s3.data",
                            [f'step_name:{metadata.step_name}', f'step_function:{metadata.function}'],
                            len(metadata.images)
                        )
                    )
                res = yield self.DB.image_step.insert(customer_id, headers, metadata)
            res = yield self.metricq.put((headers.nodename, metrics))
            return True
        except Exception as err:
            logger.exception(f"atlapi.routes.external_data_handler exception: " + repr(err))
            return False

    @Core.APP.route("/gateway_service_status", methods=["post", "put"])
    @Core.APP.route("/{}/gateway_service_status".format(Core.API_VERSION), methods=["post", "put"])
    @inlineCallbacks
    def gateway_service_status(self, request):
        yield
        return self._return_success("gateway_service_status", request)

    def generate_asset_attribute_records(self, ca_result):
        try:
            ca_result = AD(ca_result)
            images = []
            ca_result.images = [str(img) for img in ca_result.images]

            function_name = "generate_asset_attribute_records"
            ca_result.collection_time = f"{to_datetime(ca_result.collection_time).timestamp():.6f}"

            image_key = {}
            image_key["gateway_id"] = ca_result.gateway_id
            image_key["camera_id"] = ca_result.camera_id
            image_key["collection_time"] = ca_result.collection_time
            image_key["step_name"] = ca_result.step_name
            if 'output' in ca_result and 'filedata' in ca_result.output and len(ca_result.output.filedata):
                image_key["name"] = ca_result.output.filedata[0][0].split('_')[-1]

            image_link = ca_result.input_images[0] if "input_images" in ca_result else None

            ca_result.bundle = list(ca_result.bundle)
            ca_result.bundle.append(function_name)
            ca_result.function = function_name
            ca_result.step_name = function_name

            record_tmpl = AD({
                "asset_id": ca_result.unique_id,
                "attribute_value": None,
                "qualifier": None,
                "first_seen_time": ca_result.collection_time,
                "latest_seen_time": ca_result.collection_time,
                "asset_info": {
                    "source_image_key": image_key,
                    "source_image_link": image_link
                }
            })

            output = []
            if 'subjects' in ca_result:  #If subjects key present we are likely dealing with ocr output
                image_key["name"] = ca_result.images[0].split('_')[-1] if ca_result.images else 'sourceimg.jpg'
                for subject in ca_result.subjects:
                    record = AD(record_tmpl)
                    if subject in ca_result.output and ca_result.output[subject]:
                        record.attribute_value = ca_result.output[subject]
                        record.qualifier = subject
                        output.append(record)

            if "beacon_id" in ca_result.output:
                record = AD(record_tmpl)
                record.attribute_value = ca_result.output.beacon_id
                record.qualifier = "beacon_id"
                output.append(record)

            if 'identifier' in ca_result.output:
                record = AD(record_tmpl)
                record.attribute_value = ca_result.unique_id
                record.qualifier = "license_plate"
                output.append(record)

                if ca_result.output.is_whitelist:
                    record = AD(record_tmpl)
                    record.attribute_value = "whitelist"
                    record.qualifier = "qc_tag"
                    output.append(record)

            return output
        except Exception as err:
            logger.exception(f"generate_asset_attribute_records EXCEPTION: " + repr(err))

    @Core.APP.route("/{}/image_reading".format(Core.API_VERSION), methods=["post", "put"])
    @inlineCallbacks
    def image_reading(self, request):
        try:
            customer_id, headers, args, records, raw_data = yield self._req_data(
                "image_reading", request
            )
            if self.DB.customer_ready(customer_id):
                if 'nodename' not in headers and 'gateway_id' in headers:
                    headers.nodename = headers.gateway_id
                res = yield self.irq.put((customer_id, headers, args, records))
                res = yield self.metricq.put((headers.nodename, [(f"atlapi.route.image_reading", [f'cgr:{customer_id}'], len(records))]))
                return self._return_success("image_reading", request)
            else:
                return self._return_error("image_reading", request, status=404, msg="Database not found")
        except Exception as err:
            logger.exception("image_reading EXCEPTION: " + repr(err))

    @exec_timed
    @inlineCallbacks
    def image_reading_handler(self, params):
        metrics = []
        customer_id, headers, args, records = params
        self.irq.get().addCallback(self.image_reading_handler)
        try:
            if not isinstance(records, list):
                records = [records]

            for rec_data in unique(records):
                try:
                    # if any([sdr.function == 'andonState' for sdr in rec_data.stepData.values()]):
                    #     yield self.andon_data_handler(customer_id, headers, args, rec_data)

                    for step_name, step in rec_data["stepData"].items():
                        start = time.time()
                        if not isinstance(step, AD):
                            step = AD(step)

                        step.collection_time = f"{to_datetime(step.collection_time).timestamp():.6f}"

                        step.recId = f"{step.nodename}/{step.camera_id}/{step.step_name}"
                        try:
                            cnt = 0
                            for fname in step["filelist"]:
                                if "json" in fname:
                                    fdata = AD()
                                    fdata.edge_config = step.filedata[fname]
                                    if step.nodename in self.tree._customerModel[customer_id].devices.gateways:
                                        fdata.customerModel = self.tree._customerModel[customer_id].devices.gateways[step.nodename]
                                    else:
                                        fdata.customerModel = AD()
                                else:
                                    fdata = step.filedata[fname]

                                full_fname = f"{step.gateway_id}_{step.camera_id}_{step.collection_time}_{step.step_name}_{fname}"

                                bucket, key, path = self.AWS.S3.create_key(
                                    customer_id,
                                    "image_readings",
                                    step.recId,
                                    step.collection_time,
                                    full_fname
                                )
                                if key.rsplit('.', 1)[-1] == 'jpg':
                                    fdata = pack_image(fdata, color_space='bgr')

                                res = yield self.AWS.S3.put_customer_data(bucket, key, fdata, rec_type=fname.rsplit(".", 1)[-1])
                                step.images.append(res)
                                metrics.append((f"atlapi.image_reading_handler.s3.rawdata",
                                                [f'step_name:{step_name}', f'step_function:{step.function}'],
                                                1
                                            ))
                                cnt += 1
                        except Exception as err:
                            logger.exception(f"image_reading_handler s3 persistence EXCEPTION: " + repr(err))

                        logger.info(f"atlapi.image_reading_handler - received raw data result: {headers.nodename}-{step_name}:{step.function}@{step.collection_time}")

                        if step_name not in ["baseEvent", "base_event"]:
                            if "status" not in step.output:
                                step.output.status = "success"
                            if "reason" not in step.output:
                                step.output.reason = None
                            if 'function' not in step:
                                step.function = 'missing'
                            try:
                                res = yield self.DB.image_step.insert(customer_id, headers, step)
                                step.recorded = True
                                # logger.info(f"atlapi.route:image_reading_handler db image_step record insert:  " + str(res))
                            except psycopg2.IntegrityError as err:
                                logger.exception(f"atlapi.image_reading_handler db image_step record insert EXCEPTION: " + repr(err))
                            metrics.append(("atlapi.image_reading_handler.db.image_step", [f'step_name:{step_name}', f'step_function:{step.function}'], 1))
                        logger.info(f"atlapi.route:image_reading_handler persisting raw data for {headers.nodename}:{step.step_name}:{step.function} took: {time.time() - start} ")
                except Exception as err:
                    logger.exception(f"image_reading_handler record data handler exception: " + repr(err))
                res = yield self.vpq.put((headers, rec_data))
            res = yield self.metricq.put((headers.nodename, metrics))
        except Exception as err:
            logger.exception(f"image_reading_handler exception: " + repr(err))

    @Core.APP.route("/{}/mvresult".format(Core.API_VERSION), methods=["post", "put"])
    @inlineCallbacks
    def mv_result(self, request):
        try:
            customer_id, headers, args, records, raw_data = yield self._req_data("mv_result", request)
            # logger.debug("mv_result received - args: {} - headers: {}".format(args, headers))
            res = yield self.mvrq.put((customer_id, headers, args, records))
            res = yield self.metricq.put((headers.nodename, [(f"atlapi.route.mv_result", [f'cgr:{customer_id}'], len(records))]))
            return self._return_success("mv_result", request)
        except Exception as err:
            logger.exception(f"mv_result endpoint exception: " + repr(err))

    @exec_timed
    @inlineCallbacks
    def mv_result_handler(self, params):
        metrics = []
        customer_id, headers, args, records = params
        self.mvrq.get().addCallback(self.mv_result_handler)
        try:
            nodename = headers.nodename
            save_s3 = True
            if not isinstance(records, list):
                records = [records]
            records = [AD(r) for r in records]

            if records[0].function == 'external_data' and records[0].step_name == 'connex_event_data':
                records = self.prepare_connex_data_records(records[0])
            elif records[0].function == 'external_data' and records[0].step_name == 'webform_event_data':
                records = self.prepare_webform_data_records(records[0])

            records = [m for m in unique(records) if m.step_name not in ["baseEvent", "base_event"]]
            for mvres in records:
                try:
                    start = time.time()
                    if 'step_output' in mvres and 'output' not in mvres:
                        mvres.output = mvres.step_output
                        del mvres['step_output']
                    elif 'step_output' in mvres and 'output' in mvres:
                        mvres.output.update(mvres.step_output)
                        del mvres['step_output']
                    elif 'step_output' in mvres:
                        del mvres['step_output']

                    mvres.collection_time = f"{to_datetime(mvres.collection_time).timestamp():.6f}"

                    if 'resubmitted' in mvres:
                        logger.info(f"routes.mv_result_handler: resubmitted result for nodename - {headers.nodename} step - {mvres.step_name}")
                        save_s3 = False
                    # else:

                    if "images" not in mvres:
                        mvres.images = []
                    category = mvres.step_name
                    _recId =  f"{mvres.nodename}/{mvres.camera_id}/{mvres.step_name}"
                    prefix = f"{mvres.gateway_id}_{mvres.camera_id}_{mvres.collection_time}_{mvres.step_name}_"

                    if 'start_time' not in mvres or not mvres.start_time:
                        if 'startTime' in mvres and mvres.startTime:
                             mvres.start_time = to_datetime(mvres.startTime).isoformat()
                        else:
                            mvres.end_time = datetime.utcnow().isoformat()

                    if 'startTime' in mvres:
                        del mvres['startTime']

                    if 'end_time' not in mvres or not mvres.end_time:
                        if 'endTime' in mvres and mvres.endTime:
                            mvres.end_time = to_datetime(mvres.endTime).isoformat()
                        else:
                            mvres.end_time = datetime.utcnow().isoformat()

                    if 'endTime' in mvres:
                        del mvres['endTime']

                    if save_s3:
                        if "filedata" in mvres.output:
                            try:
                                files = mvres.output.filedata[:]
                                mvres.output.filedata = []

                                cnt = 0
                                for fTuple in files:
                                    if len(fTuple) == 3:
                                        fName, fFormat, fData = fTuple
                                    else:
                                        fName, fData = fTuple
                                        fFormat = "b64"
                                    recId = _recId + "/{}"
                                    if fName.count("_") > 1:
                                        fName = prefix + fName.split("_")[-1]
                                    else:
                                        fName = prefix + fName

                                    if "anonymous" in fName:
                                        recId = recId.format("anonymous")
                                    elif "annotated" in fName or "verbose" in fName:
                                        recId = recId.format("annotated")
                                    elif "json" in fName:
                                        recId = recId.format("metadata")
                                    elif "jpg" in fName:
                                        recId = recId.format("images")
                                    elif "mp4" in fName:
                                        recId = recId.format("movies")
                                    else:
                                        recId = recId.format("other")

                                    bucket, key, path = self.AWS.S3.create_key(
                                        customer_id, mvres.function, recId, mvres.collection_time, fName
                                    )

                                    if key.rsplit('.', 1)[-1] == 'jpg':
                                        fData = pack_image(fData, color_space=mvres.color_space)


                                    res = yield self.AWS.S3.put_customer_data(bucket,
                                                                              key,
                                                                              fData,
                                                                              rec_type=fName.rsplit(".", 1)[-1],
                                                                              compress=False)
                                    mvres.images.append(str(res))
                                    metrics.append(
                                            (
                                                f"atlapi.mv_result_handler.s3.data",
                                                [f'step_name:{mvres.step_name}',f'step_function:{mvres.function}'],
                                                1
                                            )
                                        )
                                    # logger.info(f"mv_result_handler AWS.s3.put_customer_data result: " + str(res))
                                    cnt += 1
                            except Exception as err:
                                logger.exception(f"mv_result_handler s3 persistence EXCEPTION: " + repr(err))

                        try:
                            fName = prefix + "metadata.json"
                            bucket, key, path = self.AWS.S3.create_key(
                                customer_id, mvres.function, _recId + "/state", mvres.collection_time, fName
                            )
                            res = yield self.AWS.S3.put_customer_data(bucket, key, mvres, rec_type="json")
                            metrics.append(
                                    (
                                        f"atlapi.mv_result_handler.s3.data",
                                        [f'step_name:persist_state', f'step_function:result_record'],
                                        1
                                    )
                                )
                        except Exception as err:
                            logger.exception(f"mv_result_handler s3 metadata persistence EXCEPTION: " + repr(err))

                    res = yield self.DB.image_step.insert(customer_id, headers, mvres)
                    res = yield self.carq.put((customer_id, headers, args, [mvres]))
                    if save_s3 and 'violation' in mvres.step_name and 'violations' in mvres.output and mvres.step_name != 'violation_truck':
                        if mvres.output.violation_record and mvres.output.vcount > 0:
                            res = yield self.vp.alert(mvres)
                    metrics.append(
                            (
                                f"atlapi.mv_result_handler.db.image_step",
                                [f'step_name:{mvres.step_name}', f'step_function:{mvres.function}'],
                                1
                            )
                        )
                    logger.info(f"routes.mv_result_handler persisting result data for {nodename}:{mvres.step_name}:{mvres.function} took: {time.time() - start} ")
                except Exception as err:
                    logger.exception(f"mv_result_handler record handler exception: " + repr(err))
            res = yield self.metricq.put((nodename, metrics))
        except Exception as err:
            logger.exception(f"mv_result_handler exception: " + repr(err))

        return True

    def prepare_connex_data_records(self, record):
        entries = record['output']['items'][:]
        tmpl = AD(record)
        tmpl.output = AD()
        tmpl.collection_interval = 4
        records = []
        for r in entries:
            try:
                r = AD(r)
                for k, v in r.deep_items():
                    if isinstance(v, str):
                        v = v.replace("'", '')
                    r[k] = v
                for field_name in ['modifyDate', 'originatorCreatedDateTime', 'sk', 'supplierSalesOrder']:
                    if field_name not in r:
                        logger.info(f"prepare_connex_data_records - field name '{field_name}' not present skipping record: {r.jstr()}")
                        continue
                r.originatorCreatedDateTime = to_datetime(r.originatorCreatedDateTime)
                rec = AD(tmpl)
                rec.output = AD()
                rec.gateway_id = r.sk.sourceTenantRef.split('-')[-1]
                rec.step_name = 'connex_eticket'
                rec.step_function = rec.function = 'external_data'
                rec.camera_id = 'ed0'
                rec.collection_time =  f"{to_datetime(r.originatorCreatedDateTime).timestamp():.6f}"
                rec.start_time = rec.end_time = to_datetime(rec.collection_time).isoformat()
                rec.output.connex_data = r
                rec.output.crn = r.sk.crn
                rec.output.ticket_num = r.sk.lookupKey
                rec.output.secondary_identifier = r.sk.vehicleId
                rec.output.project_name = r.sk.projectName
                rec.output.project_id = r.sk.projectId
                rec.output.site = r.supplierSalesOrder.name.lower().replace(' ', '_').strip('"')
                rec.output.last_modified = r.modifyDate = to_datetime(r.modifyDate).isoformat()
                records.append(rec)
            except Exception as err:
                logger.info(f"prepare_connex_data_records exception {err} with record {r.jstr()}")
                continue
        return records

    def prepare_webform_data_records(self, record):
        rec = AD(record)
        rec.collection_interval = 60
        new_datetime = to_datetime(rec.collection_time)
        rec.collection_time = f"{new_datetime.timestamp():.6f}"
        rec.start_time = rec.end_time = new_datetime.isoformat()
        rec.output = AD({k.lower().replace(' ', '_'): v for k, v in  rec.output.items()})
        records = [rec]
        return records
