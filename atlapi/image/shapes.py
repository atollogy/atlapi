

from atlapi.attribute_dict import AD
from atlapi.common import *
import atlapi.image.planar as planar
from .color import *
from .annotypes import *


import cv2
import functools
import logging
logger = logging.getLogger('atlapi')
from matplotlib import path, transforms
import math
import numpy as np
from planar.util import cached_property, assert_unorderable, cos_sin_deg
from typing import Tuple, Dict, List, Optional, Any, Union

CV2_FONTS = [f for f in dir(cv2) if 'FONT' in f]

COORDINATE_SYSTEMS = """
                            Origin           Coordinate         Axis
Framework          Type    Orientation          System        Numeric-Type     Representation
-----------------------------------------------------------------------------------------------------------------------------
np                  'm'     topLeft             matrix           int           [x_min, y_min, width(columns), height(rows)]
opencv              'c'    bottomLeft         cartesian          int           [x_min, y_min, x_max, y_max]
matplotlib          'c'    bottomLeft         cartesian        int/float       [x_min, y_min, x_max, y_max]
          """
_cartesian = [3, 'c', 'opencv', 'matplotlib']
_matrix = [13, 'm', 'np', 'numpy']

coordinate_systems = AD({
    'np': 'matrix',
    'numpy': 'matrix',
    'opencv': 'cartesian',
    'matplotlib': 'cartesian'
})


def as_point(p, ntype=int, ptype=tuple):
    tlist = []
    if isinstance(p, (AD, dict)):
        tlist.append(ntype(p['x']))
        tlist.append(ntype(p['y']))
    elif hasattr(p, 'x') and hasattr(p, 'y'):
        tlist.append(ntype(p.x))
        tlist.append(ntype(p.y))
    else:
        for i in p:
            tlist.append(ntype(i))
    return ptype(tlist)

as_int = as_int_point = functools.partial(as_point, ntype=int, ptype=tuple)
as_float = as_float_point = functools.partial(as_point, ntype=float, ptype=tuple)

def as_points(*pts, ntype=int, ptype=tuple, wrap=list):
    if len(pts) == 1:
        pts = pts[0]
    return wrap(sorted([as_point(p, ntype=ntype, ptype=ptype) for p in pts]))


as_list_of_int_points = functools.partial(as_points, ntype=int, ptype=tuple)
as_list_of_float_points = functools.partial(as_points, ntype=float, ptype=tuple)
as_tuple_of_int_points = functools.partial(as_points, ntype=int, ptype=tuple, wrap=tuple)
as_tuple_of_float_points = functools.partial(as_points, ntype=float, ptype=tuple, wrap=tuple)


def angle(points):
    pts = sorted(as_list_of_float_points(points[:]))
    xd = (pts[1][0] - pts[0][0])
    yd = (pts[1][1] - pts[0][1])
    angle = 90 if xd == 0 else -1 * (math.degrees(math.atan(yd/xd)))
    return angle

def angle_from_points(points, magnitude=False):
    # points = sorted(points)
    p0 = Point(*points[0])
    if len(points) >= 2:
        p1 = Point(*points[-1])
        angle = p0.angle_to(p1)
        mag = p0.distance_to(p1)
    else:
        angle = p0.angle
        mag = p0.length
    if angle < 0:
        angle = 360 + angle
    angle % 360
    if magnitude:
        return (angle, mag)
    else:
        return angle

def midpoint(pts, ptype=int):
    _pts = sorted(pts)
    return (ptype((_pts[1][0] + _pts[0][0])/2), ptype((_pts[1][1] + _pts[0][1])/2))


class Point(planar.Vec2):
    '''Creates a point on a coordinate plane with values x and y.'''

    def __init__(self, *args, **kwargs):
        if len(args):
            if len(args) == 1:
                if isinstance(args[0], (AD, dict)):
                    planar.Vec2.__init__(self, args[0]['x'] * 1.0, args[0]['y'] * 1.0)
                else:
                    planar.Vec2.__init__(self, args[0][0] * 1.0, args[0][1] * 1.0)
            else:
                planar.Vec2.__init__(self, args[0] * 1.0, args[1] * 1.0)
        else:
            return planar.Vec2.__init__(self, kwargs['x'] * 1.0, kwargs['y'] * 1.0)

    def __str__(self):
        return f"({self.x},{self.y})"

    def __repr__(self):
        return f"({self.x},{self.y})"

    def as_int_point(self):
        return as_point(self, ntype=int)

    def as_float_point(self):
        return as_point(self, ntype=float)

    def distance(self, other):
        if not isinstance(other, Point):
            other = Point(other)
        return self.length_to(other)

    def dumps(self):
        return  self.__repr__()

    def __setstate__(self, state):
        args = tuple(map(float, state[1:-1].split(',')))
        return Point(*args)

    def __order_points(self, pts, ptype='float32'):
        pts = np.array(pts[:4], dtype = ptype)
        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype = ptype)

        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]

        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]

        return rect

    def inside_polygon(self, poly) -> bool:
        # Zones currently are bounding boxes so we can simplify
        # calculations for efficiency and clarity
        if isinstance(poly, Zone):
            is_inside_x = poly.x < self.x < poly.x2
            is_inside_y = poly.y < self.y < poly.y2
            return is_inside_x and is_inside_y

        # Perform Polygon intersection test given poly is a list of vertices
        if isinstance(poly, (dict, AD)):
            poly = poly.values()

        # requires at least 3 vertices for check
        if not isinstance(poly, (list, tuple)) or len(poly) < 3:
            return False

        # Sort polygon vertices so we can produce edges; not perfectly general
        poly = sorted(poly, key=lambda x: x[0])
        poly = [Point(x) for x in poly]

        # create an infinite line extending to the +x
        extended_point = Point(float('inf'), self.y)
        intersection_count = 0

        for v1, v2 in zip(poly, poly[-1:]+poly[:-1]):
            if self.intersects(v1, v2, self, extended_point):
                if self.orientation(v1, self, v2) == 0:
                    return self.on_segment(v1, self,v2)
                intersection_count += 1

        return (intersection_count % 2) == 1

    @classmethod
    def intersects(cls, a1, b1, a2, b2) -> bool:
        o1 = cls.normalized_orientation(a1, b1, a2)
        o2 = cls.normalized_orientation(a1, b1, b2)
        o3 = cls.normalized_orientation(a2, b2, a1)
        o4 = cls.normalized_orientation(a2, b2, b1)

        # General case:
        # a1, b1, a2 and a1, b1, b2 orientations differ and
        # a2, b2, a1 and a2, b2, b1 orientations differ
        if ((o1 != o2) and (o3 != o4)):
            return True

        # Special Cases
        # a1 , b1 and a2 are colinear and a2 lies on segment a1b1
        if ((o1 == 0) and cls.on_segment(a1, a2, b1)):
            return True
        # a1 , b1 and b2 are colinear and b2 lies on segment a1b1
        if ((o2 == 0) and cls.on_segment(a1, b2, b1)):
            return True
        # a2 , b2 and a1 are colinear and a1 lies on segment a2b2
        if ((o3 == 0) and cls.on_segment(a2, a1, b2)):
            return True
        # a2 , b2 and b1 are colinear and a1 lies on segment a2b2
        if ((o4 == 0) and cls.on_segment(a2, b1, b2)):
            return True

        return False

    @classmethod
    def on_segment(cls, a, b, c) -> bool:
        return (
            (b.x <= max(a.x, c.x)) and
            (b.x >= min(a.x, c.x)) and
            (b.y <= max(a.y, c.y)) and
            (b.y >= min(a.y, c.y))
        )

    @classmethod
    def orientation(cls, a, b, c) -> int:
        """
        Accepts three ordered points and returns their orientation
        =0 : colinear points
        >0 : clockwise points
        <0 : counterclockwise
        """
        a, b, c = sorted(as_list_of_float_points(a, b, c), key=lambda p: p[0])
        return (
            float(b[1] - a[1]) * (c[0] - b[0]) -
            float(b[1] - a[0]) * (c[1] - b[1])
        )

    @classmethod
    def normalized_orientation(cls, a, b, c):
        """
        Accepts three ordered points and returns their orientation
        0 : colinear points
        1 : clockwise points
        -1 : counterclockwise
        """
        orientation = cls.orientation(a, b, c)

        if orientation > 0:
            return 1
        elif orientation < 0:
            return -1
        return orientation

    def values(self):
        return (self.x, self.y)

null = Point(0, 0)


class Line(planar.Vec2Array):
    """Sequence of 2D vectors for batch operations
        - This is wrapper class of planar Vec2Array supporting naming and annotation
            options:
                label=None,
                coord='np',
                font=cv2.FONT_HERSHEY_PLAIN,
                font_scale=1,
                color=(255,255,255),
                bold=1-5 line thinkness
    """
    _props = ['bold', 'color', 'coord', 'font', 'font_color', 'font_scale',
              'label', 'alt_label', 'line_color', 'line_type', 'line_size']

    def __init__(self, vectors, **options):
        if not isinstance(vectors[0], Point):
            vectors = [Point(*p) for p in vectors]
        self._vectors = vectors
        self.label = None
        self.alt_label = None
        self.coord = 1
        self.bold = 1
        self.line_color = tuple(Color("yellow", 0, 255, 255, ctype="bgr"))
        self.line_size = 4
        self.line_type = cv2.LINE_AA
        self.font = cv2.FONT_HERSHEY_PLAIN
        self.font_color = tuple(Color("light_grey", 210, 210, 210, ctype="bgr"))
        self.font_scale = 1

        self.__dict__.update({k: v for k, v in options.items() if k in Line._props})
        if len(self) == 2:
            self.get_midpoint()

    @classmethod
    def from_points(cls, points):
        """Create a new 2D sequence from an iterable of points"""
        self = cls.__new__(cls)
        self._vectors = list(points)
        return self

    def __getitem__(self, index):
        return self._vectors[index]

    def __setitem__(self, index, value):
        if isinstance(index, slice):
            self._vectors[index] = [Point(*i) for i in value]
        else:
            self._vectors[index] = Point(*value)

    def append(self, vector):
        """Append a vector to the end of the array.

        :param vector: Vector to append.
        :type vector: Vec2 or 2-number sequence.
        """
        self._vectors.append(Point(*vector))

    def extend(self, iterable):
        """Append all vectors in iterable to the end of the array.

        :param iterable: Iterable object containing vectors.
        """
        self._vectors.extend(Point(*vector) for vector in iterable)

    def as_float_points(self):
        return [tuple(self[0]), tuple(self[1])]

    # def angle(self):
    #     if len(self) > 2:
    #         pts = sorted(self, reverse=True)
    #         return pts[0].angle_to(pts[-1])
    #     else:
    #         return self[0].angle_to(self[1])

    def as_int_points(self):
        return [as_point(self[0], ntype=int), as_point(self[1], ntype=int)]

    def as_float_points(self):
        return [as_point(self[0], ntype=float), as_point(self[1], ntype=float)]

    def invert(self):
        pts = self[:]
        self[0] = pts[1]
        self[1] = pts[0]

    def length(self):
        if len(self) > 2:
            pts = sorted(self)
            return pts[0].distance_to(pts[-1])
        else:
            return self[0].distance_to(self[1])

    magnitude = length

    def set(self, prop, value):
        if prop in Line._props:
            setattr(self, prop, value)

    def get_midpoint(self):
        if not len(self) == 2:
            return None
        self.midpoint = (
            int(self[0][0] + self[1][0]/2),
            int(self[0][1] + self[1][1]/2)
        )
        return self.midpoint

    def draw(self, img):
        cv2.line(img, (int(self[0].x), int(self[0].y)), (int(self[1].x), int(self[1].y)), self.line_color, self.line_size)

    def draw_and_label(self, img, alt=False):
        cv2.line(img, (int(self[0].x), int(self[0].y)), (int(self[1].x), int(self[1].y)), self.line_color, self.line_size)
        cv2.putText(img, self.label, self.midpoint, cv2.FONT_HERSHEY_PLAIN, 1, (210, 210, 210), 1, cv2.LINE_AA)


class Zone(list):
    """defines a bounding box based on these types:
                                  Origin           Coordinate         Axis
     Framework          Type    Orientation          System        Numeric-Type     Representation
    -----------------------------------------------------------------------------------------------------------------------------
     np                  'm'     topLeft             matrix           int           [x_min, y_min, width(columns), height(rows)]
     opencv              'c'    bottomLeft         cartesian          int           [x_min, y_min, x_max, y_max]
     matplotlib          'c'    bottomLeft         cartesian        int/float       [x_min, y_min, x_max, y_max]
     to-do tf-bbox*      'tc'   bottomLeft      transposed_cartesian float/%        [y_min, x_min, y_max, x_max]
     to-do tf-image**    'tm'    topLeft        transposed_matrix    float/%        [y_min, x_min, height, width]
     * requires transpose_cartesian(<reference_shape>)
     * requires transpose_matrix(<reference_shape>)

     Zone types are defined as:
     - 'm' = matrix
     - 'c' = cartesian
     to-do - 'tc' = transposed_cartesian
     to-do - 'tm' = transposed_matrix

     Threshold
     - percentage overlap given as a float"""
    __getitem = list.__getitem__

    def __init__(self, label, x, y, w, h, atype=None, exclusionpts=[], ptype=int, result=None, rtype=None, threshold=0.5):
        self.label = label
        self.exclusionpts = [Point(*p) for p in exclusionpts]
        self.result = result
        self.rtype = rtype if rtype else atype
        self._threshold = float(threshold) if float(threshold) <= 1.0 else 0.5
        self.m = AD()
        self.c = AD()
        self.atype_name = atype
        self.atype = None
        self.__calc(ptype(x), ptype(y), ptype(w), ptype(h))
        list.__init__(self, (ptype(x), ptype(y), ptype(w), ptype(h), self._threshold))

    def __contains__(self, subject):
        '''Test if a given shape "subject" can be considered within the area defined by self,
        a zone of interest.  It is a measure of the overlap between the two objects as a percentage
        of the subject's area.  If the overlap_area is greater than or equal to subject.area times
        self._threshold, then the subject is considered within the zone of interest.  The way to
        look at it is "can the subject be considered sufficiently within self"'''

        if (
            not isinstance(subject, Zone) and
            isinstance(subject, (list, tuple)) and
            len(subject) >= 4
        ):
            return self.overlaps(Zone.create("subject", list(subject)[:4]))
        elif isinstance(subject, Zone):
            return self.overlaps(subject)
        else:
            return subject in self.list

    def __calc(self, x, y, w, h):
        self.exclude = False
        self.dset = False
        self.x = self.m.x = x
        self.y = self.m.y = y
        self.h = self.m.h = h
        self.w = self.m.w = w
        self.x2 = self.m.x2 = self.x + self.w
        self.y2 = self.m.y2 = self.y + self.h
        self.xd = (self.x2 - self.x)
        self.yd = (self.y2 - self.y)
        self.xmid = self.x + (self.xd / 2)
        self.ymid = self.y + (self.yd / 2)
        self.x_slice = slice(self.x, self.x2)
        self.y_slice = slice(self.y, self.y2)
        self.sd = (self.yd + self.xd) / 2
        self.sqx_slice = slice(int(self.xmid - self.sd/2), int(self.xmid + self.sd/2))
        self.sqy_slice = slice(int(self.ymid - self.sd/2), int(self.ymid + self.sd/2))
        self.list = [self.x, self.y, self.w, self.h]
        self.list2 = [self.x, self.y, self.x2, self.y2]
        self.area = self.w * self.h
        self.slope = self.yd/self.xd if self.xd else 1
        self.inverse_slope = self.xd/self.yd if self.yd else 0
        self.length = math.sqrt(self.xd**2 + self.yd**2)
        self.angle = math.degrees(math.atan(self.slope))
        self.inv_angle = math.degrees(math.atan(self.inverse_slope))
        self.midpoints = AD({
            'top': Point(int(self.x + self.w/2), self.y),
            'bottom': Point(int(self.x + self.w/2), self.y2),
            'left': Point(self.x, int(self.y + self.h/2)),
            'right': Point(self.x2, int(self.y + self.h/2))
        })
        self.centroid = self.sd_centroid = Point(int(self.x + self.w/2),int(self.y + self.h/2))
        if self.h > self.w and self.w/self.h < 0.9:
            self.sd_centroid = Point(int(self.x + self.w/2), int((self.y + self.yd *0.65)))

        self.annotation = AD({
            'atype_name': self.atype_name,
            'coords': [self.x, self.y, self.w, self.h],
            'label': self.label,
            'point': self.sd_centroid,
            'result': self.result,
            'rtype': self.rtype,
            'threshold': self.threshold
        })

        # matrix orientation - top_left to bottom_right  points
        self.m.origin = Point(self.x, self.y)
        self.m.term = Point(self.x, self.y2)
        self.line = (self.m.origin, self.m.term)
        self.tLeft = self.m.tLeft = Point(self.x, self.y)
        self.bLeft = self.m.bLeft = Point(self.x, self.y2)
        self.tRight = self.m.tRight = Point(self.x2, self.y)
        self.bRight = self.m.bRight = Point(self.x2, self.y2)
        self.m.corners = AD(
            {"tl": self.tLeft, "tr": self.tRight, "bl": self.bLeft, "br": self.bRight}
        )
        self.m.sides = AD({
            'top': (self.m.corners.tl, self.m.corners.tr),
            'bottom': (self.m.corners.bl, self.m.corners.br),
            'left': (self.m.corners.tl, self.m.corners.bl),
            'right': (self.m.corners.tr, self.m.corners.br)
        })
        self.m.slopes = AD(
            {
                "tl": {
                    "origin": self.tLeft,
                    "term": self.bRight,
                    "line": (self.tLeft, self.bRight),
                },
                "tr": {
                    "origin": self.tRight,
                    "term": self.bLeft,
                    "line": (self.tRight, self.bLeft),
                },
                "bl": {
                    "origin": self.bLeft,
                    "term": self.tRight,
                    "line": (self.bLeft, self.tRight),
                },
                "br": {
                    "origin": self.bRight,
                    "term": self.tLeft,
                    "line": (self.bRight, self.tLeft),
                },
            }
        )
        self.m.list = [self.x, self.y, self.x2, self.y2]
        for slp, sdict in self.m.slopes.items():
            self.m.slopes[slp] = self.slope_angle_length(sdict)

        # Cartesian orientation - bottom_left to top_right points
        self.c.tLeft = Point(self.x, self.y2)
        self.c.bLeft = Point(self.x, self.y)
        self.c.tRight = Point(self.x2, self.y2)
        self.c.bRight = Point(self.x2, self.y)
        self.c.corners = AD(
            {
                "tl": self.c.tLeft,
                "tr": self.c.tRight,
                "bl": self.c.bLeft,
                "br": self.c.bRight,
            }
        )
        self.c.sides = AD({
            'top': (self.c.corners.tl, self.c.corners.tr),
            'bottom': (self.c.corners.bl, self.c.corners.br),
            'left': (self.c.corners.tl, self.c.corners.bl),
            'right': (self.c.corners.tr, self.c.corners.br)
        })
        self.c.list = [self.x, self.y, self.x2, self.y2]
        self.excludes_any()

    @staticmethod
    def create(
        label,
        shape,
        atype="subject",
        threshold=0.4,
        notwh=False,
        exp=0,
        exclusionpts=[],
        ptype=int,
        result=None,
        rtype=None
    ):
        if shape is None or not isinstance(shape, (list, tuple, np.ndarray, Zone)) or len(shape) < 4:
            logger.exception(f"Zone shape should be (x, y, w, h). Input value has wrong format: {shape}")
            return None
        if isinstance(shape, Zone):
            return shape
        if notwh:
            shape[2] = shape[2] - shape[0]
            shape[3] = shape[3] - shape[1]
        threshold = float(shape[4]) if len(shape) > 4 else float(threshold)
        if ptype:
            shape = [ptype(x) for x in shape[:4]]

        return Zone(
            label,
            shape[0] - exp,
            shape[1] - exp,
            shape[2] + exp * 2,
            shape[3] + exp * 2,
            atype=atype,
            ptype=ptype,
            result=result,
            rtype=rtype,
            threshold=threshold,
            exclusionpts=exclusionpts,
        )

    def excludes_any(self):
        self.exclude = any([p.inside_polygon(self) for p in self.exclusionpts])
        if self.exclude:
            logger.info('image_utilities exclusion point trigger')

    def expand(self, n):
        if self.x >= n and self.y >= n:
            self.__calc(
                self.x - n,
                self.y - n,
                self.w + 2 * n,
                self.h + 2 * n
            )
        return self.list

    def get(self, key):
        return self.__dict__.get(key)

    def get_offset(self):
        if self.dset:
            if self.dset == "tl":
                return -self.invAngle
            elif self.dset == "tr":
                return self.invAngle
            elif self.dset == "bl":
                return -self.angle
            elif self.dset == "br":
                return self.angle
        else:
            raise Image_Utility_Exception("Zone diagonal has not been set")

    def nearest(self, other, ppi_x, ppi_y):
        if not isinstance(other, Zone) and isinstance(other , (list, tuple)) and (3 < len(other) < 6):
            other = Zone.create('nearest', other)
        distances = []
        for p1 in self.midpoints:
            p1n, p1p = p1
            for p2 in other.midpoints:
                p2n, p2p = p2
                vector = f"{p1n}-{p2n}"
                xd = (p1[0] - p2[0])/ppi_x
                yd = (p1[1] - p2[1])/ppi_y
                dist = math.hypot(xd, yd)
                distances.append([vector, [p1, p2], dist, dist/12])
        minp = distances[0]
        for d in distances[1:]:
            if d[2] < minp[2]:
                minp = d
        return minp

    def overlaps(self, subject, threshold=None):
        if threshold is None:
            threshold = self.threshold
        path_coords = np.array(
            [subject.tLeft, subject.tRight, subject.bLeft, subject.bRight]
        )
        poly = path.Path(
            np.vstack((path_coords[:, 0], path_coords[:, 1])).T, closed=True
        )
        clip_rect = transforms.Bbox([self.tLeft, self.bRight])
        poly_clipped = poly.clip_to_bbox(clip_rect).to_polygons()

        if len(poly_clipped):
            poly_clipped = poly_clipped[0]
        else:
            return False

        overlap_box = np.array(
            [np.min(poly_clipped, axis=0), np.max(poly_clipped, axis=0)]
        )
        overlap_area = (overlap_box[1, 0] - overlap_box[0, 0]) * (
            overlap_box[1, 1] - overlap_box[0, 1]
        )
        subject_overlap = subject.area * threshold

        if (overlap_area >= self.area):
            return True
        else:
            return (overlap_area >= subject_overlap)

    contains = overlaps

    def re_calc(self):
        self.__calc(
            self.x,
            self.y,
            self.w,
            self.h
        )

    def set_diagonal(self, corner):
        if corner not in self.m.slopes:
            raise Image_Utility_Exception("Corner not found: {}".format(corner))
        if self.dset:
            raise Image_Utility_Exception("Zone diagonal has already been set")
        else:
            for k, v in self.m.slopes[corner].items():
                setattr(self, k, v)
            self.dset = corner
            return self.m.slopes[corner]

    def set_label(self, label, append=False):
        self.label = label if not append else f"{self.label} {label}"

    def shrink(self, n):
        self.__calc(
            self.x + n,
            self.y + n,
            self.w - (2 * n),
            self.h - (2 * n)
        )
        return self.list

    def slope_angle_length(self, td):
        o = td.origin
        t = td.term
        td.xd = xd = (o.x - t.x)
        td.yd = yd = (o.y - t.y)
        td.slope = yd/xd if xd else 1
        td.inverse_slope = xd/yd if yd else 0
        td.angle = math.degrees(math.atan(td.slope))
        td.inv_angle = math.degrees(math.atan(td.inverse_slope))
        td.length = math.sqrt(xd**2 + yd**2)
        return td

    @property
    def threshold(self):
        return self._threshold

    @threshold.setter
    def threshold(self, threshold):
        self._threshold = float(threshold / 100 if threshold > 1 else threshold)
        self.re_calc()
