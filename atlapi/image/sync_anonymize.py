#!/usr/bin/env python3.6

from atlapi.attribute_dict import AD
from atlapi.common import *
from .image_functions import *

import base64
import cv2
from datetime import datetime, date, timedelta, timezone
import functools
import io
import json
import logging
import numpy as np
import os
import requests
import requests.exceptions
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred

logger = logging.getLogger('atlapi')

subject = AD({
    "confidence_thresh": 0.4,
    "nms_threshold": 0.4,
    "states": {
        "person": [
            [
                "bbox",
                None
            ],
            True,
            True,
            True
        ]
    }
})

anonymize_step_cfg = AD({
    "blur_boxes": True,
    "color_space": "rgb",
    "function": "friends",
    "input_crop": None,
    "input_image_indexes": [
        0
    ],
    "input_images": [],
    "input_step": "default",
    "region_filter": False,
    "regions": {
    },
    "brightness": {
        "track_history_length": 2,
        "low_threshold": 20,
        "alert": 3,
        "normalize": False,
        "track": True,
        "high_threshold": 150
    },
    "subjects": {
    }
})

@inlineCallbacks
def anonymize_image(image, step_cfg, region=None, region_name='work_area'):
    payload = AD(anonymize_step_cfg.copy())
    headers = AD({"Content-Type": "application/json"})
    for k in ['collection_time', 'customer_id', 'gateway_id', 'nodename']:
        headers[k] = str(step_cfg[k])
        anonymize_step_cfg[k] = step_cfg[k]
    for k in ['cid', 'scid', 'bundle']:
        anonymize_step_cfg[k] = step_cfg[k]

    payload.image = pack_image(image, b64=True, color_space=step_cfg.color_space)

    if region is not None and isinstance(region, list) and len(region) >= 4:
        payload.region_filter = True
        payload.regions[region_name] = region
        payload.subjects['operators'] = AD(subject.copy())
    else:
        payload.subjects['people'] = AD(subject.copy())

    try:
        resp = yield requests.post(
                    json=payload,
                    headers=headers,
                    url=f"{BCM.endpoints.mvapi.url}/anonymize"
                )
        resp.raise_for_status()
        mvres = AD(resp.json())
        # logger.info(f'anonymize_image: results {AD({k: v for k, v in mvres.items() if k != "filedata"}).jstr()}')
        if "output.filedata" in mvres:
            fdata = [fb[1] for fb in mvres.output.filedata if 'anonymous' in fb[0]]
            if len(fdata):
                # logger.info(f'anonymize_image: image type after unpacking - type: {type(image)} | size: {len(image)}')
                return load_image(bytearray(base64.b64decode(fdata[0])), color_space='bgr')
        # logger.info(f'annoymize_image exception - anonymized image returned null {resp} - {AD({k: v for k, v in mvres.items() if k != "filedata"}).jstr()}')
        return None
    except requests.exceptions.HTTPError as err:
        logger.exception(f"annoymize_image exception - failure response {resp}: {err}")
        return None
