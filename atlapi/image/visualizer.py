
from atlapi.attribute_dict import AD
from atlapi.common import *
from .annotation import *
from .image_functions import *
import cv2
import io
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from skimage import measure
from typing import Tuple, Dict, List, Optional, Any, Union


def save_visualized_img(
    img: np.ndarray, title: str, save_path: str, return_value: object
) -> None:
    img_id = id(img)
    plt.figure(num=img_id)
    plt.title(title)
    plt.imshow(img)
    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    plt.close(img_id)
    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    return_value.filedata.append((save_path, "bio", image))

####### sticker_base ######

def save_two_img(
    img_1: np.ndarray,
    img_2: np.ndarray,
    title_1: str,
    title_2: str,
    save_path: str,
    return_value: object,
) -> None:
    img1_id = id(img_1)
    img2_id = id(img_2)

    plt.figure(num=img1_id)
    plt.title(title_1)
    plt.imshow(img_1)

    plt.subplot(2, 1, 2)
    plt.title(title_2)
    plt.imshow(img_2)

    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    plt.close(img1_id)

    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    return_value.filedata.append((save_path, "bio", image))


def save_each_color_mask(
    hsv_img: np.ndarray,
    draw_mask: np.ndarray,
    color_pick: List[int],
    color_title: str,
    minpixel: float,
    return_value: AD,
) -> None:

    ### draw each color filter result
    draw_img = np.zeros(hsv_img.shape).astype("uint8")
    copy_mask = draw_mask.copy()
    copy_mask[copy_mask == 1] = 255
    copy_mask[copy_mask == 2] = color_pick[0]
    draw_img[:, :, 0] = copy_mask

    copy_mask = draw_mask.copy()
    copy_mask[copy_mask == 1] = 255
    copy_mask[copy_mask == 2] = color_pick[1]
    draw_img[:, :, 1] = copy_mask

    copy_mask = draw_mask.copy()
    copy_mask[copy_mask == 1] = 255
    copy_mask[copy_mask == 2] = color_pick[2]
    draw_img[:, :, 2] = copy_mask

    img_id = id(draw_img)
    plt.figure(num=img_id)
    plt.title(color_title + " / minpixel: " + str(minpixel))
    plt.imshow(draw_img)
    plt.text(
        -30,
        -50,
        "white is noise pixel",
        bbox=dict(facecolor="red", alpha=0.5),
        fontsize=10,
        color="white",
    )

    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    return_value.filedata.append(("verbose/" + "3-" + color_title + ".jpg", "bio", image))

    draw_label = draw_mask.copy()
    draw_label[draw_label != 0] = 1
    labels = measure.label(draw_label, connectivity=1, background=0)
    for label in np.unique(labels):
        if label == 0:
            continue
        labelMask = np.zeros(draw_label.shape, dtype="uint8")
        labelMask[labels == label] = 1
        numPixels = np.count_nonzero(labelMask)

        if numPixels >= minpixel:
            cnts, hierarchy = cv2.findContours(
                labelMask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
            )

            cnt = cnts[0]
            M = cv2.moments(cnt)

            cx = int(M["m10"] / M["m00"]) if M["m10"] != 0 else 0
            cy = int(M["m01"] / M["m00"]) if M["m01"] !=0 else 0

            plt.text(
                cx,
                cy,
                str(numPixels),
                bbox=dict(facecolor="blue", alpha=0.5),
                fontsize=10,
                color="white",
            )

    plt.title("*Text*" + color_title + " / minpixel: " + str(minpixel))
    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    plt.close(img_id)

    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    return_value.filedata.append(("verbose/" + "3-" + color_title + "-text.jpg", "bio", fo))

###########################

####### seven_segment ######

def save_histogram(
    img: np.ndarray,
    brightness_lower_range: int,
    brightness_upper_range: int,
    avg_brightness: int,
    overexposure_threshold: int,
    save_path: str,
    return_value: AD,
) -> None:

    img_id = id(img)
    plt.figure(num=img_id)
    plt.subplot(2, 1, 1)
    plt.title("Image histogram")

    histr, _ = np.histogram(img[:, :, 2].ravel(), 256, [1, 256])
    max_peak = max(histr)
    max_value = max_peak
    plt.plot(histr, "-r", label="red channel")
    histr, _ = np.histogram(img[:, :, 0].ravel(), 256, [1, 256])
    max_peak = max(histr)
    max_value = max(max_peak, max_value)
    plt.plot(histr, "-b", label="blue channel")
    histr, _ = np.histogram(img[:, :, 1].ravel(), 256, [1, 256])
    plt.plot(histr, "-g", label="green channel")
    max_peak = max(histr)
    max_value = max(max_peak, max_value)

    plt.plot()
    y_range = list(range(max_value))
    x_lower = [brightness_lower_range] * len(y_range)
    x_upper = [brightness_upper_range] * len(y_range)
    plt.plot(x_lower, y_range, "--k", label="lower range")
    plt.plot(x_upper, y_range, "-k", label="upper range")
    plt.subplots_adjust(right=0.9)
    plt.legend(bbox_to_anchor=(1.05, 1), loc=0, borderaxespad=0.0)

    if avg_brightness > overexposure_threshold:
        plt.subplot(2, 1, 2)
        plt.title("Shifted brightness to compensate for overexposure")
        plt.imshow(img[:, :, (2, 1, 0)])
    else:
        plt.subplot(2, 1, 2)
        plt.title("Transformed image")
        plt.imshow(img[:, :, (2, 1, 0)])

    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    plt.close(img_id)

    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    return_value.filedata.append((save_path, "bio", image))


def save_color_channel(
    img: np.ndarray, channel_list: str, save_path: str, return_value: AD
) -> None:
    img_id = id(img)
    plt.figure(num=img_id)
    plt.suptitle("Selected color channels {}".format(channel_list))
    plt.subplot(3, 1, 1)

    title_text = "R, pixel count is: " + str(np.count_nonzero(img[:, :, 2]))
    plt.title(title_text)
    plt.axis("off")
    plt.imshow(img[:, :, 2])

    plt.subplot(3, 1, 2)
    title_text = "G, pixel count is: " + str(np.count_nonzero(img[:, :, 1]))
    plt.title(title_text)
    plt.axis("off")
    plt.imshow(img[:, :, 1])

    plt.subplot(3, 1, 3)
    title_text = "B, pixel count is: " + str(np.count_nonzero(img[:, :, 0]))
    plt.title(title_text)
    plt.axis("off")
    plt.imshow(img[:, :, 0])

    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    plt.close(img_id)

    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    return_value.filedata.append((save_path, "bio", image))


def save_padding_area(
    img: np.ndarray,
    sign_border: np.ndarray,
    padded_border: np.ndarray,
    total_digits: int,
    return_value: AD,
) -> None:

    cv_bl = (255, 0, 0)
    cv_rd = (0, 0, 255)

    line_img = img.copy()
    cv2.drawContours(line_img, [sign_border], 0, cv_bl, 2)

    cv2.drawContours(line_img, [padded_border], 0, cv_rd, 1)

    title_text = "Bounding Box image, " + str(total_digits) + " digits"

    save_visualized_img(
        cv2.cvtColor(line_img, cv2.COLOR_BGR2RGB),
        title_text,
        "verbose/" + "1-padding-img.jpg",
        return_value,
    )


def save_each_digit(
    img: np.ndarray,
    each_digit_img: np.ndarray,
    left_list: List[int],
    right_list: List[int],
    total_digits: int,
    save_path1: str,
    save_path2: str,
    return_value: np.ndarray,
) -> None:

    img_id = id(img)
    plt.figure(num=img_id)
    plt.title("dilate image")

    for t in left_list:
        plt.axvline(x=t)

    for t in right_list:
        plt.axvline(x=t)

    plt.imshow(img)
    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    plt.close(img_id)

    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    return_value.filedata.append((save_path1, 'bio', image))

    img_id = id(image)
    plt.figure(num=img_id)
    for i in range(total_digits):
        plt.subplot(1, total_digits, i + 1)
        plt.imshow(each_digit_img[total_digits - 1 - i])
        if i == int(total_digits / 2):
            plt.title("Each digit image")

    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    plt.close(img_id)

    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    return_value.filedata.append((save_path2, 'bio', image))


def save_segment_region(
    img: np.ndarray,
    segments: List[Tuple[Tuple[int, int], Tuple[int, int]]],
    placement: int,
    percentage_info: List[float],
    on_stage_threshold: int,
    text_info: List[str],
    return_value: AD,
) -> None:

    (h, w) = img.shape

    blank_image = np.zeros((h, w, 3), np.uint8)
    blank_image[:, :, 0] = img
    blank_image[:, :, 1] = img
    blank_image[:, :, 2] = img
    for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
        cv2.rectangle(blank_image, (xA, yA), (xB, yB), (255, 0, 0), 1)

    if placement == 1:
        placement = str(placement) + "st_digit"
    elif placement == 2:
        placement = str(placement) + "nd_digit"
    else:
        placement = str(placement) + "th_digit"

    img_id = id(img)
    plt.figure(num=img_id)
    plt.title(placement)
    plt.axis("off")
    plt.imshow(blank_image)

    text_location = 0
    for j in range(len(text_info)):
        text_location += h / 10
        if percentage_info[j] >= on_stage_threshold:
            plt.text(-(w * 0.5), text_location, text_info[j])
        else:
            plt.text(-(w * 0.5), text_location, text_info[j], color="red")

    fo = io.BytesIO()
    plt.savefig(fo, format="jpeg", pad_inches=0)
    plt.close(img_id)

    image = load_image(bytearray(fo.getvalue()), color_space='bgr')
    fname = "6-" + placement + "-recog.jpg"
    return_value.filedata.append(("verbose/" + fname, "bio", image))

###########################
