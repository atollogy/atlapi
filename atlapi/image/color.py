
from atlapi.attribute_dict import *
from atlapi.common import *

import cv2
import logging
import math
import numpy as np
from matplotlib import colors as mcolors
import matplotlib._color_data as mcd
from skimage.color import rgb2lab

logger = logging.getLogger('atlapi')
XKCD_COLORS = AD({k[5:]: v for k, v in mcd.XKCD_COLORS.items()})

### core classes  ############################################################################

class Color(list):
    def __init__(self, label, v1, v2, v3, ctype="bgr", xkcd=None):
        list.__init__(self, [v1, v2, v3])
        self._ =  (v1, v2, v3)
        self.__dict__[label] = [v1, v2, v3]
        self.ctype = ctype
        self.__dict__[ctype] = (v1, v2, v3)

        i = np.zeros((1, 1, 3), np.uint8)
        if ctype in ["bgr", "rgb"]:
            self.__dict__[ctype[::-1]] = (v3, v2, v1)
            i[:] = self.bgr
            self.hsv = tuple(cv2.cvtColor(i, cv2.COLOR_BGR2HSV)[0][0][:])
        elif ctype == "hsv":
            i[:] = self.hsv
            self.bgr = tuple(cv2.cvtColor(i, cv2.COLOR_HSV2BGR)[0][0][:])
            self.rgb = tuple(self.bgr[::-1])
        del i

        self.hex = "".join(["#"] + [hex(v)[2:] for v in self.rgb])
        # if xkcd:
        #     self.mpl = XKCD_COLORS[xkcd]
        # elif label.replace('_', ' ') in XKCD_COLORS:
        #     self.mpl = XKCD_COLORS[label.replace('_', ' ')]
        # else:
        self.mpl = tuple([v / 255 for v in self.rgb])

        self.cnames = {
            "r": "red",
            "b": "blue",
            "g": "green",
            "h": "hue",
            "s": "saturation",
            "v": "value",
        }
        ctypes = ["bgr", "hsv"]
        for ct in ctypes:
            for i, c in enumerate(ct):
                if c in self.cnames:
                    self.__dict__[c] = self.__dict__[ct][i]
                    self.__dict__[self.cnames[c]] = self.__dict__[ct][i]

    def __json_encode__(self):
        return json.dumps(self.__dict__[self.ctype])

    @staticmethod
    def create(label, color, ctype="bgr", xkcd=None):
        if isinstance(color, Color):
            return color
        elif isinstance(color, str):
            return Color.ct[color]
        assert len(color) == 3
        color = [int(c) for c in color]
        ctype = ctype.lower()
        assert len(ctype) == 3
        return Color(label, color[0], color[1], color[2], ctype=ctype, xkcd=xkcd)

class Color_Range(object):
    """Color_Range is either:
        - a single color 2 or 3 item list/tuple = (min, max, [threshold])
        - a single level dict-like object with multiple key/value pairs = {<color_name>: (min, max, [threshold])}
    """

    def __init__(self, rtype, label, min, max, threshold=1.0):
        self.rType = rtype
        self.label = label
        self.min = Color.create("min", min, ctype=rtype)
        self.max = Color.create("max", max, ctype=rtype)
        self.threshold = threshold

    def __repr__(self):
        return json.dumps([self.min, self.max, self.threshold])


    def __json_encode__(self):
        return json.dumps([self.min, self.max, self.threshold])

    @staticmethod
    def create(rtype, label, color_range):
        # color_range is assumed to be either (min, max) or (min, max, threshold)
        if isinstance(color_range, Color_Range):
            return color_range
        elif isinstance(color_range, list) or isinstance(color_range, tuple):
            if len(color_range) == 2:
                return Color_Range(rtype, label, color_range[0], color_range[1])
            elif len(color_range) == 3:
                return Color_Range(
                    rtype, label, color_range[0], color_range[1], threshold=color_range[2]
                )
            else:
                raise Image_Utility_Exception(
                    "Wrong number of items for color range: {}".format(len(color_range))
                )
        elif hasattr(color_range, "keys"):
            color_ranges = {}
            for color_name, color_range in color_range[rtype].items():
                if not isinstance(color_range, Color_Range):
                    color_range = Color_Range.create(rtype, color_name, color_range)
                color_ranges[color_name] = color_range
            return color_ranges
        else:
            raise Image_Utility_Exception(
                "Single color list/tuple or dict of color list/tuples required to create color range"
            )

class ColorLookup:
    def __init__(self, color_definition, **kargs):
        if isinstance(color_definition, (dict, AD)):
            self.table, self.labels = self.__color_table_from_dict(color_definition, **kargs)
        elif isinstance(color_definition, (list,tuple)):
            self.table, self.labels = self.__color_table_from_list(color_definition, **kargs)

        self.normalized = kargs.get('normalized', False)
        self.rgb = kargs.get('rgb', True)

    def __getitem__(self, key):
        return self.__single_color_lookup(key)

    @staticmethod
    def __color_table_from_dict(color_dict, normalized=False, rgb=True, **kargs):
        # {color_name: rgb_definition}
        labels = np.array(color_dict.keys())
        values = np.array([color_dict[label] for label in labels], dtype=float)

        if not normalized:
            values = values / 255

        if rgb:
            original_shape = values.shape
            values = rgb2lab(values.reshape((1,)+original_shape)).reshape(original_shape)

        values.flags.writeable = False
        return values, labels

    @staticmethod
    def __color_table_from_list(color_list, normalized=False, rgb=True, **kargs):
        # [(color_name, rgb_definition), ....]
        labels = np.array([x[0] for x in color_list])
        values = np.array([x[1] for x in color_list], dtype=float)

        if not normalized:
            values = values / 255

        if rgb:
            original_shape = values.shape
            values = rgb2lab(values.reshape((1,)+original_shape)).reshape(original_shape)

        values.flags.writeable = False
        return values, labels

    def __single_color_lookup(self, new_color) -> str:

        if isinstance(new_color, np.ndarray):
            pass
        elif isinstance(new_color, (list, tuple)) and len(new_color) == 3:
            new_color = np.array(new_color)
        else:
            return None

        if not self.normalized:
            new_color = new_color / 255

        if self.rgb: # convert to lab
            original_shape = new_color.shape
            new_color = rgb2lab(new_color.reshape((1,1)+original_shape))[0][0]

        return self.labels[np.argmin(np.linalg.norm(self.table - new_color, axis=1))]

    def color_lookup(self, new_colors: np.ndarray):
        """
        Expects a 2d numpy array of colors of shape (x, 3), returns color strings
        This is about 285x faster than using single color lookups.
            ie 40,000 elements in 14ms instead of 4s with list comprehension
        Lab conversion triples the time taken to execute, so if you can get away with rgb=False, do it
        """
        if not isinstance(new_colors, np.ndarray):
            return None

        if not self.normalized:
            new_colors = new_colors / 255

        if self.rgb:
            original_shape = new_colors.shape
            new_colors = rgb2lab(new_colors.reshape((1,)+original_shape))[0]

        return self.labels[
            np.argmin(
                np.linalg.norm(
                    new_colors.reshape((new_colors.shape[0], 1, 3)) - self.table,
                    axis=2
                ),
                axis=1
            )
        ]

    def extract_colors(self, color_labels: np.ndarray):
        """
        Not optimized, don't use in production code because it is not vectorized.
        """
        color_array = []
        
        for c in color_labels:
            rgb = self.table[np.where(self.labels == c)[0][0]]
            
            if not self.normalized:
                rgb = rgb * 255
            
            color_array.append(rgb)
        return np.array(color_array)

# color table based on https://www.rapidtables.com/web/color/RGB_Color.html
# by using the color object, we get rgb, bgr, and hsv representations of a color in one definition
# access color_table.green.rgb, color_table.green.bgr,  color_table.green.hsv

color_table = AD()
color_table.black = Color("black", 0, 0, 0, ctype="bgr", xkcd='black')
color_table.gray = Color("gray", 128, 128, 128, ctype="bgr", xkcd='grey')
color_table.grey = Color("grey", 128, 128, 128, ctype="bgr", xkcd='grey')
color_table.dark_gray = Color("dark_gray", 100, 100, 100, ctype="bgr")
color_table.dark_grey = Color("dark_grey", 100, 100, 100, ctype="bgr")
color_table.light_gray = Color("light_gray", 200, 200, 200, ctype="bgr", xkcd='light grey')
color_table.light_grey = Color("light_grey", 200, 200, 200, ctype="bgr", xkcd='light grey')
color_table.white = Color("white", 255, 255, 255, ctype="bgr")
color_table.off_white = Color("off_white", 240, 255, 255, ctype="bgr")
color_table.beige = Color("beige", 220, 245, 245, ctype="bgr")
color_table.tan = Color("tan", 140, 180, 210, ctype="bgr")
color_table.light_brown = Color("light_brown", 96, 164, 244, ctype="bgr")
color_table.brown = Color("brown", 42, 42, 165, ctype="bgr")
color_table.red = Color("red", 0, 0, 232, ctype="bgr", xkcd='bright red')
color_table.dark_red = Color("dark_red", 0, 0, 139, ctype="bgr", xkcd='brick red')
color_table.pink = Color("pink", 203, 192, 255, ctype="bgr", xkcd='bright pink')
color_table.hot_pink = Color("hot_pink", 180, 192, 255, ctype="bgr", xkcd='hot pink')
color_table.orange_red = Color("orange_red", 0, 69, 255, ctype="bgr", xkcd='burnt orange')
color_table.orange = Color("orange", 0, 165, 255, ctype="bgr")
color_table.green = Color("green", 0, 128, 0, ctype="bgr")
color_table.lime_green = Color("lime_green", 0, 255, 0, ctype="bgr")
color_table.light_green = Color("light_green", 144, 238, 144, ctype="bgr")
color_table.yellow_green = Color("yellow_green", 50, 205, 154, ctype="bgr")
color_table.yellow = Color("yellow", 0, 255, 255, ctype="bgr")
color_table.light_yellow = Color("light_yellow", 224, 255, 255, ctype="bgr")
color_table.blue = Color("blue", 255, 0, 0, ctype="bgr")
color_table.light_blue = Color("light_blue", 255, 191, 0, ctype="bgr")
color_table.cyan = Color("cyan", 255, 255, 0, ctype="bgr", xkcd='bright cyan')
color_table.dark_cyan = Color("dark_cyan", 255, 255, 0, ctype="bgr", xkcd='dark cyan')
color_table.dark_blue  = Color("dark_blue", 175, 0, 0, ctype="bgr")
color_table.navy_blue = Color("navy_blue", 128, 0, 0, ctype="bgr")
color_table.purple = Color("purple", 128, 0, 128, ctype="bgr")
CT = color_table

######### color_space helpers ############################################

CV2_CONVERTERS = AD()
for k in [c for c in cv2.__dict__.keys() if c.startswith("COLOR_")]:
    kp = k.lower().split("_", 1)[-1].split("2", 1)
    if len(kp) == 2:
        kl = kp[0]
        fColor, toColor = kp
        if fColor not in CV2_CONVERTERS:
            CV2_CONVERTERS[fColor] = AD({toColor: getattr(cv2, k)})
        else:
            CV2_CONVERTERS[fColor][toColor] = getattr(cv2, k)


def color_space_convert(color_space_image, image_color_space, target_color_space):
    try:
        if target_color_space is None or image_color_space.lower() == target_color_space.lower() or color_space_image.shape[2] == 1:
            return color_space_image
        return cv2.cvtColor(color_space_image, CV2_CONVERTERS[image_color_space.lower()][target_color_space.lower()])
    except Exception as err:
        msg = "Image_utilities color_space_convert error: {}".format(err)
        logger.exception(msg)
        return color_space_image

def get_color_space(color_space):
    if not isinstance(color_space, str):
        raise Image_Utility_Exception(
            "Color space must be lowercase string: {}".format(color_space)
        )
    else:
        color_space = color_space.lower()
        if color_space == "bgr":
            return "bgr"
        elif color_space not in CV2_CONVERTERS:
            raise Image_Utility_Exception("Color space not found: {}".format(color_space))
    return getattr(cv2, CV2_CONVERTERS[color_space])
