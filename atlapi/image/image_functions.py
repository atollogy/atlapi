#! /usr/bin/env python3.6

from atlapi.attribute_dict import *
from atlapi.common import *
from .annotypes import *
from .color import *
from .shapes import *

import base64
import binascii
from contextlib import contextmanager
import cv2
import ffmpeg
import functools
import imageio as iio
import io
import logging
logger = logging.getLogger('atlapi')
from logging.handlers import RotatingFileHandler
import math
import  multiprocessing as mp
import numpy as np
import os
from os import devnull, kill
from skimage.measure import compare_ssim
import subprocess
import subprocess as _sp
import tempfile
from typing import Tuple, Dict, List, Optional, Any, Union

### image functions  ############################################################################

def auto_canny(image: np.ndarray, sigma: float = 0.33) -> np.ndarray:
    # compute the median of the single channel pixel intensities
    v = np.median(image)
    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    # return the edged image
    return edged

def boundary_auto_crop(image: np.ndarray,
                       low: int=196,
                       high: int=200,
                       reduce: int=3,
                       square: bool=True) -> np.ndarray:
    img = image.copy()

    # first pass to get rotation angle of target area
    gray = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2GRAY)

    # gray = cv2.medianBlur(gray, 3)
    cv2.GaussianBlur(gray, (5,5), 0, img)
    edged = auto_canny(gray)
    thresh = cv2.threshold(edged, low, high, cv2.THRESH_TOZERO)[-1]
    cnts, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    cnt = cnts[0]
    rect, r_box, angle_of_rotation = get_rotated_rec(cnt)

    # rotate image and crop again
    r_img = image_rotation(img.copy(), angle_of_rotation)
    box = default_zones(image).image
    box.shrink(reduce)
    r_img = r_img[box.y_slice, box.x_slice]

    # second pass to tighten crop
    gray = cv2.cvtColor(r_img.copy(), cv2.COLOR_BGR2GRAY)
    edged = auto_canny(gray)
    thresh = cv2.threshold(edged, low, high, cv2.THRESH_TOZERO)[-1]
    cnts, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    bboxes = sorted([Zone.create('contour', cv2.boundingRect(x)) for x in cnts], key=lambda x: x.area, reverse=True)
    bbox = bboxes[0]
    bbox.shrink(reduce)

    if square:
        return r_img.copy()[bbox.sqy_slice, bbox.sqx_slice]
    else:
        return r_img.copy()[bbox.y_slice, bbox.x_slice]

def check_image(color_space_image: np.ndarray,
                sc: Union[AD, Dict],
                return_value:Union[AD, Dict]):
    dimension_check("check_image", color_space_image)
    return_value.brightness = np.mean(color_space_image)
    return_value.resolution = {
                            "w": color_space_image.shape[1],
                            "h": color_space_image.shape[0]
                        }

    if return_value.brightness < sc.brightness.low_threshold:
        return_value.warning = "warning : Image is very dark {} < {}".format(
            return_value.brightness, sc.brightness.low_threshold
        )
    elif return_value.brightness > sc.brightness.high_threshold:
        return_value.warning = "warning : Image is very bright {} > {}".format(
            return_value.brightness, sc.brightness.high_threshold
        )

def close_subprocess(process):
    if not process:
        return
    try:
        process.terminate()
        process.wait(3)
        if process.returncode is None:
            kill(process.pid, 9)
    except:
        kill(process.pid, 9)

def color_space_zone(color_space_image, image_color_space, zone_color_space, zone):
    try:
        image_color_space = image_color_space.lower()
        zone_color_space = zone_color_space.lower()
        if not zone:
            zone = default_zones(color_space_image).image
        else:
            zone = Zone.create("color_space_zone", zone)

        if image_color_space == zone_color_space:
            img = color_space_image.copy()[zone.y_slice, zone.x_slice]
        elif image_color_space in CV2_CONVERTERS and zone_color_space in CV2_CONVERTERS[image_color_space]:
            img = cv2.cvtColor(color_space_image.copy(), CV2_CONVERTERS[image_color_space][zone_color_space])[
                zone.y_slice, zone.x_slice
            ]
        else:
            raise Image_Utility_Exception(
                "Colorspaces not found: {} {}".format(image_color_space, zone_color_space)
            )
        return dimension_check("color_space_zone", img)

    except Exception as err:
        msg = "Image_utilities color_space_zone error: {}".format(err)
        logger.exception(msg)
        return None

def default_zones(image, atype='subject', center_crop_by=0.6):
    h, w = image.shape[:2]
    cropped_y = int(0.5 * (1 - center_crop_by) * h)
    cropped_x = int(0.5 * (1 - center_crop_by) * w)

    return AD(
        {
            "image": Zone.create(atype, [0, 0, w, h], atype=atype),
            "center": Zone.create(
                "center", [cropped_x, cropped_y, w - cropped_x, h - cropped_y]
            )
        }
    )

def dimension_check(context: str, image: np.ndarray) -> np.ndarray:
    if not hasattr(image, "shape"):
        logger.exception("dimenension_check in context {} input image is wrong type: {}".format(context, type(image)))
    elif not all([d > 0 for d in image.shape]):
        logger.exception("Processing in context {} - image has invalid dimensions: {}".format(context, image.shape))
    return image

def equalize_image(image: np.ndarray, color_space: str) -> np.ndarray:
    img_yuv = color_space_convert(image, color_space, "yuv")
    # equalize the histogram of the Y channel
    img_yuv[:, :, 0] = cv2.equalizeHist(img_yuv[:, :, 0])
    return color_space_convert(img_yuv, "yuv", color_space)
    # media_debug[iname] = ','.join([f'{k}={v}' for k, v in media_debug[iname].items()])

def get_media_metadata(iname: str, media: np.ndarray, media_debug: Union[AD, Dict]) -> AD:
    try:
        fext = iname.rsplit('.', 1)[-1]
        media_debug[iname] = AD()
        media_debug[iname].ftype = fext
        media_debug[iname].size = len(str(media))

        if fext == 'jpg':
            img = load_image(media)
            media_debug[iname].brightness = np.mean(img)
            media_debug[iname].resolution = AD(dict(zip(['width', 'height', 'channels'], img.shape)))
        elif fext == 'mp4':
            with tempfile.NamedTemporaryFile() as temp:
                temp.write(media)
                temp.flush()
                temp.seek(0)
                video = cv2.VideoCapture(temp.name)
                fps = media_debug[iname].fps = video.get(cv2.CAP_PROP_FPS)
                tf = media_debug[iname].total_frames = video.get(cv2.CAP_PROP_FRAME_COUNT)
                media_debug[iname].length = media_debug[iname].duration = tf/fps if fps > 0 else 0
                video.release()
    except Exception as err:
        logger.exception(f"image_utilities.get_media_metadata exception: {err}")

def get_rotated_rec(cnt: np.ndarray) -> Tuple[np.ndarray, np.ndarray, float]:
    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    angle_of_rotation = rect[2]
    M = cv2.getRotationMatrix2D(rect[0], rect[2], 1)
    pts = np.ones((4, 3))
    pts[:,:-1] = box
    r_box = np.int0(np.dot(pts, M.T))
    return rect, r_box, angle_of_rotation

def image_rotation(image: np.ndarray,
                   angle: Union[int, float],
                   center: Tuple[int, int]=None,
                   scale: float=1.0,
                   crop: Zone=None) -> np.ndarray:
    rows, cols = image.shape[:2]
    if center is None:
        center = (cols / 2, rows / 2)
    M = cv2.getRotationMatrix2D(center, angle, scale)
    if crop:
        if not isinstance(crop, Zone):
            crop = Zone.create("imgRotation", crop)
        result_img = cv2.warpAffine(image.copy(), M, (cols, rows))[
            crop.y_slice, crop.x_slice
        ]
    else:
        result_img = cv2.warpAffine(image.copy(), M, (cols, rows))

    return dimension_check("image_rotation", result_img)

rotate_image = image_rotation

def load_image(raw_image: Union[bytearray, bytes, str, io.BytesIO, np.ndarray],
               b64: bool=False,
               color_space: str='bgr',
               crop: Zone=None) -> np.ndarray:
    try:
        image = img_nda = None
        color_space = color_space.lower()
        if b64:
            img_nda = np.asarray(bytearray(base64.b64decode(raw_image.encode())), dtype=np.uint8)
        elif isinstance(image, io.BytesIO):
            img_nda = np.asarray(bytearray(raw_image.getvalue()), dtype=np.uint8)
        elif isinstance(raw_image, bytearray):
            img_nda = np.asarray(raw_image, dtype=np.uint8)
        elif isinstance(raw_image, bytes):
            img_nda = np.asarray(bytearray(raw_image), dtype=np.uint8)
        elif isinstance(raw_image, str):
            if os.path.exists(raw_image):
                image = raw_image = cv2.imread(raw_image)
            else:
                img_nda = np.asarray(bytearray(raw_image.encode()), dtype=np.uint8)
        elif isinstance(raw_image, np.ndarray) and raw_image.ndim == 1:
            img_nda = raw_image
        elif isinstance(raw_image, np.ndarray) and raw_image.ndim == 2:
            img_nda = raw_image
        elif isinstance(raw_image, np.ndarray) and raw_image.ndim >= 3:
            image = raw_image.copy()
        else:
             raise Image_Utility_Exception(f"image_utilities.load_image exception un-supported input image format: {type(raw_image)}")
        if image is None:
            image = cv2.imdecode(img_nda, cv2.IMREAD_COLOR)
        if crop:
            if not isinstance(crop, Zone):
                crop = Zone.create("load_image", crop)
            image = image[crop.y_slice, crop.x_slice]
        if color_space and color_space != 'bgr':
            return color_space_convert(image, 'bgr', color_space)
        else:
            return image
    except Exception as err:
        raise Image_Utility_Exception(f"image_utilities.load_image exception un-supported input image format: " + repr(err))

get_np_image = load_image

def iterate_frames(raw_video):
    with tempfile.NamedTemporaryFile() as temp:
        temp.write(raw_video)
        temp.flush()
        temp.seek(0)
        reader = iio.get_reader(temp, format='mp4')
        for image in reader:
            yield np.asarray(image)
        reader.close()

def load_frames(raw_video):
    success = False
    image = None
    frames = []
    if isinstance(raw_video, str) and os.path.exists(raw_video):
        success, image = video.read()
        return [load_image(i) for i in iio.get_reader(raw_video, format='mp4')]
    else:
        with tempfile.NamedTemporaryFile() as temp:
            temp.write(raw_video)
            temp.flush()
            temp.seek(0)
            return [load_image(i) for i in iio.get_reader(temp, format='mp4')]

def load_n_frames(raw_video, color_space='rgb', min_fcount=12, max_fcount=20, min_duration=15, erate=None, region=None, scene_bridge=10, scene_threshold=None):
    try:
        success = False
        frames = []
        frame_times = []
        video = None

        @contextmanager
        def load_video(raw_video, region=region):
            if isinstance(raw_video, str) and os.path.exists(raw_video):
                vinfo = video_info(raw_video, region=region, scene_bridge=scene_bridge, threshold=scene_threshold)
                yield (raw_video, vinfo)
            else:
                with tempfile.NamedTemporaryFile() as temp:
                    temp.write(raw_video)
                    temp.flush()
                    temp.seek(0)
                    vinfo = video_info(temp.name, region=region, scene_bridge=scene_bridge, threshold=scene_threshold)
                    yield (temp.name, vinfo)

        with load_video(raw_video, region=region) as v:
            start = time.time()
            vname, vinfo = v
            logger.debug(f"image_function.load_frames {vname} vinfo: {vinfo}")

            if scene_threshold and len(vinfo.scenes.forward):
                scenes = vinfo.scenes.forward.values()
            else:
                scenes = [AD({'frames': vinfo.frames, 'fps': vinfo.fps, 'duration': vinfo.duration, 'total_frames': vinfo.total_frames})]
            for scene in scenes:
                if scene.duration > min_duration:
                    if erate is None:
                        erate = 3
                        if scene.duration < 40:
                            fcount = int(min_fcount)
                        else:
                            fcount = int(max_fcount)
                        current_extractions = 0
                        while current_extractions < fcount:
                            current_extractions = scene.duration//erate
                            if current_extractions < fcount:
                                erate = erate * 0.65
                        fstep = int(scene.fps * erate)
                    else:
                        fstep = int(scene.fps * erate)
                        fcount = scene.total_frames//fstep
                else:
                    fstep = int(scene.fps * 2)
                    fcount = scene.total_frames//fstep

                logger.debug(f"image_function.load_frames duration: {scene.duration} - erate: {erate} - frame_count: {fcount} - fstep: {fstep}")
                ia = iio.get_reader(vname, format='mp4')
                frames = [load_image(ia.get_data(i), color_space='rgb') for i in [fi[0] for fi in scene.frames[::fstep]]][:max_fcount]
                if color_space != 'rgb':
                    frames = [color_space_convert(f, 'rgb', color_space) for f in frames]
                frame_times  = [fi[1] for fi in scene.frames[::fstep]][:max_fcount]

        logger.debug(f"image_function.load_n_frames total frames extracted: {len(frames)} in {time.time()-start} seconds")
        return (frames, frame_times, None)
    except Exception as err:
        logger.debug(f"image_function.load_frames exception: " + repr(err))

def make_image_utility_objects(step, indicator=None):
    # color and zone config notation may use lists or tuples
    # make color (r,g,b)|(h,s,v) and color range (min, max, [threshold]) objects
    try:
        if not step:
            return step

        if not isinstance(step, AD):
            step = AD(step)

        if "regions" in step and step.regions:
            step.regions = AD(
                {
                    zn: Zone.create(zn, zone, atype="region")
                    for zn, zone in step.regions.items()
                    if len(zone) >= 4
                }
            )
        if 'input_crop' in step and step.input_crop and len(step.input_crop) >= 4:
            step.input_crop = Zone.create("input_crop", step.input_crop, atype="crop")
        if 'color_space' not in step:
            if 'colorSpace' in step:
                step.color_space = step.colorSpace
            else:
                step.color_space = 'bgr'
        if "subjects" in step:
            for subject_name in step.subjects:
                subject = step.subjects[subject_name]
                if "color_space" not in subject:
                    subject.color_space = step.color_space
                if "colors" in subject:
                    for color_type in subject.colors.keys():
                        for color_name, color_range in subject.colors[color_type].items():
                            subject.colors[color_type][color_name] = Color_Range.create(
                                color_type, color_name, color_range
                            )

                # make zone objects - [x, y, w, h]
                if "input_crop" in subject and subject.input_crop and len(subject.input_crop) >= 4:
                    subject.input_crop = Zone.create("{}_crop", subject.input_crop, atype="crop")
                if (
                    "zone" in subject
                    and isinstance(subject.zone, list)
                    and len(subject.zone) >= 4
                ):
                    subject.zone = Zone.create(subject_name, subject.zone, atype="zone")

    except Exception as err:
        raise Image_Utility_Exception(
            "make_image_utility_objects error for step: {} - error: {}".format(
                step.step_name, err
            )
        )
    return step

def mean_color(color_space_image, zone) -> np.ndarray:
    return np.mean(color_space_image[zone.y_slice, zone.x_slice], axis=(0, 1)).astype(int)

def mmse_compare_images(image: np.ndarray,
                        ref_image: np.ndarray,
                        lower_threshold: int=40,
                        upper_threshold: int=100) -> bool:
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    # see https://www.pyimagesearch.com/2014/09/15/python-compare-two-images/

    err = np.sum((image.astype("float") - ref_image.astype("float")) ** 2)
    err /= float(image.shape[0] * image.shape[1])
    err = err % 100
    logger.debug(f"mmse_compare_images result: {err}")
    return lower_threshold < err < upper_threshold

def order_points(points, dtype='int32'):
    if isinstance(points[0], (AD, dict)):
        if 'int' in dtype:
            pts = as_list_of_int_points(points)
        else:
            pts = as_list_of_float_points(points)
    else:
        pts = points

    pts = np.array(sorted(pts), dtype=dtype)
    # initialize a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype=dtype)

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    return rect

def pack_image(input_image, b64=False, color_space='bgr'):
    image = img_nda = None
    if isinstance(input_image, io.BytesIO):
        img_nda = np.asarray(bytearray(input_image.getvalue()), dtype=np.uint8)
    elif isinstance(input_image, bytearray):
        img_nda = np.asarray(input_image, dtype=np.uint8)
    elif isinstance(input_image, bytes):
        img_nda = np.asarray(bytearray(input_image), dtype=np.uint8)
    elif isinstance(input_image, np.ndarray) and input_image.ndim == 1:
        img_nda = input_image
    elif isinstance(input_image, np.ndarray) and input_image.ndim == 2:
        image = cv2.imdecode(np.asarray(bytearray(input_image.tobytes()), dtype=np.uint8), cv2.IMREAD_COLOR)
    elif isinstance(input_image, np.ndarray) and input_image.ndim >= 3:
        image = input_image.copy()
    else:
         raise Image_Utility_Exception(f"pack_image_utilities.pack_image exception un-supported input pack_image format: {type(input_image)}")

    if image is None:
        image = cv2.imdecode(img_nda, cv2.IMREAD_UNCHANGED)

    if color_space != 'bgr' and image.shape[2] == 3:
        image = color_space_convert(image, color_space, 'bgr')
    packed_image = bytearray(cv2.imencode('.jpg', image)[-1].tobytes())

    if b64:
        return base64.b64encode(packed_image).decode("utf-8")
    else:
        return packed_image

def read_json_label(json_path):
    ### Read json label data
    with open(json_path) as f:
        data_index = json.load(f)
        return data_index

readJsonLabel = read_json_label

def save_annotation(rv, bb, origins, color_space):
    if not 'annotations' in rv:
        rv.annotations = AD()
    if bb.rtype not in rv.annotations:
        rv.annotations[bb.rtype] = AD()
    rv.annotations[bb.rtype][id(bb)] = bb.annotation
    rv.annotations[bb.rtype][id(bb)].origins = origins
    rv.annotations[bb.rtype][id(bb)].color_space = color_space

def save_annotations(rv, bboxes, origins, color_space):
    for bb in [b for b in bboxes if isinstance(b, Zone)]:
        save_annotation(rv, bb, origins, color_space)

@contextmanager
def scratch_space(fid, size='25m'):
    try:
        path='/tmp/scratch_space/{fid}'
        if path not in str(shcmd('mount')[0]):
            if not os.path.exists(path):
                shcmd(f'mkdir -p {path}')
            shcmd(f'''/bin/mount -t tmpfs -o size={size} tmpfs {path}''')
        yield path
    finally:
        shcmd(f'''/bin/umount -l {path}''')

def show_image(iname, img):
    if not hasattr(img, 'shape'):
        img = load_image(img)
    cv2.imshow(iname, img)
    cv2.waitKey(0)

def ssim_compare_images(image: np.ndarray,
                        ref_image: np.ndarray,
                        full: bool=True) -> Tuple[int, int]:
    # NOTE: the two images must have the same dimension
    # see https://www.pyimagesearch.com/2014/09/15/python-compare-two-images/

    (score, diff) = compare_ssim(image, ref_image, full=True)
    diff = (diff * 255).astype("uint8")
    return score, diff

def transcode(raw_video, frame_rate, crop=None, overwrite=True):
    def convert(src):
        tgt, fext = src.rsplit('.', 1)
        tgt += '_tfr_{frame_rate}.{ext}'
        command = '/usr/bin/nice -n10 /usr/bin/ffmpeg -y -hide_banner -loglevel fatal '
        command += '-flags low_delay -strict experimental '
        command += f'-fflags genpts -fflags discardcorrupt -i {src} '
        command += f'-c:v libx264 -crf 24 -filter:v '
        if crop and len(crop) >= 4:
            command += f'\"crop={crop[2]}:{crop[3]}:{crop[0]}:{crop[1]},fps=fps={frame_rate}\" {tgt}'
        else:
            command =+ f'fps=fps={frame_rate} {tgt}'

        with open(devnull, "wb") as dev_null:
            start = datetime.utcnow().timestamp()
            process = _sp.Popen(command.split(' '), stdout=_sp.PIPE, stderr=_sp.PIPE)
            while self.subprocess_is_running(process) and running_time <= timeout:
                time.sleep(1)
                running_time += 1
            else:
                if process.returncode is None and process.poll() is None:
                    close_subprocess(process)
                    return None
        return tgt

    try:
        if isinstance(raw_video, str) and os.path.exists(raw_video):
            tgt = convert(raw_video)
            if not tgt:
                logger.info(f"transcode_failed for {raw_video}")
                return None
            if overwrite:
                os.system(f'mv {tgt} {raw_video}')
                return np.asarray(bytearray(open(raw_video, 'b').read()), dtype=np.uint8)
            else:
                return np.asarray(bytearray(open(tgt, 'b').read()), dtype=np.uint8)
        else:
            with tempfile.NamedTemporaryFile() as temp:
                temp.write(raw_video)
                temp.flush()
                temp.seek(0)
                tgt = convert(raw_video)
                if not tgt:
                    logger.info(f"transcode_failed {temp}")
                    return None
                return np.asarray(bytearray(open(tgt, 'b').read()), dtype=np.uint8)
    except Exception as err:
        logger.exception(f"transcode.excpetion - {err}")
        return None

def video_info(vname, extraction_modes=['forward'], max_scene_length=0, scene_bridge=10, region=None, threshold=None, threshold_decay = 0.10):
    start = time.time()
    vinfo = AD(ffmpeg.probe(vname)['streams'][0])
    vinfo.update(vinfo.tags)
    vinfo.filename = vname
    vinfo.params = AD()
    vinfo.params.extraction_modes = extraction_modes
    vinfo.params.max_scene_length = max_scene_length
    vinfo.params.scene_bridge = scene_bridge
    if not threshold:
        vinfo.params.threshold = 1
    elif isinstance(threshold, (int, float)) and threshold > 1:
        vinfo.params.threshold = threshold/100
    else:
        vinfo.params.threshold  = threshold

    vinfo.params.threshold_decay = threshold_decay
    vinfo.scenes = AD()
    vinfo.stats = AD()

    cmd = f'ffprobe -preset ultrafast -f lavfi -i movie={vname},'
    if region is not None and len(region) >=4:
        cmd += f'crop={region[3]}:{region[2]}:{region[0]}:{region[1]},'
    cmd += f'signalstats -show_entries frame=pkt_pts_time,pict_type:frame_tags=lavfi.signalstats.YDIF -of json'

    ydiff_json = _sp.check_output(cmd.split())
    vinfo.frames = [(i+1, float(x["pkt_pts_time"]), x['pict_type'], float(x['tags']['lavfi.signalstats.YDIF']))
                    for i, x in enumerate(json.loads(ydiff_json)['frames'])]
    vinfo.total_frames = len(vinfo.frames)
    vinfo.duration = float(vinfo.duration)
    vinfo.fps = vinfo.total_frames//vinfo.duration
    logger.debug(f'filename: {vinfo.filename}, frames: {vinfo.total_frames}, duration: {vinfo.duration}')

    if threshold:
        logger.debug(f'beginning scene detection on filename: {vinfo.filename}')
        raw_ydiff_list = sorted([f[-1] for f in vinfo.frames])
        vinfo.stats.oyd_max_diff = max(raw_ydiff_list)
        vinfo.stats.oyd_avg = sum(raw_ydiff_list)/len(raw_ydiff_list)
        vinfo.stats.oyd_len = len(raw_ydiff_list)
        vinfo.stats.oyd_std = math.sqrt(sum([(v - vinfo.stats.oyd_avg) ** 2 for v in raw_ydiff_list])/vinfo.stats.oyd_len)
        msg = f"\toriginal: vinfo.stats.oyd_max_diff: {vinfo.stats.oyd_max_diff}, oyd_avg: {vinfo.stats.oyd_avg}, oyd_len: {vinfo.stats.oyd_len}, oyd_std: {vinfo.stats.oyd_std} - {raw_ydiff_list}\n"

        ydiff_list = raw_ydiff_list[:int(vinfo.stats.oyd_len * 0.98)]
        vinfo.stats.yd_avg = sum(ydiff_list)/len(ydiff_list)
        vinfo.stats.yd_len = len(ydiff_list)
        vinfo.stats.yd_std = math.sqrt(sum([(v - vinfo.stats.yd_avg) ** 2 for v in ydiff_list])/vinfo.stats.yd_len)
        vinfo.stats.yd_max_diff = max(ydiff_list)
        msg += f"\tfiltered: yd_max_diff: {vinfo.stats.yd_max_diff}, yd_avg: {vinfo.stats.yd_avg}, yd_len: {vinfo.stats.yd_len}, yd_std: {vinfo.stats.yd_std} - {ydiff_list}\n"

        vinfo.params.adjusted_threshold = vinfo.params.threshold * vinfo.stats.yd_max_diff
        if vinfo.params.adjusted_threshold < 1.50:
            vinfo.params.activation_threshold = 1.50
        else:
            vinfo.params.activation_threshold = vinfo.params.adjusted_threshold
        msg += f"\tActivation threshold: {vinfo.params.activation_threshold}"
        logger.debug(msg)

        for xm in vinfo.params.extraction_modes:
            if xm == 'reverse':
                mode_frames = vinfo.frames[::-1]
            else:
                mode_frames = vinfo.frames[:]

            vinfo.scenes[xm] = AD()
            scenes = []
            scene_start = 0
            bridge = []
            do_bridging = vinfo.params.scene_bridge > 0
            activation_threshold = vinfo.params.activation_threshold
            num_scenes = 0
            num_scene_breaks = 0
            scene_too_long = False
            for fm in mode_frames:
                if vinfo.params.threshold_decay is not None and ((len(scenes) - num_scene_breaks - 1) > num_scenes):
                    activation_threshold = vinfo.params.activation_threshold - ((vinfo.params.activation_threshold * vinfo.params.threshold_decay) * (len(scenes) - num_scene_breaks))
                    num_scenes += 1
                fnum, frame_time, ftype, ydiff = fm
                activate = (ydiff >= activation_threshold)
                if do_bridging:
                    bridging = len(bridge)
                    if xm == 'reverse':
                        end_bridge = ((bridge[0][1] - frame_time) > vinfo.params.scene_bridge) if bridging else False
                    else:
                        end_bridge = ((frame_time - bridge[0][1]) > vinfo.params.scene_bridge) if bridging else False
                    if end_bridge and activate:
                        scenes[-1].extend(bridge)
                        bridge = []
                        scene_start = frame_time
                        scenes.append([fm])
                    elif end_bridge:
                        scene_start = 0
                        bridge = []
                    elif not scene_start and activate:
                        scene_start = frame_time
                        scenes.append([fm])
                    elif scene_start and activate:
                        if bridging:
                            scenes[-1].extend(bridge)
                            bridge = []
                        scenes[-1].append(fm)
                    elif scene_start:
                        bridge.append(fm)
                else:
                    if vinfo.params.max_scene_length:
                        if xm == 'reverse':
                            scene_too_long = ((scene_start - frame_time) > vinfo.params.max_scene_length) if scene_start else False
                        else:
                            scene_too_long = ((frame_time - scene_start) > vinfo.params.max_scene_length) if scene_start else False
                    if activate:
                        if not scene_start or scene_too_long:
                            scene_start = frame_time
                            scenes.append([fm])
                            if scene_too_long:
                                num_scene_breaks += 1
                                logger.debug(f'Scene exceeded max duration: {vinfo.params.max_scene_length}')
                        else:
                            scenes[-1].append(fm)
                    else:
                        scene_start = 0

            for scn_num, scene in enumerate(scenes):
                if len(scene) >= 3:
                    vinfo.scenes[xm][scn_num] = AD({
                        'duration': scene[-1][1] - scene[0][1],
                        'fps': vinfo.fps,
                        'frames': scene,
                        'total_frames': len(scene)
                    })
                    logger.debug(f"Scene Detection - mode {xm} selected scene #{scn_num} length: {len(scene)}")

    logger.debug(f'vinfo processing time: {time.time()-start} seconds')
    return vinfo
