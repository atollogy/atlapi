import cv2
import numpy as np

from typing import Tuple, List, Union, Optional


# Function Format: function(image, **kargs). This allows easy binding by unpacking
# and passing dictionaries to each function. These could be extracted to classes.

# -------------------- RESIZE/SCALE --------------------
INTERPOLATION_MAP = {
    'area': cv2.INTER_AREA,
    'cubic': cv2.INTER_CUBIC,
    'linear': cv2.INTER_LINEAR,
}


def resize(image: np.ndarray, width: int, height: int,
            interpolation: str = None) -> np.ndarray:
    if width <= 0 or height <= 0:
        raise ValueError(f'resize: width and height must be positive integers')

    if interpolation is None:
        h, w = image.shape[:2]
        interpolation = 'area' if (w+h) < (width + height) else 'linear'

    return cv2.resize(
        image, (width, height),
        interpolation=INTERPOLATION_MAP[interpolation]
    )


def scale(image: np.ndarray, scale_factor: float,
            interpolation: str = None) -> np.ndarray:
    if scale_factor <= 0:
        raise ValueError(f'scale: scale_factor must be positive')

    if interpolation is None:
        interpolation = 'area' if scale_factor < 1.0 else 'linear'

    return cv2.resize(
        image, None, fx=scale_factor, fy=scale_factor,
        interpolation=INTERPOLATION_MAP[interpolation]
    )


# -------------------- CROP --------------------
def crop_pixels(image: np.ndarray, x: int, y: int, width: int,
                height: int) -> np.ndarray:
    h, w = image.shape[:2]

    if not (0 <= x <= w):
        raise ValueError(f'0 ≤ x ≤ {w}')
    elif not (0 <= y <= h):
        raise ValueError(f'0 ≤ y ≤ {h}')

    return image[y:y+height, x:x+width]


def crop_percentage(image: np.ndarray, xmin: float, ymin: float, xmax: float,
                    ymax: float) -> np.ndarray:
    height, width = image.shape[:2]

    if not (0.0 <= xmin <= 100.0 and 0.0 <= xmax <= 100.0):
        raise ValueError(f'0% ≤ x ≤ 100%')
    elif not (0.0 <= ymin <= 100.0 and 0.0 <= ymax <= 100.0):
        raise ValueError(f'0 ≤ y ≤ 100%')

    # Convert to pixels
    xmin = int(xmin/100 * width)
    ymin = int(ymin/100 * height)
    xmax = int(xmax/100 * width)
    ymax = int(ymax/100 * height)

    return image[ymin:ymax, xmin:xmax]


# -------------------- SKEW/ROTATE --------------------
def rotate(image: np.ndarray, angle: float) -> np.ndarray:
    height, width = image.shape[:2]
    center_x, center_y = (width // 2, height // 2)

    # rotate clockwise and get rotation components of matrix
    rot_matrix = cv2.getRotationMatrix2D((center_x, center_y), -angle, 1.0)
    cos_component = np.abs(rot_matrix[0, 0])
    sin_component = np.abs(rot_matrix[0, 1])

    # compute the new bounding dimensions of the image
    new_width = int((height * sin_component) + (width * cos_component))
    new_height = int((height * cos_component) + (width * sin_component))

    # adjust the rotation matrix to take into account translation
    rot_matrix[0, 2] += (new_width / 2) - center_x
    rot_matrix[1, 2] += (new_height / 2) - center_y

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, rot_matrix, (new_width, new_height))


# -------------------- DEFORM/TRANSFORM/WARP --------------------
PixelPoint = Tuple[int, int]

def order_points(pts: Tuple[PixelPoint, PixelPoint, PixelPoint, PixelPoint]
                 ) -> Tuple[PixelPoint, PixelPoint, PixelPoint, PixelPoint]:
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype = "float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect

def deform(image: np.ndarray,
           points: Tuple[PixelPoint, PixelPoint, PixelPoint, PixelPoint],
           width: int = 0, height: int = 0) -> np.ndarray:

    points = np.array(points, dtype = "int16")
    if not rect.shape == (4, 2):
        raise ValueError(f'deform: points must consist of 4 xy coordinates')

    # obtain a consistent order of the points and unpack them individually
    rect = order_points(points)
    (tl, tr, br, bl) = rect

    if width == 0 or height == 0:
        # compute the new image width which is the
        # maximum distance between x-coordinates of either the
        # top_right<->top_left or bottom_right<->bottom_left
        width = int(max(
            np.linalg.norm(tr - tl),  # Top Width
            np.linalg.norm(br - bl)   # Bottom Width
        ))
        # compute the new image height which is the
        # maximum distance between y-coordinates of either the
        # top_left|bottom_left or top_right|bottom_right
        height = int(max(
            np.linalg.norm(tl - bl),  # Left Width
            np.linalg.norm(tr - br)   # Right Width
        ))

    transform_dst = np.float32([[0, 0],[width, 0],[0, height],[width, height]])
    perspective_matrix = cv2.getPerspectiveTransform(rect, transform_dst)
    return cv2.warpPerspective(image, perspective_matrix, (width, height))

# -------------------- GAUSSIAN BLUR --------------------

def gaussian_blur(image: np.ndarray, size: int = 5) -> np.ndarray:
    if not size % 2 == 1:
        raise ValueError(f'gaussian_blur: size must be odd')

    return cv2.GaussianBlur(image, (size, size), cv2.BORDER_DEFAULT)

def median_blur(image, size: int = 5) -> np.ndarray:
    if not size % 2 == 1:
        raise ValueError(f'median_blur: size must be odd')
    return cv2.medianBlur(image, size)

# -------------------- SHARPEN --------------------
SHARPENING_KERNEL = np.float32([
    [ 0, -1,  0],
    [-1,  5, -1],
    [ 0, -1,  0]
])

def sharpen(image: np.ndarray) -> np.ndarray:
    return cv2.filter2D(image, -1, SHARPENING_KERNEL)

def unsharp_mask(image: np.ndarray, kernel_size: int = 5, sigma: float=1.0,
                    amount: float=1.0, threshold: int = 0) -> np.ndarray:
    blurred = cv2.GaussianBlur(image, (kernel_size, kernel_size), sigma)
    sharpened = (amount + 1.0) * image - float(amount) * blurred
    sharpened = np.clip(sharpened, 0, 255).round().astype(np.uint8)

    if threshold > 0:
        low_contrast_mask = np.absolute(image - blurred) < threshold
        np.copyto(sharpened, image, where=low_contrast_mask)

    return sharpened

# -------------------- BRIGHTNESS --------------------

def gamma_correction(image: np.ndarray, gamma: float) -> np.ndarray:
    lut = np.array([
        np.clip(pow(i / 255.0, 1.0 / gamma) * 255.0, 0, 255) for i in range(256)
    ], dtype=np.uint8)

    return cv2.LUT(image, lut)

def linear_brightness_contrast(image: np.ndarray, contrast: float = 1,
                            brightness: float = 0) -> np.ndarray:
    lut = np.array([
        np.clip(contrast * i + brightness, 0, 255) for i in range(256)
    ], dtype=np.uint8)

    return cv2.LUT(image, lut)

def brighten(image: np.ndarray, amount: float) -> np.ndarray:
    return linear_brightness_contrast(image, 1.0, amount)

# -------------------- CONTRAST --------------------

def contrast(image: np.ndarray, factor: float) -> np.ndarray:
    return linear_brightness_contrast(image, factor, 0.0)

# ------------------ CV FUNCTIONS ------------------

def opening(image: np.ndarray, size: int = 5) -> np.ndarray:
    kernel = np.ones((size, size),np.uint8)
    return cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

def otsu_thresholding(image: np.ndarray) -> np.ndarray:
    return cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

# -------------------- MAPPING --------------------

ADJUSTMENT_MAPPING = {
    'resize': resize,
    'scale': scale,
    'rotate': rotate,
    'crop': crop_pixels,
    'ratio_crop': crop_percentage,
    'sharpen': sharpen,
    'unsharp_mask': unsharp_mask,
    'opening': opening,
    'otsu_thresholding': otsu_thresholding,
    'blur': gaussian_blur,
    'median_blur': median_blur,
    'brighten': gamma_correction,
    'brighten_linear': brighten,
    'contrast': contrast,
    'brightness_contrast': linear_brightness_contrast,
    'deform': deform,

    # quick color hacks to allow tesseract ocr to work better if needed
    'rgb2gray': lambda image: cv2.cvtColor(image, cv2.COLOR_RGB2GRAY),
    'bgr2gray': lambda image: cv2.cvtColor(image, cv2.COLOR_BGR2GRAY),
    'gray2rgb': lambda image: cv2.cvtColor(image, cv2.COLOR_GRAY2RGB),
    'gray2bgr': lambda image: cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
}

# NOTE: on failure, adjustment layer will be skipped. This may cause cascading
# failures for layers depending on a specific pixel size
def apply_transforms(image: np.ndarray,
                        adjustments: Tuple[Tuple[str, dict]]) -> (np.ndarray, str):
    status = []

    for x, layer in enumerate(adjustments):
        # Handle incorrect layer specifications
        if len(layer) != 2:
            status.append(f'Layer {x} malformed: {layer}; ignoring')
            continue

        try:
            adjustment, params = layer if len(layer) == 2 else (layer[0], {})
            image = ADJUSTMENT_MAPPING[adjustment.lower()](image, **params)
        except Exception as e:
            status.append(f'Error in layer ({adjustment}, {x}): {e}')

    return image, ('\n'.join(status))
