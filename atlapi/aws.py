#!/usr/bin/env python3

from atlapi.core import Core
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.queuing import ATLEvent, SyncSQSQueueManager

import boto3
import botocore
import cv2
from datetime import datetime
import functools
from functools import lru_cache
import gzip
import io
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
import mimetypes
import numpy as np
import os
from twisted.internet import task, reactor, threads
from twisted.internet.threads import deferToThread, blockingCallFromThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

STEPSTATE_CACHE_SIZE = 1024

# --------------------------------------------------------------------------------------------------------------
### AWS service accessor class
class Aws(Core):
    s3FetchSuccess = 230
    s3PersistSuccess = 231
    s3FetchError = 430
    s3PersistError = 431

    class AWSException(Exception):
        """Raised when hard to identify AWS api errors occur"""

    def __init__(self):
        super().__init__('aws')
        self.sessions = AD()
        self.boto_config = botocore.client.Config(signature_version='s3v4', max_pool_connections=50)
        self.sessions[BCM.cgr] = boto3.Session()
        self.services = AD()
        self.services[BCM.cgr] = AD()

        self.services[BCM.cgr]["s3"] = self.sessions[BCM.cgr].client(
            service_name="s3",
            region_name=BCM.aws_region,
            aws_access_key_id=BCM.aws_creds.akid,
            aws_secret_access_key=BCM.aws_creds.sak,
            config=self.boto_config
        )

        self.S3 = S3(aws=self)
        self.SQS = SQS(aws=self)
        self.SNS = SNS(aws=self)

    def __call__(self, svc, cgr=None):
        if cgr is None:
            cgr = self.cgr

        if cgr in self.sessions:
            session = self.sessions[cgr]
        else:
            session = self.sessions[cgr] = boto3.Session()

        if cgr not in self.services:
            self.services[cgr] =  AD()

        if svc not in self.services[cgr]:
            self.services[cgr][svc] = session.client(
                service_name=svc,
                region_name=BCM.aws_region,
                aws_access_key_id=BCM.aws_creds.akid,
                aws_secret_access_key=BCM.aws_creds.sak,
                config=self.boto_config
            )
        return self.services[cgr][svc]

### Class to copy data from S3 to memory or local file
class S3(Core):
    def __init__(self, aws=None):
        super().__init__('aws_s3', aws=aws)
        self.state_cache = AD()
        self.cache_mgr_task = task.LoopingCall(self.cache_mgr)
        self.cache_mgr_task.start(900, now=False)

    def cache_mgr(self):
        for cgr, cgr_data in self.state_cache.items():
            now = time.time()
            for path, img_info in cgr_data.deep_items():
                if now - img_info[0] > 900:
                    del cgr_data[path]

    # choose a version of S3 path based on a timestamp (we need to update)
    def create_key(self, customer_id, category, rec_id, rec_time, filename):
        d = to_datetime(rec_time)
        date_hour = d.strftime("%Y/%m/%d/%H")
        if category in ["beacons", "image_readings", "external_data"]:
            rec_type = "rawdata"
        else:
            rec_type = "data"

        bucket = "atl-%s-%s-%s" % (BCM.env, customer_id, rec_type)
        key = "{}/{}/{}/{}".format(category, rec_id, date_hour, filename)
        path = f"s3://{bucket}/{key}"
        return bucket, key, path

    def generate_bucket_key(self, nodename: str, key: str) -> (str, str):
        node, cgr, env = nodename.split('_')
        bucket = f'atl-{env}-{cgr}-rawdata'
        key = f'image_readings/{nodename}/state/{key}'
        path = f's3://{bucket}/{key}'
        return bucket, key, path

    ### Fetch s3 object
    @inlineCallbacks
    def get(self, bucket, key, store_path=None):
        """
        :param bucket:
        :param key:
        :param store_path:
        :return: if store_path is not None then return path of file saved to disk else return the data
        """
        try:
            _, env, cgr, btype = bucket.split("-")
            if key[0] == '/':
                key = key[1:]
            d = yield deferToThread(
                        functools.partial(
                            self.AWS("s3", cgr=cgr).get_object,
                            Bucket=bucket,
                            Key=key
                        )
                    )
            data = d["Body"].read()
            # logger.info("AWS Fetch: {}:{} - size: {} - store_path: {}".format(bucket, key, len(data), store_path))
            if store_path:
                f = open(store_path, "wb")
                f.write(data)
                f.close()
                return store_path
            elif key.endswith(".gz"):
                return json.loads(gzip.decompress(data).decode("utf-8"))
            elif key.endswith(".json"):
                return json.loads(data)
            else:
                return data
        except Exception as e:
            error = AD({
                "bucket": bucket,
                "error": repr(e),
                "key": key,
                "message": "AWS Fetch {}:{} - {}".format(bucket, key, repr(e)),
                "storePatch": store_path,
            })
            logger.exception(error.jstr())
            raise

    @inlineCallbacks
    def get_bucket(self, bucket):
        _, env, cgr, btype = bucket.split("-")
        bucket_obj = yield deferToThread(functools.partial(self.AWS("s3", cgr=cgr).Bucket, bucket))
        return bucket_obj

    @inlineCallbacks
    def get_image(self, s3_url: str, cacheable=False, color_space: str='bgr', crop: Zone=None) -> bytes:
        now = time.time()
        bucket, key = self.key_parts(s3_url)
        _, env, cgr, btype = bucket.split("-")
        if cgr not in self.state_cache:
            self.state_cache[cgr] = AD()
        if cacheable:
            if s3_url in self.state_cache[cgr]:
                if now - self.state_cache[cgr][s3_url][0] <= 300:
                    self.state_cache[cgr][s3_url][0] = now
                    logger.info(f"aws.get_image serving image from cache: {s3_url}")
                return self.state_cache[cgr][s3_url][1]

        try:
            data = yield self.get(bucket, key)
        except Exception as err:
            logger.exception(f"aws.get_image exception: " + repr(err))
            raise

        if cacheable:
            logger.info(f"aws.get_image adding image from {s3_url} into cache")
            self.state_cache[cgr][s3_url] = [now, load_image(data, color_space=color_space, crop=crop)]
            return self.state_cache[cgr][s3_url][1]
        else:
            return load_image(data, color_space=color_space, crop=crop)

    @inlineCallbacks
    def get_image_state(self, nodename: str, key: str) -> bytes:
        res = yield self.get_image(self.generate_bucket_key(nodename, key)[-1], cacheable=True)
        return res

    @inlineCallbacks
    def get_path(self, uri, store_path=None):
        files = AD()
        try:
            bucket_name, path = self.key_parts(uri)
            bucket_obj = yield self.get_bucket(bucket_name)
            if not path[-1] == "/":
                path += "/"
            for f in bucket_obj.list(path, ""):
                fo = yield self.get(bucket_name, f)
                files[f] = io.BytesIO(fo)
            if store_path:
                if not os.path.exists(store_path):
                    os.makedirs(store_path)
                for f in files:
                    _, fpath = self.key_parts(f)
                    fpath = fpath.strip(path)
                    if "/" in fpath:
                        subdir, filename = fpath.rsplit("/", 1)
                        subdir = os.path.join(store_path, subdir)
                        if not os.path.exists(subdir):
                            os.makedirs(subdir)
                    else:
                        subdir = store_path
                        filename = fpath
                    full_path = os.path.join(subdir, filename)
                    with open(full_path, "w") as fo:
                        fo.write(files[f])
            return files
        except Exception:
            logger.exception("aws.get_path - store_path not provided")
            raise

    def has_state(self, nodename: str, key: str) -> bool:
        bucket, key, s3_url = self.generate_bucket_key(nodename, key)
        prefix, term = key.rsplit('/', 1)
        _, env, cgr, btype = bucket.split("-")
        if cgr not in self.state_cache:
            self.state_cache[cgr] = AD()
        if s3_url in self.state_cache[cgr]:
            return True
        else:
            return self.list_objects(bucket, prefix=prefix, term=term)

    def key_parts(self, uri):
        if "https://" in uri:
            uri_parts = uri.split("://")[-1]
            s3_endpoint, bucket, key = uri_parts.split("/", 2)
            key = "/{}".format(key)
        elif "s3://" in uri:
            uri_parts = uri.split("://")[-1]
            bucket, key = uri_parts.split("/", 1)
            key = "/{}".format(key)
        else:
            if uri[0] == "/":
                bucket, key = uri[1:].split("/", 1)
            else:
                bucket, key = uri.split("/", 1)
            key = "/{}".format(key)
        return bucket, key

    @exec_timed
    @inlineCallbacks
    def list_objects(self, bucket, prefix=None, term=None):
        _, env, cgr, btype = bucket.split("-")
        try:
            next_token = None
            data_list = []
            while True:
                ### list_objects_v2 can call only returns the first 1000 keys.
                if not next_token:
                    ### load s3 file list
                    res = yield deferToThread(
                                functools.partial(
                                    self.AWS("s3", cgr=cgr).list_objects_v2,
                                    Bucket=bucket,
                                    Prefix=prefix
                                )
                            )
                else:
                    res = yield deferToThread(
                                functools.partial(
                                        self.AWS("s3", cgr=cgr).list_objects_v2,
                                        Bucket=bucket,
                                        Prefix=prefix,
                                        ContinuationToken=next_token
                                    )
                            )

                if len(res["Contents"]):
                    if term:
                        for item in res["Contents"]:
                            if term in item['Key']:
                                return item['Key']
                    data_list.extend([k['Key'] for k in res["Contents"]])

                if "NextContinuationToken" in res:
                    next_token = res["NextContinuationToken"]
                else:
                    break
            if not term:
                return data_list
            else:
                return False
        except Exception as err:
            logger.exception("aws.list_objects exception: " + repr(err))
            raise

    @exec_timed
    @inlineCallbacks
    def put(self, bucket, key, body, metadata=AD()):
        to_store = []
        if BCM.env not in bucket:
            bparts = bucket.split("-")
            bparts[1] = BCM.env
            bucket = "-".join(bparts)
        _, env, cgr, btype = bucket.split("-")
        path = f"s3://{bucket}/{key}"
        if body is None:
            raise APIError(f'AWS PUT ERROR: {path} [FAILURE] body was "None"')
        try:
            content_type = mimetypes.guess_type(key)[0]
            if content_type is None:
                # logger.info("Asset didn't have a recognizable mimetype \nDetails: bucket: {}, key: {}, body_type: {}".format(
                #     bucket, key, type(body)))
                content_type = 'application/octet-stream'
            to_store.append([bucket, key, body])
            # if key.split('.')[-1] == 'jpg' and 'violation' not in key:
            #     to_store.extend(self.thumbnail(bucket, key, body))
            res = None
            for ft in to_store:
                if all([len(e) for e in ft]):
                    # fmeta = AD(metadata) if isinstance(metadata, (AD, dict)) else AD()
                    # if 'media_info' not in fmeta:
                    #     fmeta.media_info = AD()
                    # get_media_metadata(ft[1], ft[2], fmeta.media_info)
                    if isinstance(ft[2], np.ndarray):
                        ft[2] = bytearray(ft[2].tobytes())
                    try:
                        d = yield deferToThread(
                                    functools.partial(
                                        self.AWS("s3", cgr=cgr).put_object,
                                        Bucket=ft[0],
                                        Key=ft[1],
                                        Body=ft[2],
                                        ContentType=content_type
                                        # Metadata={k: str(v) for k, v in fmeta.deep_items()}
                                    )
                                )
                        # logger.info(f"AWS PUT {ft[0]}/{ft[1]} [FINISH]")
                        if res is None:
                            res = d
                    except Exception as err:
                        logger.exception(f"AWS PUT EXCEPTION: {ft[0]}/{ft[1]} [FAILURE] - {err}")
            if res is not None and res["ResponseMetadata"]["HTTPStatusCode"] < 300:
                return path
        except Exception as e:
            logger.exception("AWS PUT EXCEPTION: {path} [FAILURE] - {e}")
            raise

    @inlineCallbacks
    def put_customer_data(self, bucket, key, rec_data, rec_type="identity", compress=False, safe=False):
        try:
            # serialize object as json format string and compress
            if BCM.env not in bucket:
                bparts = bucket.split("-")
                bparts[1] = BCM.env
                bucket = "-".join(bparts)

            if rec_type == "json":
                rec_data = json.dumps(rec_data, default=str)
                compress=False

            if compress:
                if not key.endswith(".gz"):
                    key += ".gz"
                if hasattr(rec_data, "encode"):
                    data = gzip.compress(rec_data.encode())
                else:
                    data = gzip.compress(rec_data)
            else:
                data = rec_data

            key_path = "s3://%s/%s" % (bucket, key)
            if safe:
                res = yield self.safe_put(bucket, key, data)
                # logger.info(f"AWS.s3.put_customer_data safe result: " + str(res))
            else:
                res = yield self.put(bucket, key, data)
                # logger.info(f"AWS.s3.put_customer_data result: " + str(res))

            # logger.info(f"AWS Persist ({res}) [FINISH]")
            return res
        except Exception as e:
            logger.exception("aws.put_customer_data Exception {}: bucket: {}, key: {}, data_type: {}, rec_type: {}, compressed: {}, safe: {}".format(
                e, bucket, key, type(rec_data), rec_type, compress, safe))
            raise

    @inlineCallbacks
    def put_local_path(self, local_path, uri, prefix=None, rm=False):
        bucket_name, path = self.key_parts(uri)
        if BCM.env not in bucket_name:
            bparts = bucket_name.split("-")
            bparts[1] = BCM.env
            bucket_name = "-".join(bparts)
        data = "fs"
        if isinstance(local_path, str):
            file_list = shcmd("find {} -type f".format(local_path))
        elif isinstance(local_path, list):
            file_list = local_path
        elif hasattr(local_path, "keys"):
            data = "args"
            file_list = local_path.keys()
        links = []
        for f in file_list:
            if prefix:
                fkey = f.strip(prefix)
            else:
                fkey = f.split("/")[-1]
            if data == "fs":
                with open(f, "r") as fo:
                    res = yield self.put(bucket_name, os.path.join(path, fkey), fo.read())
                    # logger.info(f"AWS.s3.put_path result: " + str(res))
                if rm:
                    os.remove(f)
            else:
                res = yield self.put(bucket_name, os.path.join(path, fkey), local_path[f].read())
                # logger.info(f"AWS.s3.put_path result: " + str(res))
            links.apend(res)
        return links

    @inlineCallbacks
    def safe_put(self, bucket, key, body):
        _, env, cgr, btype = bucket.split("-")
        cnt = 0
        list_objs = yield deferToThread(
                        functools.partial(self.AWS("s3", cgr=cgr).list_objects_v2, Bucket=bucket, Prefix=key)
                    )
        if 'KeyCount' in list_objs and int(list_objs["KeyCount"]) > 0:
            cnt = int(list_objs["KeyCount"]) + 1
        if cnt > 0:
            old_key = key
            kparts = key.rsplit('.', 1)
            key = f'{kparts[0]}_v{cnt}.{kparts[-1]}'
            # logger.info(f'AWS.safe_put detected a filename collision: {old_key} - using versioned filename: {key}')
        res = yield self.put(bucket, key, body)
        # logger.info(f"AWS.s3.safe_put result: " + str(res))
        return res

    @inlineCallbacks
    def put_path(self, path, body):
        bucket_name, key = self.key_parts(path)
        res = yield self.put(bucket, key, body)
        logger.info(f"AWS.s3.put_path result: " + str(res))
        return res

    @inlineCallbacks
    def save_data(self, nodename, gwid, step_name, step_function, collection_time, data, raw_data=AD(), metadata=AD()):
        try:
            cgr, env = nodename.split('_')[1:]
            bucket = f'atl-{env}-{cgr}-data'
            date_hour = to_datetime(collection_time).strftime("%Y/%m/%d/%H")
            event = AD({
                        'data': AD(data),
                        'collection_time': collection_time,
                        'data_links': [],
                        'media_info': AD(),
                        'metadata': AD(metadata),
                        'nodename': nodename,
                        'step_function': step_function,
                        'step_name': step_name
            })
            data_paths = AD()
            data_paths.event_path = (f"captures/{nodename}/{date_hour}/{gwid}_{collection_time}_capture_data.json", event)
            for fname, fdata in AD(raw_data).deep_items():
                cd_type, fext = fname.rsplit('.', 1)
                path = f"readings/{nodename}/{device}/{step_name}/{date_hour}/{gwid}_{collection_time}_{step_name}_{fname}"
                key_path = f"s3://{bucket}/{path}"
                capture_event.data_links.append(key_path)
                capture_event.media_info[key_path] = get_media_metadata(fname, fdata)
                data_paths[cd_type] = (path, pack_image(fdata))

            links = []
            for dtype, dtuple in data_paths:
                res = yield self.put(bucket, dtuple[0], dtuple[1], metadata=event.metadata)
                logger.info(f"AWS.s3.save_captured_data {dtype}-data result: " + str(res))
                links.append(res)
            return links
        except Exception as e:
            logger.exception("aws.save_captured_data exception for {nodename}/{gwid}/{step_name}/{step_function}/{collection_time}: " + repr(e))
            raise

    @inlineCallbacks
    def save_observation_data(self, origin, obx, subject, attribute, vtype, value, image_data=AD(), metadata=AD()):
        try:
            nodename, collection_time, process_path = origin.split(':')
            cgr, env = nodename.split('_')[1:]
            bucket = f'atl-{env}-{cgr}-data'
            date_hour = to_datetime(collection_time).strftime("%Y/%m/%d/%H")
            observation = AD({
                    'collection_time': collection_time,
                    'data_links': [],
                    'media_info': AD(),
                    'metadata': AD(metadata),
                    'nodename': nodename,
                    'obx': obx,
                    'process_path': process_path,
                    'subject': subject,
                    'attribute': attribute,
                    'vtype': vtype,
                    'value': value
            })
            data_paths = AD()
            base_path = f"observations/{subject}/{attribute}/{date_hour}"
            data_paths.observation = (f"{obx}_{subject}_{collection_time}_{attribute}_data.json", observation)
            for fname, fdata in AD(image_data).deep_items():
                rd_type = fname.rsplit('.', 1)[0]
                path = f"{base_path}/{rd_type}/{date_hour}/{obx}_{subject}_{collection_time}_{attribute}_{fname}"
                key_path = f"s3://{bucket}/{path}"
                observation.data_links.append(key_path)
                observation.media_info[key_path] = get_media_metadata(fname, fdata)
                data_paths[rd_type] = (path, pack_image(fdata))

            links = []
            for dtype, dtuple in data_paths:
                res = yield self.put(bucket, dtuple[0], dtuple[1], metadata=observation.metadata)
                logger.info(f"AWS.s3.save_result_data {dtype}-data result: " + str(res))
                links.append(res)
            return links
        except Exception as e:
            logger.exception("aws.save_data exception for {subject}/{attribute}/{collection_time}: " + repr(e))
            raise

    @inlineCallbacks
    def save_result_data(self, nodename, gwid, step_name, step_function, collection_time, result_data, image_data=AD(), metadata=AD(), device='video0'):
        try:
            cgr, env = nodename.split('_')[1:]
            bucket = f'atl-{env}-{cgr}-data'
            date_hour = to_datetime(collection_time).strftime("%Y/%m/%d/%H")
            result = AD({
                    'collection_time': collection_time,
                    'data_links': [],
                    'media_info': {},
                    'metadata': AD(metadata),
                    'nodename': nodename,
                    'result_data': AD(result_data),
                    'step_function': step_function,
                    'step_name': step_name
            })
            data_paths = AD()
            base_path = f"{step_function}/{nodename}/{device}/{step_name}"
            data_paths.result = (f"{base_path}/state/{date_hour}/{gwid}_{device}_{collection_time}_{step_name}_result_data.json", result)
            for fname, fdata in AD(image_data).deep_items():
                rd_type = fname.rsplit('.', 1)[0]
                path = f"{base_path}/{rd_type}/{date_hour}/{gwid}_{device}_{collection_time}_{step_name}_{fname}"
                key_path = f"s3://{bucket}/{path}"
                result.data_links.append(key_path)
                state.media_info[key_path] = get_media_metadata(fname, fdata)
                data_paths[rd_type] = (path, pack_image(fdata))

            links = []
            for dtype, dtuple in data_paths:
                res = yield self.put(bucket, dtuple[0], dtuple[1], metadata=result.metadata)
                logger.info(f"AWS.s3.save_result_data {dtype}-data result: " + str(res))
                links.append(res)
            return links
        except Exception as e:
            logger.exception("aws.save_data exception for {nodename}/{step_name}/{step_function}/{collection_time}: " + repr(e))
            raise

    @inlineCallbacks
    def set_image_state(self, nodename: str, key: str, data: bytes):
        bucket, key, s3_url = self.generate_bucket_key(nodename, key)
        _, env, cgr, btype = bucket.split("-")
        if cgr not in self.state_cache:
            self.state_cache[cgr] = AD()
        self.state_cache[cgr][s3_url] = [time.time(), data]
        res = yield self.put(bucket, key, data)
        return res

    def thumbnail(self, bucket, key, body):
        thumbs = []
        try:
            if isinstance(body, np.ndarray) and body.ndim >= 3:
                image = body
            else:
                image = load_image(body, color_space='bgr')
            fname, ext = key.rsplit('.', 1)
            y, x = image.shape[:2]
            t200_dim = (200, int(y * 200/x))
            thumbs.append([bucket,
                           f"{fname}_thumbnail-200.{ext}",
                           pack_image(cv2.resize(image.copy(), t200_dim, interpolation=cv2.INTER_AREA))
                          ])
            t400_dim = (400, int(y * 400/x))
            thumbs.append([bucket,
                           f"{fname}_thumbnail-400.{ext}",
                           pack_image(cv2.resize(image.copy(), t400_dim, interpolation=cv2.INTER_AREA))
                          ])
            return thumbs
        except Exception as err:
            logger.exception("aws.thumbnail exception: " + repr(err))
            raise

class SNS(Core):
    def __init__(self, aws=None):
        super().__init__('aws_sns', aws=aws)
        self.boto_config = botocore.client.Config(signature_version='s3v4', max_pool_connections=100)
        self.session = boto3.Session()
        self.client = self.session.client(
            service_name="sns",
            region_name=BCM.aws_region,
            aws_access_key_id=BCM.aws_creds.akid,
            aws_secret_access_key=BCM.aws_creds.sak,
            config=self.boto_config
        )
    @inlineCallbacks
    def publish_message(self, topic: str, message: str) -> None:
        res = yield deferToThread(
                    functools.partial(
                        self.client.publish,
                        TopicArn=topic,
                        Message=message
                    )
                )
        logger.info(f"AWS.SNS.publish_message result: " + str(res))
        return res

class SQS(Core):
    def __init__(self, aws=None):
        super().__init__('aws_sqs', aws=aws)
        self.boto_config = botocore.client.Config(signature_version='s3v4', max_pool_connections=100)
        self.session = boto3.Session()
        self.client = self.session.client(
            service_name="sqs",
            region_name=BCM.aws_region,
            aws_access_key_id=BCM.aws_creds.akid,
            aws_secret_access_key=BCM.aws_creds.sak,
            config=self.boto_config
        )
        self.queues = AD()
        self.dlq_watchers =  AD()
        self.qinfo = AD()
        self.dlq_name = f"atl-{self.env}-{self.cgr}-dlq"

        qurls = self.client.list_queues(QueueNamePrefix=self.dlq_name)["QueueUrls"]
        qurls.extend(self.client.list_queues()["QueueUrls"])
        for qurl in [q for q in qurls if self.env in q]:
            qname = qurl.split("/")[-1]
            if "." in qname:
                qname = qname.split(".")[0]
            qarn =  self.client.get_queue_attributes(QueueUrl=qurl, AttributeNames=["QueueArn"])["Attributes"]["QueueArn"]
            self.qinfo[qname] = AD({
                                    'qname': qname,
                                    'qurl': qurl,
                                    'qarn': qarn
                                   })

        self.queues[self.dlq_name] = SyncSQSQueueManager(sqs=self, qname=self.dlq_name, qinfo=self.qinfo)
        self.dl_queue = self.queues[self.dlq_name]

        for q in self.tree.mvapi_models:
            if q not in self.queues:
                self(q)
            lateq = f'{q}_late'
            if lateq not in self.queues:
                self(lateq)
        for q in ["mv_results", "ca_results"]:
            if q not in self.queues:
                self(q)

        self.dlq_watcher = task.LoopingCall(self.check_for_dlq_msgs)
        self.dlq_watcher.start(300, now=False)

    def __call__(self, qname, fifo=True, dlq=True):
        if qname not in self.queues:
            if 'dlq' in qname:
                dlq = False
            self.queues[qname] = SyncSQSQueueManager(
                                    sqs=self,
                                    qname=qname,
                                    qinfo=self.qinfo,
                                    fifo=fifo,
                                    dlq=dlq
                                )
        return self.queues[qname]

    def get_qinfo(self):
        return self.qinfo

    @inlineCallbacks
    def check_for_dlq_msgs(self):
        # logger.info(f"AWS.SQS.check_for_dlq_msgs for queue {self.dl_queue.qname}")
        empty = yield self.dl_queue.empty()
        while not empty:
            msgs = yield self.dl_queue.get_msg(mcount=10)
            if msgs:
                for m in msgs:
                    # logger.info(f"AWS.SQS.check_for_dlq_msgs - message {m}")
                    try:
                        if m.scid:
                            if ((time.time() - float(m.mattrs.last_dlq_time.StringValue)) <= 3600):
                                logger.info(f"AWS.SQS.check_for_dlq_msgs - message not old enough to re-queue {m.scid}")
                            else:
                                m.increment_dlq_counters()
                                if m.dlq_count <= 15:
                                    logger.info(f"AWS.SQS.check_for_dlq_msgs - re-queuing {m.scid}")
                                    if m.data.records[0].function in self.queues:
                                        res = yield self.queues[m.data.records[0].function].put_msg(m)
                                    res = yield self.dl_queue.delete(m)
                                else:
                                    logger.info(f"AWS.SQS.check_for_dlq_msgs - message has exceeded rmaximum dlq_count - persisting and deleting: {m.scid}")
                                    res = yield self.persist_message(m)
                                    res = yield self.dl_queue.delete(m)
                        else:
                            logger.info(f"AWS.SQS.check_for_dlq_msgs - unknown message format {m}")
                    except Exception as err:
                        logger.exception(f"AWS.SQS.check_for_dlq_msgs exception {err} handling: {m}")
                        raise
            empty = yield self.dl_queue.empty()

    @inlineCallbacks
    def persist_message(self, msg):
        bucket, key, path = self.AWS.S3.create_key(
            msg.cgr, 'dlq', f"{msg.headers.nodename}/{msg.data.records[0].step_name}", msg.data.records[0].collection_time, f"{msg.mid}.json"
        )
        res = yield self.AWS.S3.put(bucket, key, msg.pack())
        logger.info(f"AWS.SQS.persist_message result: " + repr(res))
        return res

    @inlineCallbacks
    def delete(self, **kwargs):
        result = yield deferToThread(
                    functools.partial(
                        self.client.delete_message_batch,
                        **kwargs
                    )
                )
        return result

    @inlineCallbacks
    def get(self, **kwargs):
        result = yield deferToThread(
                    functools.partial(
                        self.client.receive_message,
                        **kwargs
                    )
                )
        return result

    @inlineCallbacks
    def put(self, **kwargs):
        result = yield deferToThread(
                    functools.partial(
                        self.client.send_message,
                        **kwargs
                    )
                )
        return result

    @inlineCallbacks
    def send(self, **kwargs):
        result = yield deferToThread(
                    functools.partial(
                        self.client.send_message,
                        **kwargs
                    )
                )
        return result
