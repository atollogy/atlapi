#!/usr/bin/env python3

from atlapi.attribute_dict import *
# from atlapi.common import *
import asyncio
from asyncio import ensure_future
import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
import functools
import pyconsul as consul
import json
import logging
import os
import subprocess as sp
from pprint import pprint as pp
import pyconsul as consul
from random import randint
import requests
from threading import Thread
import time
from twisted.internet.defer import inlineCallbacks, ensureDeferred
from twisted.internet.threads import deferToThread
from twisted.internet import task, reactor
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logger = logging.getLogger('atlapi')

def _to_x(d, tgt=None):
    if tgt is None:
        return d
    elif isinstance(d, tgt):
        return d
    elif hasattr(d, "items"):
        td = tgt()
        for k, v in d.items():
            k = str(k)
            if isinstance(k, bytes):
                k = k.decode()
            else:
                k = str(k)
            td[k] = _to_x(v, tgt=tgt)
        return td
    elif isinstance(d, list) and d.__class__ == 'list':
        return [_to_x(v, tgt=tgt) for v in d]
    return d


class CAD_Cache_Exception(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv

class CAD_Cache(CAD):
    meta = AD({
            'key_prefixes': {},
            'default': {
               'subscriptions': {}
            }
        })
    consul_localhost = 'at0l' in sp.check_output(['hostname']).decode()

    def __init__(self, prefix, *args, callback=None, env='prd', interval=60, read_write=False, **kwargs):
        """
        loop: :parent asycio.loop
        prefix: :string consul kv path scope
        """
        if prefix in CAD_Cache.meta.key_prefixes:
            self = CAD_Cache.meta.key_prefixes[prefix]
            myself = self.__myself__(self)
        else:
            CAD.__init__(self, *args, **kwargs)
            myself = self.__myself__(self, params=kwargs)
            CAD_Cache.meta.key_prefixes[prefix] = self
            myself.prefix = prefix
            myself.interval = interval
            myself.last_check = 0
            myself.last_force=0
            myself.read_write = read_write
            if CAD_Cache.consul_localhost:
                myself.ckv = consul.Consul(host=f"localhost",
                                           port=8543,
                                           scheme="https",
                                           verify=False).kv
            else:
                myself.ckv = consul.Consul(host='consul.prd.at0l.io',
                                           port=443,
                                           scheme="https",
                                           verify=False).kv
            myself.updater = task.LoopingCall(self._check_update)
            myself.updater.start(300, now=False)
            data = myself.ckv.get(prefix)
            if data:
                self.update(data)
                myself.last_check = datetime.utcnow().timestamp()

    def __getattr__(self, key):
        try:
            return self.__getitem__(key)
        except:
            raise AttributeError(key)

    def __getitem__(self, key):
        try:
            if isinstance(key, bytes):
                key = key.decode()
            else:
                key = str(key)
            if '.' in key:
                path, key = key.split('.', 1)
                return dict.__getitem__(self, path)[key]
            else:
                return dict.__getitem__(self, key)
        except:
            raise KeyError(key)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        myself = self.__myself__(self)
        if isinstance(key, bytes):
            key = key.decode()
        else:
            key = str(key)
        value = _to_x(value, tgt=CAD)
        if "." in key:
            path, key = key.split(".", 1)
            if isinstance(self.setdefault(path, CAD()), (AD, CAD, dict)):
                if isinstance(dict.__getitem__(self, path), dict):
                    dict.__setitem__(self, path, _to_x(dict.__getitem__(self, path), tgt=CAD))
                dict.__getitem__(self, path).__setitem__(key, value)
            else:
                dict.__setitem__(self, path, AD())
                dict.__getitem__(self, path).__setitem__(key, value)
        else:
            dict.__setitem__(self, key, value)

        if key in myself['subscriptions'] and len(myself['subscriptions'][key]):
            self.__notify__(key, value)

    def __myself__(self, me, params=None):
        my_id = id(me)
        if my_id not in CAD_Cache.meta:
            CAD_Cache.meta[my_id] = AD({'subscriptions': {}})
            if params and isinstance(params (dict, AD, CAD)):
                CAD_Cache.meta[my_id].update(params)
        return CAD_Cache.meta[my_id]

    @staticmethod
    def __kparts(key):
        if isinstance(key, bytes):
            key = key.decode()
        dkey = key.replace("/", ".")
        if dkey[0] == ".":
            dkey = dkey[1:]
        if dkey[-1] == ".":
            dkey = dkey[:-1]
        skey = key.replace(".", "/")
        if skey[0] == "/":
            skey = skey[1:]
        if skey[-1] == "/":
            skey = skey[:-1]
        return dkey, skey

    @inlineCallbacks
    def _check_update(self, force=False):
        myself = self.__myself__(self)
        now = datetime.utcnow().timestamp()
        if (now - myself.last_check > 300) or (force and (now - myself.last_force > 15)):
            myself.last_check = now
            if force:
                myself.last_force = now
            data = yield deferToThread(myself.ckv.get, myself.prefix)
            if data and len(data):
                self.update(data)

Consul_Cache = CAD_Cache
