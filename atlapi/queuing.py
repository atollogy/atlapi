#!/usr/bin/env python3.6


from atlapi.core import Core
from atlapi.attribute_dict import *
from atlapi.common import *

import asyncio
import boto3
import boto3.resources.base as base
import botocore
import concurrent
from concurrent.futures import ProcessPoolExecutor
from datetime import datetime
import functools
import gzip
import iso8601
import json

import logging

logger = logging.getLogger('atlapi')

import multiprocessing as mp
import os
import threading
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread, deferToThreadPool
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue, maybeDeferred


class ATLEvent(object):
    def __init__(self, data={}, handle=None, mattrs={}, mid=None, cid=None, scid=None, origin=None, rawdata=None, unpack=None):
        """ATLEvent
            data: <dict> if not None it is expected to have the following structure:
                {
                    cid: <context_id>,
                    customer_id: <string>,
                    headers: <dict> with customer_id, nodename, gateway_id, etc...
                    args: <dict>,
                    mattrs: <dict of message attributes>,
                    records: [<step_data_structure>, ...]
                },
            handle: <string> -> ReceiptHandle,
            mid: <string> - MessageId,
            rawdata: <raw dequeue object> or None,
            unpack: <gzip'ed json data string with above structure> or None
        """
        self.data = AD(data)
        if unpack is not None:
            self.data.update(json.loads(unpack))
        self.origin = origin
        if 'mattrs' in self.data:
            self.mattrs = self.data.mattrs
        else:
            self.mattrs = self.data.mattrs = AD({'origin': {'DataType': 'String', 'StringValue': origin if origin else 'unknown'}})
            self.set_mattrs(mattrs)
        self.mid = str(mid or id(self))
        self.handle = handle
        self.rawdata = rawdata

        if 'records' in self.data and isinstance(self.data.records, list):
            self.data.records = [AD(r) for r in self.data.records]
        self.cid = str(self.data.records[0].cid if 'cid' in self.data.records[0] else str(cid))
        self.scid = str(self.data.records[0].scid if 'scid' in self.data.records[0] else str(scid))
        for k, v in self.mattrs.items():
            if hasattr(v, 'StringValue'):
                v.StringValue = str(v.StringValue)

    def __repr__(self):
        return self.data.jstr()

    def __str__(self):
        return self.data.jstr()

    def pack(self):
        return self.data.jstr()

    def increment_dlq_counters(self):
        self.dlq_count = int(self.mattrs.dlq_count.StringValue) + 1
        self.last_dlq_time = datetime.utcnow().timestamp()
        self.mattrs.dlq_count.StringValue = str(self.dlq_count)
        self.mattrs.last_dlq_time.StringValue = str(self.last_dlq_time)

    def set_mattrs(self, mattrs):
        try:
            if 'history' in mattrs:
                self.mattrs['history'] = {
                        'DataType': 'String',
                        'StringValue': f"{self.origin}, {mattrs['history']}"
                    }
            else:
                self.mattrs['history'] = {'DataType': 'String', 'StringValue': f"{self.origin}"}

            if 'dlq_count' in mattrs:
                self.mattrs['dlq_count'] = mattrs['dlq_count']
            else:
                self.mattrs['dlq_count'] = {'DataType': 'Number', 'StringValue': '0'}
            self.dlq_count = int(self.mattrs.dlq_count.StringValue)

            if 'last_dlq_time' in mattrs:
                self.mattrs['last_dlq_time'] = mattrs['last_dlq_time']
            else:
                self.mattrs['last_dlq_time'] = {'DataType': 'Number', 'StringValue': '0'}
            self.last_dlq_time = float(self.mattrs.last_dlq_time.StringValue)

            for k, v in [i for i in mattrs.items() if i[0] not in ['origin', 'history', 'dlq_count', 'last_dlq_time']]:
                if isinstance(v, (dict, AD)) and 'DataType' in v:
                    self.mattrs[k]=v
                else:
                    self.mattrs[k]= {
                        'DataType': 'String',
                        'StringValue': str(v)
                    }
        except Exception as err:
            logger.exception(f"ATLEvent.set_mattrs exception - {err}")
            self.mattrs = ad({
                'history': {
                        'DataType': 'String',
                        'StringValue': f"{self.origin}, {mattrs['history']}"
                    },
                'dlq_count': {'DataType': 'Number', 'StringValue': '0'},
                'last_dlq_time': {'DataType': 'Number', 'StringValue': '0'}
            })

class SyncSQSQueueManager(object):
    def __init__(self, sqs=None, qname=None, qinfo=None, fifo=True, dlq=True, vtime="30", wait=20):
        Core.__init__(self, f"sqs_{qname}", sqs=sqs)
        self.qname = qname
        if self.cgr not in self.qname:
            self.qname = "{}-{}".format(self.qname, self.cgr)
        if self.env not in self.qname:
            self.qname = "{}-{}".format(self.qname, self.env)
        self.qinfo = qinfo
        self.req_fields = ["customer_id", "gateway_id", "args", "records"]
        self._current_messages = []
        self._current_messages_byid = {}
        self._maybe_done = []
        self._vtime = vtime
        self.has_dlq = dlq
        self.is_dlq = 'dlq' in self.qname
        self.dlq_name = f"atl-{self.env}-{self.cgr}-dlq"
        self.is_fifo = fifo
        self.wait = wait
        self.loop = asyncio.get_event_loop()
        self.last_get = self.loop.time() - self.wait
        if self.qname not in self.qinfo:
            self.qurl, self.qarn = self.__prep_q(self.qname, fifo=self.is_fifo)
        else:
            self.qurl = self.qinfo[self.qname].qurl
            self.qarn = self.qinfo[self.qname].qarn

    def __prep_q(self, qname, fifo=True):
        logger.info(f"Creating queue {qname}")
        if fifo:
            if '.fifo' not in qname:
                q = f"{qname}.fifo"
            else:
                q = qname
            qurl = self.SQS.client.create_queue(QueueName=q,
                                                Attributes={
                                                    "FifoQueue": "true",
                                                    "VisibilityTimeout": self._vtime,
                                                    "ContentBasedDeduplication": "true"}
                                                )["QueueUrl"]
            qarn =  self.SQS.client.get_queue_attributes(QueueUrl=qurl, AttributeNames=["QueueArn"])["Attributes"]["QueueArn"]
        else:
            qurl = self.SQS.client.create_queue(QueueName=qname)["QueueUrl"]
            qarn = self.SQS.client.get_queue_attributes(QueueUrl=qurl, AttributeNames=["QueueArn"])["Attributes"]["QueueArn"]

        self.qinfo[qname] = AD({
                                'qname': qname + ".fifo" if fifo else qname,
                                'qurl': qurl,
                                'qarn': qarn
                               })

        if not self.is_dlq and f"{self.dlq_name}.qarn" in self.qinfo:
            self.dlq_arn = self.qinfo[self.dlq_name].qarn
            res = self.SQS.client.set_queue_attributes(QueueUrl=qurl,
                                                        Attributes={"RedrivePolicy": json.dumps(
                                                                 {"deadLetterTargetArn": self.dlq_arn, "maxReceiveCount": "3"}
                                                             )
                                                         }
                                                 )
        return (self.qinfo[qname].qurl, self.qinfo[qname].qarn)

    @inlineCallbacks
    def __delete_messages(self, *args, **kwargs):
        # args keyword must include Entries=[{'Id': m.mid, 'ReceiptHandle': m.handle}]
        res = yield self.SQS.delete(
                            QueueUrl=self.qurl,
                            **kwargs
                        )
        return res

    @inlineCallbacks
    def __get(self, mcount=1):
        data = yield self.SQS.get(
                        QueueUrl=self.qurl,
                        AttributeNames=["All"],
                        MessageAttributeNames=["All"],
                        MaxNumberOfMessages=mcount,
                        WaitTimeSeconds=self.wait,
                    )

        # messages are stored as [mid, receipt_handle, ATL event, raw message]
        if "Messages" in data:
            _msgs = []
            for m in [AD(m) for m in data["Messages"]]:
                m.Body = AD(json.loads(m.Body))
                # if not all([f in m['Body'] for f in self.req_fields]):
                #     logger.debug('record does not have the required fields: {}'.format(m['Body']))
                #     continue
                m.Body.records = [AD(r) for r in m["Body"]["records"] if isinstance(r, dict)]
                if len(m.Body.records):
                    _msgs.append(m)

            if len(_msgs):
                _msgs = [
                    ATLEvent(
                        data=m["Body"],
                        handle=m["ReceiptHandle"],
                        mattrs=m['MessageAttributes'] if 'MessageAttributes' in m else {},
                        cid=str(m["Body"]['cid'] if 'cid' in m["Body"] else id(m)),
                        scid=(m["Body"]['records'][0]['scid'] if 'scid' in m["Body"]['records'][0] else id(m)),
                        mid=str(m["MessageId"]),
                        origin=self.qname,
                        rawdata=m
                    )
                    for m in _msgs
                ][::-1]
                self._current_messages = _msgs + self._current_messages
                self._current_messages_byid = {m.mid: m for m in self._current_messages}

        self.last_get = time.time()
        return len(self._current_messages)

    @inlineCallbacks
    def __put(self, msg, *args, **kwargs):
        res = yield self.SQS.send(
                                    QueueUrl=self.qurl,
                                    MessageBody=msg.pack(),
                                    MessageGroupId=str(msg.scid),
                                    MessageAttributes=msg.mattrs
                              )
        # logger.info(f"SyncSQSQueueManager.__put result: " + repr(res))
        return res

    @inlineCallbacks
    def all_done(self):
        res = yield self.__delete_messages(
            Entries=[
                {"Id": m.mid, "ReceiptHandle": m.handle}
                for m in self._current_messages
                if m.handle
            ]
        )
        self._current_messages = []
        self._maybe_done = []
        self._current_messages_byid = {}
        return res

    @inlineCallbacks
    def delete(self, msgs):
        if not isinstance(msgs, list):
            msgs = [msgs]
        res = yield self.__delete_messages(
            Entries=[{"Id": m.mid, "ReceiptHandle": m.handle} for m in msgs if m.handle]
        )
        self._current_messages = [m for m in self._current_messages if m not in msgs]
        self._current_messages_byid = {m.mid: m for m in self._current_messages}
        self._maybe_done = [m for m in self._current_messages if m not in msgs]
        return res

    @inlineCallbacks
    def empty(self, mcount=1):
        if len(self._current_messages):
            return False
        else:
            mcnt = yield self.__get(mcount=mcount)
            return mcnt

    @inlineCallbacks
    def get_msg(self, msgid=None, mcount=1):
        if msgid:
            if msgid in self._current_messages_byid:
                return self._current_messages_byid[msgid]
            else:
                return None
        mcnt = len(self._current_messages)
        if mcnt == 0 or mcount > mcnt:
            mcnt = yield self.__get(mcount=mcount)
        if mcnt == 0:
            return None

        if mcount == 1:
            msg = self._current_messages.pop()
            self._maybe_done.append(msg)
            return msg
        elif mcount > 1:
            if mcount <= mcnt:
                msgs = self._current_messages[-mcount:]
                self._current_messages = self._current_messages[:-mcount]
                self._maybe_done.extend(msgs)
                return msgs
            else:
                msgs = self._current_messages[:]
                self._maybe_done.extend(msgs)
                self._current_messages = []
                return msgs
        else:
            return None

    @inlineCallbacks
    def pop(self, mcount=1):
        mcnt = len(self._current_messages)
        if mcnt == 0 or mcount > mcnt:
            mcnt = yield self.__get(mcount=mcount)
        if mcnt == 0:
            return None
        if mcount == 1:
            m = self._current_messages.pop()
            res = yield self.delete([m])
            return m.data
        else:
            if mcount < mcnt:
                msgs = self._current_messages[-mcount:]
                self._current_messages = self._current_messages[:-mcount]
                res = yield self.delete(msgs)
                return [m.data for m in msgs]
            else:
                msgs = [m.data for m in self._current_messages]
                res = yield self.all_done()
                return msgs

    @inlineCallbacks
    def put_msg(self, msg):
        if isinstance(msg, ATLEvent):
            resp = yield self.__put(msg)
        else:
            resp = yield self.__put(
                                    ATLEvent(
                                        data=msg,
                                        cid=str(msg.cid  if 'cid' in msg else id(msg)),
                                        scid=str(msg.records[0].scid  if 'scid' in msg.records[0] else id(msg)),
                                        mid=str(msg.cid  if 'cid' in msg else id(msg)),
                                        origin=self.qname
                                    )
                                )
        return resp

    def release(self, msgs):
        if not isinstance(msgs, list):
            msgs = [msgs]
        self._current_messages = [m for m in self._current_messages if m not in msgs]
        self._current_messages_byid = {m.mid: m for m in self._current_messages}
        self._maybe_done = [m for m in self._current_messages if m not in msgs]
