#!/usr/bin/env python3

from atlapi.core import Core
from atlapi.attribute_dict import AD
from atlapi.common import *
from atlapi.image.utilities import *
from atlapi.alerts.safe_cage import render_safe_cage_alert_email

import base64
import boto3
from botocore.exceptions import ClientError
import cv2
from datetime import datetime, timedelta, timezone
import functools
from glob import glob
import io
import json
import logging
import os
logger = logging.getLogger('atlapi')

import operator
import psycopg2
import requests
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread, deferToThreadPool
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue, Deferred
from urllib.parse import quote


class Violation_Processor(Core):
    def __init__(self):
        Core.__init__(self, 'violation_processor')
        self.AWS = self.svcs.aws
        self.DB = self.svcs.db
        self.SES =  self.AWS('ses')

    @inlineCallbacks
    def alert(self, mvres):
        try:
            mvres.collection_time = to_datetime(mvres.collection_time)
            vimages = AD()
            for violation_group, categories in mvres.output.violations.items():
                avg = mvres.output.violation[violation_group]
                avg.violations= AD()
                logger.info(f"Violation_Processor images: " + str(mvres.images))
                for category_name, violations in categories.items():
                    if len(violations):
                        conditions = avg.categories[category_name].conditions
                        violations = sorted([violation for violation in violations if violation in conditions and conditions[violation].alert])
                        if len(violations):
                            avgc = avg.violations[category_name] = AD()
                            if category_name == 'ppe':
                                for violation_name in violations:
                                    vname = f'{violation_name}_violation'
                                    avgc[vname] = AD({'image_link': None})
                                    avgc[vname].images = [i for i in mvres.images if vname in i]
                                    if len(avgc[vname].images):
                                        avgc[vname].image_link = self.create_image_link(avgc[vname].images[0])
                            else:
                                for violation_name in [v for v in violations if 'alignment' not in v]:
                                    avgc[violation_name] = AD({'image_link': None})
                                    if violation_name == 'cage_violation':
                                        avgc[violation_name].images = [i for i in mvres.images
                                                                       if violation_name in i and 'extreme' not in i]
                                        if len(avgc[violation_name].images):
                                            avgc[violation_name].image_link = self.create_image_link(avgc[violation_name].images[0])
                                    elif violation_name == 'extreme_cage_violation':
                                        avgc[violation_name].images = [i for i in mvres.images if violation_name in i]
                                        if len(avgc[violation_name].images):
                                            avgc[violation_name].image_link = self.create_image_link(avgc[violation_name].images[0])

                    # if (datetime.utcnow().timestamp() - mvres.collection_time.timestamp()) < alert.params.active_alert_window:
                #     logger.info(f'Violation_Processor violation collection_time - {mvres.collection_time.isoformat()}' +
                #                 f'is older than the active alert window {(datetime.utcnow() - timedelta(seconds=alert.params.active_alert_window)).isoformat()}')
                #     return
                active = len(list(avg.violations.keys()))
                if not active:
                    logger.info(f"Violation_Processor no violations to process for {violation_group}: - {avg.violations}")
                    return None
                else:
                    try:
                        facilities = yield self.DB.customer_report_config.get_facilities(mvres.cgr, mvres.gateway_id)
                        # logger.info(f"violation.alert facility: {facilities.jstr()}")
                        for facility, fdata in facilities.items():
                            # logger.info(f"violation.alert facility: {facility}: {fdata.jstr()}")
                            res = yield self.process_safe_cage(mvres, avg, fdata)
                            # logger.info(f"violation.alert process_safe_cage result: " + repr(res))
                            alert_recipients = yield self.DB.users.alert_recipients(mvres.cgr, facility)
                            # logger.info(f"violation.alert alert_recipients: {alert_recipients}")
                            if len(alert_recipients) or len(avg.alert.atollogy_recipients):
                                # logger.info(f"violation.alert sending alert email for {violation_group}: {alert_recipients} - {avg.alert.atollogy_recipients}")
                                res = yield self.send_alert_email(violation_group, fdata.subject_name, mvres.cgr, facility, alert_recipients, avg, mvres)
                                # logger.info(f"Violation_Processor.send_alert_email result: " + str(res))
                                return res
                            else:
                                logger.info(f"Violation_Processor alert recipient list and atollogy_recipients are both empty - not sending alert")
                    except Exception as err:
                        logger.info(f"Violation_Processor.main block exception: " + str(err))
                        raise
        except Exception as err:
            logger.exception("Violation_Processor alert processing failed: " + repr(err))
            raise

    def create_image_link(self, s3link):
        # media.atollogy.com/<cgr>/<gwid>/<camera_id>/<ctime>/<step_name>/<fname>
        lparts = s3link[4:].split('/')
        fparts = lparts[-1].split('_')
        nodename = lparts[3]
        cgr = nodename.split('_')[1]
        tenv = BCM.env
        step_name =lparts[5]
        fname = lparts[-1].rsplit('_', 1)[-1]
        gwid = fparts[0]
        camera_id = fparts[1]
        ctime = fparts[2]
        if tenv == 'prd':
            image_link = f"https://media.atollogy.com/{cgr}/{gwid}/{camera_id}/{ctime}/{step_name}/{fname}"
        else:
            image_link = f"https://{tenv}.at0l.io/portal/image/{cgr}/{gwid}/{camera_id}/{ctime}/{step_name}/{fname}"
        return image_link

    def lpr_correlation(self, rows, event_interval=4, unknown_image_index=0):
        events = AD()
        #logger.info(f"Violation_Processor.lpr_correlation rows: {rows}")
        try:
            if not len(rows):
                return None
            for r in rows:
                enumber = int(to_datetime(r.collection_time).timestamp()/event_interval)
                if enumber not in events:
                    events[enumber] = AD({"confidence": 0,
                                          "enumber": enumber,
                                          "identifier": 'unknown',
                                          "image": None,
                                          "record": None,
                                          "records": [r]
                    })
                else:
                    events[enumber].records.append(r)
            event_ids = tuple(sorted(events.keys(), reverse=True))
            logger.info(f"event_numbers: {event_ids}")
            if not len(event_ids):
                return None
            identity_found = None
            for event_id in event_ids:
                edata = events[event_id]
                edata.identifiers = [(r.step_output.identifier,
                                      r.step_output.confidence,
                                      to_datetime(r.collection_time),
                                      r.images[0],
                                      r)
                                     for r in edata.records
                                     if (r.step_output.reason not in ['No License plate identified', 'error'] and
                                         'identifier' in r.step_output and
                                         'confidence' in r.step_output and
                                         'plate' in r.step_output)]
                if len(edata.identifiers):
                    winner = max(edata.identifiers, key=lambda r: r[1])
                    if isinstance(edata, (AD, dict)) and isinstance(winner, tuple) and len(winner) == 5:
                        # logger.info(f"Violation_Processor.lpr_correlation winning record: {winner}")
                        edata.identifier, edata.confidence, edata.collection_time, edata.image, edata.record = winner
                        identity_found = event_id
                        break

            if identity_found:
                event = events[identity_found]
                step_output = event.record.step_output
                del event.record.step_output
                event.record.output = step_output
                del event.records
                repopulate_mvres_from_step_record(event.record, event.image)  # repopulate_mvres_from_step_record from common
                return event
            elif len(events) and len(event_ids):
                event = events[event_ids[0]]
                event.record = sorted([r for r in event.records if len(r.images)],
                                      key=lambda r: r.collection_time.timestamp())[0]
                event.image = event.record.images[0]
                step_output = event.record.step_output
                del event.record.step_output
                event.record.output = step_output
                if 'identifier' not in event:
                    event.identifier = 'unknown'
                del event.records
                repopulate_mvres_from_step_record(event.record, event.image)
                return event
            else:
                logger.info(f"Violation_Processor.lpr_correlation found no results")
                return None
        except Exception as err:
            logger.exception(f"Violation_Processor.lpr_correlation failed: " + repr(err))
            raise

    @inlineCallbacks
    def process_safe_cage(self, mvres, avg, fdata):
        try:
            rows = yield self.DB.image_step.correlate(
                        mvres.cgr,
                        avg.correlation.sources,
                        avg.correlation.step_functions,
                        mvres.collection_time - timedelta(minutes=15),
                        mvres.collection_time
                    )
            logger.info(f"violations.process_safe_cage lpr_correlation rows: " + str(rows))
            result = self.lpr_correlation(
                            rows,
                            event_interval=avg.correlation.event_interval if 'event_interval' in avg.correlation else 4,
                            unknown_image_index=avg.correlation.unknown_image_index if 'unknown_image_index' in avg.correlation else 0
               )
            if not result:
                logger.info(f"violations.process_safe_cage lpr_correlation_result is empty")
                return None
            else:
                # logger.info(f"violations.process_safe_cage lpr_correlation_result: {result}")
                avgc = avg.violations['lpr_correlation'] = AD()
                crecord = avgc[avg.correlation.name] = AD()
                crecord.identifier = result.identifier
                correlation_mvres = AD(result.record)
                image = yield self.AWS.S3.get_image(result.image)
                correlation_mvres.output.filedata = [(f"truck-violation.jpg", "bio", image)]
                step_name = f"{fdata.subject_name.lower().replace(' ', '_')}_{avg.correlation.name}"
                iparts = result.image.split('//')[-1].split('/')
                iparts[4] = step_name
                iparts[5] = 'annotated'
                correlation_mvres.step_name = step_name
                correlation_mvres.step_function = 'lpr'
                correlation_mvres.images = []
                iparts[-1] = '_'.join(iparts[-1].split('_')[0:3] + [f"annotated_{step_name}-violation.jpg"])
                crecord.image = '/'.join(['s3:/'] + iparts)
                crecord.image_link = self.create_image_link(crecord.image)
                res = yield self.mvrq.put((correlation_mvres.customer_id, correlation_mvres.headers, {}, [correlation_mvres]))
                return res
        except Exception as err:
            logger.exception(f"Violation_Processor.process_safe_cage exception: " + repr(err))
            raise

    @inlineCallbacks
    def send_alert_email(self, violation_group, subject_name, cgr, facility, alert_recipients, avg, mvres):
        try:
            #logger.info(f"send_email_alert parameters: \n{violation_group}\n{cgr}\n{facility}\n{alert_recipients}\n{avg}")
            alert_emails = AD({
                "safe_cage": ('SAFETY COMPLIANCE', render_safe_cage_alert_email)
            })
            if violation_group in alert_emails:
                topic, render_email = alert_emails[violation_group]
                email_body = render_email(
                                    mvres.step_name,
                                    facility,
                                    subject_name,
                                    mvres.collection_time,
                                    mvres.collection_time,
                                    avg.violations
                                )

                params = AD({
                    "Destination": {
                        "ToAddresses": [],
                        "BccAddresses": [],
                        "CcAddresses": []
                    },
                    "Message": {
                        'Subject': {
                            'Data': f'{topic} ALERT (BETA): Incident at Site/Facility: {cgr}/{facility} at location {subject_name}',
                            'Charset': 'UTF-8'
                        },
                        'Body': {
                            'Text': {
                                'Data': quote(email_body),
                                'Charset': 'UTF-8'
                            },
                            'Html': {
                                'Data': email_body,
                                'Charset': 'UTF-8'
                            }
                        }
                    },
                    "Source": "admin@atollogy.com"
                })

                if self.env != 'prd':
                    params["SourceArn"] = "arn:aws:ses:us-west-2:434432432932:identity/admin@atollogy.com"

                params.Destination.ToAddresses = [e for e in unique(alert_recipients) if e and len(e) > 5]
                if len(params.Destination.ToAddresses) == 0:
                    params.Destination.ToAddresses = [e for e in unique(avg.alert.atollogy_recipients) if e and len(e) > 5]
                else:
                    params.Destination.BccAddresses = [e for e in unique(avg.alert.atollogy_recipients) if e and len(e) > 5]
                try:
                    res = yield self.SES.send_email(**params)
                    logger.info(f"Violation_Processor.send_alert_email ses result: " + str(res))
                except ClientError as e:
                    logger.exception(f"Violation_Processor send_email exception - {violation_group}: {e.response['Error']['Message']}")
                    raise
                else:
                    logger.info(f"Violation_Processor send_email success - {violation_group}: {res}")
        except Exception as err:
            logger.exception( f"Violation_Processor send_email exception - {violation_group}: " + repr(err))
            raise
