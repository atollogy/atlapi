#!/usr/bin/env python3.6

from atlapi.attribute_dict import *
from atlapi.core import Core
from atlapi.common import *
from atlapi.consul_kvs import CAD_Cache
from atlapi.database.helpers import *
from atlapi.database.db_events  import Event_Handler
import atlapi.database.pgpubsub as pgpubsub
import atlapi.database.tables as tables

import asyncio
from collections import namedtuple
from contextlib import contextmanager
import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta, timezone
import functools
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
import os
from pprint import pprint as pp
import psycopg2
import psycopg2.pool
from psycopg2 import sql
import pyconsul as consul
from random import randint
import threading
import time
from twisted.enterprise import adbapi
from twisted.internet import task, reactor
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue


DEBUG_SQL = False

# --------------------------------------------------------------------------------------------------------------


class DB(Core):
    def __init__(self, version=None):
        Core.__init__(self, 'db', version=version)
        self.connections = AD()
        self.event_handlers = AD()
        self.table_cache = AD()
        for t in tables.Tables:
            setattr(self, '_'.join(t.split('_')[:-1]).lower(), getattr(tables, t)(self, version))
        self.cgr_watcher = task.LoopingCall(self.check_active_cgrs)
        self.cgr_watcher.start(30, now=True)

    def __call__(self, cgr, ctype='pool'):
        if cgr not in self.connections:
            db = self.get_database(cgr)
        else:
            db = self.connections[cgr]
        if ctype in ['dsn', 'mgmt', 'pool', 'pubsub']:
            if ctype == 'pool':
                return PoolCursor(db)
            elif ctype == 'mgmt':
                return  CursorContext(**db.dsn)
            elif ctype == 'pubsub':
                return  db.pubsub
            else:
                return db.dsn
        else:
            logger.error(f"Failed to connect database for: {cgr}")
            return None

    @inlineCallbacks
    def beacon_reading_insert(self, table, cgr, headers, key_dict, data, key_map_dict, results):
        try:
            with self.get_cursor(cgr) as cur:
                normal = [rec for rec in data if not "backlog" in rec]
                if len(normal):
                    normal_sqldict = self.build_insert_sql(table, key_dict, normal, key_map_dict)
                    normal_final_sql = "{} {} ON CONFLICT DO NOTHING;".format(
                        normal_sqldict["sql"].as_string(cur),
                        ",".join(
                            [
                                cur.mogrify(normal_sqldict["tmpl"].as_string(cur), row).decode(
                                    "utf-8"
                                )
                                for row in normal_sqldict["rows"]
                            ]
                        )
                    )
                    yield deferToThread(cur.execute, normal_final_sql)

                backlog = [rec for rec in data if "backlog" in rec]
                if len(backlog):
                    backlog_sqldict = self.build_insert_sql(table, key_dict, backlog, key_map_dict)
                    backlog_final_sql = "{} {} ON CONFLICT DO NOTHING;".format(
                        backlog_sqldict["sql"].as_string(cur),
                        ",".join(
                            [
                                cur.mogrify(backlog_sqldict["tmpl"].as_string(cur), row).decode(
                                    "utf-8"
                                )
                                for row in backlog_sqldict["rows"]
                            ]
                        )
                    )
                    yield deferToThread(cur.execute, backlog_final_sql)

                msg = "DB {} insert success:  normal record count: {} - backlog record count - {}".format(
                    table, len(normal), len(backlog)
                )
                results["success"].append(msg)
                # logger.debug(msg)
                return  results
        except Exception as e:
            logger.exception("DB {} insert error: {}".format(table, e))
            results["failure"].append("DB {} insert error: {}".format(table, e))
            raise

    def build_insert_sql(self, table, key_dict, data, key_map_dict):
        """Preps a db insert by taking a  key dictionary of namedtuples,
           some row data dictionaries in a list, and an alternate keys dictionary.
           Returns a dictionary keyed by query signature = [sql, tmpl, values]
        """
        table_sql = None

        if not isinstance(data, (list, tuple)):
            raw_rows = [data]
        else:
            raw_rows = data

        try:
            insert_data = []
            for vdict in raw_rows:
                value_dict = AD()
                for k, v in AD(vdict).deep_items():
                    if isinstance(v, str):
                        v = v.replace("'", '')
                    value_dict[k] = v
                value_dict = {
                    k if k not in key_map_dict else key_map_dict[k]: v for k, v in value_dict.items()
                }
                vdict_keys = sorted(value_dict.keys())
                insert_dict = {}

                for k in vdict_keys:
                    if k in key_dict:
                        if value_dict[k] is not None:
                            insert_dict[k] = key_dict[k].ftype(value_dict[k])
                        else:
                            insert_dict[k] = None

                for k in insert_dict.keys():
                    if isinstance(insert_dict[k], bytes):
                        insert_dict[k] = insert_dict[k].decode("utf-8")

                if not table_sql:
                    dkeys = tuple(sorted(insert_dict.keys()))
                    table_sql = {
                        "_keys": dkeys,
                        'gateway_id': value_dict['gateway_id'] if 'gateway_id' in value_dict else None,
                        "sql": sql.SQL("INSERT INTO {0} ({1}) VALUES ").format(
                            sql.Identifier(table), sql.SQL(", ").join(map(sql.Identifier, dkeys))
                        ),
                        "tmpl": sql.SQL("({})").format(
                            sql.SQL(", ").join(map(sql.Placeholder, dkeys))
                        ),
                        "rows": [insert_dict],
                    }
                else:
                    table_sql["rows"].append(insert_dict)

            return table_sql
        except Exception as e:
            logger.exception("build_insert_sql Error: {}".format(e))
            raise

    @inlineCallbacks
    def check_active_cgrs(self):
        regions = yield deferToThread(self.svcs.ckv.get, f"envs/{BCM.env}/regions")
        res = yield deferToThread(self.svcs.ckv.get, f"cgrsActive/{BCM.env}")
        cgrsActive = [cgr for cgr in res if cgr != 'apdx']
        for cgr in cgrsActive:
            try:
                if cgr == 'apdx':
                    continue
                if cgr not in self.connections:
                    res = self.get_database(cgr)
                if 'atlapi.db_event_master' in BCM.local and BCM.local.atlapi.db_event_master:
                    if cgr not in self.event_handlers:
                        self.event_handlers[cgr] = Event_Handler(self, regions[cgr].endpoints.db, cgr)
            except Exception as e:
                logger.exception(f"atlapi.db.check_active_cgrs exception for cgr {cgr}: {err}")
        try:
            if 'atlapi.db_event_master' in BCM.local and BCM.local.atlapi.db_event_master:
                for cgr in [cgr for cgr in self.event_handlers if cgr not in cgrsActive]:
                    del self.event_handlers[cgr]
        except Exception as e:
            logger.exception(f"atlapi.db.check_active_cgrs exception for cgr {cgr}: {err}")

    def check_health(self):
        """Cycle through connections and execute simple query to check db health"""
        results = {}
        scode = 200
        for cust in self.connections.keys():
            try:
                with self.get_cursor(cust) as cursor:
                    res = cursor.execute("SELECT 1")
                    res = cursor.fetchall()
                    # logger.debug('health check: {}'.format(res))
                    if str(res) == "[(1,)]":
                        results[cust] = "okay"
                    else:
                        results[cust] = "error"
            except Exception as e:
                logger.exception("DB HealthCheck Error: " + repr(e))
                scode = 400
                results[cust] = e
                raise
        # logger.debug('DB check health results: {}'.format(results))
        return (scode, results)

    def customer_ready(self, cgr):
        if cgr in self.connections:
            return True
        else:
            return self.get_database(cgr)

    @contextmanager
    def get_cursor(self, cgr):
        if cgr not in self.connections:
            db = self.get_database(cgr)
        else:
            db = self.connections[cgr]
        with PoolCursor(db.pool) as cur:
            yield cur

    def get_database(self, cgr):
        if cgr not in self.connections:
            dbsrv = AD(self.svcs.ckv.get(f"envs/{BCM.env}/regions/{cgr}/endpoints/db"))
            try:
                dsn = AD(
                        dict(
                            host=dbsrv["host"],
                            port=dbsrv["port"],
                            user=dbsrv["user"],
                            password=dbsrv["password"],
                            dbname=dbsrv["database"]
                        )
                    )
                self.connections[cgr] = AD({
                    'dsn': dsn,
                    'mgmt': CursorContext(**dsn),
                    'pool': PoolBoy(cgr, **dsn),
                    'pubsub': pgpubsub.connect(**dsn)
                })
            except Exception as err:
                logger.exception(f"atlapi.db.get_database exception: {err}")
                return None
        return self.connections[cgr]

    @inlineCallbacks
    def insert(self, table, cgr, headers, keys, recdata, key_map, update_keys=[], return_sql_dict=False):
        try:
            table_id_sequences = AD({
                'image_step': 'image_id_seq',
                'gateway_beacon_stats': 'stats_id_seq',
                'gateway_service_status': 'status_id_seq',
                'image_step_annotation_id': 'image_step_annotation_id_seq',
                'beacon_reading': 'reading_id_seq'
            })
            results = AD({"success": [], "data": [],  "failure": [], "sql_dict": None})
            tstart = time.time()
            if table == "beacon_reading":
                res = yield self.beacon_reading_insert(
                    table, cgr, headers, keys, recdata, key_map, results
                )
                # logger.info(f"db.insert beacon_reading_insert result: " + str(res))
            else:
                sql_dict = self.build_insert_sql(table, keys, recdata, key_map)
                if return_sql_dict:
                    results.sql_dict = sql_dict
                with self.get_cursor(cgr) as cur:
                    final_sql = "{} {} ".format(
                        sql_dict["sql"].as_string(cur),
                        ",".join(
                            [
                                cur.mogrify(sql_dict["tmpl"].as_string(cur), row).decode("utf-8")
                                for row in sql_dict["rows"]
                            ]
                        )
                    )
                    if ' Infinity,' in final_sql:
                        final_sql = final_sql.replace(' Infinity,', " 'Infinity',")

                    try:
                        ##############################################################################################
                        # "ON CONFLICT DO NOTHING" HIDES DUPE KEY ERRORS, WHICH WE ARE TOO STUPID TO FIND A FIX FOR
                        ##############################################################################################
                        sets = []
                        rd = AD(sql_dict['rows'][0])
                        if table == 'image_step':
                            final_sql += " ON CONFLICT (gateway_id, camera_id, collection_time, step_name) DO UPDATE SET "
                            final_sql += f"step_output = '{rd.step_output}', insert_time = now()"
                            if len(rd.images):
                                final_sql += f''', images = ARRAY[{",".join([f"'{i}'" for i in rd.images])}]::text[];'''
                            else:
                                final_sql += ", images = NULL;"

                        elif len(update_keys) and table == 'image_step_annotation':
                            for key in update_keys: #Construct UPSERT statement based on provided keys
                                update_value = ''
                                if key == "latest_seen_time":
                                    update_value = to_datetime(rd[key])
                                elif key == "asset_info":
                                    update_value = json.dumps(rd[key], default=str)
                                sets.append("{} = '{}'".format(key, update_value))
                            final_sql += " ON CONFLICT (asset_id, qualifier, attribute_value) DO UPDATE SET " + ', '.join(sets)
                        else:
                            final_sql += " ON CONFLICT DO NOTHING;"
                        if not final_sql.endswith(';'):
                            final_sql += ';'
                        # logger.info(f"db.insert final_sql: {final_sql}")
                        try:
                            yield deferToThread(cur.execute, final_sql)
                        except psycopg2.errors.SerializationFailure:
                            time.sleep(1)
                            yield deferToThread(cur.execute, final_sql)

                        if table == 'image_step' and hasattr(recdata, 'keys') and 'images' in recdata:
                            yield self.resolver.update(cgr, recdata.images)
                        # logger.info(f"db.insert standard result: {res}")
                        # logger.info(f"db.insert - {cgr} {table} {headers}: cur.execute success")
                    except Exception as err:
                        logger.exception(f"database.insert collision exception on {cgr}.{table}: {repr(err)} - {final_sql}")
                        # if table == 'image_step':
                        #     yield deferToThread(cur.execute, f"""
                        #         select *
                        #         from image_step i
                        #         where i.gateway_id = '{recdata[0].gateway_id if isinstance(recdata, list) else recdata.gateway_id}' and
                        #               i.collection_time = '{to_datetime(recdata[0].collection_time if isinstance(recdata, list) else recdata.collection_time).isoformat()}' and
                        #               i.step_name = '{recdata.step_name}';""")
                        #     row = yield cur.fetchall()
                        #     logger.exception(f"database.insert collision row {cgr}.{table}: {row}")
            tend = time.time()
            results["query_time"] = tend - tstart
            # logger.debug(f"database.insert {cgr}.{table}: {len(recdata) if isinstance(recdata, list) else 1} record(s) took: {results.query_time} secs")
            return results
        except Exception as err:
            logger.exception(f"database.insert exception on {cgr}.{table}: {repr(err)}")

    @inlineCallbacks
    def list_tables(self, cgr):
        final_sql = f"""SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';"""
        now = time.time()
        if cgr not in self.table_cache or (now - self.table_cache[cgr][0] > 900):
            dsn = self.get_database(cgr).dsn
            with CursorContext(dict_cursor=True, **dsn) as cur:
                try:
                    yield deferToThread(functools.partial(cur.execute, final_sql))
                    res = yield deferToThread(cur.fetchall)
                    if len(res):
                        tables = [r['tablename'] for r in res]
                        self.table_cache[cgr] = (now, tables)
                        logger.info(f'atlapi.database.list_tables result {tables}')
                except Exception:
                    logger.info(f'atlapi.database.list_tables exception {err}')
        if cgr in self.table_cache:
            return self.table_cache[cgr][-1]
        else:
            return []

    @contextmanager
    def _mgmt_cursor(self, cgr, dict_cursor=False):
        if cgr not in self.connections:
            db = self.get_database(cgr)
        else:
            db = self.connections[cgr]
        with self.get_database(cgr).mgmt as cur:
            yield cur

    def table_exists(self, cgr, table):
        if cgr in self.table_cache :
            return table in self.table_cache[cgr][-1]
        else:
            return False

    @exec_timed
    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        results = AD({"success": [], "data": [], "failure": []})
        tstart = time.time()
        try:
            with self.get_cursor(cgr) as cur:
                try:
                    yield deferToThread(cur.execute, final_sql)
                    temp_result = yield cur.fetchall()
                except psycopg2.ProgrammingError as e:
                    logger.exception("No results returned from{final_sql}")
                    results["data"]  = []
                else:
                    results["data"] = temp_result if isinstance(temp_result, (list, tuple)) else []
                results["query_time"] = time.time() - tstart
                results["success"].append("Raw Query {} took: {} secs".format(
                    final_sql, results["query_time"]))
        except Exception as e:
            logger.exception("DB {} error: {}".format(final_sql, e))
            tend = time.time()
            results["query_time"] = tend - tstart
            results["failure"].append("DB {} error: {}".format(final_sql, e))
        return results
