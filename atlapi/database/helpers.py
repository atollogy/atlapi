
from atlapi.attribute_dict import *
from atlapi.common import *
import logging
logger = logging.getLogger('atlapi')
import psycopg2
import psycopg2.pool
from psycopg2 import sql
from psycopg2.extras import DictCursor

AUTO_COMMIT = psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
REPEATABLE_READ = psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ
READ_COMMITTED = psycopg2.extensions.ISOLATION_LEVEL_READ_COMMITTED
SERIALIZABLE = psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE

class Fake_Cursor(object):
    def __init__(self, cgr, connset):
        self.cgr = cgr
        for k, v in connset.items():
            setattr(self, f"_{k}", v)
        self.conn = psycopg2.connect(
                            host=self._cfg.host,
                            port=self._cfg.port,
                            user=self._cfg.user,
                            password=self._cfg.password,
                            dbname=self._cfg.database
                        )
        self.cur = self.conn.cursor()
        self.result = None

    @inlineCallbacks
    def execute(self, *args, **kwargs):
        try:
            self.result = None
            if len(args) > 1:
                query = args[0].format(*tuple(args[1:]))
            else:
                query = args[0]
            sql_check = query.lower()
            try:
                if 'insert' in query or 'delete' in query:
                    self.results = yield self._pool.runOperation(query)
                else:
                    self.results = yield self._pool.runQuery(query)
            except psycopg2.ProgrammingError:
                pass
        except Exception as err:
            logger.exception(f"Fake_Cursor.execute exception: " + repr(err))
            raise

    def mogrify(self, *args, **kwargs):
        try:
            res = self.cur.mogrify(*args, **kwargs)
            return res
        except Exception as err:
            logger.exception(f"Fake_Cursor.mogrify exception: " + repr(err))
            raise

    def fetchone(self):
        try:
            if self.result and isinstance(self.result, tuple) and len(self.result) == 1:
                value = self.result[0]
                self.result = None
                return value
            elif self.result and isinstance(self.result, tuple) and len(self.result) > 1:
                value = self.result[0]
                self.result = self.result[1:]
                return value
            else:
                return None
        except Exception as err:
            logger.exception(f"Fake_Cursor.fetchone exception: " + repr(err))
            raise

    def fetchall(self):
        try:
            if not isinstance(self.result, (list, tuple)):
                self.result = None
                value = []
            else:
                value = self.result
                self.result = None
            return value
        except Exception as err:
            logger.exception(f"Fake_Cursor.fetchall exception: " + repr(err))
            raise


class PoolBoy:
    def __init__(self, cgr, **dbserv):
        self.cgr = cgr
        self.dbserv = dbserv
        self.__pool =  psycopg2.pool.ThreadedConnectionPool(
                                    5,
                                    50,
                                    **dbserv)

    def get_connection(self):
        conn = self.__pool.getconn()
        conn.set_session(autocommit=True)
        return conn

    def return_connection(self, connection):
        self.__pool.putconn(connection)

    def close_all_connections(self):
        self.__pool.closeall()


class CursorContext:
    """Cursor context manager"""

    def __init__(self, dict_cursor=False, **dbserv):
        self.dbserv = dbserv
        self.dict_cursor = dict_cursor
        self.connection = None
        self.cursor = None

    def __enter__(self):
        # get a connection from the pool
        self.connection = psycopg2.connect(**self.dbserv)
        self.connection.set_session(autocommit=True)
        if self.dict_cursor:
            self.cursor = self.connection.cursor(cursor_factory=DictCursor)
        else:
            self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Rollback if not everything is good
        try:
            if exc_val is not None:
                self.connection.rollback()
            else:
                self.cursor.close()
                self.connection.commit()

            self.connection.close()
            self.connection = None
        except Exception as err:
            logger.exception(f"CursorContext.exit exception@{self.dbserv.host}:{self.dbserv.database}")
            raise

class PoolCursor:
    """Get a cursor from a connection from pool of connections."""

    def __init__(self, db):
        self.db = db
        self.connection = None
        self.cursor = None

    def __enter__(self):
        # get a connection from the pool
        self.connection = self.db.get_connection()
        self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Rollback if not everything is good
        if exc_val is not None:
            self.connection.rollback()
        else:
            self.cursor.close()
            self.connection.commit()

        # put it back to pool
        self.db.return_connection(self.connection)
