#!/usr/bin/env python3

from atlapi.core import Core
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.consul_kvs import CAD_Cache
from atlapi.database.helpers import *
from atlapi.image.utilities import *
from atlapi.results import lpr

import asyncio
from collections import namedtuple
from datetime import datetime, timedelta, timezone
import functools
import iso8601
import json
import logging
import os
logger = logging.getLogger('atlapi')
from pprint import pprint as pp
import psycopg2
from psycopg2.extras import DictCursor
from psycopg2 import sql
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import Deferred, ensureDeferred, inlineCallbacks, maybeDeferred, waitForDeferred, returnValue


image_step_columns = [
    'image_step_id', 'gateway_id', 'camera_id', 'collection_time',
    'collection_interval', 'step_name', 'step_function',
    'step_function_version', 'step_config_version', 'step_output',
    'step_start_time', 'step_end_time', 'processor_role',
    'images', 'insert_time'
]

image_step_annotation_columns = [
    'image_step_annotation_id', 'collection_time', 'gateway_id',
    'camera_id', 'step_name', 'step_function_version',
    'step_config_version', 'session_id', 'user_id' ,'comment', 'is_accurate',
    'created_at_date' ,'qc_result', 'applied', 'ref_id'
]


class Event_Handler(Core):
    def __init__(self, db, cfg, cgr):
        super().__init__(f'db_{cgr}_event_handler', db=db)
        self.cgr = cgr
        self.cfg = cfg
        self.dsn = self.DB(self.cgr, ctype='dsn')
        self.pubsub = self.DB(self.cgr, ctype='pubsub')
        self.subscribed_to = []
        self.setup_finished = False
        self.duration = 6 * 3600
        self.checking = False
        self.ready = True
        self.tables = None
        self.seen = []
        self.missed_annotation_check = task.LoopingCall(self.check_for_missed_annotations)
        self.missed_annotation_check.start(300, now=True)
        self.poller = task.LoopingCall(self.poll_events)
        self.poller.start(90, now=False)
        logger.info(f"PG_Event_Handler.started for: {self.cgr}")

    ### periodic check for missed annotations
    @inlineCallbacks
    def check_for_missed_annotations(self):
        check_sql = '''
            SELECT  isa.image_step_annotation_id,
                    isa.collection_time,
                    isa.gateway_id,
                    isa.camera_id,
                    isa.step_name,
                    isa.step_function_version,
                    isa.step_config_version,
                    isa.session_id,
                    isa.user_id,
                    isa.comment,
                    isa.is_accurate,
                    isa.created_at_date,
                    isa.qc_result,
                    isa.applied
            FROM image_step_annotation isa
            WHERE isa.qc_result -> 'readingStatus' ->> 'read_incorrectly' IS NOT NULL and
                  isa.step_name != 'generate_asset_attribute_records' and
                  isa.applied is null and
                  cast(isa.qc_result -> 'readingStatus' ->> 'read_incorrectly' as text) != 'true'
            ORDER BY isa.gateway_id ,isa.collection_time DESC
        '''
        try:
            if not self.checking and self.ready:
                self.checking = time.time()
                with CursorContext(dict_cursor=True, **self.dsn) as cur:
                    try:
                        yield deferToThread(
                                    functools.partial(cur.execute, check_sql)
                                )
                        missed_annotations = yield cur.fetchall()
                        # logger.info(f"PG_Event_Handler.check_for_missed_annotations - {self.cgr} - query results: {missed_annotations}")
                        if isinstance(missed_annotations,(list, tuple)) and len(missed_annotations):
                            corrections = [AD(r) for r in missed_annotations]
                            for rec in corrections:
                                event = {
                                    "timestamp": str(datetime.utcnow()),
                                    "operation": "MISSED",
                                    "schema": "image_step_annotation",
                                    "table": "image_step_annotation",
                                    "data": rec
                                }
                                res = yield self.process_event(event)
                                if self.checking and isinstance(self.checking, float) and (time.time() - self.checking) > 3600:
                                    self.checking = False
                                    break
                    except psycopg2.errors.UndefinedColumn as err:
                        logger.exception(f"PG_Event_Handler.missing_column_error - {self.cgr} - exception: {err} - disabling event_handler!!!!!")
                        self.ready = False
                    except Exception as err:
                        logger.exception(f"PG_Event_Handler.check_for_missed_annotations - {self.cgr} - exception: {err}")
                self.checking = False
            if self.checking and isinstance(self.checking, float) and (time.time() - self.checking) > 3600:
                self.checking = False

        except Exception as err:
            logger.exception(f"PG_Event_Handler.check_for_missed_annotations - {self.cgr} - exception: {err}")
            self.checking = False

    @inlineCallbacks
    def create_image_step_record(self, event):
        fields = self.get_event_fields(event)
        if 'qc_result.readingStatus.synthetic' in event.data and event.data.qc_result.readingStatus.synthetic:
            if 'offset' in event.data.qc_result.readingStatus:
                offset = int(event.data.qc_result.readingStatus.offset)
                start_time = fields.collection_time - timedelta(seconds=offset)
                end_time = fields.collection_time + timedelta(seconds=offset)
            else:
                start_time = fields.collection_time
                end_time = fields.collection_time + timedelta(seconds=10)
        else:
            start_time = fields.collection_time - timedelta(seconds=1)
            end_time = fields.collection_time + timedelta(seconds=1)

        origin_sql = f'''select *
                  from image_step i
                  where i.gateway_id = '{fields.gateway_id}' and
                      i.collection_time between '{start_time.isoformat()}' and '{end_time.isoformat()}' and
                      i.step_name = '{fields.step_name}' and
                      i.step_output -> 'identifier' is null
                  limit 1;'''

        with CursorContext(dict_cursor=True, **self.dsn) as cur:
            try:
                yield deferToThread(functools.partial(cur.execute, origin_sql))
                res = yield deferToThread(cur.fetchall)

                if len(res):
                    vdict = AD(res[0])
                    vdict.images = unique(vdict.images)
                    vdict.update(fields)
                    return vdict
                else:
                    placeholder_image = self.create_placeholder_image(fields.new_plate)
                    step_record = lpr.new_result(fields, [])
                    logger.info(f"new step_record: {step_record}")
                    step_record.step_output.filedata = [('default_annotated.jpg',"bio", pack_image(placeholder_image))]
                    atlapi = self.svcs.atlapi
                    yield atlapi.mv_result_handler((self.cgr, fields, {}, step_record))
                    time.sleep(1)
                    res = yield self.get_image_step_result(event)
                    return res
            except Exception as err:
                logger.exception(f"PG_Event_Handler.create_image_step_record - {self.cgr} - exception: {err}")
                return None

    def create_placeholder_image(self, new_plate):
        label = f"Synthetic Record: {new_plate}"
        lwidth, lheight = cv2.getTextSize(label, cv2.FONT_HERSHEY_DUPLEX, 1.0, 1)[0]
        placeholder_image = np.zeros((720,1080,3), np.uint8)
        placeholder_image[:,:] = (175, 100, 100)
        cv2.putText(placeholder_image, label, (640 - lwidth//2, 360), cv2.FONT_HERSHEY_DUPLEX, 1.0, (240,255,255), 1, cv2.LINE_AA)
        return placeholder_image

    @inlineCallbacks
    def create_triggers(self):
        list_triggers_sql = '''
            SELECT tgname
            FROM pg_trigger, pg_class
            WHERE tgrelid=pg_class.oid AND
                  relname='{}';
        '''

        delete_trigger = '''DROP TRIGGER {} on {};'''

        create_trigger_sql = '''
            CREATE TRIGGER {}
                AFTER INSERT OR UPDATE OR DELETE
            ON {}
            FOR EACH ROW
                EXECUTE PROCEDURE notify_trigger(
                    {}
                );
        '''
        logger.info(f"{self.cfg}")
        for ttype, table_details in self.cfg.subscriptions.items():
            tables, columns = table_details
            for table in tables:
                if table in self.tables:
                    trigger_name = f"{table}_{ttype}_trigger"
                    try:
                        with CursorContext(**self.dsn) as cur:
                            yield deferToThread(
                                    functools.partial(
                                        cur.execute,
                                        list_triggers_sql.format(table)
                                    )
                                )
                            res = yield cur.fetchall()
                            triggers = [r[0] for r in res]
                            logger.info(f"PG_Event_Handler.create_triggers - {self.cgr} - triggers {triggers}")
                            trigger_exists = trigger_name in triggers
                            if trigger_exists:
                                yield deferToThread(
                                    functools.partial(cur.execute, delete_trigger.format(trigger_name, table))
                                )
                                yield deferToThread(
                                    functools.partial(
                                            cur.execute,
                                            create_trigger_sql.format(trigger_name, table, str(image_step_annotation_columns)[1:-1])
                                        )
                                    )
                    except Exception as e:
                        logger.exception(f"PG_Event_Handler.create_triggers {self.cgr} - exception creating trigger {trigger_name}")
                    logger.info(f"PG_Event_Handler.create_triggers - {self.cgr} - created trigger {trigger_name}")
                else:
                    logger.info(f"PG_Event_Handler.create_triggers -{self.cgr} - create_triggers table '{table}' does not exist")

    @inlineCallbacks
    def get_image_step_result(self, event):
        fields = self.get_event_fields(event)
        start_time = fields.collection_time - timedelta(seconds=1)
        end_time = fields.collection_time + timedelta(seconds=1)

        sql = f'''select *
                  from image_step i
                  where i.gateway_id = '{fields.gateway_id}' and
                        i.collection_time between '{start_time.isoformat()}' and '{end_time.isoformat()}' and
                        i.step_name = '{fields.step_name}' and
                        i.step_output -> 'identifier' is not null
                  limit 1;'''

        with CursorContext(dict_cursor=True, **self.dsn) as cur:
            try:
                yield deferToThread(functools.partial(cur.execute, sql))
                res = yield deferToThread(cur.fetchall)
                if len(res):
                    vdict = AD(res[0])
                    vdict.images = unique(vdict.images)
                    if 'step_output.annotations.plate' in vdict:
                        for x in vdict.step_output.annotations.plate:
                            if 'origins' in vdict.step_output.annotations.plate[x]:
                                try:
                                    vdict.step_output.annotations.plate[x].origins = unique(vdict.step_output.annotations.plate[x].origins)
                                except Exception:
                                    vdict.step_output.annotations.plate[ats].origins = []
                    vdict.collection_time = to_datetime(vdict.collection_time)
                    return vdict
                else:
                    return None
            except Exception as err:
                logger.exception(f"PG_Event_Handler.get_image_step_result - {self.cgr} - exception: {err}")
                return None

    def get_event_fields(self, event):
        try:
            if 'qc_result' in event.data and isinstance(event.data.qc_result, (bytes, str)):
                event.data.qc_result = AD.loads(event.data.qc_result)
            fields = AD({k: event.data[k] for k in ['gateway_id', 'collection_time',
                                                    'step_name', 'created_at_date',
                                                    'image_step_annotation_id']})
            if '"' in fields.gateway_id:
                fields.gateway_id = fields.gateway_id.split('"')[1]
            fields.collection_time = to_datetime(fields.collection_time)
            fields.nodename = get_nodename(self.cgr, fields.gateway_id)
            fields.new_plate = None
            fields.customer_id = fields.cgr = self.cgr
            fields.trailer = True if 'qc_result.vehicleClass.trailer' in event else False

            if ('qc_result.readingStatus.read_incorrectly' in event.data and
                event.data.qc_result.readingStatus.read_incorrectly and
                len(event.data.qc_result.readingStatus.read_incorrectly) and
                event.data.qc_result.readingStatus.read_incorrectly not in ['true', 'false']):
                fields.new_plate = fields.identifier = event.data.qc_result.readingStatus.read_incorrectly.upper()

            if 'qc_result.readingStatus.id_type' in event.data:
                fields.modifier = 'synthetic_manual_vehicle_event'
            elif 'qc_result.readingStatus.synthetic' in event.data:
                fields.modifier = 'synthetic_hitl_correction_annotation'
            else:
                fields.modifier = 'hitl_correction_annotation'

            fields.ready = (fields.new_plate and not event.data.applied)
            return fields
        except Exception as err:
            logger.exception(f"PG_Event_Handler.get_event_fields - {self.cgr} - exception: {err} - event: {event}")
            raise

    @inlineCallbacks
    def get_tables(self):
        if not self.tables:
            self.tables = yield self.DB.list_tables(self.cgr)
        else:
            return self.tables

    @inlineCallbacks
    def poll_events(self):
        try:
            if self.ready:
                if not self.setup_finished:
                    if self.DB.customer_ready(self.cgr):
                        # logger.info(f"PG_Event_Handler -{self.cgr} - {self.cfg}")
                        yield self.get_tables()
                        res = yield self.trigger_procedure_setup()
                        res = self.create_triggers()
                        res = self.setup_listeners()
                        #self.cfg.register_callback(self.setup_listeners, 'subscriptions')
                        self.setup_finished = True
                else:
                    logger.debug(f"PG_Event_Handler.poll_events - {self.cgr} - polling for events")
                    event = yield self.pubsub.get_event()
                    while event:
                        logger.info(f"PG_Event_Handler.poll_events - {self.cgr} - received event for channel: {event.channel} with payload - {event.payload}")
                        res = yield self.process_event(AD(event.payload))
                        event = yield self.pubsub.get_event()
        except Exception as err:
            logger.exception(f"PG_Event_Handler.poll_events - {self.cgr} - listen exception - {err}")

    @inlineCallbacks
    def process_event(self, event):
        # event structure = {
        #   "timestamp": str(datetime.utcnow()),
        #   "operation": "INSERT",
        #   "schema": "image_step_annotation",
        #   "table": "image_step_annotation",
        #   "data": rec
        # }
        try:
            now = datetime.utcnow()
            event = AD(event)
            if 'step_name' in event.data and event.data.step_name == 'generate_asset_attribute_records':
                return
            elif 'data.image_step_annotation_id' in event and event.data.image_step_annotation_id in self.seen:
                return
            fields = self.get_event_fields(event)
            old_plate = None
            if event.operation in ['INSERT', 'UPDATE', 'MISSED'] and fields.ready:
                logger.info(f"PG_Event_Handler.process_event - {self.cgr} - received  correction event: {event}")
                step_result = yield self.get_image_step_result(event)
                if step_result is None:
                    step_result = yield self.create_image_step_record(event)
                if step_result:
                    step_result.hdrs = fields
                    if step_result.processor_role == 'synthetic':
                        res = yield self.update_asset_attributes(step_result, modes='plate', old_plate=old_plate)
                        res = yield self.update_asset_attributes(step_result, mode='vehicle_type')
                        res = yield self.update_image_step_annotation(step_result.image_step_id, fields)
                    else:
                        old_plate = step_result.step_output.identifier if 'identifier' in step_result.step_output else None
                        if not fields.nodename and len(step_result.images):
                            fields.nodename = step_result.images[0].split('/')[4]
                        updated_result = yield lpr.correct_result(event, step_result, fields, self.svcs.aws)
                        if not updated_result:
                            logger.info(f"PG_Event_Handler.process_event - {self.cgr} - null updated result for original record: {step_result}")
                            return
                        res = yield self.update_image_step_result(updated_result)
                        res = yield self.update_asset_attributes(updated_result, mode='plate', old_plate=old_plate)
                        res = yield self.update_image_step_annotation(step_result.image_step_id, fields)

                    if ('qc_result.vehicleClass.trailer' in event.data and
                        event.data.qc_result.vehicleClass.trailer):
                        res = yield self.update_asset_attributes(step_result, mode='vehicle_type')
                        res = yield self.update_image_step_annotation(step_result.image_step_id, self.get_event_fields(event))
                else:
                    logger.info(f"PG_Event_Handler.process_event - {self.cgr} - event returned no step_result: {event}")
            else:
                logger.debug(f"PG_Event_Handler.process_event non-insert operation - {self.cgr} - skipping event {event}")
            self.seen.append(event.data.image_step_annotation_id)
            if len(self.seen) > 1000:
                self.seen = self.seen[-1000:]
        except Exception as err:
            logger.exception(f"PG_Event_Handler.process_event exception - {self.cgr} - event: {event} - error: {err}")

    @inlineCallbacks
    def setup_listeners(self, *args):
        # subscriptions = {
        #    'change_events': []  <- tables
        #
        yield self.get_tables()
        for ttype, table_details in self.cfg.subscriptions.items():
            tables, columns = table_details
            for table in tables:
                if table in self.tables:
                    channel = f"{table}_{ttype}"
                    if channel not in self.subscribed_to:
                        res = yield self.pubsub.listen(channel)
                        self.subscribed_to.append(channel)
                        # logger.info(f"PG_Event_Handler -{self.cgr} - setup listener for channel: {channel}")
                else:
                    logger.debug(f"PG_Event_Handler - {self.cgr} - setup listener error: table '{table}' does not exist")

    @inlineCallbacks
    def trigger_procedure_setup(self):
        function_exists_sql = """
            with func_exists as (
                select  true
                from pg_proc p
                left join pg_namespace n on p.pronamespace = n.oid
                where n.nspname not in ('pg_catalog', 'information_schema') and
                      p.proname = 'notify_trigger')
                select count(*)
                from func_exists;"""

        function_sql = """
                CREATE OR REPLACE FUNCTION notify_trigger() RETURNS trigger AS $trigger$
                DECLARE
                    rec RECORD;
                    payload TEXT;
                    column_name TEXT;
                    column_value TEXT;
                    payload_items JSONB;
                BEGIN
                    -- Set result row depending on operation
                    CASE TG_OP
                    WHEN 'INSERT', 'UPDATE' THEN
                       rec := NEW;
                    WHEN 'DELETE' THEN
                       rec := OLD;
                    ELSE
                       RAISE EXCEPTION 'Unknown TG_OP: "%". Should not occur!', TG_OP;
                    END CASE;

                    -- Get required fields
                    FOREACH column_name IN ARRAY TG_ARGV LOOP
                      EXECUTE format('SELECT $1.%I::TEXT', column_name)
                      INTO column_value
                      USING rec;
                      payload_items := coalesce(payload_items,'{}')::jsonb || json_build_object(column_name,column_value)::jsonb;
                    END LOOP;

                    -- Build the payload
                    payload := json_build_object(
                      'timestamp',CURRENT_TIMESTAMP,
                      'operation',TG_OP,
                      'schema',TG_TABLE_SCHEMA,
                      'table',TG_TABLE_NAME,
                      'data',payload_items
                    );

                    -- Notify the channel
                    PERFORM pg_notify(concat(TG_TABLE_NAME, '_change_events'), payload);
                    RETURN rec;
                END;
                $trigger$ LANGUAGE plpgsql;"""
        try:
             with CursorContext(**self.dsn) as cur:
                yield deferToThread(
                                    functools.partial(cur.execute, function_sql)
                                )
        except psycopg2.ProgrammingError:
            pass
        except Exception as err:
            logger.exception(f"PG_Event_Handler.trigger_procedure_setup - {self.cgr} - exception: {err}")


    @inlineCallbacks
    def update_asset_attributes(self, result, mode='plate', old_plate=None):
        if result.images is not None and len(result.images) and result.step_function == 'lpr':
            if  mode == 'plate':
                asset_record = AD({
                    "asset_id": result.step_output.identifier,
                    "attribute_value": result.step_output.identifier,
                    "qualifier": "license_plate",
                    "first_seen_time": result.collection_time,
                    "latest_seen_time": result.collection_time,
                    "asset_info": {
                        "source_image_key": {
                            "gateway_id": result.gateway_id,
                            "camera_id": result.camera_id,
                            "collection_time": result.collection_time,
                            "step_name": result.step_name,
                            "name": result.images[0].split('_')[-1]
                        },
                        "source_image_link": result.images[0]
                    }
                })
                res = yield self.DB.asset_attribute.insert(self.cgr, None, [asset_record])

                inclusion_record = AD({
                    "asset_id": result.step_output.identifier,
                    "attribute_value": 'whitelist',
                    "qualifier": "qc_tag",
                    "first_seen_time": result.collection_time,
                    "latest_seen_time": result.collection_time,
                    "asset_info": {
                        "source_image_key": {
                            "gateway_id": result.gateway_id,
                            "camera_id": result.camera_id,
                            "collection_time": result.collection_time,
                            "step_name": result.step_name,
                            "name": result.images[0].split('_')[-1]
                        },
                        "source_image_link": result.images[0]
                    }
                })
                res = yield self.DB.asset_attribute.insert(self.cgr, None, [inclusion_record])

                # if old_plate is not None:
                #     delete_whitelist_sql = f'''delete from asset_attribute where asset_id = '{old_plate}' and attribute_value = 'whitelist';'''
                #     yield self.DB.asset_attribute.raw_query(self.cgr, delete_whitelist_sql)

            if mode == 'vehicle_type':
                vehicle_class_record = AD({
                    "asset_id": result.step_output.identifier,
                    "attribute_value": 'trailer',
                    "qualifier": "customer_vehicle_type",
                    "first_seen_time": result.collection_time,
                    "latest_seen_time": None,
                    "asset_info": {
                        "source_image_key": {
                            "gateway_id": result.gateway_id,
                            "camera_id": result.camera_id,
                            "collection_time": result.collection_time,
                            "step_name": result.step_name,
                            "name": result.images[0].split('_')[-1]
                        },
                        "source_image_link": result.images[0]
                    }
                })
                res = yield self.DB.asset_attribute.insert(self.cgr, None, [vehicle_class_record])

    @inlineCallbacks
    def update_image_step_annotation(self, image_step_id, fields):
        update_annotation = f'''
            UPDATE image_step_annotation isa
                SET applied = {image_step_id}
            WHERE  isa.gateway_id = '{fields.gateway_id}' and
                   isa.collection_time = '{fields.collection_time.isoformat()}' and
                   isa.step_name = '{fields.step_name}' and
                   isa.created_at_date = '{fields.created_at_date}'
            RETURNING isa.image_step_annotation_id;
        '''
        try:
            with CursorContext(dict_cursor=True, **self.dsn) as cur:
                yield deferToThread(cur.execute, update_annotation)
                res = yield deferToThread(cur.fetchall)
                logger.debug(f"PG_Event_Handler -{self.cgr} - update_image_step_annotation applied = {image_step_id}")
        except psycopg2.ProgrammingError:
            pass
        except Exception as err:
            logger.exception(f"PG_Event_Handler.update_image_step_annotation - {self.cgr} - exception for {image_step_id}: {err}")

    @inlineCallbacks
    def update_image_step_result(self, result):
        try:
            if not result or result.image_step_id == 40185767:
                return
            for f in ['filedata', 'filelist']:
                if f in result:
                    del result[f]
                if 'output' in result:
                    if f in result.output:
                        del result.output[f]
                if 'step_output' in result:
                    if f in result.step_output:
                        del result.step_output[f]
            # logger.info(f"PG_Event_Handler -{self.cgr} - update_image_step_result enumeration")
            # for k, v in result.items():
            #     logger.info(f"PG_Event_Handler -{self.cgr}: {k} = {type(v)}")
            if len(result.images) > 100:
                result.images = result.images[:100]
            if 'image_step_id' not in result:
                yield self.DB.image_step.insert(self.cgr, result.hdrs, [result])
            else:
                if 'images' not in result:
                    result.images = []
                update_sql = f"""
                    update image_step i
                    set step_output = '{result.step_output.jstr()}',
                        images = ARRAY[{",".join([f"'{i}'" for i in result.images if isinstance(i, str) ])}]::text[]
                    where i.image_step_id = {result.image_step_id}
                    RETURNING i.image_step_id;
                """
                with CursorContext(dict_cursor=True, **self.dsn) as cur:
                    yield deferToThread(
                                functools.partial(
                                    cur.execute,
                                    update_sql
                                )
                            )
                    res = yield deferToThread(cur.fetchall)
                    logger.debug(f"PG_Event_Handler -{self.cgr} - update_image_step_result: {res}")
        except psycopg2.ProgrammingError as err:
            logger.exception(f"PG_Event_Handler.update_image_step_result - {self.cgr} - query exception for {result.image_step_id}: {err}")
        except Exception as err:
            logger.exception(f"PG_Event_Handler.update_image_step_result - {self.cgr} - exception {err}")
