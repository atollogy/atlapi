
from atlapi.core import Core
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.consul_kvs import CAD_Cache
from atlapi.database.helpers import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import functools
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

class Configuration(Core):
    def __init__(self, version=None):
        Core.__init__(self, 'config', version=version)
        self.db = self.svcs.db
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = AD({
        })
        self.keyset = list(self.keys.keys())
        self.key_map = {}

    @inlineCallbacks
    def get_saferack_parameters(self, cgr, nodename, start_time="""now() - interval '1 month'"""):
        try:
            cage_parameters_sql = '''
                with bboxes as (
                    select  ((select array(select * from json_array_elements((
                                            select * from json_array_elements(((step_output -> 'bboxes')::json#> '{saferack_cage}')))))::text[])[:4]::int[]) bb,
                           gateway_id,
                           fn_uid_name(gateway_id) nodename,
                           image_step_id isid
                    from image_step
                    where gateway_id = fn_name_uid('{nodename}') and
                          collection_time >= {start_time} and
                          length((step_output -> 'bboxes' ->> 'saferack_cage')::text) > 5 and
                          length((step_output -> 'bboxes' ->> 'saferack_cage')::text) < 40
                    ),
                    points as (
                         select nodename,
                                bb[1]                                               x,
                                bb[2]                                               y,
                                bb[3]                                               w,
                                bb[4]                                               h,
                                (bb[1] + bb[3])                                     x2,
                                (bb[2] + bb[4])                                     y2,
                                array[bb[1], bb[2]]::int[]                          x1y1,
                                array[bb[1], (bb[2] + bb[4])]::int[]                x2y1,
                                array[(bb[1] + bb[3]), bb[2]]::int[]                x1y2,
                                array[(bb[1] + bb[3]), (bb[2] + bb[4])]::int[]      x2y2,
                                array[(bb[1] + (bb[3]- bb[1])/2), (bb[2] + (bb[4] - bb[2])/2)]::int[]  centroid,
                                (bb[3] * bb[4])                                     area
                         from bboxes
                        group by nodename, bb
                     )
                select nodename,
                       min(centroid)        min_centroid,
                       max(centroid)        max_centroid,
                       array[min(centroid), max(centroid)] cvector,
                       (((max(centroid))[2] - (min(centroid))[2]::float)/((max(centroid))[1] - (min(centroid))[1]::float)) cvector_slope,
                       degrees((atan((((max(centroid))[2] - (min(centroid))[2]::float)/((max(centroid))[1] - (min(centroid))[1]::float))))) cvector_angle,
                       min(x1y1)            min_x1y1,
                       max(x1y1)            max_x1y1,
                       min(x2y1)            min_x2y1,
                       max(x2y1)            max_x2y1,
                       min(x1y2)            min_x1y2,
                       max(x1y2)            max_x1y2,
                       min(x2y2)            min_x2y2,
                       max(x2y2)            max_x2y2,
                       min(area)            min_area,
                       max(area)            max_area
                from points
                group by nodename
                order by nodename;'''
            res = yield self.raw_query(cgr, cage_parameters_sql)
            return res
        except Exception as err:
            logger.exception(f"configuration.get_safecage_parameters {cgr} exception: {err}")
            raise

    # @inlineCallbacks
    # def insert(self, cgr, headers, recdata):
    #     # these are the keys we wanna update in-case a insert conflict occurs
    #     update_keys = []
    #     try:
    #         results = yield deferToThread(
    #                         functools.partial(
    #                             self.db.insert,
    #                             "configuration",
    #                             cgr,
    #                             headers,
    #                             self.keys,
    #                             recdata,
    #                             self.key_map,
    #                             update_keys=update_keys
    #                         )
    #             )
    #         return results
    #     except Exception as err:
    #         logger.exception(f"configuration.insert {cgr} exception: {err} - {recdata}")
    #         raise

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        dsn = self.db(cgr, ctype='dsn')
        with CursorContext(dict_cursor=True, **dsn) as cur:
            yield deferToThread(functools.partial(cur.execute, final_sql))
            res = yield deferToThread(cur.fetchall)
            res = [AD(r) for r in res]
            return res

    def unpack(self, data):
        return [AD({k: self.keys[k].otype(v) for k, v in dict(zip(self.keyset, r)).items()}) for r in data]
