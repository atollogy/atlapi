from atlapi.core import Core
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.database.helpers import *

from collections import namedtuple
from contextlib import contextmanager
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
import psycopg2
import psycopg2.pool
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.defer import inlineCallbacks, ensureDeferred


class Users_Table(Core):
    def __init__(self, db, version):
        Core.__init__(self, 'db_users_table')
        self.datadb = db
        self.version = version
        self.ckv = self.svcs.ckv
        self.dsn = AD(
                    dict(
                        host=BCM.endpoints.userdb.host,
                        port=5432,
                        user=BCM.endpoints.userdb.user,
                        password=BCM.endpoints.userdb.password,
                        dbname=BCM.env
                    )
                )
        self.db = PoolBoy('apdx', **self.dsn)

        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = {
            "username": self._column(lambda x: "{}".format(x), True, lambda x: x),
            "password": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "cgrs": self._column(lambda x: ','.join(list(x)), False, lambda x: x.split(',')),
            "company": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "first_name": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "last_name": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "phone_number": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "job_title": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "street_address": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "city": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "state": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "zip": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "reset_token_created": self._column(lambda x: to_datetime(x), False, lambda x: x),
            "reset_token": self._column(lambda x: str(x)[:100], False, lambda x: x),
            "permitted_facilities": self._column(lambda x: [str(i) for i in x], False, lambda x: x),
            "is_active": self._column(lambda x: bool(x), False, lambda x: x),
            "user_access": self._column(lambda x: json.dumps(x, default=str), False, lambda x: AD(x)),
            "alerts": self._column(lambda x: json.dumps(x, default=str), False, lambda x: [AD(d) for d in x if isinstance(d, dict)] if x else [])
        }
        self.keyset = list(self.keys.keys())
        self.key_map = AD()

    @inlineCallbacks
    def alert_recipients(self, cgr, facility, alert='safetyalert'):
        recipients = []
        recipient_sql = f'''
            select {', '.join([k for k in self.keyset if k not in ['password', 'cgrs']])}
            from users u
            where u.is_active and
                  u.cgrs like '%{cgr}%' and
                  array['{facility}'] && u.permitted_facilities;
            '''
        try:
            results = yield self.raw_query(recipient_sql)
            udata = self.unpack(results)
            recipients.extend([u.username for u in udata
                               if len([a for a in u.alerts
                                       if a.facility == facility and a.notification[alert]])
                        ])
            return recipients
        except Exception as err:
            logger.exception(f"Users_Table.alert_recipients exception: " + repr(err))
            raise

    @contextmanager
    def get_cursor(self, cgr):
        with PoolCursor(self.db) as cur:
            yield cur

    @inlineCallbacks
    def get_data(self, cgr, active=True):
        try:
            get_data_sql = f'''
            select {', '.join([k for k in self.keyset if k not in ['password', 'cgrs']])}
            from users u
            where u.is_active and
                  u.cgrs like '%{cgr}%' and
                  array['{facility}'] && u.permitted_facilities;
            '''
            if active:
                get_data_sql += ' and u.is_active;'
            else:
                get_data_sql += ';'
            results = yield self.raw_query(get_data_sql)
            return self.unpack(results)
        except Exception as err:
            logger.exception(f"Users_Table.get_data {cgr} exception: " + repr(err))
            raise

    @inlineCallbacks
    def raw_query(self, final_sql):
        if 'users' in final_sql:
            logger.debug(f'Querying Users_Table with raw_query: {final_sql}')
            try:
                with self.get_cursor() as cur:
                    yield deferToThread(cur.execute, final_sql)
                    results = yield deferToThread(cur.fetchall)
                    return results
            except psycopg2.ProgrammingError as err:
                logger.exception(f"Users_Table.raw_query exception: {err}- {final_sql}")
        else:
            raise DB_Exception(f'Attempt to query non-Users_Table_Table with raw_query: {final_sql}')

    def unpack(self, data):
        return [AD({k: self.keys[k].otype(v) for k, v in dict(zip(self.keyset, r)).items()}) for r in data]
