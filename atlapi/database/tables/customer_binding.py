
from atlapi.attribute_dict import *
from atlapi.common import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue


class Customer_Binding_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = AD({
            "namespace": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "beacon_id": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "start_time": self._column(lambda x: to_datetime(x), True, lambda x: to_datetime(x)),
            "end_time": self._column(lambda x: to_datetime(x) if x else x, False, lambda x: to_datetime(x) if x else x),
            "subject": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "subject_name": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "radius": self._column(lambda x: float(x) if x else x, False, lambda x: float(x) if x else x),
            "zone_name": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "facility": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "rssi_smoothing_window_slots": self._column(lambda x: int(x) if x else x, False, lambda x: int(x) if x else x),
            "beacon_slot_size_seconds": self._column(lambda x: int(x) if x else x, False, lambda x: int(x) if x else x)
        })
        self.keyset = list(self.keys.keys())
        self.key_map = {}

    @inlineCallbacks
    def get_data(self, cgr, period=None):
        try:
            get_data_sql = f'''
                select *
                from customer_binding
            '''
            if period:
                get_data_sql += ''' where start_time <= '{period.isoformat()}' and end_time < '{period.isoformat}';'''
            else:
                get_data_sql += ';'
            res = yield self.raw_query(cgr, get_data_sql)
            return res
        except Exception as err:
            logger.exception(f"Customer_Binding_Table.get_data {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        # these are the keys we wanna update in-case a insert conflict occurs
        update_keys = []
        try:
            results = yield deferToThread(
                            functools.partial(
                                self.db.insert,
                                "customer_binding",
                                cgr,
                                headers,
                                self.keys,
                                recdata,
                                self.key_map,
                                update_keys=update_keys
                            )
                )
            return results
        except Exception as err:
            logger.exception(f"Customer_Binding_Table.insert {cgr} exception: {err} - {recdata}")
            raise

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if 'customer_binding' in final_sql:
            # logger.debug(f'Querying customer_transition_event_binding with raw_query: {final_sql}')
            dsn = self.db(cgr, ctype='dsn')
            with CursorContext(dict_cursor=True, **dsn) as cur:
                yield deferToThread(functools.partial(cur.execute, final_sql))
                res = yield deferToThread(cur.fetchall)
                res = [AD(r) for r in res]
                return res
        else:
            raise DB_Exception(f'Customer_Binding_Table.raw_query {cgr} - Attempt to query non-Customer_Binding_Table with raw_query: {final_sql}')

    def unpack(self, data):
        return [AD({k: self.keys[k].otype(v) for k, v in dict(zip(self.keyset, r)).items()}) for r in data]
