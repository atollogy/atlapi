
from atlapi.attribute_dict import *
from atlapi.common import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.defer import inlineCallbacks, ensureDeferred


class Event_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = {
            "e_number": self._column(lambda x: int(x), True, int),
            "e_path": self._column(lambda x: "{}".format(x), True, str),
            "e_type": self._column(lambda x: "{}".format(x), True, str),
            "e_interval": self._column(lambda x: timedelta(seconds=int(x)), True, lambda x: timedelta(seconds=int(x))),
            "start_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True, to_datetime),
            "end_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True, to_datetime)
        }
        self.key_map = AD()
        self.fields = ('e_number', 'e_path', 'e_type', 'e_interval', 'start_time', 'end_time')

    @inlineCallbacks
    def create_event_number(self, cgr, nodename, gateway_id, step_name, step_function, collection_time, collection_interval):
        pass

    @inlineCallbacks
    def get_event_number(self, cgr, origin, step_name, step_function, collection_time, collection_interval):
        try:
            tables = yield self.db.list_tables(cgr)
            if 'event' not in tables:
                logger.debug(f'event table does not exist - insert failed')
                return None
            collection_time = to_datetime(collection_time)
            enumber = int(collection_time.timestamp()/int(wsize))
            prev_enumber = enumber - 1
            next_enumber = enumber + 1

            get_sql = f"""select *
                            from event
                            where source = '{source}' and
                                  etype = '{etype}' and
                                  name = '{name}' and
                                  wsize = '{int(wsize)}' and
                                  enumber between {prev_enumber} and {next_enumber};"""

            results = yield self.raw_query(cgr, get_sql)
            if len(results["failure"]):
                logger.debug(f'Failure doing event.get_event basic query {results["failure"]}')
                return False
            events = self.unpack(results['data'])
            if enumber in events and events[enumber].event_start <= collection_time:
                return events[enumber]
            elif prev_enumber in events:
                return events[prev_enumber]
            elif upsert:
                end_time = self.create_end_time(collection_time, wsize, units)
                recdata = AD()
                recdata.source = source
                recdata.etype = etype
                recdata.name = name
                recdata.enumber = int(enumber)
                recdata.wsize = int(wsize)
                recdata.event_start = collection_time
                recdata.event_end = end_time
                recdata.units = units
                recdata.selection = None
                try:
                    results = self.insert(cgr, None, recdata)
                    return recdata
                except Exception as err:
                    event = AD(recdata)
                    event.enumber = next_enumber
                    event.event_start = end_time
                    event.event_end = self.create_end_time(end_time, wsize, units)
                    results = self.insert(cgr, None, event)
                    return event
            else:
                return None
        except Exception as err:
            logger.exception("db.time_window_table.get_event exception: {}".format(err))
            return None


    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        tables = yield self.db.list_tables(cgr)
        if 'event' not in tables:
            logger.debug(f'event table does not exist - raw_query {final_sql} failed')
            return False
        if 'event' in final_sql:
            logger.debug(f'Querying event table  with raw_query: {final_sql}')
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results['failure']):
                logger.debug(f'Failure doing event raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'Attempt to query non event table with raw_query: {final_sql}')

    @inlineCallbacks
    def set_selection(self, cgr, source, selection, name=None, enumber=None, etype=None, event_table_id=None, wsize=None):
        if not selection:
            return False
        tables = yield self.db.list_tables(cgr)
        if 'event' not in tables:
            logger.debug(f'event table does not exist - set_selection failed')
            return False
        try:
            if event_table_id:
                set_sql = f"""update event
                                set selection = {int(selection)}
                                where event_table_id = '{event_table_id};"""
                results = self.raw_query(cgr, set_sql)
                if len(results["failure"]):
                    logger.debug(f'Failure doing event.set_selection {results["failure"]}')
                    return False
                else:
                    return True
            elif all([etype, name, enumber, wsize]):
                set_sql = f"""update event
                                set selection = {int(selection)}
                                where   source = '{source}' and
                                        event_table_id = {int(event_table_id)} and
                                        wtype = '{etype}' and
                                        wsize = {int(wsize)};"""
                results = yield self.raw_query(cgr, set_sql)
                if len(results['failure']):
                    logger.debug(f'Failure doing event.set_selection {results["failure"]}')
                    return False
                else:
                    return True
            else:
                logger.error("db.time_window_table.set_selection error - invalid parameters: " +
                             f"{cgr}, {selection}, {event_table_id}, {source}, {event_table_id}, {etype}, {wsize}")
                return False
        except Exception as err:
            logger.exception("db.time_window.set_selection exception: {}".format(err))
            return None

    def setup_table(self):
        table_sql = '''abs
            CREATE TABLE IF NOT EXISTS  event
            (
                event_id uuid default gen_random_uuid(),
                e_number bigint not null,
                e_path ltree not null,
                e_type ltree not null,
                e_interval interval not null,
                start_time timestamp with time zone,
                end_time timestamp with time zone,
                insert_time timestamp with time zone default now(),
                constraint event_pkey
                    primary key (e_number, e_path, e_type, e_interval),
                constraint event_check
                    check (end_time > start_time)
            );

            alter table event owner to atollogy;

            create index idx_e_path
                on event (e_path);

            create index idx_e_type
                on event (e_type);

            create index idx_validity
                on event (e_number, e_path, e_type, e_interval, start_time, end_time);'''

    def unpack(self, records):
        if records is None or not len(records):
            return AD()
        else:
            return AD({record[2]: dict(zip(self.fields, record)) for record in records})

    def upsert(self, recdata):
        sets = []
        for key in update_keys: #Construct UPSERT statement based on provided keys
            update_value = ''
            if key == "latest_seen_time":
                update_value = datetime.fromtimestamp(int(recdata[0][key]), tz=timezone.utc)
            elif key == "asset_info":
                update_value = json.dumps(recdata[0][key], default=str)
            sets.append("{} = '{}'".format(key, update_value))
        final_sql += " ON CONFLICT (asset_id, qualifier, attribute_value) DO UPDATE SET " + ', '.join(sets)
