
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.database.helpers import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue, Deferred


class Asset_Attribute_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = {
            "asset_id": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "attribute_value": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "qualifier": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "first_seen_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True, to_datetime),
            "latest_seen_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True, to_datetime),
            "asset_info": self._column(lambda x: json.dumps(x, default=str), False, lambda x: AD(json.loads(x)) if isinstance(x, str) else AD(x))
        }
        self.keyset = list(self.keys.keys())
        self.key_map = {"process_start": "last_state_change", "data": "asset_info"}

    @inlineCallbacks
    def add_identifier(self, cgr, asset_id, identifier):
        try:
            add_identifier_sql = f"""INSERT INTO asset_attribute (asset_id, qualifier, attribute_value)
                                          VALUES ('{asset_id}'::text, '{identifier}'::text, 'identifier'::text)
                                     ON CONFLICT DO NOTHING;
            """
            result = yield self.raw_query(cgr, add_identifier_sql)
        except Exception as err:
            logger.exception(f"Asset_Attribute_Table.add_identifier {cgr} exception :{err}")

    @inlineCallbacks
    def get_data(self, cgr):
        try:
            get_data_sql = f"select * from asset_attribute;"
            result = yield self.raw_query(cgr, get_data_sql)
            return result
        except Exception as err:
            logger.exception(f"Asset_Attribute_Table.insert {cgr} exception :{err}")
            raise

    @inlineCallbacks
    def get_exclusion_list(self, cgr):
        try:
            exclusion_sql = f"""
                select array_agg(asset_id) exclusion_list
                from asset_attribute
                where attribute_value = 'blacklist';
            """
            result = yield self.raw_query(cgr, exclusion_sql)
            if isinstance(result, list) and len(result):
                return result[0].exclusion_list
            else:
                return []
        except Exception as err:
            logger.exception(f"Asset_Attribute_Table.get_exclusion_list {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def insert(self, cgr, headers, recdata, update_keys=[]):
        try:
            results = yield self.db.insert(
                            "asset_attribute",
                            cgr,
                            headers,
                            self.keys,
                            recdata,
                            self.key_map,
                            update_keys=update_keys
                        )
            return results
        except Exception as err:
            logger.exception(f"Asset_Attribute_Table.insert {cgr} exception: {err} - {recdata}")
            raise

    @inlineCallbacks
    def is_blocked(self, cgr, candidate):
        try:
            is_blocked_sql = f"""
                SELECT BOOL_OR('{candidate}' like asset_id) AS blocked
                  FROM asset_attribute
                 WHERE attribute_value = 'blacklist';
            """
            is_blocked_result = yield self.raw_query(cgr, is_blocked_sql)
            if isinstance(is_blocked_result, list) and len(is_blocked_result):
                return is_blocked_result[0].blocked
            else:
                return None
        except Exception as err:
            logger.exception(f"Asset_Attribute_Table.is_blocked {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def is_identifier(self, cgr, candidate):
        try:
            is_identifier_sql = f"""
                select attribute_value
                from asset_attribute
                where attribute_value = '{candidate}'
                  and qualifier = 'identifier';
            """
            is_identifier_result = yield self.raw_query(cgr, is_identifier_sql)
            if isinstance(is_identifier_result, list) and len(is_identifier_result):
                return is_identifier_result[0].attribute_value
            else:
                return None
        except Exception as err:
            logger.exception(f"Asset_Attribute_Table.is_identifier {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def has_identifier(self, cgr, candidate):
        try:
            has_identifier_sql = f"""
                select attribute_value
                  from asset_attribute
                 where asset_id = '{candidate}'
                   and qualifier = 'identifier'
                 limit 1;
            """
            has_identifier_result = yield self.raw_query(cgr, has_identifier_sql)
            if isinstance(has_identifier_result, list) and len(has_identifier_result):
                return has_identifier_result[0].attribute_value
            else:
                return None
        except Exception as err:
            logger.exception(f"Asset_Attribute_Table.has_identifier {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        try:
            if 'asset_attribute' in final_sql.lower():
                dsn = self.db(cgr, ctype='dsn')
                with CursorContext(dict_cursor=True, **dsn) as cur:
                    yield deferToThread(functools.partial(cur.execute, final_sql))
                    if 'insert' not in final_sql.lower():
                        raw_result = yield cur.fetchall()
                        result = [AD(d) for d in raw_result]
                        return result
                    else:
                        return True
            else:
                raise DB_Exception(f'Asset_Attribute_Table.raw_query - querying non-asset_attribute table not permitted',
                                   payload=final_sql)
        except Exception as err:
            logger.exception(f"Asset_Attribute_Table.raw_query {cgr} exception: {err}")
            raise
