
from atlapi.attribute_dict import *
from atlapi.common import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import functools
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread, deferToThreadPool
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue, Deferred

### image step table access class
class Image_Step_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = AD({
            "gateway_id": self._column(lambda x: "{}".format(x), True, lambda x: x),
            "camera_id": self._column(lambda x: "{}".format(x), True, lambda x: x),
            "collection_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True, to_datetime),
            "step_name": self._column(lambda x: "{}".format(x), True, lambda x: x),
            "step_function": self._column(lambda x: "{}".format(x), True, lambda x: x),
            "step_function_version": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "step_config_version": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "step_output": self._column(lambda x: "{}".format(json.dumps(x, default=str)), False, lambda x: AD(json.loads(x)) if isinstance(x, str) else AD(x)),
            "step_start_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), False, to_datetime),
            "step_end_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), False, to_datetime),
            "processor_role": self._column(lambda x: "{}".format(x), False, lambda x: x),
            "collection_interval": self._column(lambda x: x if isinstance(x, timedelta) else timedelta(seconds=int(x)) if int(x) < 5000 else timedelta(milliseconds=int(x)), False, lambda x: x),
            "images": self._column(lambda x: list(x), False, lambda x: x)
        })

        self.keyset = ['image_step_id', 'gateway_id', 'camera_id', 'collection_time', 'collection_interval',
                       'step_name', 'step_function', 'step_function_version', 'step_config_version',
                       'step_output', 'step_start_time', 'step_end_time', 'processor_role', 'images', 'insert_time', 'ocid']

        self.key_map = AD({
            "collectionInterval": "collection_interval",
            "config_version": "step_config_version",
            "configVersion": "step_config_version",
            "endTime": "step_end_time",
            "end_time": "step_end_time",
            "function": "step_function",
            "function_version": "step_function_version",
            "functionVersion": "step_function_version",
            "output": "step_output",
            "startTime": "step_start_time",
            "start_time": "step_start_time",
            "version": "step_function_version",
        })
        self.seen = AD()

    @inlineCallbacks
    def correlate(self, cgr, sources, fnames, start_time, end_time, selector=None):
        gather_sql = f'''
        select *
        from image_step i
        where i.gateway_id in ({','.join([f"'{s}'" for s in sources if s])}) and
              i.step_function in ({','.join([f"'{f}'" for f in fnames if f])}) and
              i.collection_time between '{to_datetime(start_time).isoformat()}' and '{to_datetime(end_time).isoformat()}';
        '''
        result = yield self.raw_query(cgr, gather_sql)
        data = self.unpack(result.data)
        if selector:
            result = selector(data)
            return result
        else:
            return data

    @exec_timed
    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        # logger.debug('inserting into image_step: {}'.format(recdata))
        # logger.info(f"{type(cgr)} - {type(headers)} - {type(recdata)}")

        if 'image_step_pkey' in recdata:
            logger.error(f"IMAGE_STEP PKEY COLLISION: {recdata.image_step_pkey}")
            del recdata.image_step_pkey
        try:
            results = yield self.db.insert(
                    "image_step",
                    cgr,
                    headers,
                    self.keys,
                    recdata,
                    self.key_map
                )
            # logger.info(f"db.Image_Step_Table db.insert result: " + str(results))

            return results
        except Exception as err:
            logger.error(f'db.Image_Step_Table db.insert error {err}')
        # logger.debug("DB image_step insert results: {}".format(results))
            raise

    @exec_timed
    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if 'image_step' in final_sql:
            # logger.debug(f'Querying image_step_table with raw_query: {final_sql}')
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results['failure']):
                logger.debug(f'Failure doing image_step raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'Attempt to query non-image_step_table with raw_query: {final_sql}')

    def unpack(self, data):
        return [AD({k: self.keys[k].otype(v) if k in self.keys else v for k, v in dict(zip(self.keyset, r)).items()}) for r in data]

    # @inlineCallbacks
    # def update(self, cgr, image_step_id, data):
    #     update_sql = f'''update {sql.Identifier('image_step')} set '''
    #     for k, v in data.items():
    #         if k in self.keys:
    #             update_sql += f'''{k} =
    #                         set step_output = '{result.step_output.jstr()}',
    #                     images = ''' + "'{"
    #     sql += str(result.images)[1:-1]
    #     sql += '''}' where i.image_step_id = {result.image_step_id};'''
    #     cur.mogrify(sql_dict["tmpl"].as_string(cur), row).decode("utf-8")
    #     results = yield self.db.insert(
    #         "image_step", cgr, headers, self.keys, recdata, self.key_map
    #     )
    #     # logger.debug("DB image_step insert results: {}".format(results))
    #     return results
