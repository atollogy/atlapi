
from atlapi.attribute_dict import *
from atlapi.common import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import functools
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

class Customer_Report_Config_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = AD({
            "subject_name": self._column(lambda x: "{}".format(x), True,lambda x: "{}".format(x)),
            "measure_name": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "facility": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "gateway_name": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "camera_id": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "step_name": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "extract_type": self._column(lambda x: "{}".format(x), True, lambda x: "{}".format(x)),
            "extract_key": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "extract_image_name": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "cs_config": self._column(lambda x: json.dumps(x, default=str), False, lambda x: AD(json.loads(x)) if isinstance(x, str) else AD(x)),
            "composite_definition": self._column(lambda x: json.dumps(x, default=str), False,  lambda x: AD(json.loads(x)) if isinstance(x, str) else AD(x)),
            "hidden": self._column(lambda x: True if x else False, False, lambda x: bool(x)),
            "start_time": self._column(lambda x: to_datetime(x) if x else x, True, lambda x: to_datetime(x)),
            "end_time": self._column(lambda x: to_datetime(x) if x else x, False, lambda x: to_datetime(x) if x else x),
            "smoothing_look_ahead_slots": self._column(lambda x: int(x) if x else x, False, lambda x: int(x) if x else x),
            "smoothing_look_back_slots": self._column(lambda x: int(x) if x else x, False, lambda x: int(x) if x else x),
            "secondary_extract_key": self._column(lambda x: "{}".format(x), False, lambda x: "{}".format(x)),
            "extract_confidence": self._column(lambda x: float(x) if x else x, False, lambda x: float(x) if x else x),
            "extract_threshold_definitions": self._column(lambda x: json.dumps(x, default=str), False,  lambda x: AD(json.loads(x)) if isinstance(x, str) else AD(x)),
            "violation": self._column(lambda x: True if x else False, False, lambda x: bool(x)),
            "priority": self._column(lambda x: int(x) if x else x, False, lambda x: int(x) if x else x),
            "missing_data_enabled": self._column(lambda x: True if x else False, False, lambda x: bool(x))
        })
        self.keyset = list(self.keys.keys())
        self.key_map = {}

    @inlineCallbacks
    def get_data(self, cgr, period=None):
        try:
            get_data_sql = f'''
                select {', '.join(self.keyset)}
                from customer_report_config
            '''
            if period:
                get_data_sql += ''' where start_time <= '{period.isoformat()}' and end_time < '{period.isoformat}';'''
            else:
                get_data_sql += ';'
            res = yield self.raw_query(cgr, get_data_sql)
            return self.unpack(res.data)
        except Exception as err:
            logger.exception(f"Customer_Report_Config_Table.get_data {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def get_facilities(self, cgr, gwid, period=None):
        try:
            get_data_sql = f'''
                select crc.facility,
                       crc.step_name,
                       crc.subject_name
                from customer_binding cb
                         inner join customer_report_config crc on cb.subject_name = crc.gateway_name
                where cb.beacon_id = '{gwid}'
            '''
            if period:
                get_data_sql += ''' and start_time <= '{period.isoformat()}' and end_time < '{period.isoformat}';'''
            else:
                get_data_sql += ' and crc.end_time is null;'
            res = yield self.raw_query(cgr, get_data_sql)
            facilities = AD({t[0]: {'facility_name': t[0], 'step_name': t[1], 'subject_name': t[2]} for t in res.data})
            return facilities
        except Exception as err:
            logger.exception(f"Customer_Report_Config_Table.get_data {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        # logger.debug('inserting into customer_item_attribute: {}'.format(recdata))
        # these are the keys we wanna update in-case a insert conflict occurs
        update_keys = []
        try:
            results = yield deferToThread(self.db.insert,
                *("customer_report_config", cgr, headers, self.keys, recdata, self.key_map), **dict(update_keys=update_keys)
                )
            return results
        except Exception as err:
            logger.exception(f"Customer_Report_Config_Table.insert {cgr} exception: {err} - {recdata}")
            raise

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if 'customer_report_config' in final_sql:
            # logger.debug(f'Querying customer_report_config with raw_query: {final_sql}')
            results = yield deferToThread(
                            functools.partial(
                                self.db.raw_query,
                                cgr,
                                final_sql
                            )
                        )
            if len(results['failure']):
                logger.debug(f'Customer_Report_Config_Table.raw_query {cgr} - Query failure: {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'Customer_Report_Config_Table.raw_query {cgr} - Attempt to query non-Customer_Report_Config_Table with raw_query: {final_sql}')

    def unpack(self, data):
        return [AD({k: self.keys[k].otype(v) for k, v in dict(zip(self.keyset, r)).items()}) for r in data]
