
from atlapi.attribute_dict import *
from atlapi.common import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.defer import inlineCallbacks, ensureDeferred


class Resource_Types(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull"])
        self.keys = {
            "uuid": self._column(lambda x: int(x), True),
            "namespace": self._column(lambda x: "{}".format(x), True),
            "schema_format": self._column(lambda x: "{}".format(x), True),
            "schema_standard": self._column(lambda x: "{}".format(x), True),
            "schema_template": self._column(lambda x: json.dumps(x), True),
            "schema_validator": self._column(lambda x: json.dumps(x), True),
            "locator": self._column(lambda x: "{}".format(x), True),
            "metadata": self._column(lambda x: json.dumps(x), False),
            "semver": self._column(lambda x: "{}".format(x), True),
            "valid_from": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True),
            "valid_from": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True)
        }
        self.key_map = AD({
        })

        self.fields = ('uuid', 'namespace', 'schema_format', 'schema_standard', 'schema_template',
                       'schema_validator', 'locator','metadata', 'version')

    @inlineCallbacks
    def bind(self, cgr, rec_id, source, source_id, event, etype, collection_time):
        try:
            if not self.db.table_exists(cgr, 'events'):
                logger.debug(f'db.events table for {cgr} does not exist - failed to execute: {final_sql}')
                return

            event_tnumber = event = None
            tnumber = to_tnum(collection_time, wsize)
            prev_tnumber = tnumber - 1
            next_tnumber = tnumber + 1
            etime = to_datetime(collection_time).timestamp()
            estart = tnumber * wsize
            estop = estart + wsize

            get_sql = f"""select e.*
                          from events e
                          where e.tnumber in ({prev_tnumber}, {tnumber}, {next_tnumber})
                          order by e.tnumber"""

            results = yield self.raw_query(cgr, get_sql)
            events = self.unpack_as_dict(results['data'], 'tnumber')
            for etnum, erec in events.items():
                if erec.event_start <= estart and erec.event_stop < estop:
                    event_tnum = etum
                    event = erec
                    break
            if tnumber in events and events[tnumber].event_start >= etime:
                return event
            elif prev_tnumber in events and events[prev_tnumber].event_stop >= etime:
                return events[prev_tnumber]
            else:
                event = AD()
                event.source = source
                event.etype = etype
                event.name = name
                event.tnumber = tnumber
                event.wsize = wsize
                event.event_start = estart
                event.event_stop = estop
                event.units = units
                event.selection = None
                next_event = AD(event)
                next_event.event_start = estop
                next_event.event_stop = estop + wsize
                try:
                    results = yield self.insert(cgr, None, [event, next_event])
                except Exception as err:
                    return None
                return event
        except Exception as err:
            logger.exception("db.time_window_table.get_event exception: {}".format(err))
            return None

    @inlineCallbacks
    def create_resource(self, cgr, headers, recdata):
        pass

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        sql_dict = self.db.build_insert_sql('events', self.keys, recdata, key_map)

        tables = yield self.db.list_tables(cgr)
        if 'event' not in tables:
            logger.debug(f'event when table does not exist - insert failed')
            return False
        results = yield self.db.insert("event", cgr, headers, self.keys, recdata, self.key_map)
        if len(results['failure']):
            logger.debug(f'Failure doing event insert {results["failure"]}')
            return False
        else:
            return True

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if not self.db.table_exists(cgr, 'events'):
            logger.debug(f'db.events table for {cgr} does not exist - failed to execute: {final_sql}')
            return
        if 'event' in final_sql:
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results['failure']):
                logger.error(f'db.events failure doing event raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'db.events attempt to query non event table with raw_query: {final_sql}')

    def unpack(self, records):
        if records is None or not len(records):
            return AD()
        else:
            return AD({record[2]: dict(zip(self.fields, record)) for record in records})


class Resource_Domains(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull"])
        self.keys = {
            "uuid": self._column(lambda x: int(x), True),
            "schema_path": self._column(lambda x: "{}".format(x), True),
            "schema_type": self._column(lambda x: "{}".format(x), True),
            "schema_standard": self._column(lambda x: "{}".format(x), True),
            "schema_template": self._column(lambda x: json.dumps(x), True),
            "schema_validator": self._column(lambda x: json.dumps(x), True),
            "locator": self._column(lambda x: "{}".format(x), True),
            "metadata": self._column(lambda x: json.dumps(x), False),
            "version": self._column(lambda x: datetime.utcnow().isoformat(timespec='microseconds'), True)
        }
        self.key_map = AD({
        })

        self.fields = ('uuid', 'schema_path', 'schema_type', 'schema_standard', 'schema_template',
                       'schema_validator', 'locator','metadata', 'version')

    @inlineCallbacks
    def bind(self, cgr, rec_id, source, source_id, event, etype, collection_time):
        try:
            if not self.db.table_exists(cgr, 'events'):
                logger.debug(f'db.events table for {cgr} does not exist - failed to execute: {final_sql}')
                return

            event_tnumber = event = None
            tnumber = to_tnum(collection_time, wsize)
            prev_tnumber = tnumber - 1
            next_tnumber = tnumber + 1
            etime = to_datetime(collection_time).timestamp()
            estart = tnumber * wsize
            estop = estart + wsize

            get_sql = f"""select e.*
                          from events e
                          where e.tnumber in ({prev_tnumber}, {tnumber}, {next_tnumber})
                          order by e.tnumber"""

            results = yield self.raw_query(cgr, get_sql)
            events = self.unpack_as_dict(results['data'], 'tnumber')
            for etnum, erec in events.items():
                if erec.event_start <= estart and erec.event_stop < estop:
                    event_tnum = etum
                    event = erec
                    break
            if tnumber in events and events[tnumber].event_start >= etime:
                return event
            elif prev_tnumber in events and events[prev_tnumber].event_stop >= etime:
                return events[prev_tnumber]
            else:
                event = AD()
                event.source = source
                event.etype = etype
                event.name = name
                event.tnumber = tnumber
                event.wsize = wsize
                event.event_start = estart
                event.event_stop = estop
                event.units = units
                event.selection = None
                next_event = AD(event)
                next_event.event_start = estop
                next_event.event_stop = estop + wsize
                try:
                    results = yield self.insert(cgr, None, [event, next_event])
                except Exception as err:
                    return None
                return event
        except Exception as err:
            logger.exception("db.time_window_table.get_event exception: {}".format(err))
            return None

    @inlineCallbacks
    def create_resource(self, cgr, headers, recdata):
        pass

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        sql_dict = self.db.build_insert_sql('events', self.keys, recdata, key_map)

        tables = yield self.db.list_tables(cgr)
        if 'event' not in tables:
            logger.debug(f'event when table does not exist - insert failed')
            return False
        results = yield self.db.insert("event", cgr, headers, self.keys, recdata, self.key_map)
        if len(results['failure']):
            logger.debug(f'Failure doing event insert {results["failure"]}')
            return False
        else:
            return True

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if not self.db.table_exists(cgr, 'events'):
            logger.debug(f'db.events table for {cgr} does not exist - failed to execute: {final_sql}')
            return
        if 'event' in final_sql:
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results['failure']):
                logger.error(f'db.events failure doing event raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'db.events attempt to query non event table with raw_query: {final_sql}')

    def unpack(self, records):
        if records is None or not len(records):
            return AD()
        else:
            return AD({record[2]: dict(zip(self.fields, record)) for record in records})


class Resources(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull"])
        self.keys = {
            "obx_id": self._column(lambda x: int(x), True),
            "resource_path": self._column(lambda x: "{}".format(x), True),
            "resource_uuid": self._column(lambda x: "{}".format(x), True),
            "observer_uuid": self._column(lambda x: "{}".format(x), True),
            "wf_uuid": self._column(lambda x: "{}".format(x), True),
            "obx_type": self._column(lambda x: "{}".format(x), True),
            "obx_timestamp": self._column(lambda x: to_datetime(x).timestamp(), True),
            "obx_status": self._column(lambda x: "{}".format(x), True),
            "obx_record_format": self._column(lambda x: "{}".format(x), True),
            "obx_record": self._column(lambda x: json.dumps(x), True),
            "obx_selecion": self._column(lambda x: int(x), False),
            "observatory": self._column(lambda x: int(x), False),
            "metadata": self._column(lambda x: json.dumps(x), False),
            "selection": self._column(lambda x: int(x), False),
            "version": self._column(lambda x: to_datetime(x), True)
        }
        self.key_map = AD({
        })

        self.fields = ('obx_id', 'resource_path', 'resource_uuid', 'observer_uuid', 'wf_uuid', 'obx_type', 'obx_timestamp',
                       'obx_status', 'obx_record_format', 'obx_record', 'obx_selection', 'observatory', 'metadata', 'version')

    @inlineCallbacks
    def bind(self, cgr, rec_id, source, source_id, event, etype, collection_time):
        try:
            if not self.db.table_exists(cgr, 'events'):
                logger.debug(f'db.events table for {cgr} does not exist - failed to execute: {final_sql}')
                return

            event_tnumber = event = None
            tnumber = to_tnum(collection_time, wsize)
            prev_tnumber = tnumber - 1
            next_tnumber = tnumber + 1
            etime = to_datetime(collection_time).timestamp()
            estart = tnumber * wsize
            estop = estart + wsize

            get_sql = f"""select e.*
                          from events e
                          where e.tnumber in ({prev_tnumber}, {tnumber}, {next_tnumber})
                          order by e.tnumber"""

            results = yield self.raw_query(cgr, get_sql)
            events = self.unpack_as_dict(results['data'], 'tnumber')
            for etnum, erec in events.items():
                if erec.event_start <= estart and erec.event_stop < estop:
                    event_tnum = etum
                    event = erec
                    break
            if tnumber in events and events[tnumber].event_start >= etime:
                return event
            elif prev_tnumber in events and events[prev_tnumber].event_stop >= etime:
                return events[prev_tnumber]
            else:
                event = AD()
                event.source = source
                event.etype = etype
                event.name = name
                event.tnumber = tnumber
                event.wsize = wsize
                event.event_start = estart
                event.event_stop = estop
                event.units = units
                event.selection = None
                next_event = AD(event)
                next_event.event_start = estop
                next_event.event_stop = estop + wsize
                try:
                    results = yield self.insert(cgr, None, [event, next_event])
                except Exception as err:
                    return None
                return event
        except Exception as err:
            logger.exception("db.time_window_table.get_event exception: {}".format(err))
            return None

    @inlineCallbacks
    def create_resource(self, cgr, headers, recdata):
        pass


    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        sql_dict = self.db.build_insert_sql('events', self.keys, recdata, key_map)

        tables = yield self.db.list_tables(cgr)
        if 'event' not in tables:
            logger.debug(f'event when table does not exist - insert failed')
            return False
        results = yield self.db.insert("event", cgr, headers, self.keys, recdata, self.key_map)
        if len(results['failure']):
            logger.debug(f'Failure doing event insert {results["failure"]}')
            return False
        else:
            return True

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if not self.db.table_exists(cgr, 'events'):
            logger.debug(f'db.events table for {cgr} does not exist - failed to execute: {final_sql}')
            return
        if 'event' in final_sql:
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results['failure']):
                logger.error(f'db.events failure doing event raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'db.events attempt to query non event table with raw_query: {final_sql}')

    @inlineCallbacks
    def set_selection(self, cgr, source, selection, name=None, enumber=None, etype=None, event_table_id=None, wsize=None):
        if not selection:
            return False
        if not self.db.table_exists(cgr, 'events'):
            logger.debug(f'event table does not exist - set_selection failed')
            return False
        try:
            if event_table_id:
                set_sql = f"""update event
                                set selection = {int(selection)}
                                where event_table_id = '{event_table_id};"""
                results = yield self.raw_query(cgr, set_sql)
                if len(results["failure"]):
                    logger.debug(f'Failure doing event.set_selection {results["failure"]}')
                    return False
                else:
                    return True
            elif all([etype, name, enumber, wsize]):
                set_sql = f"""update event
                                set selection = {int(selection)}
                                where   source = '{source}' and
                                        event_table_id = {int(event_table_id)} and
                                        wtype = '{etype}' and
                                        wsize = {int(wsize)};"""
                results = yield self.raw_query(cgr, set_sql)
                if len(results['failure']):
                    logger.debug(f'Failure doing event.set_selection {results["failure"]}')
                    return False
                else:
                    return True
            else:
                logger.error("db.time_window_table.set_selection error - invalid parameters: " +
                             f"{cgr}, {selection}, {event_table_id}, {source}, {event_table_id}, {etype}, {wsize}")
                return False
        except Exception as err:
            logger.exception("db.time_window.set_selection exception: {}".format(err))
            return None

    def unpack(self, records):
        if records is None or not len(records):
            return AD()
        else:
            return AD({record[2]: dict(zip(self.fields, record)) for record in records})


class Registry(Core):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull"])
        self.keys = {
            "obx_id": self._column(lambda x: int(x), True),
            "resource_path": self._column(lambda x: "{}".format(x), True),
            "resource_uuid": self._column(lambda x: "{}".format(x), True),
            "observer_uuid": self._column(lambda x: "{}".format(x), True),
            "wf_uuid": self._column(lambda x: "{}".format(x), True),
            "obx_type": self._column(lambda x: "{}".format(x), True),
            "obx_timestamp": self._column(lambda x: to_datetime(x).timestamp(), True),
            "obx_status": self._column(lambda x: "{}".format(x), True),
            "obx_record_format": self._column(lambda x: "{}".format(x), True),
            "obx_record": self._column(lambda x: json.dumps(x), True),
            "obx_selecion": self._column(lambda x: int(x), False),
            "observatory": self._column(lambda x: int(x), False),
            "metadata": self._column(lambda x: json.dumps(x), False),
            "selection": self._column(lambda x: int(x), False),
            "version": self._column(lambda x: to_datetime(x), True)
        }
        self.key_map = AD({
        })

        self.fields = ('obx_id', 'resource_path', 'resource_uuid', 'observer_uuid', 'wf_uuid', 'obx_type', 'obx_timestamp',
                       'obx_status', 'obx_record_format', 'obx_record', 'obx_selection', 'observatory', 'metadata', 'version')

    @inlineCallbacks
    def bind(self, cgr, rec_id, source, source_id, event, etype, collection_time):
        try:
            if not self.db.table_exists(cgr, 'events'):
                logger.debug(f'db.events table for {cgr} does not exist - failed to execute: {final_sql}')
                return

            event_tnumber = event = None
            tnumber = to_tnum(collection_time, wsize)
            prev_tnumber = tnumber - 1
            next_tnumber = tnumber + 1
            etime = to_datetime(collection_time).timestamp()
            estart = tnumber * wsize
            estop = estart + wsize

            get_sql = f"""select e.*
                          from events e
                          where e.tnumber in ({prev_tnumber}, {tnumber}, {next_tnumber})
                          order by e.tnumber"""

            results = yield self.raw_query(cgr, get_sql)
            events = self.unpack_as_dict(results['data'], 'tnumber')
            for etnum, erec in events.items():
                if erec.event_start <= estart and erec.event_stop < estop:
                    event_tnum = etum
                    event = erec
                    break
            if tnumber in events and events[tnumber].event_start >= etime:
                return event
            elif prev_tnumber in events and events[prev_tnumber].event_stop >= etime:
                return events[prev_tnumber]
            else:
                event = AD()
                event.source = source
                event.etype = etype
                event.name = name
                event.tnumber = tnumber
                event.wsize = wsize
                event.event_start = estart
                event.event_stop = estop
                event.units = units
                event.selection = None
                next_event = AD(event)
                next_event.event_start = estop
                next_event.event_stop = estop + wsize
                try:
                    results = yield self.insert(cgr, None, [event, next_event])
                except Exception as err:
                    return None
                return event
        except Exception as err:
            logger.exception("db.time_window_table.get_event exception: {}".format(err))
            return None

    @inlineCallbacks
    def create_resource(self, cgr, headers, recdata):
        pass


    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        sql_dict = self.db.build_insert_sql('events', self.keys, recdata, key_map)

        tables = yield self.db.list_tables(cgr)
        if 'event' not in tables:
            logger.debug(f'event when table does not exist - insert failed')
            return False
        results = yield self.db.insert("event", cgr, headers, self.keys, recdata, self.key_map)
        if len(results['failure']):
            logger.debug(f'Failure doing event insert {results["failure"]}')
            return False
        else:
            return True

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if not self.db.table_exists(cgr, 'events'):
            logger.debug(f'db.events table for {cgr} does not exist - failed to execute: {final_sql}')
            return
        if 'event' in final_sql:
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results['failure']):
                logger.error(f'db.events failure doing event raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'db.events attempt to query non event table with raw_query: {final_sql}')

    @inlineCallbacks
    def set_selection(self, cgr, source, selection, name=None, enumber=None, etype=None, event_table_id=None, wsize=None):
        if not selection:
            return False
        if not self.db.table_exists(cgr, 'events'):
            logger.debug(f'event table does not exist - set_selection failed')
            return False
        try:
            if event_table_id:
                set_sql = f"""update event
                                set selection = {int(selection)}
                                where event_table_id = '{event_table_id};"""
                results = yield self.raw_query(cgr, set_sql)
                if len(results["failure"]):
                    logger.debug(f'Failure doing event.set_selection {results["failure"]}')
                    return False
                else:
                    return True
            elif all([etype, name, enumber, wsize]):
                set_sql = f"""update event
                                set selection = {int(selection)}
                                where   source = '{source}' and
                                        event_table_id = {int(event_table_id)} and
                                        wtype = '{etype}' and
                                        wsize = {int(wsize)};"""
                results = yield self.raw_query(cgr, set_sql)
                if len(results['failure']):
                    logger.debug(f'Failure doing event.set_selection {results["failure"]}')
                    return False
                else:
                    return True
            else:
                logger.error("db.time_window_table.set_selection error - invalid parameters: " +
                             f"{cgr}, {selection}, {event_table_id}, {source}, {event_table_id}, {etype}, {wsize}")
                return False
        except Exception as err:
            logger.exception("db.time_window.set_selection exception: {}".format(err))
            return None

    def unpack(self, records):
        if records is None or not len(records):
            return AD()
        else:
            return AD({record[2]: dict(zip(self.fields, record)) for record in records})


    # def create_context(self, cgr):
    #     pass
    #
    # def assign_context(self, cgr):
    #     pass

    def bind_resource(self, origin, path):
        pass

    def create_entity(self, cgr, namespace, path, e_type):
        pass

    def lookup_by_id(self, key, res_path):
        pass

    def lookup_by_path(self, key, res_path):
        pass

    def lookup_by_type(self, key, res_path):
        pass
