
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.consul_kvs import *
from atlapi.database.helpers import *
from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from psycopg2 import sql
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

class Resolver_Table(object):
    def __init__(self, db, version):
        self.name = 'resolver'
        self.db = db
        self.version = version
        self.cache = AD()
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = {
            "uid": self._column(str, True, str),
            "dname": self._column(str, True, str),
            "sensor": self._column(str, True, str),
            "sname": self._column(str, True, str),
            "ftype": self._column(str, True, str),
            "epath": self._column(str, False, str),
            "etype": self._column(str, False, str),
            "hostname": self._column(str, False, str),
            "cgr": self._column(str, False, str),
            "env": self._column(str, False, str),
            "fqdn": self._column(str, False, str),
            "label": self._column(str, False, str),
            "label_2": self._column(str, False, str),
            "label_3": self._column(str, False, str),
            "alt_label_1": self._column(str, False, str),
            "alt_label_2": self._column(str, False, str),
            "valid_from": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True, to_datetime),
            "valid_until": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), False, to_datetime),
            "updated": self._column(lambda x: datetime.utcnow().isoformat(timespec='microseconds'), True, to_datetime)
        }
        self.key_map = {
        }
        self.keyset = ('uid', 'dname', 'sensor', 'sname', 'ftype',
                       'epath', 'etype',
                       'hostname', 'cgr', 'env', 'fqdn'
                       'label', 'label_2', 'label_3',
                       'atl_label_1', 'atl_label_2',
                       'valid_from', 'valid_until', 'updated')

    @inlineCallbacks
    def exists(self, cgr):
        tables = yield self.db.list_tables(cgr)
        if 'resolver' not in tables:
            logger.debug(f'db.resolver.exists -  table does not exist: {tables} - calling create table')
            yield self.setup_table(cgr)
            return True
        else:
            return True

    @inlineCallbacks
    def get_uid(self, cgr, nodename):
        get_uid_sql = """
            SELECT fn_name_uid('{nodename'}');
        """
        try:
            yield self.exists(cgr)
            res = yield self.raw_query(cgr, get_uid_sql)
            if len(res):
                return res[0]
            else:
                return None
        except Exception as err:
            raise DBError(f"db.resolver.get_uid exception cgr: {cgr} - {err}")

    @inlineCallbacks
    def get_nodename(self, cgr, gateway_id):
        get_nodename_sql = """
            SELECT fn_uid_name('{gateway_id'}');
        """
        try:
            yield self.exists(cgr)
            res = yield  self.raw_query(cgr, get_nodename_sql)
            if len(res):
                return res[0]
            else:
                return None
        except Exception as err:
            raise DBError(f"db.resolver.get_nodename exception cgr: {cgr} - {err}")


    @inlineCallbacks
    # def insert(self, cgr, gwid, nodename, headers={}, valid_from=None, valid_until=None):
    #     resolver_insert_sql = f"""
    #         INSERT INTO resolver (gwid, nodename, scode, host, cgr_path, fqdn, valid_from)
    #             VALUES (%s, %s, %s, %s, %s, %s, %s)
    #             RETURNING resolver_id;
    #     """
    #     self.exists(cgr)
    #     try:
    #         fqdnd = parse_fqdn(nodename)
    #         if valid_from is None:
    #             valid_from = datetime.utcnow()
    #         fqdnd.valid_from = valid_from
    #         current = self.get_valid(cgr, nodename)
    #         if current:
    #             if valid_until is None:
    #                 valid_until = datetime.utcnow()
    #             self.make_invalid(cgr, gwid, nodename, resolver_id=current.resolver_id, valid_until=valid_until)
    #         with self.db.get_cursor(self.cgr) as cursor:
    #             insert_sql = cursor.mogrify(resolver_insert_sql, (
    #                                 tuple([self.keys[k].ftype(fqdnd[k]) for k in self.fields[1:-1]])
    #                             ))
    #             yield cursor.execute(insert_sql)
    #             res = cursor.fetchall()
    #             logger.info(f"db.resolver.insert {cgr} success: {nodename} - {res[0]} ")
    #             return res[0]
    #     except Exception as err:
    #         raise DBError(f"db.resolver.insert exception cgr: {cgr} - {err}")
    #

    @inlineCallbacks
    def raw_query(self, cgr, final_sql, dict_cursor=False):
        yield self.exists(cgr)
        if 'resolver' in final_sql:
            logger.debug(f'Querying resolver with raw_query: {final_sql}')
            dsn = self.db(cgr, ctype='dsn')
            with CursorContext(dict_cursor=dict_cursor, **dsn) as cur:
                yield deferToThread(functools.partial(cur.execute, final_sql))
                res = yield deferToThread(cur.fetchall)
                return res
        else:
            raise DBError(f'db.resolver.raw_query - attempt to query non-resolver table with raw_query: {final_sql}')

    @inlineCallbacks
    def setup_table(self, cgr):
        create_table_sql = '''
            CREATE TABLE IF NOT EXISTS resolver (
                row_id                  UUID DEFAULT gen_random_uuid(),
                uid                     text NOT NULL,
                dname                   text NOT NULL,
                sensor                  text NOT NULL,
                sname                   text NOT NULL,
                ftype                   text NOT NULL,
                epath                   ltree,
                etype                   ltree,
                hostname                text,
                cgr                     text,
                env                     text,
                fqdn                    text,
                label_1                 text,
                label_2                 text,
                label_3                 text,
                alt_label_1             text,
                alt_label_2             text,
                alt_label_3             text,
                valid_from              timestamp with time zone NOT NULL DEFAULT now(),
                valid_until             timestamp with time zone default NULL,
                updated                 timestamp with time zone NOT NULL DEFAULT now()
            );

            select fn_create_constraint_if_not_exists('resolver', 'resolver_pkey',
                    'ALTER TABLE resolver ADD CONSTRAINT resolver_pkey PRIMARY KEY (uid, dname, sensor, sname, ftype);'
            );

            ALTER TABLE resolver OWNER TO atollogy;

            CREATE INDEX IF NOT EXISTS idx_resolver_label_search ON resolver (uid, label_1, label_2, label_3, alt_label_1, alt_label_2, alt_label_3);

            CREATE INDEX IF NOT EXISTS idx_resolver_device_search ON resolver (uid, row_id, dname, hostname, fqdn);

            CREATE INDEX IF NOT EXISTS idx_resolver_epath ON resolver using gist (epath);

            CREATE INDEX IF NOT EXISTS idx_resolver_etype ON resolver using gist (etype);

        '''
        try:
            dsn = self.db(cgr, ctype='dsn')
            with CursorContext(dict_cursor=True, **dsn) as cur:
                yield deferToThread(functools.partial(cur.execute, create_table_sql))
                logger.info(f"db.resolver.create_table success")
        except Exception as err:
            raise DBError(f"db.resolver.create_table exception creating table for cgr: {cgr} - {err}")

    def unpack(self, data):
        return [AD({k: self.keys[k].otype(v) if k in self.keys else v for k, v in dict(zip(self.keyset, r)).items()}) for r in data]

    @inlineCallbacks
    def update(self, cgr, images):
        try:
            exists = yield self.exists(cgr)
            if not isinstance(images, list) and images and isinstance(images, str):
                images = [images]
            if isinstance(images, list) and not len(images):
                return
            dsn = self.db(cgr, ctype='dsn')
            with CursorContext(dict_cursor=True, **dsn) as cur:
                update_sql = f'''select fn_update_resolver_from_array(ARRAY[{",".join([f"'{i}'" for i in images])}]::text[]);'''
                yield deferToThread(functools.partial(cur.execute, update_sql))
        except Exception as err:
            logger.exception(f"db.resolver.create_table exception creating table for cgr: {cgr} - {err}")
