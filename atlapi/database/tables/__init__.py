
from .asset_attribute import Asset_Attribute_Table
from .beacon_reading import Beacon_Reading_Table
from .customer_binding import Customer_Binding_Table
from .customer_report_config import Customer_Report_Config_Table
from .customer_shift import Customer_Shift_Table
from .customer_transition_event_binding import Customer_Transition_Event_Binding_Table
# from .descriptors import Descriptors_Table
from .image_step import Image_Step_Table
# from .interest import Interest_Table
# from .lpn_sighting import LPN_Sighting_Table
from .lpr_correlation import LPR_Correlation_Table
from .resolver import Resolver_Table
from .userdb import Users_Table

Tables = [
    'Asset_Attribute_Table',
    'Beacon_Reading_Table',
    'Customer_Binding_Table',
    'Customer_Report_Config_Table',
    'Customer_Shift_Table',
    'Customer_Transition_Event_Binding_Table',
    # 'Descriptors_Table',
    'Image_Step_Table',
    # 'Interest_Table',
    # 'LPN_Sighting_Table',
    'LPR_Correlation_Table',
    'Resolver_Table',
    'Users_Table'
]

#__all__ = 'Asset_Attribute_Table, Beacon_Reading_Table, Customer_Binding_Table, Customer_Report_Config_Table, Image_Step_Table, Interest_Table, LPN_Sighting_Table, LPR_Correlation_Table, Resolver_Table, Time_Window_Table'
