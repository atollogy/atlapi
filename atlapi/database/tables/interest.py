
from atlapi.attribute_dict import *
from atlapi.common import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.defer import inlineCallbacks, ensureDeferred


class Interest_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull"])
        self.keys = {
            "name": self._column(lambda x: "{}".format(x), True),
            "stype": self._column(lambda x: "{}".format(x), True),
            "valid_from": self._column(lambda x: to_datetime(x), True),
            "valid_until": self._column(lambda x: to_datetime(x), False),
            "origination": self._column(lambda x: "{}".format(x) if x else 'dynamic', True),
            "alias_for": self._column(lambda x: "{}".format(x), False),
            "tags": self._column(lambda x: [str(i) for i in list(x)], False)
        }
        self.key_map = {
        }
        self.fields = ('interest_id', 'name', 'stype', 'wtype', 'valid_from',
                       'valid_until', 'origination', 'alias_for', 'tags')

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        tables = yield self.db.list_tables(cgr)
        if 'interest' not in tables:
            logger.debug(f'interest table does not exist - insert failed')
            return False
        results = yield self.db.insert(
            "interest", cgr, headers, self.keys, recdata, self.key_map
        )
        if len(results['failure']):
            logger.debug(f'Failure doing interest.insert {results["failure"]}')
            return False
        else:
            return True

    @inlineCallbacks
    def get_interest(self, cgr, interest, stype, collection_time, origination='dynamic', tags=[], upsert=True):
        try:
            tables = yield self.db.list_tables(cgr)
            if 'interest' not in tables:
                logger.debug(f'interest table does not exist - get_interest failed')
                return None
            collection_time = to_datetime(collection_time)

            get_sql = f"""select *
                            from interest
                            where name = '{interest}' and
                                  stype = '{stype}' and
                                  valid_from <= '{collection_time.isoformat()}' and
                                  valid_until is NULL or valid_until > '{collection_time.isoformat()}'
                            limit 1;"""

            results = yield self.db.list_tables(get_sql)
            interest = self.unpack(results['data'])
            if interest:
                return interest
            elif upsert:
                interest = AD()
                interest.interest = interest
                interest.stype = stype
                interest.valid_from = collection_time
                interest.valid_until = None
                interest.origination = origination
                interest.tags = tags
                results = self.insert(cgr, None, interest)
                if len(results["failure"]):
                    logger.debug(f'Failure doing interest.get_interest {results["failure"]}')
                    return None
                else:
                    return interest
            else:
                return None
        except Exception as err:
            logger.exception("db.interest.get_interest exception: {}".format(err))
            return None

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        tables = yield self.db.list_tables(cgr)
        if 'interest' not in tables:
            logger.debug(f'interest table does not exist - raw_query {final_sql} failed')
            return None
        if 'interest' in final_sql:
            logger.debug(f'Querying interest with raw_query: {final_sql}')
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results["failure"]):
                logger.debug(f'Failure doing interest.raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'Attempt to query non-interest table with raw_query: {final_sql}')

    def unpack(self, records):
        if records is None or not len(records):
            return None
        elif len(records) == 1:
            return AD(dict(zip(self.fields, records[0])))
        else:
            return AD({rec[1]: dict(zip(self.fields, rec)) for rec in records})
