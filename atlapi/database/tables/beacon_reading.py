
from atlapi.attribute_dict import *
from atlapi.common import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

### beacon reading table access class
class Beacon_Reading_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull"])
        self.keys = {
            "namespace": self._column(lambda x: "{}".format(x), True),
            "beacon_id": self._column(lambda x: "{}".format(x), True),
            "gateway_id": self._column(lambda x: "{}".format(x), True),
            "mac": self._column(lambda x: "{}".format(x), True),
            "slot": self._column(lambda x: to_datetime(x), True),
            "avg_distance": self._column(lambda x: float(x), True),
            "sd_distance": self._column(lambda x: float(x), True),
            "distance_proximity": self._column(lambda x: "{}".format(x), True),
            "avg_rssi": self._column(lambda x: float(x), True),
            "sd_rssi": self._column(lambda x: float(x), True),
            "tx_power_1m": self._column(lambda x: float(x), True),
            "provenance": self._column(lambda x: "{}".format(json.dumps(x, default=str)), True),
            "node_name": self._column(lambda x: "{}".format(x), True),
            "beacon_type": self._column(lambda x: "{}".format(x), True),
            "beacon_subtype": self._column(lambda x: "{}".format(x), False),
            "asset_key": self._column(lambda x: "{}".format(x), True),
            "best_rssi": self._column(lambda x: float(x), False),
            "best_tx_power_1m": self._column(lambda x: float(x), False),
            "best_distance": self._column(lambda x: float(x), False),
            "best_time": self._column(lambda x: to_datetime(x), False),
            "reading_count": self._column(lambda x: int(x), False),
            "slot_raw_data": self._column(lambda x: "{}".format(json.dumps(x, default=str)), True),
            "api_version": self._column(lambda x: self.version, True),
        }
        self.key_map = {"nodename": "node_name"}

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        for rec in recdata:
            rec["api_version"] = self.version

        results = yield self.db.insert("beacon_reading", cgr, headers, self.keys, recdata, self.key_map)
        # logger.debug("DB beacon_reading insert: {}".format(results))
        return results

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if 'beacon_reading' in final_sql:
            logger.debug(f'Querying beacon_reading_table with raw_query: {final_sql}')
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results['failure']):
                logger.debug(f'Failure doing beacon_reading raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'Attempt to query non-beacon_reading_table with raw_query: {final_sql}')
