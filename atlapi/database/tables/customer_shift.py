
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.database.helpers import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import functools
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

class Customer_Shift_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = AD({
            "shift_name": self._column(str, True,str),
            "shift_timezone_name": self._column(str, True, str),
            "day_of_week": self._column(int, True, int),
            "start_offset": self._column(lambda x: x if isinstance(x, timedelta) else timedelta(minutes=int(x)), False, lambda x: x),
            "end_offset": self._column(lambda x: x if isinstance(x, timedelta) else timedelta(minutes=int(x)), False, lambda x: x),
            "facility": self._column(str, True, str)
        })
        self.keyset = list(self.keys.keys())
        self.key_map = {}

    @inlineCallbacks
    def get_current_shift_time(self, cgr, facility, shift='day', shift_date='now()'):
        try:
            current_shift_time_sql = f'''
                WITH shift_date AS (
                    SELECT
                        d AS date,
                        s.shift_name,
                        d AT TIME ZONE s.shift_timezone_name + s.start_offset AS shift_start,
                        d AT TIME ZONE s.shift_timezone_name + s.end_offset   AS shift_end,
                        s.shift_timezone_name,
                        s.facility
                    FROM generate_series(date_trunc('day',
                                                    to_timestamp('{shift_date}','YYYY-MM-DD HH24: MI: SS')) :: TIMESTAMP WITHOUT TIME ZONE - INTERVAL '14 days',
                                                    date_trunc('day', to_timestamp(:time,'YYYY-MM-DD HH24: MI: SS')) :: TIMESTAMP WITHOUT TIME ZONE + INTERVAL '14 days',
                                                    INTERVAL '1 day') d
                    JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
                    WHERE (CAST('{facility}' AS TEXT) IS NULL OR COALESCE(facility, '{facility}') = '{facility}')
                )
                SELECT *
                FROM ((SELECT   DISTINCT ON (shift_name, in_progress)
                                date :: timestamp :: date :: text,
                                shift_name,
                                shift_start,
                                shift_end,
                                ('{shift_date}' ::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name) BETWEEN shift_start AND shift_end AS in_progress,
                                FALSE AS completed,
                                shift_timezone_name
                       FROM shift_date
                       WHERE shift_end > '{shift_date}' ::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name
                       ORDER BY shift_name, in_progress, shift_start ASC)
                UNION
                      (SELECT   DISTINCT ON (shift_name)
                                date::timestamp::date::text,
                                shift_name,
                                shift_start,
                                shift_end,
                                FALSE AS in_progress,
                                TRUE  AS completed,
                                shift_timezone_name
                       FROM shift_date
                       WHERE shift_end < '{shift_date}' ::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name
                       ORDER BY shift_name, shift_end DESC)) x
                       WHERE x.in_progress = TRUE
                ORDER BY shift_start ASC;
            '''
            res = yield self.raw_query(cgr, current_shift_time_sql)
            return res
        except Exception as err:
            logger.exception(f"customer_shift.get_cycle_step {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def get_shift_time(self, cgr, facility, shift='day', shift_date='now()'):
        try:
            shift_times_sql = f'''
                shift_times_new AS (
                    SELECT DISTINCT ON (date, shift_name) shift_name,
                                                          date::date::text,
                                                          shift_start start_time,
                                                          shift_end   end_time,
                                                          shift_timezone_name
                    FROM (SELECT date,
                                 shift_name,
                                 shift_timezone_name,
                                 shift_start,
                                 shift_end
                          FROM (SELECT d date,
                                       s.shift_name,
                                       d AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
                                       d AT TIME ZONE s.shift_timezone_name + s.end_offset shift_end,
                                      s.shift_timezone_name
                                FROM generate_series(COALESCE('{shift_date}', now()::date)::TIMESTAMP WITHOUT TIME ZONE - INTERVAL '7 days',
                                                     COALESCE('{shift_date}', now()::date)::TIMESTAMP WITHOUT TIME ZONE,
                                                     INTERVAL '1 day'
                                ) d
                                JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
                                WHERE shift_name = '{shift}' AND facility = '{facility}'
                          ) x
                    ) y
                    WHERE date = COALESCE('{shift_date}', now()::date)::TIMESTAMP WITHOUT TIME ZONE
                )
                SELECT to_timestamp('{shift_date}', 'YYYY-MM-DD')::timestamp without time zone
                            AT TIME ZONE (s.shift_timezone_name + s.start_offset)                       start_time,
                       to_timestamp('{shift_date}', 'YYYY-MM-DD')::timestamp without time zone
                            AT TIME ZONE (s.shift_timezone_name + s.end_offset)                         end_time
                FROM customer_shift s
                WHERE s.shift_name = '{shift}'
                  AND (CAST(:facility AS TEXT) IS NULL OR COALESCE(s.facility, '{facility}') = '{facility}')
                  AND (s.day_of_week is null OR s.day_of_week = date_part(
                            'dow',
                            to_timestamp('{shift_date}', 'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE
                            'UTC'));
            '''
            res = yield self.raw_query(cgr, shift_times_sql)
            return res
        except Exception as err:
            logger.exception(f"customer_shift.get_cycle_step {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        # these are the keys we wanna update in-case a insert conflict occurs
        update_keys = []
        try:
            results = yield deferToThread(
                            functools.partial(
                                self.db.insert,
                                "customer_binding",
                                cgr,
                                headers,
                                self.keys,
                                recdata,
                                self.key_map,
                                update_keys=update_keys
                            )
                )
            return results
        except Exception as err:
            logger.exception(f"customer_shift.insert {cgr} exception: {err} - {recdata}")
            raise

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if 'customer_shift' in final_sql:
            # logger.debug(f'Querying customer_transition_event_binding with raw_query: {final_sql}')
            dsn = self.db(cgr, ctype='dsn')
            with CursorContext(dict_cursor=True, **dsn) as cur:
                yield deferToThread(functools.partial(cur.execute, final_sql))
                res = yield deferToThread(cur.fetchall)
                res = [AD(r) for r in res]
                return res
        else:
            raise DBError(f'customer_shift.raw_query - attempt to query non-customer_transition_event_binding table with raw_query: {final_sql}')

    def unpack(self, data):
        return [AD({k: self.keys[k].otype(v) for k, v in dict(zip(self.keyset, r)).items()}) for r in data]
