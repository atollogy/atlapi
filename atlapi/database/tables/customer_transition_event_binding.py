
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.database.helpers import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import functools
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks, ensureDeferred, returnValue

class Customer_Transition_Event_Binding_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = AD({
            "gateway_id": self._column(str, True,str),
            "camera_id": self._column(str, True, str),
            "step_name": self._column(str, True, str),
            "start_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), True, to_datetime),
            "end_time": self._column(lambda x: to_datetime(x).isoformat(timespec='microseconds'), False, to_datetime),
            "cycle_name": self._column(str, True, str),
            "state_name": self._column(str, True, str),
            "start_cycle": self._column(bool, False, bool),
            "end_cycle": self._column(bool, False, bool),
            "facility": self._column(str, True, str),
            "is_whitelist": self._column(bool, False, bool),
            "priority": self._column(int, False, int)
        })
        self.keyset = list(self.keys.keys())
        self.key_map = AD({
            'location': 'state_name',
            'order': 'priority'
        })

    @inlineCallbacks
    def get_cycle_step(self, cgr, gateway_id, step_name, camera_id='video0', start_time='now()', end_time='now()'):
        try:
            cycle_step_sql = f'''
                select cycle_name,
                       state_name as location,
                       start_cycle,
                       end_cycle,
                       facility,
                       is_whitelist,
                       priority as order
                from customer_transition_event_binding
                where gateway_id = '{gateway_id}' and
                      step_name = '{step_name}' and
                      camera_id = '{camera_id}' and
                      start_time <= {start_time} and
                      (end_time is null or end_time > {end_time});
            '''
            res = yield self.raw_query(cgr, cycle_step_sql)
            return res
        except Exception as err:
            logger.exception(f"customer_transition_event_binding.get_cycle_step {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def get_cycle_config(self, cgr, start_time='now()', end_time='now()'):
        try:
            cycle_config_sql = f'''
                select gateway_id,
                       step_name,
                       camera_id,
                       cycle_name,
                       state_name as location,
                       facility,
                       is_whitelist,
                       priority as order
                from customer_transition_event_binding
                where start_time <= {start_time} and
                      (end_time is null or end_time > {end_time})
                order by priority;
            '''
            res = yield self.raw_query(cgr, cycle_config_sql)
            return res
        except Exception as err:
            logger.exception(f"customer_transition_event_binding.get_cycle_config {cgr} exception: {err}")
            raise

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        # these are the keys we wanna update in-case a insert conflict occurs
        update_keys = []
        try:
            results = yield deferToThread(
                            functools.partial(
                                self.db.insert,
                                "customer_binding",
                                cgr,
                                headers,
                                self.keys,
                                recdata,
                                self.key_map,
                                update_keys=update_keys
                            )
                )
            return results
        except Exception as err:
            logger.exception(f"customer_transition_event_binding.insert {cgr} exception: {err} - {recdata}")
            raise

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        if 'customer_transition_event_binding' in final_sql:
            # logger.debug(f'Querying customer_transition_event_binding with raw_query: {final_sql}')
            dsn = self.db(cgr, ctype='dsn')
            with CursorContext(dict_cursor=True, **dsn) as cur:
                yield deferToThread(functools.partial(cur.execute, final_sql))
                res = yield deferToThread(cur.fetchall)
                res = [AD(r) for r in res]
                return res
        else:
            raise DBError(f'db.customer_transition_event_binding.raw_query - attempt to query non-customer_transition_event_binding table with raw_query: {final_sql}')

    def unpack(self, data):
        return [AD({k: self.keys[k].otype(v) for k, v in dict(zip(self.keyset, r)).items()}) for r in data]
