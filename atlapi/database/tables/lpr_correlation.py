
from atlapi.attribute_dict import *
from atlapi.common import *
from atlapi.database.helpers import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
import numpy as np
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.defer import inlineCallbacks, ensureDeferred


class LPR_Correlation_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull", "otype"])
        self.keys = {
            "alias": self._column(str, True, str),
            "plate": self._column(str, True, str),
            "alias_confidence": self._column(float, True, float),
            "confidence": self._column(float, True, float),
            "score": self._column(float, True, float),
            "distance": self._column(int, True, int),
            "identifier": self._column(bool, False, bool),
            "secondary": self._column(bool, False, bool),
            "block": self._column(bool, False, bool),
            "vehicle_type": self._column(str, False, str),
        }
        self.key_map = {
        }
        self.fields = ('lct_id', 'alias', 'plate', 'alias_confidence',
                       'confidence', 'score', 'distance', 'identifier'
                       'secondary', 'block',  'vehicle_type', 'insert_time')

    @inlineCallbacks
    def get_correlation(self, cgr, plate, confidence):
        correlation_sql = f"""
            select  alias,
                    plate,
                    alias_confidence,
                    confidence,
                    score,
                    distance,
                    identifier,
                    secondary,
                    block,
                    vehicle_type
           from lct
           where alias = '{plate}'
             and confidence >= {confidence}
           limit 1;
        """
        try:
            correlated_plate = yield self.raw_query(cgr, correlation_sql)
            if isinstance(correlated_plate, list) and len(correlated_plate):
                return correlated_plate[0]
            else:
                return None
        except Exception as err:
            logger.exception(f"LPR_Correlation_Table.get_correlations {cgr} exception: {err}")
            return None

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        try:
            if 'lct' in final_sql:
                dsn = self.db(cgr, ctype='dsn')
                with CursorContext(dict_cursor=True, **dsn) as cur:
                    yield deferToThread(functools.partial(cur.execute, final_sql))
                    raw_result = yield cur.fetchall()
                    result = [AD(d) for d in raw_result]
                    return result
            else:
                raise DB_Exception(f'LPR_Correlation_Table.raw_query - querying non-lpr_correlation table not permitted',
                                   payload=final_sql)
        except Exception as err:
            logger.exception(f"LPR_Correlation_Table.raw_query {cgr} exception: {err}")
            raise

