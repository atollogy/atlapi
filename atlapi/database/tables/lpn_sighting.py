
from atlapi.attribute_dict import *
from atlapi.common import *

from collections import namedtuple
from datetime import datetime, timedelta, timezone
import iso8601
import json
import logging
logger = logging.getLogger('atlapi')
from random import randint
import time
from twisted.internet import task, reactor
from twisted.internet.defer import inlineCallbacks, ensureDeferred

class LPN_Sighting_Table(object):
    def __init__(self, db, version):
        self.db = db
        self.version = version
        self._column = namedtuple("Column", ["ftype", "cannotBeNull"])
        self.keys = {
            "lpn": self._column(lambda x: "{}".format(x), True),
            "source": self._column(lambda x: "{}".format(x), True),  # gateway_id
            "event_id": self._column(lambda x: int(x), False),
            "collection_time": self._column(lambda x: to_datetime(x), True),
            "confidence": self._column(lambda x: float(x), True),
            "matches_template": self._column(lambda x: int(x), True),
            "coordinates": self._column(lambda x: "{}".format(json.dumps(x, default=str)), True),
            "media": self._column(lambda x: [str(i) for i in list(x)], True),
            "image_step_id": self._column(lambda x: int(x), True),
            "cycle_id": self._column(lambda x: int(x), False),
        }
        self.key_map = {
        }
        self.fields = ('lpn_sighting_id', 'lpn', 'source', 'collection_time',
                       'confidence', 'matches_template', 'coordinates', 'media',
                       'event_id', 'cycle_id', 'cycle_phase', 'link', 'insert_time')

    @inlineCallbacks
    def insert(self, cgr, headers, recdata):
        tables = yield self.db.list_tables(cgr)
        if 'lpn_sighting' not in tables:
            logger.debug(f'lpn_sighting table does not exist - insert failed')
            return False
        results = yield self.db.insert(
            "lpn_sighting", cgr, headers, self.keys, recdata, self.key_map
        )
        if len(results['failure']):
            logger.debug(f'Failure doing lpn_sighting.insert {results["failure"]}')
            return False
        else:
            return True

    @inlineCallbacks
    def get_sighting(self, cgr, lpn, source, event_id, collection_time):
        try:
            tables = yield self.db.list_tables(cgr)
            if 'lpn_sighting' not in tables:
                logger.debug(f'lpn_sighting table does not exist - get_sighting failed')
                return False
            collection_time = to_datetime(collection_time)

            get_sql = f"""
                with hits as (
                    select *
                    from lpn_sighting
                    where lpn = '{lpn}' and
                          source = '{source}' and
                          event_id = '{event_id}'
                    limit 1;"""

            results = yield self.db.list_tables(get_sql)
            if len(results['failure']):
                logger.debug(f'Failure doing lpn_sighting.get_sighting {results["failure"]}')
                return None
            else:
                return self.unpack(results['data'])
        except Exception as err:
            logger.exception("db.lpn_sighting.get_sighting exception: {}".format(err))
            return None

    @inlineCallbacks
    def raw_query(self, cgr, final_sql):
        tables = yield self.db.list_tables(cgr)
        if 'lpn_sighting' not in tables:
            logger.debug(f'lpn_sighting table does not exist - raw_query {final_sql} failed')
            return None
        if 'lpn_sighting' in final_sql:
            logger.debug(f'Querying lpn_sighting with raw_query: {final_sql}')
            results = yield self.db.raw_query(cgr, final_sql)
            if len(results['failure']):
                logger.debug(f'Failure doing lpn_sighting raw_query {results["failure"]}')
            return results
        else:
            raise DB_Exception(f'Attempt to query non-lpn_sighting table with raw_query: {final_sql}')

    def unpack(self, records):
        if records is None or not len(records):
            return None
        elif len(records) == 1:
            return AD(dict(zip(self.fields, records[0])))
        else:
            return AD({rec[1]: dict(zip(self.fields, rec)) for rec in records})
