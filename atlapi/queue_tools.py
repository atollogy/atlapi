#!/usr/bin/env python3.6

if __package__ is None or __package__ == "":
    from attribute_dict import AD
else:
    print("Package_name: {}".format(__package__))
    from .attribute_dict import AD

import asyncio
import boto3
import boto3.resources.base as base
import botocore
import concurrent
from concurrent.futures import ProcessPoolExecutor
from datetime import datetime
import functools
import gzip
import iso8601
import json

import logging

logger = logging.getLogger('atlapi')

import multiprocessing as mp
import os
import threading
import time
from twisted.internet import reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import ensureDeferred, inlineCallbacks, maybeDeferred


class ATLEvent(object):
    def __init__(self, data=None, handle=None, mid=None, rawdata=None, unpack=None):
        """ATLEvent
            data: <dict> if not None it is expected to have the following structure:
                {
                    cid: <context_id>,
                    customer_id: <string>,
                    headers: <dict> with customer_id, nodename, gateway_id, etc...
                    args: <dict>
                    records: [<step_data_structure>, ...]
                },
            handle: <string> -> ReceiptHandle,
            mid: <string> - MessageId,
            rawdata: <raw dequeue object> or None,
            unpack: <gzip'ed json data string with above structure> or None
        """
        self.data = AD(data)
        self.mid = mid or id(self)
        self.handle = handle
        self.rawdata = rawdata
        if unpack is not None:
            self.data.update(json.loads(unpack))
        self.data = AD(self.data)
        self.cid = self.data["cid"]
        self.scid = self.data["records"][0]["scid"]

    def __repr__(self):
        return json.dumps(self.data)

    def pack(self):
        return json.dumps(self.data)


class AWSQueueManager(object):
    def __init__(
        self, aws=None, cgr=None, env=None, qname=None, fifo=True, dlq=True, vtime="30", wait=20
    ):
        self.qname = qname
        if cgr not in self.qname:
            self.qname = "{}-{}".format(self.qname, cgr)
        if env not in self.qname:
            self.qname = "{}-{}".format(self.qname, env)

        self.req_fields = ["customer_id", "gateway_id", "args", "records"]
        self._current_messages = []
        self._current_messages_byid = {}
        self._maybe_done = []
        self._queues = {}
        self._qmap = {}
        self._vtime = vtime
        self.CGR = cgr
        self.ENV = env
        self.AWS = aws
        self.has_dlq = dlq
        self.is_fifo = fifo
        self.wait = wait
        self.loop = asyncio.get_event_loop()
        self.last_get = self.loop.time() - self.wait

        qlist = self.AWS("sqs").list_queues()
        if "QueueUrls" in qlist:
            for qurl in qlist["QueueUrls"]:
                qname = qurl.split("/")[-1]
                if "." in qname:
                    qname = qname.split(".")[0]
                qarn = self.AWS("sqs").get_queue_attributes(
                    QueueUrl=qurl, AttributeNames=["QueueArn"]
                )["Attributes"]["QueueArn"]
                self._queues[qname] = (qurl, qarn)
                self._qmap[qname] = qurl

        if self.has_dlq:
            self.dlq_name = "atl-{}-{}-dlq".format(env, cgr)
            self.dlq_url, self.dlq_arn = self.__prep_q(self.dlq_name, fifo=self.is_fifo)
        self.qurl, self.qarn = self.__prep_q(self.qname, fifo=self.is_fifo)

    def __prep_q(self, qname, fifo=True):
        if qname not in self._queues:
            logger.warning("Creating queue {}".format(qname))
            if fifo:
                qurl = self.AWS("sqs").create_queue(
                    QueueName=qname + ".fifo",
                    Attributes={
                        "FifoQueue": "true",
                        "VisibilityTimeout": self._vtime,
                        "ContentBasedDeduplication": "true",
                    },
                )["QueueUrl"]
                print("fifo qurl: {}".format(qurl))
                qarn = self.AWS("sqs").get_queue_attributes(
                    QueueUrl=qurl, AttributeNames=["QueueArn"]
                )["Attributes"]["QueueArn"]
                print("fifo qarn: {}".format(qarn))
            else:
                qurl = self.AWS("sqs").create_queue(QueueName=qname)["QueueUrl"]
                print("non-fifo qurl: {}".format(qurl))
                qarn = self.AWS("sqs").get_queue_attributes(
                    QueueUrl=qurl, AttributeNames=["QueueArn"]
                )["Attributes"]["QueueArn"]
                print("non-fifo qarn: {}".format(qarn))
            self._queues[qname] = (qurl, qarn)
            self._qmap[qname] = qurl
            if "dlq" not in qname:
                self.AWS("sqs").set_queue_attributes(
                    QueueUrl=qurl,
                    Attributes={
                        "RedrivePolicy": json.dumps(
                            {"deadLetterTargetArn": self.dlq_arn, "maxReceiveCount": "3"}
                        )
                    },
                )
        return self._queues[qname]

    def __get(self, mcount=1):
        data = self.AWS("sqs").receive_message(
                QueueUrl=self.qurl,
                AttributeNames=["All"],
                MessageAttributeNames=["All"],
                MaxNumberOfMessages=mcount,
                WaitTimeSeconds=self.wait,
        )
        # messages are stored as [mid, receipt_handle, ATL event, raw message]
        if "Messages" in data:
            _msgs = []
            for m in data["Messages"]:
                m["Body"] = json.loads(m["Body"])
                m["Body"] = AD(m["Body"])
                # if not all([f in m['Body'] for f in self.req_fields]):
                #     logger.debug('record does not have the required fields: {}'.format(m['Body']))
                #     continue
                m["Body"]["records"] = [r for r in m["Body"]["records"] if isinstance(r, dict)]
                if len(m["Body"]["records"]) > 0:
                    _msgs.append(m)

            if len(_msgs):
                _msgs = [
                    ATLEvent(
                        mid=m["MessageId"], handle=m["ReceiptHandle"], data=m["Body"], rawdata=m
                    )
                    for m in _msgs
                ][::-1]
                self._current_messages = _msgs + self._current_messages
                self._current_messages_byid = {m.mid: m for m in self._current_messages}

        self.last_get = time.time()
        return len(self._current_messages)

    def __put(self, msg, *args, **kwargs):
        resp = self.AWS("sqs").send_message(
                QueueUrl=self.qurl,
                MessageBody=msg.pack(),
                MessageGroupId=msg.scid
        )
        return resp

    def __delete_messages(self, *args, **kwargs):
        # args keyword must include Entries=[{'Id': m.mid, 'ReceiptHandle': m.handle}]
        self.AWS("sqs").delete_message_batch(QueueUrl=self.qurl, **kwargs)

    def empty(self, mcount=1):
        if len(self._current_messages):
            return False
        else:
            mcnt = self.__get(mcount=mcount)
            if mcnt > 0:
                return False
            else:
                return True

    def pop(self, mcount=1):
        mcnt = len(self._current_messages)
        if mcnt == 0 or mcount > mcnt:
            mcnt = self.__get(mcount=mcount)
        if mcnt == 0:
            return None
        if mcount == 1:
            m = self._current_messages.pop()
            self.delete([m])
            return m.data
        else:
            if mcount < mcnt:
                msgs = self._current_messages[-mcount:]
                self._current_messages = self._current_messages[:-mcount]
                self.delete(msgs)
                return [m.data for m in msgs]
            else:
                msgs = [m.data for m in self._current_messages]
                self.all_done()
                return msgs

    def get_msg(self, msgid=None, mcount=1):
        if msgid:
            if msgid in self._current_messages_byid:
                return self._current_messages_byid[msgid]
            else:
                return None
        mcnt = len(self._current_messages)
        if mcnt == 0 or mcount > mcnt:
            mcnt = self.__get(mcount=mcount)
        if mcnt == 0:
            return None

        if mcount == 1:
            msg = self._current_messages.pop()
            self._maybe_done.append(msg)
            return msg
        elif mcount > 1:
            if mcount <= mcnt:
                msgs = self._current_messages[-mcount:]
                self._current_messages = self._current_messages[:-mcount]
                self._maybe_done.extend(msgs)
                return msgs
            else:
                msgs = self._current_messages[:]
                self._maybe_done.extend(msgs)
                self._current_messages = []
                return msgs
        else:
            return None

    def put_msg(self, msg):
        if isinstance(msg, ATLEvent):
            resp = self.__put(msg)
        else:
            resp = self.__put(ATLEvent(data=msg))
        return resp

    def all_done(self):
        self.__delete_messages(
            Entries=[
                {"Id": m.mid, "ReceiptHandle": m.handle}
                for m in self._current_messages
                if m.handle
            ]
        )
        self._current_messages = []
        self._maybe_done = []
        self._current_messages_byid = {}

    def delete(self, msgs):
        if not isinstance(msgs, list):
            msgs = [msgs]
        self.__delete_messages(
            Entries=[{"Id": m.mid, "ReceiptHandle": m.handle} for m in msgs if m.handle]
        )
        self._current_messages = [m for m in self._current_messages if m not in msgs]
        self._current_messages_byid = {m.mid: m for m in self._current_messages}
        self._maybe_done = [m for m in self._current_messages if m not in msgs]

    def release(self, msgs):
        if not isinstance(msgs, list):
            msgs = [msgs]
        self._current_messages = [m for m in self._current_messages if m not in msgs]
        self._current_messages_byid = {m.mid: m for m in self._current_messages}
        self._maybe_done = [m for m in self._current_messages if m not in msgs]
