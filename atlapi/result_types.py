
- basic capture (sample/simple)
  - scheduled data capture

- activity (triggered)
  - motion detection
  - scene detection



- feature (indicator) (unit based)
  - color
  - andon
  - line
  - identifier (identity)
    - ocr
    - scale
    - lpr

- comparative (interpreted/approximate/inferred) (point)
  - object_detection (od)
  - object_classification (oc)
  - sticker

- composite (calculated) (defined/finite)

- stateful (correlated) (dynamic/conditional)

- aggregate (collection/summary)

- complex (combine any of the above)


source:
  - context
  - location
  - device
  - profile

indicator:
  - source
  - scope (region, zone, crop)
  - extractor (function)
  - feature (value(s))
  - filter(s)

status

state

condition

metric

insight



