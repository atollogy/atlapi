#!/usr/bin/env python3
from pathlib import Path
from collections import namedtuple
from statistics import stdev
from tempfile import NamedTemporaryFile
import json
import subprocess as sp

DATA_DIRECTORY = Path('/var/lib/atollogy/media')
FFMPEG_CMD = 'ffmpeg'
FFPROBE_CMD = 'ffprobe'

YDIFF_CAP_PERCENT = 0.98

def load_video(raw_video) -> NamedTemporaryFile:
    '''
    Returns a NamedTemporaryFile which holds the video data provided and
    will be deleted when it is closed.
    '''    
    temp = NamedTemporaryFile()
    temp.write(raw_video)
    temp.flush()
    temp.seek(0)

    return temp

def yuv_to_rgb(y, u, v):
    r = y + 1.4075 * (v - 128)
    g = y - 0.3455 * (u - 128) - (0.7169 * (v - 128))
    b = y + 1.7790 * (u - 128) 
    return r, g, b

FrameMetadata = namedtuple('FrameMetadata', ('number', 'time', 'type', 'ydiff', 'yavg', 'color'))
FrameRGB = namedtuple('RGB', ('r', 'g', 'b'))
FfmpegCrop = namedtuple('FfmpegCrop', ('x', 'y', 'width', 'height'))

def crop_video(filename, output_filename, region, **args):
    if isinstance(region,(list,tuple)):
        crop = FfmpegCrop(region[0], region[1], region[2], region[3])
    else: # AD or Zone
        crop = FfmpegCrop(region.x, region.y, region.w, region.h)

    ffmpeg_results = ffmpeg(
        '-i', filename,
        '-filter:v', f'crop={crop.width}:{crop.height}:{crop.x}:{crop.y}',
        '-c:a', 'copy',
        output_filename
    )

    return output_filename

def frame_metadata_extraction(filename, region=None, **args):
    filters = ""
    if region is not None:
        crop = FfmpegCrop(*region)
        filters = f'crop={crop.width}:{crop.height}:{crop.x}:{crop.y},'
    
    filters += "signalstats"

    ydiff_stats = 'lavfi.signalstats.YDIF'
    yuv_stats = 'lavfi.signalstats.YAVG,lavfi.signalstats.UAVG,lavfi.signalstats.VAVG'

    ffprobe_result = ffprobe(
        '-f', 'lavfi',
        '-i', f'movie={filename},{filters}',
        '-show_entries', f'frame=pkt_pts_time,pict_type:frame_tags={ydiff_stats},{yuv_stats},lavfi.signalstats.HUEMED',
        '-of', 'json'
    ) 

    # check appropriate return code
    if ffprobe_result.returncode != 0:
        # TODO: Better error handling
        raise OSError(f'failed - {ffprobe_result.error}')

    raw_frame_metadata = json.loads(ffprobe_result.output)

    if 'frames' not in raw_frame_metadata:
        # TODO: Do somnething 
        raise ValueError()

    frames = []
    ydiffs = []
    for frame_number, frame_data in enumerate(raw_frame_metadata['frames'], start=1):
        pkt_pts_time = float(frame_data['pkt_pts_time'])
        frame_type = frame_data['pict_type']
        frame_ydiff = float(frame_data['tags']['lavfi.signalstats.YDIF'])
        frame_hue = float(frame_data['tags']['lavfi.signalstats.HUEMED'])

        frame_yavg = float(frame_data['tags']['lavfi.signalstats.YAVG'])
        frame_uavg = float(frame_data['tags']['lavfi.signalstats.UAVG'])
        frame_vavg = float(frame_data['tags']['lavfi.signalstats.VAVG'])

        ydiffs.append(frame_ydiff)
        frames.append(FrameMetadata(
            frame_number, 
            pkt_pts_time,
            frame_type,
            frame_ydiff,
            frame_yavg,
            # NOTE: this makes some assumptions about encoding and averaging
            FrameRGB(*yuv_to_rgb(frame_yavg, frame_uavg, frame_vavg))
        ))

    total_frames = len(frames)

    vclock_time = 0
    if total_frames > 1:
        # NOTE: This assumes a constant framerate and may be an assumption we
        # want to change in the future
        vclock_time = ydiffs[1] - ydiffs[0]
    
    ydiffs = sorted(ydiffs)
    oyd_max = ydiffs[-1]
    oyd_avg = sum(ydiffs)/len(ydiffs)
    oyd_std = stdev(ydiffs)
    oyd_len = len(ydiffs)

    ydiffs = ydiffs[:int(len(ydiffs)*YDIFF_CAP_PERCENT)]
    myd_avg = sum(ydiffs)/len(ydiffs)
    myd_std = stdev(ydiffs)
    myd_max = ydiffs[-1]
    myd_len = len(ydiffs)
    
    summary = {
        'ydiff_std': oyd_std,
        'total_frames': total_frames,
        'vclock_time': vclock_time
    }

    return frames, summary

ShellResult = namedtuple('ShellResult', ('output', 'error', 'returncode'))

def shell(*cmd):
    try:
        result = sp.run(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    except OSError as e:
        return (None, e, -1)

    return ShellResult(result.stdout, result.stderr, result.returncode)

def ffmpeg(*args):
    return shell(FFMPEG_CMD, *args)

def ffprobe(*args):
    return shell(FFPROBE_CMD, *args)