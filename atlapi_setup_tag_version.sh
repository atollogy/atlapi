#!/bin/bash
# This script will push latest tag version @setup.py
set -xe
atlapi_version=$1
atlapi_path="/data/atlgit/atlapi"
cd $atlapi_path

old_version_line=`grep "version=" $atlapi_path/setup.py`
new_version_line=' \ \ \ \ version="'$atlapi_version'",'
new_version_line=`echo $new_version_line |sed 's/\_/./g'`
sed -i "s@$old_version_line@$new_version_line@" $atlapi_path/setup.py