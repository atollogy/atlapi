attrs==21.2.0
s3transfer==0.4.2
awscli==1.19.97
boto3==1.17.97
botocore==1.20.97
bcrypt==3.2.0
cmake==3.18.4.post1
cryptography==3.4.7
cython==0.29.23
datadog-api-client==1.2.0
expiringdict==1.2.1
ffmpeg-python==0.2.0
func-timeout===4.3.5
hyperlink==21.0.0
imageio-ffmpeg==0.4.4
imutils==0.5.4
iso8601==0.1.14
jmespath==0.10.0
klein==20.6.0
matplotlib==3.3.4
numpy==1.19.5
mahotas==1.4.11
manhole== 1.8.0
multiprocessing_logging==0.3.1
opencv-python==4.5.3.56
opencv-contrib-python==4.5.3.56
pandas==1.1.5
pillow>=6.2.0
Planar==0.4.0
psycopg2==2.9.1
pyasn1==0.4.6
pydantic==0.20
pytesseract==0.3.8
python-dateutil>=2.8.1
python-stdnum==1.17
pyyaml==5.4.1
pyzbar==0.1.8
requests==2.25.1
scikit-image==0.17.2
scipy==1.5.4
service_identity==21.1.0
simplejson==3.17.2
timeframe==0.1.5
twisted==21.2.0
urllib3==1.25.4
Werkzeug==2.0.1

# internal dependencies
git+ssh://bitbucket.org/atollogy/pyconsul.git@v2_0_7#egg=pyconsul
git+ssh://bitbucket.org/atollogy/atl_data_api.git@v1_0_5#egg=atl-data-api

# testing packages
pytest
pytest_mock
pytest_twisted
